{
    const favoriteNumber = 1362

    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec15.txt', 'utf8')

    let lines = content.split('\n')

    const LINE_REGEX = /Disc\s+#\d+\s+has\s+(\d+)\s+positions;\s+at\s+time=0,\s+it\s+is\s+at\s+position\s+(\d+)./

    const discInfo = lines.map(line => line.match(LINE_REGEX)).map(groups => [Number.parseInt(groups![1]), Number.parseInt(groups![2])] as [number, number])

    const getMinTime = function(discInfo: [number, number][]) {
        let time = 0
        for (let pairSecond = 1, increment = 1; pairSecond < discInfo.length;) {
            if ([pairSecond - 1, pairSecond].every(i => (discInfo[i][1] + time + i + 1) % discInfo[i][0] === 0)) {
                for (let i of [pairSecond - 1, pairSecond]) {
                    if (increment % discInfo[i][0]) {
                        increment *= discInfo[i][0]
                    }
                }
                pairSecond++
            }
            else {
                time += increment
            }
        }
        return time
    }
    console.log(getMinTime(discInfo))
    console.log(getMinTime([...discInfo, [11, 0]]))
}