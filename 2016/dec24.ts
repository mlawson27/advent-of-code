{
    const fs = require('fs')

    const input: string = fs.readFileSync('input/dec24.txt', 'utf8')

    const getPositionKey = function(position: [number, number], foundPositions: string) {
        return [position[0].toString(), position[1].toString(), foundPositions].join(';')
    }

    const getMinMovesToAll = function(ducts: string[][], returnToZero: boolean) {
        let startingPosArr = ducts.flatMap((row, rowI) => row.map((c, colI) => [colI, rowI, c] as [number, number, string]))
                                  .filter(([_colI, _rowI, c]) => c !== '#' && c !== '.')
                                  .sort(([_colIA, _rowIA, cA], [_colIB, _rowIB, cB]) => cA.localeCompare(cB))
                                  .map(([colI, rowI, _node]) => [colI, rowI] as [number, number])

        let positionQueue = [[startingPosArr[0], 0, '0']] as [[number, number], number, string][]
        let seenMoves     = new Set<string>()
        while (true) {
            let [currentPos, movesSoFar, foundPositions] = positionQueue.shift()!

            if (ducts[currentPos[1]][currentPos[0]] !== '#' && ducts[currentPos[1]][currentPos[0]] !== '.') {
                if (!foundPositions.includes(ducts[currentPos[1]][currentPos[0]])) {
                    foundPositions = [...foundPositions.split(''), ducts[currentPos[1]][currentPos[0]]].sort().join('')
                    seenMoves.add(getPositionKey(currentPos, foundPositions))
                }
                else if (foundPositions.length == startingPosArr.length && ducts[currentPos[1]][currentPos[0]] === '0') {
                    return movesSoFar
                }
            }

            if (foundPositions.length == startingPosArr.length && !returnToZero) {
                return movesSoFar
            }

            for (let adjustment of [[-1, 0], [1, 0], [0, -1], [0, 1]]) {
                let newPos = [currentPos[0] + adjustment[0], currentPos[1] + adjustment[1]] as [number, number]
                if ((newPos[0] >= 0 && newPos[0] < ducts[currentPos[1]].length) &&
                    (newPos[1] >= 0 && newPos[1] < ducts.length) &&
                    (ducts[newPos[1]][newPos[0]] !== '#')) {
                    let newPosKey = getPositionKey(newPos, foundPositions)
                    if (!seenMoves.has(newPosKey)) {
                        positionQueue.push([newPos, movesSoFar + 1, foundPositions])
                        seenMoves.add(newPosKey)
                    }
                }
            }
        }
    }

    const ducts = input.split('\n').map(line => line.split(''))

    console.log(getMinMovesToAll(ducts, false))
    console.log(getMinMovesToAll(ducts, true))
}