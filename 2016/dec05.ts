{
    const md5 = require('ts-md5')

    const input = 'wtnhxymk'

    let pwP1    = ''
    let pwP2    = new Array(8).fill(null)
    let pwP2Len = 0
    for (let i = 0; Math.min(pwP1.length, pwP2Len) < 8; i++) {
        let a: string = md5.Md5.hashStr(input + i)
        if (a.startsWith('00000')) {
            if (pwP1.length < 8) {
                pwP1 += a[5]
            }
            let pos = Number(a[5])
            if (!isNaN(pos) && pos < pwP2.length && pwP2[pos] === null) {
                pwP2[pos] = a[6]
                pwP2Len += 1
            }
        }
    }
    console.log(pwP1)
    console.log(pwP2.join(''))
}