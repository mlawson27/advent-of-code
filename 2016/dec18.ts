{
    const fs = require('fs')

    const input: string = fs.readFileSync('input/dec18.txt', 'utf8')

    let findSafeCount = function(workingRow: string, rowCount: number) {
        let safeCount = workingRow.split('').reduce((v, c) => v + (c === '.' ? 1 : 0), 0)
        for (let row = 1; row < rowCount; row++) {
            let newWorkingRow = ''
            for (let i = 0; i < workingRow.length; i++) {
                let leftChar   = i < 1 ? '.' : workingRow.charAt(i - 1)
                let middleChar = workingRow.charAt(i)
                let rightChar  = i >= workingRow.length - 1 ? '.' : workingRow.charAt(i + 1)

                if ((leftChar === '^' && middleChar === '^' && rightChar === '.') ||
                    (leftChar === '.' && middleChar === '^' && rightChar === '^') ||
                    (leftChar === '^' && middleChar === '.' && rightChar === '.') ||
                    (leftChar === '.' && middleChar === '.' && rightChar === '^')) {
                    newWorkingRow += '^'
                }
                else {
                    newWorkingRow += '.'
                    safeCount     += 1
                }
            }
            workingRow = newWorkingRow
        }
        return safeCount
    }

    console.log(findSafeCount(input, 40))
    console.log(findSafeCount(input, 400000))
}