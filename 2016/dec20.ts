{
    const fs = require('fs')

    const input: string = fs.readFileSync('input/dec20.txt', 'utf8')

    let sortedRules = input.split('\n').map(line => line.split('-', 2).map(token => Number.parseInt(token)) as [number, number])
                                       .sort(([aLower, _aUpper], [bLower, _bUpper]) => aLower - bLower)

    let valueToTest                    = 0
    let minAllowed: number | undefined = undefined
    let numAllowed                     = 0
    for (let i = 0; i < sortedRules.length;) {
        while (i < sortedRules.length && valueToTest >= sortedRules[i][0]) {
            valueToTest = Math.max(valueToTest, sortedRules[i][1] + 1)
            i++
        }
        if (i == sortedRules.length) {
            break
        }
        let [lower, upper] = sortedRules[i]
        if (valueToTest < lower) {
            if (minAllowed === undefined) {
                minAllowed = valueToTest
            }
            numAllowed += (lower - valueToTest)
        }
        valueToTest = upper + 1
        i++
    }

    console.log(minAllowed)
    console.log(numAllowed)
}