{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec12.txt', 'utf8')

    let lines = content.split('\n')

    const CPY_REGEX = /cpy\s+(\w+)\s+(\w+)/
    const INC_REGEX = /inc\s+(\w+)/
    const DEC_REGEX = /dec\s+(\w+)/
    const JNZ_REGEX = /jnz\s+(\w+)\s+(\-?[0-9])/

    let registerContentsInit = new Map<string, number>(Array.from('abcd').map(c => [c, 0]))

    let evalLines = function(registerContentsInit: Map<string, number>) {
        let registerContents = new Map(registerContentsInit.entries())
        for (let i = 0; i < lines.length; i++) {
            let match: RegExpMatchArray | null
            if ((match = lines[i].match(CPY_REGEX))?.length) {
                let [src, dst] = match.slice(1)
                let srcInt = Number.parseInt(src)
                if (Number.isNaN(srcInt)) {
                    srcInt = registerContents.get(src)!
                }
                registerContents.set(dst, srcInt)
            }
            else if ((match = lines[i].match(INC_REGEX))?.length) {
                let reg = match[1]
                registerContents.set(reg, registerContents.get(reg)! + 1)
            }
            else if ((match = lines[i].match(DEC_REGEX))?.length) {
                let reg = match[1]
                registerContents.set(reg, registerContents.get(reg)! - 1)
            }
            else if ((match = lines[i].match(JNZ_REGEX))?.length) {
                let [toTest, amount] = match.slice(1)
                let toTestInt = Number.parseInt(toTest)
                if (Number.isNaN(toTestInt)) {
                    toTestInt = registerContents.get(toTest)!
                }
                if (toTestInt) {
                    i += Number.parseInt(amount) - 1
                }
            }
            else {
                console.assert(false)
            }
        }
        return registerContents
    }

    console.log(evalLines(registerContentsInit).get('a'))
    console.log(evalLines(new Map([...Array.from(registerContentsInit.entries()), ['c', 1]])).get('a'))
}