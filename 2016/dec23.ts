{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec23.txt', 'utf8')

    const lines = content.split('\n')

    const CPY_REGEX = /cpy\s+(\-?\w+)\s+(\w+)/
    const INC_REGEX = /inc\s+(\w+)/
    const DEC_REGEX = /dec\s+(\w+)/
    const JNZ_REGEX = /jnz\s+(\w+)\s+(\-?\w+)/
    const TGL_REGEX = /tgl\s+(\w+)/

    let registerContentsInit = new Map<string, number>(Array.from('abcd').map(c => [c, 0]))

    let evalLines = function(registerContentsInit: Map<string, number>, initA: number) {
        let registerContents = new Map(registerContentsInit.entries())
        let newLines         = Array.from(lines)

        registerContents.set('a', initA)

        for (let i = 0; i < newLines.length; i++) {
            let match: RegExpMatchArray | null
            if ((match = newLines[i].match(CPY_REGEX))?.length) {
                let [src, dst] = match.slice(1)
                let srcInt = Number.parseInt(src)
                if (Number.isNaN(srcInt)) {
                    srcInt = registerContents.get(src)!
                }
                registerContents.set(dst, srcInt)
            }
            else if ((match = newLines[i].match(INC_REGEX))?.length) {
                let reg = match[1]
                registerContents.set(reg, registerContents.get(reg)! + 1)
            }
            else if ((match = newLines[i].match(DEC_REGEX))?.length) {
                let reg = match[1]
                registerContents.set(reg, registerContents.get(reg)! - 1)
            }
            else if ((match = newLines[i].match(JNZ_REGEX))?.length) {
                let [toTest, amount] = match.slice(1)
                let toTestInt = Number.parseInt(toTest)
                if (Number.isNaN(toTestInt)) {
                    toTestInt = registerContents.get(toTest)!
                }
                if (toTestInt) {
                    let amountInt = Number.parseInt(amount)
                    if (Number.isNaN(amountInt)) {
                        amountInt = registerContents.get(amount)!
                    }
                    i += amountInt - 1
                }
            }
            else if ((match = newLines[i].match(TGL_REGEX))?.length) {
                let reg          = match[1]
                let asmToAdjustI = i + registerContents.get(reg)!
                if (asmToAdjustI >= 0 && asmToAdjustI < newLines.length) {
                    let subPattern: string
                    if (newLines[asmToAdjustI].split(/\s+/).length > 2) {
                        if (newLines[asmToAdjustI].startsWith('jnz')) {
                            subPattern = 'cpy'
                        }
                        else {
                            subPattern = 'jnz'
                        }
                    }
                    else {
                        if (newLines[asmToAdjustI].startsWith('inc')) {
                            subPattern = 'dec'
                        }
                        else {
                            subPattern = 'inc'
                        }
                    }
                    newLines[asmToAdjustI] = newLines[asmToAdjustI].replace(/[a-z]{3}/, subPattern)
                }
            }
            else {
                console.assert(false)
            }
        }
        return registerContents
    }

    console.log(evalLines(registerContentsInit,  7).get('a'))
    console.log(evalLines(registerContentsInit, 12).get('a'))
}