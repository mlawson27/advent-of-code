{
    const fs = require('fs')

    let getUniqueCharCount = function(str: string) {
        let charCount = new Map<string, number>()
        for(let c of str) {
            if (c in charCount) {
                charCount[c] += 1
            }
            else {
                charCount[c] = 1
            }
        }
        return new Set(Object.entries(charCount).sort(([ac, acount], [bc, bcount]) => (bcount - acount) || ac.localeCompare(bc))
                                                .slice(0, 5)
                                                .map(vs => vs[0]))
    }

    let content: string = fs.readFileSync('input/dec04.txt', 'utf8')

    let lines = content.split('\n')

    console.log(lines.map(line => line.split('-'))
                     .map(tokens => [tokens.splice(0, tokens.length - 1).join(''), tokens[tokens.length - 1]])
                     .map(parts => [getUniqueCharCount(parts[0]), parts[1].substring(0, parts[1].length - 1).split('[')].flat())
                     .filter(parts => (parts[2] as string).split('').every(c => (parts[0] as Set<string>).has(c)))
                     .reduce((existing, parts) => existing + Number.parseInt(parts[1] as string), 0))

    console.log(lines.map(l => l.substring(0, l.length - 7).split('-'))
                     .map(tokens => [tokens.splice(0, tokens.length - 1), Number.parseInt(tokens[tokens.length - 1])])
                     .map(vals => [(vals[0] as string[]).map(str => str.split("").map(c => [String.fromCharCode((c.charCodeAt(0) - 'a'.charCodeAt(0) + (vals[1] as number)) % 26 + 'a'.charCodeAt(0))]).join('')).join(' '), vals[1]])
                     .filter(vals => (vals[0] as string).includes('northpole'))[0][1])
}