{
    const fs = require('fs')

    const input: string = fs.readFileSync('input/dec22.txt', 'utf8')

    type node = [string, number, number, number, number]

    const makeGrid = function(nodes: node[]) {
        const getPos = function(node: node) {
            let m = node[0].match(/\/dev\/grid\/node-x(\d+)-y(\d+)/)
            return [Number.parseInt(m![1]), Number.parseInt(m![2])] as [number, number]
        }

        const [xSize, ySize] = nodes.map(getPos).reduce(([maxX, maxY], [x, y]) => [Math.max(maxX, x + 1), Math.max(maxY, y + 1)], [Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER])

        let ret = [] as node[][]
        for (let row = 0; row < ySize; row++) {
            let rowData = [] as node[]
            for (let col = 0; col < xSize; col++) {
                rowData.push(nodes[col * ySize + row])
            }
            ret.push(rowData)
        }
        return ret
    }

    const copyGrid = function(nodeGrid: node[][]) {
        return nodeGrid.map(nodeRow => nodeRow.map(node => Array.from(node) as node))
    }

    const swapData = function(nodeGrid: node[][], a: [number, number], b: [number, number]) {
        let sizeSwap = nodeGrid[b[1]][b[0]][2]
        nodeGrid[b[1]][b[0]][2] = nodeGrid[b[1]][b[0]][2] - sizeSwap
        nodeGrid[b[1]][b[0]][3] = nodeGrid[b[1]][b[0]][1]
        nodeGrid[a[1]][a[0]][2] = sizeSwap
        nodeGrid[a[1]][a[0]][3] = nodeGrid[a[1]][a[0]][1] - sizeSwap
    }

    const getNumViableTransferPairs = function(nodesFlat: node[]) {
        let count = 0
        for (let i = 0; i < nodesFlat.length; i++) {
            if (nodesFlat[i][2] === 0) {
                continue
            }
            for (let j = 0; j < nodesFlat.length; j++) {
                if (i === j) {
                    continue
                }
                count += (nodesFlat[i][2] <= nodesFlat[j][3] ? 1 : 0)
            }
        }
        return count
    }

    const getMovesToMoveXMaxToX0 = function(nodesGrid: node[][]) {
        let emptyPosArr = nodesGrid.flatMap((row, rowI) => row.map((node, colI) => [colI, rowI, node] as [number, number, node]))
                                   .filter(([_colI, _rowI, node]) => node[2] === 0)
                                   .map(([colI, rowI, _node]) => [colI, rowI] as [number, number])

        console.assert(emptyPosArr.length === 1)

        let workingBoardData = [[nodesGrid, emptyPosArr[0], 0] as [node[][], [number, number], number]]
        let seenMoves        = new Set<number>()
        for (let x = nodesGrid[0].length - 2; x >= 0; x--) {
            seenMoves.clear()
            while (true) {
                let [workingGrid, emptyPos, movesSoFar] = workingBoardData.shift()!

                if (emptyPos[1] === 0 && emptyPos[0] == x) {
                    workingBoardData = [[workingGrid, emptyPos, movesSoFar]]
                    break
                }

                for (let adjustment of [[0, -1], [-1, 0], [0, 1], [1, 0]]) {
                    let newPos = [emptyPos[0] + adjustment[0], emptyPos[1] + adjustment[1]] as [number, number]
                    if ((newPos[0] >= 0 && newPos[0] < workingGrid[emptyPos[1]].length) &&
                        (newPos[1] >= 0 && newPos[1] < workingGrid.length) &&
                        (newPos[0] != x + 1 || newPos[1] != 0) &&
                        (workingGrid[newPos[1]][newPos[0]][2] <= workingGrid[emptyPos[1]][emptyPos[0]][3]) &&
                        !seenMoves.has(newPos[1] << 16 | newPos[0])) {
                        let newGrid  = copyGrid(workingGrid)
                        swapData(newGrid, emptyPos, newPos)
                        workingBoardData.push([newGrid, newPos, movesSoFar + 1])
                        seenMoves.add(newPos[1] << 16 | newPos[0])
                    }
                }
            }
            swapData(workingBoardData[0][0], [x, 0], [x + 1, 0])
            workingBoardData[0][1][0] += 1
            workingBoardData[0][2]    += 1
        }
        return workingBoardData[0][2]
    }

    const nodesFlat = input.split('\n').map(line => line.split(/\s+/))
                                       .slice(2)
                                       .map(arr => [arr[0], ...arr.slice(1).map(token => Number.parseInt(token.slice(0, token.length - 1)))] as node)

    const nodesGrid = makeGrid(nodesFlat)

    console.log(getNumViableTransferPairs(nodesFlat))
    console.log(getMovesToMoveXMaxToX0(nodesGrid))
}