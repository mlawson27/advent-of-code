{
    const input = 3017957

    class Node {
        val: number
        next: Node
    }

    let head = new Node()
    let next = head
    for (let i = 1; i < input; i++) {
        next.val = i
        next.next = new Node()
        next = next.next
    }
    next.val  = input
    next.next = head

    let remaining = input
    next          = head
    while (remaining > 1) {
        let nextNext = next.next
        next.next = nextNext.next
        next = next.next
        remaining--
    }
    head = next

    console.log(next.val)

    let p2data = Array.from(new Array(input + 1).keys())
    p2data.shift()
    let i = 0;
    while (p2data.length > 1) {
        let acrossI = (i + ((p2data.length - (p2data.length % 2)) / 2)) % p2data.length
        p2data.splice(acrossI, 1)[0]
        i = (i + (acrossI > i ? 1 : 0)) % p2data.length
    }
    console.log(p2data[0])
}