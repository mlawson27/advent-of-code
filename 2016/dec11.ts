{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec11.txt', 'utf8')

    let lines = content.split('\n')

    const MICROCHIP_REGEX = /(\w+)\-compatible/g
    const GENERATOR_REGEX = /(\w+)\s+generator/g

    const floorsContentsInit: [string[], string[]][] = lines.map(line => [Array.from(line.matchAll(MICROCHIP_REGEX), m => m[1]).sort(),
                                                                          Array.from(line.matchAll(GENERATOR_REGEX), m => m[1]).sort()])


    const copyContents = function(existing: [string[], string[]][]) {
        return existing.map(contents => contents.map(partType => partType.map(part => part))) as [string[], string[]][]
    }

    const indexCombinations = function(numElements: number, count: number) {
        let ret: number[][] = []
        const inner = function(remainingNumElements: number, working: number[]) {
            for (let i = remainingNumElements - 1; i >= 0; i--) {
                if (!working.find(v => v === i)) {
                    working.push(i)
                    if (working.length < count) {
                        inner(i, working)
                    }
                    else {
                        ret.unshift(Array.from(working))
                    }
                    working.pop()
                }
            }
        }
        if (count > 0) {
            inner(numElements, [])
        }
        return ret.length > 0 ? ret : undefined
    }

    const getKey = function(floorsContents: [string[], string[]][], currentFloorIndex: number) {
        return currentFloorIndex.toString() + '|' + floorsContents.map(floorContents => floorContents.map(partType => partType.length).join(';')).join('|')
    }

    let difference = function<T>(setA: Set<T>, setB: Set<T>) {
        let _difference = new Set(setA);
        for (let elem of setB) {
          _difference.delete(elem);
        }
        return _difference;
    }

    const getMinMoveCount = function(floorsContents: [string[], string[]][], currentFloorIndex: number, moveCount: number = 0, seenStates: Map<string, number> = new Map<string, number>()) {
        if (floorsContents.slice(0, floorsContents.length - 1).every(floorContents => floorContents.every(floorType => floorType.length == 0))) {
            return moveCount
        }

        {
            let [microchipSet, generatorSet] = floorsContents[currentFloorIndex].map(partTypes => new Set(partTypes))
            if (generatorSet.size > 0 && difference(microchipSet, generatorSet).size > 0) {
                return undefined
            }
        }

        if (moveCount >= 60) {
            return undefined
        }

        let contentKey = getKey(floorsContents, currentFloorIndex)
        if (seenStates.has(contentKey) && seenStates.get(contentKey) <= moveCount) {
            return undefined
        }
        seenStates.set(contentKey, moveCount)

        let result: number | undefined = undefined

        let possibleMoves: [number, number[][]][] = []
        for (let direction of [1, -1]) {
            if (currentFloorIndex + direction >= floorsContents.length || currentFloorIndex + direction < 0) {
                continue
            }

            for (let microchipToMoveCount = 0; microchipToMoveCount <= Math.min(floorsContents[currentFloorIndex][0].length, 2); microchipToMoveCount++) {
                for (let generatorToMoveCount = 0; generatorToMoveCount <= Math.min(2 - microchipToMoveCount, floorsContents[currentFloorIndex][1].length); generatorToMoveCount++) {
                    if (microchipToMoveCount == 0 && generatorToMoveCount == 0) {
                        continue
                    }
                    let indices: number[][] = []
                    for (let microchipToMoveIndices of (indexCombinations(floorsContents[currentFloorIndex][0].length, microchipToMoveCount) || [[]])) {
                        indices.push(microchipToMoveIndices)
                        for (let generatorToMoveIndices of (indexCombinations(floorsContents[currentFloorIndex][1].length, generatorToMoveCount) || [[]])) {
                            indices.push(generatorToMoveIndices)
                            possibleMoves.push([direction, indices.map(p => Array.from(p))])
                            indices.pop()
                        }
                        indices.pop()
                    }
                }
            }
        }

        for (let [aa, [direction, adjustments]] of possibleMoves.map((v, i) => [i, v] as [number, [number, [number[], number[]]]])) {
            let newContent = copyContents(floorsContents)
            for (let [partTypeIndex, partTypeMoves] of adjustments.map((a, i) => [i, a] as [number, number[]])) {
                for (let index of partTypeMoves) {
                    newContent[currentFloorIndex + direction][partTypeIndex].push(newContent[currentFloorIndex][partTypeIndex][index])
                    newContent[currentFloorIndex + direction][partTypeIndex].sort()
                    newContent[currentFloorIndex][partTypeIndex].splice(index, 1)
                }
            }
            let innerRet = getMinMoveCount(newContent, currentFloorIndex + direction, moveCount + 1, seenStates)
            if (innerRet) {
                result = Math.min(result || innerRet, innerRet)
            }
        }
        return result
    }

    console.log(getMinMoveCount(floorsContentsInit, 0))

    let p2Input = copyContents(floorsContentsInit)
    for (let firstFloorParts of p2Input[0]) {
        firstFloorParts.push('elerium', 'dilithium')
        firstFloorParts.sort()
    }

    console.log(getMinMoveCount(p2Input, 0))
}