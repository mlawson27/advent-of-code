{
    const fs = require('fs')

    const decompressedLenP1 = function(str: string) {
        let numberStart: number | null = null
        let numCharsToRepeat: number
        let decompressedSize = 0
        let inParentheses    = false
        for (let i = 0; i < str.length; i++) {
            if (str[i] == '(') {
                numberStart   = i + 1
                inParentheses = true
            }
            else if (str[i] == 'x') {
                numCharsToRepeat = Number.parseInt(str.slice(numberStart as number, i))
                numberStart      = i + 1
            }
            else if (str[i] == ')') {
                let repeatCount = Number.parseInt(str.slice(numberStart as number, i))
                decompressedSize += (numCharsToRepeat * repeatCount)
                i += numCharsToRepeat
                inParentheses = false
            }
            else {
                decompressedSize += (inParentheses ? 0 : 1)
            }
        }
        return decompressedSize
    }

    const decompressedLenP2 = function(str: string, index?: number, length?: number) {
        let numberStart: number | null = null
        let numCharsToRepeat: number
        let decompressedSize  = 0
        let inParentheses     = false
        let amountToIncrement = 1
        for (let i = index || 0; i < (((index || 0) + (length || 0)) || str.length); i++) {
            if (str[i] == '(') {
                numberStart   = i + 1
                inParentheses = true
            }
            else if (str[i] == 'x') {
                numCharsToRepeat = Number.parseInt(str.slice(numberStart as number, i))
                numberStart      = i + 1
            }
            else if (str[i] == ')') {
                let repeatCount = Number.parseInt(str.slice(numberStart as number, i))
                amountToIncrement *= (numCharsToRepeat * repeatCount)
                decompressedSize += repeatCount * decompressedLenP2(str, i + 1, numCharsToRepeat)
                i += numCharsToRepeat
                inParentheses = false
            }
            else {
                decompressedSize += (inParentheses ? 0 : 1)
            }
        }
        return decompressedSize
    }

    let content: string = fs.readFileSync('input/dec09.txt', 'utf8')

    let lines = content.split('\n')

    console.log(lines.map(decompressedLenP1).reduce((existing, row) => existing + row, 0))
    console.log(lines.map(line => decompressedLenP2(line)).reduce((existing, row) => existing + row, 0))
}