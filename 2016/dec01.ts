{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec01.txt', 'utf8')

    let [x, y]       = [0, 0]
    let currentRot   = 0
    let positionsSet = new Set<number>()
    let firstPos     = null
    for (let token of content.split(', ')) {
        if (token[0] == 'R') {
            currentRot = (currentRot + 1) % 4
        }
        else {
            currentRot = (currentRot + 3) % 4
        }
        let xSteps = ((2 - currentRot) % 2) * Number.parseInt(token.slice(1))
        let ySteps = ((1 - currentRot) % 2) * Number.parseInt(token.slice(1))
        for (let xStep = 0; xStep < Math.abs(xSteps); xStep++) {
            x += xSteps / Math.abs(xSteps);
            if (firstPos === null && positionsSet.has(x * 100000 + y)) {
                firstPos = [x, y]
            }
            positionsSet.add(x * 100000 + y)
        }
        for (let yStep = 0; yStep < Math.abs(ySteps); yStep++) {
            y += ySteps / Math.abs(ySteps);
            if (firstPos === null && positionsSet.has(x * 100000 + y)) {
                firstPos = [x, y]
            }
            positionsSet.add(x * 100000 + y)
        }
    }
    console.log(Math.abs(x) + Math.abs(y))
    console.log(Math.abs(firstPos[0]) + Math.abs(firstPos[1]))
}