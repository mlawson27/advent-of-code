{
    const input = '10001110011110000'

    let find = function(value: string, desiredLen: number) {
        let reverseStr = function(value: string) {
            let result = ''
            for (let c of value) {
                result = (c === '1' ? '0' : '1') + result
            }
            return result
        }

        while (value.length < desiredLen) {
            value = value + '0' + reverseStr(value)
        }
        value = value.substring(0, desiredLen)

        while (value.length % 2 == 0) {
            let newValue = ''
            for (let pairStart = 0; pairStart < value.length; pairStart += 2) {
                newValue += value[pairStart] === value[pairStart + 1] ? '1' : '0'
            }
            value = newValue
        }

        return value
    }

    console.log(find(input, 272))
    console.log(find(input, 35651584))
}