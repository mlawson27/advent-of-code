{
    const fs = require('fs')

    function isValidTriangle(sides: number[]): boolean {
        return (sides[0] + sides[1]) > sides[2]
    }

    let content: string = fs.readFileSync('input/dec03.txt', 'utf8')

    let sides = content.split('\n').map(line => line.trim().split(RegExp('\\s+')).map(token => Number.parseInt(token)))

    console.log(sides.map(x => Array.from(x).sort((a, b) => a - b)).filter(isValidTriangle).length)

    console.log(Array.from(Array(sides.length / 3).keys()).map(i => sides.slice(i * 3, i * 3 + 3))
                                                          .flatMap(a => [a.map(b => b[0]), a.map(b => b[1]), a.map(b => b[2])].map(b => b.sort((x, y) => x - y)))
                                                          .filter(isValidTriangle).length)
}