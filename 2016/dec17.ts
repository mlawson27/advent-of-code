{
    const md5 = require('ts-md5')

    const input = 'njfxhljp'

    const adjustments = [['U', [0, -1]], ['D', [0, 1]], ['L', [-1, 0]], ['R', [1, 0]]] as [string, [number, number]][]

    let find = function(value: string, shortest: boolean) {
        let paths: [[number, number], string][] = [[[0, 0], '']]

        let longestResult: string | undefined = undefined
        while (paths.length) {
            let [[x, y], workingPath] = paths.shift()!
            if (x === 3 && y === 3) {
                if (shortest) {
                    return workingPath
                }
                else {
                    longestResult = workingPath
                    continue
                }
            }

            let hash = md5.Md5.hashStr(value + workingPath) as string
            for (let hashIndex = 0; hashIndex < 4; hashIndex++) {
                let cInt = Number.parseInt(hash.charAt(hashIndex), 16)
                if (cInt >= 0xb) {
                    let [dirChar, [adjustX, adjustY]] = adjustments[hashIndex]

                    let newPos = [x + adjustX, y + adjustY] as [number, number]
                    if (newPos.every(v => v >= 0 && v < 4)) {
                        paths.push([newPos, workingPath + dirChar])
                    }
                }
            }
        }
        return longestResult
    }

    console.log(find(input, true))
    console.log(find(input, false)?.length)
}