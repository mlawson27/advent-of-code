{
    const fs = require('fs')

    let getUsedChar = function (str: string, sortFn: (a: number, b: number) => number) {
        let charCount = new Map<string, number>()
        for(let c of str) {
            if (c in charCount) {
                charCount[c] += 1
            }
            else {
                charCount[c] = 1
            }
        }
        return Object.entries(charCount)
                     .sort(([ac, acount], [bc, bcount]) => sortFn((acount as number), (bcount as number)))[0][0]
    }

    let content: string = fs.readFileSync('input/dec06.txt', 'utf8')

    let lines = content.split('\n')

    let lineChars  = lines.map(line => line.split(''))
    let strsPerPos = Array.from(new Array(lineChars[0].length).keys()).map(index => lineChars.flatMap(chars => chars[index]).join(''))

    console.log(strsPerPos.map(str => getUsedChar(str, (a, b) => b - a)).join(''))
    console.log(strsPerPos.map(str => getUsedChar(str, (a, b) => a - b)).join(''))
}