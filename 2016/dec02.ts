{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec02.txt', 'utf8')

    let [x, y] = [1, 1]
    let code   = ""
    for (let token of content.split('\n')) {
        for (let c of token) {
            switch (c) {
                case 'U': y = Math.max(y - 1, 0); break;
                case 'D': y = Math.min(y + 1, 2); break;
                case 'L': x = Math.max(x - 1, 0); break;
                case 'R': x = Math.min(x + 1, 2); break;
            }
        }
        code += ((y * 3) + (x + 1)).toString(16)
    }
    console.log(code)

    const board = [[ null, null, '1', null, null ],
                   [ null,  '2', '3',  '4', null ],
                   [  '5',  '6', '7',  '8',  '9' ],
                   [ null,  'A', 'B',  'C', null ],
                   [ null, null, 'D', null, null ]]
    x    = 0
    y    = 2
    code = ""
    for (let token of content.split('\n')) {
        for (let c of token) {
            switch (c) {
                case 'U': y -= (y > 0 && (board[y - 1][x] !== null) ? 1 : 0); break;
                case 'D': y += (y < 4 && (board[y + 1][x] !== null) ? 1 : 0); break;
                case 'L': x -= (x > 0 && (board[y][x - 1] !== null) ? 1 : 0); break;
                case 'R': x += (x < 4 && (board[y][x + 1] !== null) ? 1 : 0); break;
            }
        }
        code += board[y][x]
    }
    console.log(code)
}