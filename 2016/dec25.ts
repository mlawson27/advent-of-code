{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec25.txt', 'utf8')

    const lines = content.split('\n')

    const CPY_REGEX = /cpy\s+(\-?\w+)\s+(\w+)/
    const INC_REGEX = /inc\s+(\w+)/
    const DEC_REGEX = /dec\s+(\w+)/
    const JNZ_REGEX = /jnz\s+(\w+)\s+(\-?\w+)/
    const OUT_REGEX = /out\s+(\w+)/

    let registerContentsInit = new Map<string, number>(Array.from('abcd').map(c => [c, 0]))

    let evalLines = function(registerContentsInit: Map<string, number>) {
        let registerContents = new Map(registerContentsInit.entries())
        let newLines         = Array.from(lines)

        for (let minA = 0; ; minA++) {
            registerContents.forEach((_v, k) => registerContents.set(k, 0))
            registerContents.set('a', minA)

            let output = [] as number[]

            for (let i = 0; i < newLines.length; i++) {
                let match: RegExpMatchArray | null
                if ((match = newLines[i].match(CPY_REGEX))?.length) {
                    let [src, dst] = match.slice(1)
                    let srcInt = Number.parseInt(src)
                    if (Number.isNaN(srcInt)) {
                        srcInt = registerContents.get(src)!
                    }
                    registerContents.set(dst, srcInt)
                }
                else if ((match = newLines[i].match(INC_REGEX))?.length) {
                    let reg = match[1]
                    registerContents.set(reg, registerContents.get(reg)! + 1)
                }
                else if ((match = newLines[i].match(DEC_REGEX))?.length) {
                    let reg = match[1]
                    registerContents.set(reg, registerContents.get(reg)! - 1)
                }
                else if ((match = newLines[i].match(JNZ_REGEX))?.length) {
                    let [toTest, amount] = match.slice(1)
                    let toTestInt = Number.parseInt(toTest)
                    if (Number.isNaN(toTestInt)) {
                        toTestInt = registerContents.get(toTest)!
                    }
                    if (toTestInt) {
                        let amountInt = Number.parseInt(amount)
                        if (Number.isNaN(amountInt)) {
                            amountInt = registerContents.get(amount)!
                        }
                        i += amountInt - 1
                    }
                }
                else if ((match = newLines[i].match(OUT_REGEX))?.length) {
                    let reg    = match[1]
                    let result = registerContents.get(reg)!
                    if ((result > 1 || result < 0) || ((output.length > 0) && output[output.length - 1] === result)) {
                        break
                    }
                    output.push(result)
                    if (output.length >= 50) {
                        return minA
                    }
                }
                else {
                    console.assert(false)
                }
            }
        }
    }

    console.log(evalLines(registerContentsInit)!)
}