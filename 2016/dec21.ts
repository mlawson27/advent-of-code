{
    const fs = require('fs')

    const swapPositions = function(str: string[], xIndex: number, yIndex: number) {
        let newStr = Array.from(str)
        newStr[xIndex] = str[yIndex]
        newStr[yIndex] = str[xIndex]
        return newStr
    }

    const swapLetters = function(str: string[], x: string, y: string) {
        return swapPositions(str, str.indexOf(x), str.indexOf(y))
    }

    const rotate = function(str: string[], x: number, left: boolean) {
        let result: string[]
        if (left) {
            result = [...str.slice(x, str.length), ...str.slice(0, x)]
        }
        else {
            result = [...str.slice(str.length - x, str.length), ...str.slice(0, str.length - x)]
        }
        console.assert(str.length === result.length)
        return result
    }

    const rotateOnLetter = function(str: string[], x: string) {
        let i = str.indexOf(x)
        return rotate(str, (i + 1 + (i >= 4 ? 1 : 0)) % str.length, false)
    }

    const rotateOnLetterReverse = function(str: string[], x: string) {
        let newStr        = Array.from(str)
        let origStrJoined = str.join('')
        while (rotateOnLetter(newStr, x).join('') !== origStrJoined) {
            newStr = rotate(newStr, 1, true)
        }
        return newStr
    }

    const reverse = function(str: string[], x: number, y: number) {
        return [...str.slice(0, x), ...str.slice(x, y + 1).reverse(), ...str.slice(y + 1, str.length)]
    }

    const move = function(str: string[], x: number, y: number) {
        let result = Array.from(str)
        result.splice(x, 1)
        result.splice(y, 0, str[x])
        return result
    }

    const lineToFn = function(line: string) {
        let match:  RegExpMatchArray | null
        let result: any;
        if ((match = line.match(/swap\s+position\s+(\d+)\s+with\s+position\s+(\d+)/))) {
            result = [swapPositions, swapPositions, [Number.parseInt(match![1]), Number.parseInt(match![2])]]
        }
        else if ((match = line.match(/swap\s+letter\s+(\w)\s+with\s+letter\s+(\w)/))) {
            result = [swapLetters, swapLetters, [match![1], match![2]]]
        }
        else if ((match = line.match(/rotate\s+((?:left)|(?:right))\s+(\d+)\s+steps?/))) {
            let isLeft = match![1] === 'left'
            result = [(v: string[], x: number) => rotate(v, x, isLeft), (v: string[], x: number) => rotate(v, x, !isLeft), [Number.parseInt(match![2])]]
        }
        else if ((match = line.match(/rotate\s+based\s+on\s+position\s+of\s+letter\s+(\w)/))) {
            result = [rotateOnLetter, rotateOnLetterReverse, [match![1]]]
        }
        else if ((match = line.match(/reverse\s+positions\s+(\d+)\s+through\s+(\d+)/))) {
            result = [reverse, reverse, [Number.parseInt(match![1]), Number.parseInt(match![2])]]
        }
        else if ((match = line.match(/move\s+position\s+(\d+)\s+to\s+position\s+(\d+)/))) {
            result = [move, (v, x, y) => move(v, y, x), [Number.parseInt(match![1]), Number.parseInt(match![2])]]
        }
        else {
            console.assert(false)
        }
        return result
    }

    const input: string = fs.readFileSync('input/dec21.txt', 'utf8')

    const functions = input.split('\n').map(lineToFn)

    console.log(functions.reduce((working, fnArgs) => fnArgs[0].apply(null, [working, ...fnArgs[2]]), "abcdefgh".split('')).join(''))
    console.log(Array.from(functions).reverse().reduce((working, fnArgs) => fnArgs[1].apply(null, [working, ...fnArgs[2]]), "fbgdceah".split('')).join(''))
}