{
    const fs = require('fs')

    let content: string = fs.readFileSync('input/dec10.txt', 'utf8')

    let lines = content.split('\n')

    const VALUE_REGEX = new RegExp(String.raw`value\s+(\d+)\s+goes\s+to\s+bot\s+(\d+)`)
    const GIVES_REGEX = new RegExp(String.raw`bot\s+(\d+)\s+gives\s+low\s+to\s+((?:bot)|(?:output))\s+(\d+)\s+and\s+high\s+to\s+((?:bot)|(?:output))\s+(\d+)`)

    let botStateInit = new Map<number, number[]>()
    let rulesInit    = new Map<number, [[boolean, number], [boolean, number]]>()
    for (let line of lines) {
        let match = VALUE_REGEX.exec(line)
        if (match) {
            let [value, bot] = match.filter((_, i) => i > 0).map(x => Number.parseInt(x))
            let newValue
            if (botStateInit.has(bot)) {
                newValue = botStateInit.get(bot)
            }
            else {
                newValue = []
            }
            botStateInit.set(bot, [...newValue, value].sort((a, b) => a - b))
        }
        else {
            match = GIVES_REGEX.exec(line)
            if (match) {
                let bot = Number.parseInt(match[1])
                if (rulesInit.has(bot)) {
                    console.assert(false)
                }
                else {
                    rulesInit.set(bot, [[match[2] === 'bot', Number.parseInt(match[3])], [match[4] === 'bot', Number.parseInt(match[5])]])
                }
            }
        }
    }

    const findBotShouldCompare = function (values: number[]) {
        let rules    = new Map<number, [[boolean, number], [boolean, number]]>()
        let botState = new Map<number, number[]>()
        let outState = new Map<number, number[]>()

        for (let [rulesKey, rulesValue] of rulesInit) {
            rules.set(rulesKey, [rulesValue[0], rulesValue[1]])
        }

        for (let [botStateKey, botStateValue] of botStateInit) {
            botState.set(botStateKey, Array.from(botStateValue))
        }

        while (rules.size > 0) {
            for (let [bot, fromData] of botState) {
                if (fromData.length > 1) {
                    let ruleData = rules.get(bot)
                    let fromData = botState.get(bot)

                    if (fromData.length === values.length && fromData.reduce((result, v, i) => result && (v === values[i]), true)) {
                        return bot
                    }

                    for (let [vIndex, [isBot, index]] of ruleData.entries()) {
                        let destData: number[]
                        if (isBot) {
                            if (!botState.has(index)) {
                                botState.set(index, [])
                            }
                            destData = botState.get(index)
                        }
                        else {
                            if (!outState.has(index)) {
                                outState.set(index, [])
                            }
                            destData = outState.get(index) || []
                        }
                        destData.push(fromData.splice(vIndex === 0 ? 0 : fromData.length - 1, 1)[0])
                        destData.sort((a, b) => a - b)
                    }
                    if (fromData.length === 0) {
                        botState.delete(bot)
                    }
                    rules.delete(bot)
                    break
                }
            }
        }
        return values.reduce((working, v) => outState.get(v)[0] * working, 1)
    }

    console.log(findBotShouldCompare([17, 61]))
    console.log(findBotShouldCompare([0, 1, 2]))
}