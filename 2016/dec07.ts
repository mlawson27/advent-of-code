{
    const fs = require('fs')

    let containsAbba = function(str: string) {
        let inBrackets            = false
        let anyFoundOutOfBrackets = false
        for (let i = 0; i < str.length - 3; i++) {
            if (str[i] == '[') {
                inBrackets = true
            }
            else if (str[i] == ']') {
                inBrackets = false
            }
            else if (str.substring(i, i + 2) == (str[i + 3] + str[i + 2]) && str[i] != str[i + 1]) {
                if (inBrackets) {
                    return false
                }
                anyFoundOutOfBrackets = true
            }
        }
        return anyFoundOutOfBrackets
    }

    let containsSsl = function(str: string) {
        let inBrackets = false
        let matches    = [new Set<string>(), new Set<string>()]
        for (let i = 0; i < str.length - 2; i++) {
            if (str[i] == '[') {
                inBrackets = true
            }
            else if (str[i] == ']') {
                inBrackets = false
            }
            else if (str[i] == str[i + 2] && str[i] != str[i + 1]) {
                let inversedMatch = str[i + 1] + str[i] + str[i + 1]
                if (matches[inBrackets ? 0 : 1].has(inversedMatch)) {
                    return true
                }
                matches[inBrackets ? 1 : 0].add(str.substring(i, i + 3))
            }
        }
        return false
    }

    let content: string = fs.readFileSync('input/dec07.txt', 'utf8')

    let lines = content.split('\n')

    console.log(lines.filter(containsAbba).length)
    console.log(lines.filter(containsSsl).length)
}