{
    const favoriteNumber = 1362

    let room = new Array(50).fill(0).map(() => new Array(50).fill('.') as string[])

    for (let y = 0; y < room.length; y++) {
        for (let x = 0; x < room[y].length; x++) {
            let sum      = x*x + 3*x + 2*x*y + y + y*y + favoriteNumber
            let setCount = 0
            while (sum > 0) {
                setCount += 1
                sum = sum & (sum - 1)
            }
            if (setCount % 2) {
                room[y][x] = '#'
            }
        }
    }

    let getKey = function(pos: [number, number]) {
        return pos[1] << 16 | pos[0]
    }

    const traverseHelper = function(currentPos: [number, number],
                                    moveCount: number,
                                    usedPositions: Set<number>,
                                    getResult: (currentKey: number, moveCount: number) => number | undefined) {
        const currentKey = getKey(currentPos)

        let result = getResult(currentKey, moveCount)
        if (result !== undefined) {
            return result
        }

        usedPositions.add(currentKey)
        for (let adjustment of [[1, 0], [0, 1], [-1, 0], [0, -1]] as [number, number][]) {
            let newPos = [currentPos[0] + adjustment[0], currentPos[1] + adjustment[1]] as [number, number]
            if (newPos[1] >= 0 &&
                newPos[1] < room.length &&
                newPos[0] >= 0 &&
                newPos[0] <= room[newPos[1]].length &&
                room[newPos[1]][newPos[0]] == '.' &&
                !usedPositions.has(getKey(newPos))) {
                let subResult = traverseHelper(newPos, moveCount + 1, usedPositions, getResult)
                if (subResult) {
                    result = Math.min(result ?? subResult, subResult)
                }
            }
        }
        usedPositions.delete(currentKey)
        return result
    }

    const findPath = function(currentPos: [number, number], desiredPosKey: number) {
        return traverseHelper(currentPos,
                              0,
                              new Set<number>(),
                              (currentKey, moveCount) => (currentKey === desiredPosKey) ? moveCount : undefined)
    }

    const getPositionCount = function(currentPos: [number, number], maxMoveCount: number) {
        let foundPositions = new Set<number>()
        traverseHelper(currentPos,
                       0,
                       new Set<number>(),
                       (currentKey, moveCount) => {
                         if (moveCount > maxMoveCount) {
                            return 0
                         }
                         else {
                            foundPositions.add(currentKey)
                            return undefined
                         }
                       })
        return foundPositions.size
    }
    console.log(findPath([1, 1], getKey([31, 39])))
    console.log(getPositionCount([1, 1], 50))
}