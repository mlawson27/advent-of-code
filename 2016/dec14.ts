{
    const md5 = require('ts-md5')

    const input = 'jlmsuwbz'

    let find = function(hashCount: number) {
        let foundHashes:   [string, number][] = []
        let workingHashes: [string, number][] = []
        for (let index = 0; foundHashes.length < 64; index++) {
            while (workingHashes.length > 0 && index > workingHashes[0][1] + 1000) {
                workingHashes.shift()
            }

            let hash = input + index;
            for (let hashAttempt = 0; hashAttempt < hashCount; hashAttempt++) {
                hash = md5.Md5.hashStr(hash)
            }

            for (let fiveMatch of (/([a-zA-Z0-9])\1{4}/).exec(hash) || []) {
                for (let [hashI, hash] of workingHashes.map((h, i) => [i, h] as [number, [string, number]]).reverse()) {
                    if (hash[0] === fiveMatch[1]) {
                        foundHashes.push(hash)
                        workingHashes.splice(hashI, 1)
                    }
                }
            }

            let threeMatch
            if ((threeMatch = hash.match(/([a-zA-Z0-9])\1{2}/))?.length) {
                workingHashes.push([threeMatch[1], index])
            }
        }

        foundHashes.sort((a, b) => a[1] - b[1])
        return foundHashes[63][1]
    }
    console.log(find(1))
    console.log(find(2017))
}