{
    const fs = require('fs')

    const rect = function(columns: number, rows: number, board: [[string]]) {
        for (let row = 0; row < rows; row++) {
            for (let column = 0; column < columns; column++) {
                board[row][column] = '#'
            }
        }
    }

    const rotateRow = function(row: number, amount: number, board: [[string]]) {
        var existingRow = Array.from(board[row])
        for (let column = 0; column < board[row].length; column++) {
            board[row][(column + amount) % board[row].length] = existingRow[column]
        }
    }

    const rotateColumn = function(column: number, amount: number, board: [[string]]) {
        var existingColumn = board.map(arr => arr[column])
        for (let row = 0; row < board.length; row++) {
            board[(row + amount) % board.length][column] = existingColumn[row]
        }
    }

    const ROWS_REGEX   = new RegExp(String.raw`rect\s+(\d+)x(\d+)`)
    const ROTATE_REGEX = new RegExp(String.raw`rotate\s+((?:column)|(?:row))\s+[xy]=(\d+)\s+by\s+(\d+)`)

    let content: string = fs.readFileSync('input/dec08.txt', 'utf8')

    let board = new Array(6).fill(null).map(_ => new Array(50).fill('.'))

    let lines = content.split('\n')

    let data = lines.map(line => (ROWS_REGEX.exec(line) || ROTATE_REGEX.exec(line))?.slice(1) as string[])
                    .map(matchData => (matchData.length == 2 ? [...matchData.map(x => Number.parseInt(x)), rect]
                                                             : [...matchData.slice(1).map(x => Number.parseInt(x)), (matchData[0] === 'row' ? rotateRow : rotateColumn)]))

    data.forEach(data => (data[data.length - 1] as Function).apply(null, [...data.splice(0, data.length - 1), board]))

    console.log(board.reduce((existing, row) => existing + row.reduce((rowExisting, elem) => rowExisting + (elem === '#'), 0), 0))
    console.log(board.map(row => row.join('')))
}