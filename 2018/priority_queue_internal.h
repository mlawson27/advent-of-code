#ifndef __PRIORITY_QUEUE_INTERNAL_H__
#define __PRIORITY_QUEUE_INTERNAL_H__

#ifndef __PRIORITY_QUEUE_H__
#error "Only include priority_queue_internal.h from priority_queue.h"
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct _priority_queue_internal_node;

struct _priority_queue_internal
{
  size_t                                data_size;
  size_t                                length;
  struct _priority_queue_internal_node* head;
  void(*free_fn)(void*);
};

struct _priority_queue_internal _priority_queue_internal_new(size_t data_size,
                                                             void(*free_fn)(void*));

void _priority_queue_internal_clean(struct _priority_queue_internal* q);

static inline bool _priority_queue_internal_is_empty(struct _priority_queue_internal const* q)
{
  return q->head == nullptr;
}

static inline size_t _priority_queue_internal_length(struct _priority_queue_internal const* q)
{
  return q->length;
}

void _priority_queue_internal_push(struct _priority_queue_internal* q, size_t priority, void const* data);

void _priority_queue_internal_pop(struct _priority_queue_internal* q, void* data);

#endif