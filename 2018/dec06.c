#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

#include "util.h"
#include "vector.h"

typedef int coordinate_pair_t[2];

MAKE_VECTOR_TYPE(coord, coordinate_pair_t)

#define P2_DISTANCE 10000

static inline unsigned int get_distance(const coordinate_pair_t a, const coordinate_pair_t b)
{
  unsigned int total = 0;
  for (int i = 0; i < ARRAY_LEN((coordinate_pair_t){}); i++)
  {
    total += (unsigned int)abs(a[i] - b[i]);
  }
  return total;
}

int main()
{
  struct vector_coord coordinates;
  coordinate_pair_t   max_coordinates = {};
  uint_fast16_t*      board;
  {
    size_t num_new_lines;
    FILE* in = open_and_count_newlines("dec06.txt", &num_new_lines);
    coordinates = vector_coord_new(num_new_lines + 1, nullptr);
    coordinate_pair_t coordinate_pair;
    while (fscanf(in, "%u, %u", &coordinate_pair[0], &coordinate_pair[1]) > 0)
    {
      for (int i = 0; i < ARRAY_LEN(max_coordinates); i++)
      {
        if (coordinate_pair[i] > max_coordinates[i])
        {
          max_coordinates[i] = coordinate_pair[i];
        }
      }
      vector_coord_push_back(&coordinates, (coordinate_pair_t*)coordinate_pair);
    }
    fclose(in);
    for (int i = 0; i < ARRAY_LEN(max_coordinates); i++)
    {
      max_coordinates[i]++;
    }
    board = malloc(max_coordinates[0] * max_coordinates[1] * sizeof(uint_fast16_t));
  }

  {
    unsigned int* closest_count_for_index = calloc(vector_coord_length(&coordinates), sizeof(unsigned int));
    bool*         any_index_is_infinite   = calloc(vector_coord_length(&coordinates), sizeof(bool));

    coordinate_pair_t iter_pair;
    for (iter_pair[1] = 0; iter_pair[1] < max_coordinates[1]; iter_pair[1]++)
    {
      bool y_is_infinite = (iter_pair[1] == 0 || iter_pair[1] == (max_coordinates[1] - 1));
      for (iter_pair[0] = 0; iter_pair[0] < max_coordinates[0]; iter_pair[0]++)
      {
        bool         is_infinite      = y_is_infinite || (iter_pair[0] == 0 || iter_pair[0] == (max_coordinates[0] - 1));
        int_fast32_t closest_index    = -1;
        unsigned int closest_distance = UINT_MAX;
        for (int i = 0; i < vector_coord_length(&coordinates); i++)
        {
          coordinate_pair_t const* coordinate_pair;
          coordinate_pair = vector_coord_get(&coordinates, i);
          unsigned int current_distance = get_distance(iter_pair, *coordinate_pair);
          if (current_distance < closest_distance)
          {
            closest_distance = current_distance;
            closest_index    = i;
          }
          else if (current_distance == closest_distance)
          {
            closest_index = -1;
          }
        }
        if (closest_index >= 0)
        {
          closest_count_for_index[closest_index]++;
          if (is_infinite)
          {
            any_index_is_infinite[closest_index] = true;
          }
        }
        board[iter_pair[1] * max_coordinates[0] + iter_pair[0]] = closest_index;
      }
    }

    unsigned int max_area = 0;
    for (int i = 0; i < vector_coord_length(&coordinates); i++)
    {
      if (!any_index_is_infinite[i] && max_area < closest_count_for_index[i])
      {
        max_area = closest_count_for_index[i];
      }
    }
    free(closest_count_for_index);
    free(any_index_is_infinite);
    printf("%u\n", max_area);
  }

  {
    coordinate_pair_t average_coordinates;
    {
      long average_coordinates_long[ARRAY_LEN((coordinate_pair_t){})] = {};
      VECTOR_FOR_EACH(coordinate_pair, &coordinates)
      {
        for (int i = 0; i < ARRAY_LEN(average_coordinates_long); i++)
        {
          average_coordinates_long[i] += (*coordinate_pair)[i];
        }
      }
      for (int i = 0; i < ARRAY_LEN(average_coordinates_long); i++)
      {
        average_coordinates[i] = average_coordinates_long[i] / vector_coord_length(&coordinates);
      }
    }

    unsigned int within_all_count    = 0;
    int          distance_adjustment = 0;
    bool         any_found_this_iter;
    do
    {
      any_found_this_iter = false;
      for (int y_adjustment = -distance_adjustment; y_adjustment <= distance_adjustment; y_adjustment++)
      {
        int remaining_adjustment = distance_adjustment - abs(y_adjustment);
        int x_adjustments[]      = { -remaining_adjustment, remaining_adjustment };
        for (int i = 0; i < ARRAY_LEN(x_adjustments) - (x_adjustments[0] == 0); i++)
        {
          coordinate_pair_t iter_pair = { average_coordinates[0] + x_adjustments[i],
                                          average_coordinates[1] + y_adjustment };
          unsigned int working_sum = 0;
          VECTOR_FOR_EACH(coordinate_pair, &coordinates)
          {
            working_sum += get_distance(iter_pair, *coordinate_pair);
            if (working_sum >= P2_DISTANCE)
            {
              break;
            }
          }
          if (working_sum < P2_DISTANCE)
          {
            within_all_count++;
            any_found_this_iter = true;
          }
        }
      }
      distance_adjustment++;
    } while (any_found_this_iter);
    printf("%u\n", within_all_count);
  }

  vector_coord_clean(&coordinates);
  free(board);
  return 0;
}
