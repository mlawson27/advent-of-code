#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

size_t resource_value_after_n_minutes(char const* grid, size_t num_rows, size_t num_cols, size_t n_minutes)
{
  struct state_node;
  struct state_node
  {
    struct state_node* next;
    size_t             min;
  };

  struct state_node* head_ptr      = malloc(sizeof(struct state_node) + num_rows * num_cols);
  struct state_node* new_state_ptr = head_ptr;
  head_ptr->min                    = 0;
  head_ptr->next                   = nullptr;
  memcpy((char*)head_ptr + sizeof(struct state_node), grid, num_rows * num_cols);
  for (size_t min = 1; min <= n_minutes; min++)
  {
    struct state_node* last_state_ptr = new_state_ptr;
    char const*        last_grid      = (char const*)last_state_ptr + sizeof(struct state_node);
    new_state_ptr                     = malloc(sizeof(struct state_node) + num_rows * num_cols);
    char*              new_grid       = (char*)new_state_ptr + sizeof(struct state_node);
    new_state_ptr->min                = min;
    new_state_ptr->next               = nullptr;
    for (size_t y = 0; y < num_rows; y++)
    {
      for (size_t x = 0; x < num_cols; x++)
      {
        size_t num_adj_trees       = 0;
        size_t num_adj_lumberyards = 0;
        for (int y_adj_i = -1; y_adj_i <= 1; y_adj_i++)
        {
          size_t y_adj = y + y_adj_i;
          if (y_adj >= num_rows)
          {
            continue;
          }
          for (int x_adj_i = -1; x_adj_i <= 1; x_adj_i++)
          {
            size_t x_adj = x + x_adj_i;
            if (x_adj >= num_cols || (y_adj_i == 0 && x_adj_i == 0))
            {
              continue;
            }
            num_adj_trees       += last_grid[y_adj * num_cols + x_adj] == '|';
            num_adj_lumberyards += last_grid[y_adj * num_cols + x_adj] == '#';
          }
        }
        switch (last_grid[y * num_cols + x])
        {
          case '.':
            new_grid[y * num_cols + x] = (num_adj_trees >= 3) ? '|' : '.';
            break;
          case '|':
            new_grid[y * num_cols + x] = (num_adj_lumberyards >= 3) ? '#' : '|';
            break;
          case '#':
            new_grid[y * num_cols + x] = (num_adj_lumberyards >= 1 && num_adj_trees >= 1) ? '#' : '.';
            break;
        }
      }
    }
    last_state_ptr->next = new_state_ptr;
    for (struct state_node* try_state_node_ptr = head_ptr; try_state_node_ptr->next != nullptr; try_state_node_ptr = try_state_node_ptr->next)
    {
      if (memcmp(new_grid, ((char const*)try_state_node_ptr) + sizeof(struct state_node), num_rows * num_cols) == 0)
      {
        size_t loop_len  = min - try_state_node_ptr->min;
        min             += (((n_minutes - try_state_node_ptr->min) / loop_len) - 1) * loop_len;
        for (; min < n_minutes; min++)
        {
          try_state_node_ptr = try_state_node_ptr->next;
        }
        new_state_ptr = try_state_node_ptr;
      }
    }
  }
  size_t num_trees       = 0;
  size_t num_lumberyards = 0;
  FOR_EACH_N(c_ptr, (char const*)new_state_ptr + sizeof(struct state_node), num_rows * num_cols)
  {
    num_trees       += (*c_ptr == '|');
    num_lumberyards += (*c_ptr == '#');
  }
  for (struct state_node* try_state_node_ptr = head_ptr; try_state_node_ptr != nullptr;)
  {
    struct state_node* next = try_state_node_ptr->next;
    free(try_state_node_ptr);
    try_state_node_ptr = next;
  }
  return num_trees * num_lumberyards;
}

int main()
{
  int         return_code = 0;
  char const* grid;
  size_t      num_rows;
  size_t      num_cols;
  {
    FILE*  in            = fopen("dec18.txt", "r");
    size_t this_line_len = 0;
    num_rows             = 1;
    num_cols             = 0;
    for (int this_char = fgetc(in); this_char != EOF; this_char = fgetc(in))
    {
      if (this_char == '\n')
      {
        num_rows++;
        if (num_cols == 0)
        {
          num_cols = this_line_len;
        }
        else
        {
          assert(num_cols == this_line_len);
        }
        this_line_len = 0;
      }
      else
      {
        this_line_len++;
      }
    }
    fseek(in, 0, SEEK_SET);
    char* working_grid = malloc(num_rows * num_cols);
    grid               = working_grid;
    char* line         = malloc(num_cols + 1);
    int   col_pos;
    while (fscanf(in, "%s%n", line, &col_pos) == 1)
    {
      assert(col_pos == num_cols);
      memcpy(working_grid, line, col_pos);
      working_grid += col_pos;
      fscanf(in, " ");
    }
    free(line);
    fclose(in);
  }

  {
    printf("%zu\n", resource_value_after_n_minutes(grid, num_rows, num_cols, 10));
  }

  {
    printf("%zu\n", resource_value_after_n_minutes(grid, num_rows, num_cols, 1000000000));
  }

  free((void*)grid);
  return return_code;
}
