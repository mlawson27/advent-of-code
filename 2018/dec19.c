#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "vector.h"

enum instruction_name
{
  ADDR,
  ADDI,
  MULR,
  MULI,
  BANR,
  BANI,
  BORR,
  BORI,
  SETR,
  SETI,
  GTIR,
  GTRI,
  GTRR,
  EQIR,
  EQRI,
  EQRR,
  _INSTRUCTION_COUNT
};

typedef size_t register_data[6];

typedef size_t inputs[3];

struct instruction_data
{
  enum instruction_name instruction;
  inputs                inputs;
};

MAKE_VECTOR_TYPE(id, struct instruction_data)

static inline void execute_instruction(enum instruction_name name, register_data registers, const inputs inputs)
{
  switch (name)
  {
    case ADDR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] + registers[inputs['B' - 'A']];
      break;
    case ADDI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] + inputs['B' - 'A'];
      break;
    case MULR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] * registers[inputs['B' - 'A']];
      break;
    case MULI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] * inputs['B' - 'A'];
      break;
    case BANR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] & registers[inputs['B' - 'A']];
      break;
    case BANI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] & inputs['B' - 'A'];
      break;
    case BORR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] | registers[inputs['B' - 'A']];
      break;
    case BORI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] | inputs['B' - 'A'];
      break;
    case SETR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']];
      break;
    case SETI:
      registers[inputs['C' - 'A']] = inputs['A' - 'A'];
      break;
    case GTIR:
      registers[inputs['C' - 'A']] = inputs['A' - 'A'] > registers[inputs['B' - 'A']];
      break;
    case GTRI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] > inputs['B' - 'A'];
      break;
    case GTRR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] > registers[inputs['B' - 'A']];
      break;
    case EQIR:
      registers[inputs['C' - 'A']] = inputs['A' - 'A'] == registers[inputs['B' - 'A']];
      break;
    case EQRI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] == inputs['B' - 'A'];
      break;
    case EQRR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] == registers[inputs['B' - 'A']];
      break;
    case _INSTRUCTION_COUNT:
      __builtin_unreachable();
  }
}

int main()
{
  int              return_code  = 0;
  size_t           ip_bound_reg;
  struct vector_id test_program;
  {
    FILE*  in            = fopen("dec19.txt", "r");
    size_t num_lines     = 0;
    size_t max_line_len  = 0;
    size_t this_line_len = 0;
    for (int this_char = fgetc(in); this_char != EOF; this_char = fgetc(in))
    {
      if (this_char == '\n')
      {
        max_line_len  = (this_line_len > max_line_len) ? this_line_len : max_line_len;
        this_line_len = 0;
        num_lines++;
      }
      else
      {
        this_line_len++;
      }
    }
    num_lines++;
    fseek(in, 0, SEEK_SET);
    test_program = vector_id_new(num_lines - 1, nullptr);
    fscanf(in, "#ip %zu", &ip_bound_reg);
    struct instruction_data working_data;
    char   working_instruction_name[4];
    while (fscanf(in, " %4c %zu %zu %zu", working_instruction_name, &working_data.inputs[0], &working_data.inputs[1], &working_data.inputs[2]) == 4)
    {
      switch (working_instruction_name[0])
      {
        case 'a':
          working_data.instruction = working_instruction_name[3] == 'r' ? ADDR : ADDI;
          break;
        case 'b':
          switch (working_instruction_name[1])
          {
            case 'a':
              working_data.instruction = working_instruction_name[3] == 'r' ? BANR : BANI;
              break;
            case 'o':
              working_data.instruction = working_instruction_name[3] == 'r' ? BORR : BORI;
              break;
          }
          break;
        case 'e':
          if (working_instruction_name[2] == 'i')
          {
            working_data.instruction = EQIR;
          }
          else if (working_instruction_name[3] == 'i')
          {
            working_data.instruction = EQRI;
          }
          else
          {
            working_data.instruction = EQRR;
          }
          break;
        case 'g':
          if (working_instruction_name[2] == 'i')
          {
            working_data.instruction = GTIR;
          }
          else if (working_instruction_name[3] == 'i')
          {
            working_data.instruction = GTRI;
          }
          else
          {
            working_data.instruction = GTRR;
          }
          break;
        case 'm':
          working_data.instruction = working_instruction_name[3] == 'r' ? MULR : MULI;
          break;
        case 's':
          working_data.instruction = working_instruction_name[3] == 'r' ? SETR : SETI;
          break;
      }
      vector_id_push_back(&test_program, &working_data);
    }
    fclose(in);
  }

  {
    register_data working    = {};
    size_t        ip         = 0;
    size_t*       ip_reg_ptr = &working[ip_bound_reg];
    while (ip < vector_id_length(&test_program))
    {
      *ip_reg_ptr = ip;
      struct instruction_data const* instruction_data_ptr = vector_id_get(&test_program, ip);
      execute_instruction(instruction_data_ptr->instruction, working, instruction_data_ptr->inputs);
      ip = *ip_reg_ptr + 1;
    }
    printf("%zu\n", working[0]);
  }

  {
    register_data working    = {1, };
    size_t        ip         = 0;
    size_t*       ip_reg_ptr = &working[ip_bound_reg];
    while (ip < vector_id_length(&test_program))
    {
      *ip_reg_ptr = ip;
      struct instruction_data const* instruction_data_ptr = vector_id_get(&test_program, ip);
      execute_instruction(instruction_data_ptr->instruction, working, instruction_data_ptr->inputs);
      ip = *ip_reg_ptr + 1;
      if (working[0] == 0)
      {
        break;
      }
    }
    size_t max_reg = 0;
    FOR_EACH(reg_ptr, working)
    {
      max_reg = *reg_ptr > max_reg ? *reg_ptr : max_reg;
    }
    double sqrt_d          = sqrt((double)max_reg);
    size_t sqrt_u          = (size_t)sqrt_d;
    size_t sum_of_divisors = max_reg + 1 + ((sqrt_d == (double)sqrt_u) * sqrt_u);
    for (size_t possible_divisor = 2; possible_divisor < sqrt_u; possible_divisor++)
    {
      if (max_reg % possible_divisor == 0)
      {
        sum_of_divisors += possible_divisor + (max_reg / possible_divisor);
      }
    }
    printf("%zu\n", sum_of_divisors);
  }
  vector_id_clean(&test_program);
  return return_code;
}
