#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

#define INPUT 513401

static constexpr size_t NUM_RECIPES   = INPUT;
static constexpr size_t RECIPES_AFTER =    10;

// Increase this if the input doesn't complete within this time,
static constexpr size_t RECIPE_BUFFER_SIZE = 1 << 25;

int main()
{
  int            return_code     = 0;
  unsigned char* recipes         = malloc(RECIPE_BUFFER_SIZE);
  int            elf_positions[] = {0, 1};
  size_t         recipe_count    = 2;
                 recipes[0]      = 3;
                 recipes[1]      = 7;
  unsigned char  to_find[sizeof(STRINGIFY(INPUT)) - 1];
  for (int i = 0; i < sizeof(to_find); i++)
  {
    to_find[i] = STRINGIFY(INPUT)[i] - '0';
  }

  {
    bool   found_input = false;
    size_t input_pos;
    while ((recipe_count < NUM_RECIPES + RECIPES_AFTER) || (!found_input && recipe_count < RECIPE_BUFFER_SIZE))
    {
      size_t        new_count = 0;
      uint_fast16_t sum       = (uint_fast16_t)recipes[elf_positions[0]] + recipes[elf_positions[1]];
      if (sum >= 10)
      {
        recipes[recipe_count] = sum / 10;
        new_count++;
        sum %= 10;
      }
      recipes[recipe_count + new_count] = sum;
      new_count++;
      for (int i = 1; i < new_count; i++)
      {
        if (((recipe_count + i) > sizeof(to_find)) && (memcmp(recipes + recipe_count + i - sizeof(to_find), to_find, sizeof(to_find)) == 0))
        {
          found_input = true;
          input_pos   = recipe_count + i - sizeof(to_find);
        }
      }
      recipe_count += new_count;
      for (int i = 0; i < ARRAY_LEN(elf_positions); i++)
      {
        elf_positions[i] = (elf_positions[i] + recipes[elf_positions[i]] + 1) % recipe_count;
      }
    }
    char recipes_to_print[RECIPES_AFTER + 1];
    for (int i = 0; i < RECIPES_AFTER; i++)
    {
      recipes_to_print[i] = (char)(recipes[NUM_RECIPES + i] + '0');
    }
    recipes_to_print[RECIPES_AFTER] = 0;
    printf("%s\n", recipes_to_print);
    if (found_input)
    {
      printf("%zu\n", input_pos);
    }
    else
    {
      printf("Input can't be found with current buffer size.\n");
      return_code = 1;
    }
  }

  free(recipes);
  return return_code;
}
