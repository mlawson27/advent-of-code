#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "vector.h"

enum direction
{
  Up,
  Right,
  Down,
  Left,
  _DirectionCount
};

struct cart_data
{
  int            current_pos[2];
  enum direction direction;
  enum direction next_intersection_action;
  bool           collided;
};

MAKE_VECTOR_TYPE(cart, struct cart_data)

static int cart_compare(struct cart_data const* c1_ptr, struct cart_data const* c2_ptr)
{
  int collided_cmp = ((c1_ptr->collided > c2_ptr->collided) -
                      (c2_ptr->collided > c1_ptr->collided));
  if (collided_cmp != 0)
  {
    return collided_cmp;
  }
  int y_cmp = ((c1_ptr->current_pos[1] > c2_ptr->current_pos[1]) -
               (c2_ptr->current_pos[1] > c1_ptr->current_pos[1]));
  if (y_cmp != 0)
  {
    return y_cmp;
  }
  return ((c1_ptr->current_pos[0] > c2_ptr->current_pos[0]) -
          (c2_ptr->current_pos[0] > c1_ptr->current_pos[0]));
}

int main()
{
  char*              board;
  size_t             board_rows;
  size_t             board_cols;
  struct vector_cart cart_data;
  size_t             cart_count;
  {
    FILE* in = open_and_count_newlines("dec13.txt", &board_rows);
    board_rows++;
    fseek(in, 0, SEEK_END);
    size_t board_size = (size_t)ftell(in) + 1;
    fseek(in, 0, SEEK_SET);
    board      = calloc(1, board_size - board_rows + 1);
    board_cols = (board_size / board_rows) - 1;
    char* in_ptr     = board;
    int   line_len;
    cart_count = 0;
    while (fscanf(in, "%[^\n]%n%*[\n]", in_ptr, &line_len) == 1)
    {
      assert(line_len == board_cols);
      FOR_EACH_N(c_ptr, in_ptr, line_len)
      {
        switch (*c_ptr)
        {
          case '^':
          case '>':
          case 'v':
          case '<':
            cart_count++;
            break;
        }
      }
      in_ptr += line_len;
    }
    fclose(in);
    cart_data = vector_cart_new(cart_count, nullptr);
    for (int y = 0; y < board_rows; y++)
    {
      for (int x = 0; x < board_cols; x++)
      {
        enum direction d = _DirectionCount;
        switch (board[y * board_cols + x])
        {
          case '^':
            d = Up;
            break;
          case '>':
            d = Right;
            break;
          case 'v':
            d = Down;
            break;
          case '<':
            d = Left;
            break;
        }
        if (d < _DirectionCount)
        {
          vector_cart_push_back(&cart_data, &(struct cart_data) {
            .current_pos              = { x, y },
            .direction                = d,
            .next_intersection_action = Left,
            .collided                 = false,
          });
        }
      }
    }
  }

  {
    size_t collision_count = 0;
    while (cart_count - collision_count > 1)
    {
      size_t new_collision_count = 0;
      VECTOR_FOR_EACH_N_MUT(cart_ptr, &cart_data, cart_count - collision_count)
      {
        if (cart_ptr->collided)
        {
          continue;
        }
        cart_ptr->current_pos[0] += (cart_ptr->direction == Right) - (cart_ptr->direction == Left);
        cart_ptr->current_pos[1] += (cart_ptr->direction == Down)  - (cart_ptr->direction == Up);
        VECTOR_FOR_EACH_N_MUT(other_cart_ptr, &cart_data, cart_count - collision_count)
        {
          if (cart_ptr == other_cart_ptr || other_cart_ptr->collided)
          {
            continue;
          }
          cart_ptr->collided = (memcmp(cart_ptr->current_pos, other_cart_ptr->current_pos, sizeof(other_cart_ptr->current_pos)) == 0);
          if (cart_ptr->collided)
          {
            if (collision_count == 0 && new_collision_count == 0)
            {
              printf("%i,%i\n", cart_ptr->current_pos[0], cart_ptr->current_pos[1]);
            }
            other_cart_ptr->collided = true;
            new_collision_count += 2;
            break;
          }
        }
        switch (board[cart_ptr->current_pos[1] * board_cols + cart_ptr->current_pos[0]])
        {
          case '|':
            assert(cart_ptr->direction == Up || cart_ptr->direction == Down);
            break;
          case '-':
            assert(cart_ptr->direction == Right || cart_ptr->direction == Left);
            break;
          case '\\':
            switch (cart_ptr->direction)
            {
              case Right:
                cart_ptr->direction = Down;
                break;
              case Up:
                cart_ptr->direction = Left;
                break;
              case Left:
                cart_ptr->direction = Up;
                break;
              case Down:
                cart_ptr->direction = Right;
                break;
              default:
                __builtin_unreachable();
            }
            break;
          case '/':
            switch (cart_ptr->direction)
            {
              case Right:
                cart_ptr->direction = Up;
                break;
              case Up:
                cart_ptr->direction = Right;
                break;
              case Left:
                cart_ptr->direction = Down;
                break;
              case Down:
                cart_ptr->direction = Left;
                break;
              default:
                __builtin_unreachable();
            }
            break;
          case '+':
            switch (cart_ptr->next_intersection_action)
            {
              case Right:
                cart_ptr->direction                = (cart_ptr->direction + 1) % _DirectionCount;
                cart_ptr->next_intersection_action = Left;
                break;
              case Up:
                cart_ptr->next_intersection_action = Right;
                break;
              case Left:
                cart_ptr->direction                = (cart_ptr->direction + _DirectionCount - 1) % _DirectionCount;
                cart_ptr->next_intersection_action = Up;
                break;
              default:
                __builtin_unreachable();
            }
            break;
        }
      }
      vector_cart_sort_N(&cart_data, cart_count - collision_count, cart_compare);
      collision_count += new_collision_count;
    }
    printf("%i,%i\n", vector_cart_get(&cart_data, 0)->current_pos[0], vector_cart_get(&cart_data, 0)->current_pos[1]);
  }

  free(board);
  vector_cart_clean(&cart_data);
  return 0;
}
