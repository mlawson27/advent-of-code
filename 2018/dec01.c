#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "hashset.h"

static_assert(sizeof(int_fast32_t) == sizeof(long long));

MAKE_HASHSET_TYPE(freq, int_fast32_t)

int main()
{
  bool                printed_first_ending_freq          = false;
  bool                found_first_freq_encountered_twice = false;
  int_fast32_t        current_frequency                  = 0;
  struct hashset_freq seen_frequencies_set               = hashset_freq_new(nullptr);
  hashset_freq_insert(&seen_frequencies_set, &current_frequency);
  {
    FILE* in = fopen("dec01.txt", "r");

    int          count;
    int_fast32_t value;
    while (!printed_first_ending_freq || !found_first_freq_encountered_twice)
    {
      while ((count = fscanf(in, "%" PRIdFAST32 "\n", &value)) > 0)
      {
        current_frequency += value;
        if (!found_first_freq_encountered_twice)
        {
          if (hashset_freq_contains(&seen_frequencies_set, &current_frequency))
          {
            printf("%" PRIdFAST32 "\n", current_frequency);
            found_first_freq_encountered_twice = true;
          }
          else
          {
            hashset_freq_insert(&seen_frequencies_set, &current_frequency);
          }
        }
      }
      if (!printed_first_ending_freq)
      {
        printf("%" PRIdFAST32 "\n", current_frequency);
        printed_first_ending_freq = true;
      }
      fseek(in, 0, SEEK_SET);
    }
    fclose(in);
  }
  hashset_freq_clean(&seen_frequencies_set);
  return 0;
}