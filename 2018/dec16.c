#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "vector.h"

enum instruction_name
{
  ADDR,
  ADDI,
  MULR,
  MULI,
  BANR,
  BANI,
  BORR,
  BORI,
  SETR,
  SETI,
  GTIR,
  GTRI,
  GTRR,
  EQIR,
  EQRI,
  EQRR,
  _INSTRUCTION_COUNT
};

typedef size_t register_data[4];

typedef size_t inputs[3];

struct instruction_data
{
  size_t instruction_num;
  inputs inputs;
};

struct processor_state
{
  register_data           before;
  register_data           after;
  struct instruction_data data;
};

MAKE_VECTOR_TYPE(id, struct instruction_data)
MAKE_VECTOR_TYPE(ps, struct processor_state)

static bool parse_instruction_data(FILE* in, char* line, struct instruction_data* instruction_data_ptr)
{
  return fscanf(in, "%zu%zu%zu%zu", &instruction_data_ptr->instruction_num,
                                    &instruction_data_ptr->inputs[0],
                                    &instruction_data_ptr->inputs[1],
                                    &instruction_data_ptr->inputs[2]) == 4;
}

static bool parse_processor_state(FILE* in, char* line, struct processor_state* state_ptr)
{
  int ret;
  ret = fscanf(in, " Before: %*c%zu%*c %zu%*c %zu%*c %zu%*c", &state_ptr->before[0],
                                                              &state_ptr->before[1],
                                                              &state_ptr->before[2],
                                                              &state_ptr->before[3]);
  if (ret < 4)
  {
    return false;
  }
  if (!parse_instruction_data(in, line, &state_ptr->data))
  {
    return false;
  }
  ret = fscanf(in, " After: %*c%zu%*c %zu%*c %zu%*c %zu%*c", &state_ptr->after[0],
                                                             &state_ptr->after[1],
                                                             &state_ptr->after[2],
                                                             &state_ptr->after[3]);
  if (ret < 4)
  {
    return false;
  }
  return true;
}

static inline void execute_instruction(enum instruction_name name, register_data registers, const inputs inputs)
{
  switch (name)
  {
    case ADDR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] + registers[inputs['B' - 'A']];
      break;
    case ADDI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] + inputs['B' - 'A'];
      break;
    case MULR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] * registers[inputs['B' - 'A']];
      break;
    case MULI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] * inputs['B' - 'A'];
      break;
    case BANR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] & registers[inputs['B' - 'A']];
      break;
    case BANI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] & inputs['B' - 'A'];
      break;
    case BORR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] | registers[inputs['B' - 'A']];
      break;
    case BORI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] | inputs['B' - 'A'];
      break;
    case SETR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']];
      break;
    case SETI:
      registers[inputs['C' - 'A']] = inputs['A' - 'A'];
      break;
    case GTIR:
      registers[inputs['C' - 'A']] = inputs['A' - 'A'] > registers[inputs['B' - 'A']];
      break;
    case GTRI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] > inputs['B' - 'A'];
      break;
    case GTRR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] > registers[inputs['B' - 'A']];
      break;
    case EQIR:
      registers[inputs['C' - 'A']] = inputs['A' - 'A'] == registers[inputs['B' - 'A']];
      break;
    case EQRI:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] == inputs['B' - 'A'];
      break;
    case EQRR:
      registers[inputs['C' - 'A']] = registers[inputs['A' - 'A']] == registers[inputs['B' - 'A']];
      break;
    case _INSTRUCTION_COUNT:
      __builtin_unreachable();
  }
}

int main()
{
  int              return_code = 0;
  struct vector_ps processor_state;
  struct vector_id test_program;
  {
    FILE*  in               = fopen("dec16.txt", "r");
    int    consec_new_lines = 0;
    size_t num_lines        = 1;
    size_t this_line_len    = 0;
    size_t max_line_len     = 0;
    for (;;)
    {
      int this_char = fgetc(in);
      if ((this_char == EOF) || ((consec_new_lines = (consec_new_lines + 1) * (this_char == '\n')) == 3))
      {
        break;
      }
      if (this_char == '\n')
      {
        max_line_len  = (this_line_len > max_line_len) ? this_line_len : max_line_len;
        this_line_len = 0;
      }
      else
      {
        this_line_len++;
      }
      num_lines += (this_char == '\n');
    }
    fseek(in, 0, SEEK_SET);
    processor_state = vector_ps_new((num_lines + 1) / 4, nullptr);
    char*                  line = malloc(max_line_len + 1);
    struct processor_state working_state;
    while (parse_processor_state(in, line, &working_state))
    {
      vector_ps_push_back(&processor_state, &working_state);
    }
    long prev_pos = ftell(in);
    fscanf(in, "%*c");
    for (num_lines = 0;;)
    {
      int this_char = fgetc(in);
      if (this_char == EOF)
      {
        break;
      }
      num_lines += (this_char == '\n');
    }
    fseek(in, prev_pos, SEEK_SET);
    test_program = vector_id_new(num_lines + 1, nullptr);
    struct instruction_data working_data;
    while (parse_instruction_data(in, line, &working_data))
    {
      vector_id_push_back(&test_program, &working_data);
    }
    free(line);
    fclose(in);
  }

  {
    register_data working;
    size_t        num_states_behaving_like_three = 0;
    VECTOR_FOR_EACH(processor_state_ptr, &processor_state)
    {
      size_t this_matching_instruction_count = 0;
      for (enum instruction_name name = 0; name < _INSTRUCTION_COUNT; name++)
      {
        memcpy(working, processor_state_ptr->before, sizeof(register_data));
        execute_instruction(name, working, processor_state_ptr->data.inputs);
        this_matching_instruction_count += (memcmp(working, processor_state_ptr->after, sizeof(register_data)) == 0);
      }
      num_states_behaving_like_three += (this_matching_instruction_count >= 3);
    }
    printf("%zu\n", num_states_behaving_like_three);
  }

  {
    enum instruction_name instructions_by_number[_INSTRUCTION_COUNT];
    size_t                found_instruction_by_num_mask  = 0;
    size_t                found_instruction_by_name_mask = 0;
    register_data         working;
    while (found_instruction_by_num_mask != (((size_t)1 << _INSTRUCTION_COUNT) - 1))
    {
      VECTOR_FOR_EACH(processor_state_ptr, &processor_state)
      {
        if ((found_instruction_by_num_mask & ((size_t)1 << processor_state_ptr->data.instruction_num)) == 0)
        {
          uint32_t this_matching_instruction_mask = 0;
          for (enum instruction_name name = 0; name < _INSTRUCTION_COUNT; name++)
          {
            if (found_instruction_by_name_mask & (UINT32_C(1) << name))
            {
              continue;
            }
            memcpy(working, processor_state_ptr->before, sizeof(register_data));
            execute_instruction(name, working, processor_state_ptr->data.inputs);
            this_matching_instruction_mask |= ((uint32_t)(memcmp(working, processor_state_ptr->after, sizeof(register_data)) == 0) << name);
          }
          if (__builtin_popcount(this_matching_instruction_mask) == 1)
          {
            enum instruction_name name = __builtin_ctz(this_matching_instruction_mask);
            instructions_by_number[processor_state_ptr->data.instruction_num] = name;
            found_instruction_by_num_mask  |= (UINT32_C(1) << processor_state_ptr->data.instruction_num);
            found_instruction_by_name_mask |= this_matching_instruction_mask;
          }
        }
      }
    }
    register_data working_regs = {};
    VECTOR_FOR_EACH(instruction_data_ptr, &test_program)
    {
      execute_instruction(instructions_by_number[instruction_data_ptr->instruction_num], working_regs, instruction_data_ptr->inputs);
    }
    printf("%zu\n", working_regs[0]);
  }
  vector_id_clean(&test_program);
  vector_ps_clean(&processor_state);
  return return_code;
}
