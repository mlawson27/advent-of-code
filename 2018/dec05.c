#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

long get_length_after_substitutions(char* data, long file_len)
{
  long num_substitutions = 0;
  for (long i = 0; i < file_len - 2;)
  {
    long j = i + 1;
    while (j < file_len - 1 && ((data[j] & CHAR_MIN) == CHAR_MIN))
    {
      j++;
    }
    long i_incr;
    if ((toupper(data[i]) == toupper(data[j])) && (data[i] != data[j]))
    {
      num_substitutions++;
      data[i] |= CHAR_MIN;
      data[j] |= CHAR_MIN;
      i_incr = -1;
    }
    else
    {
      i_incr = 1;
    }
    long new_i = i;
    do
    {
      new_i += i_incr;
      if (new_i < 0)
      {
        i_incr = 1;
        new_i  = i + 1;
      }
    } while (new_i < file_len - 2 && ((data[new_i] & CHAR_MIN) == CHAR_MIN));
    i = new_i;
  }
  FOR_EACH_N(c_ptr, data, file_len - 1)
  {
    *c_ptr &= CHAR_MAX;
  }
  return file_len - (num_substitutions * 2) - 1;
}

int main()
{
  char* data;
  long  file_len;
  {
    FILE* in = fopen("dec05.txt", "r");
    fseek(in, 0, SEEK_END);
    file_len = ftell(in) + 1;
    fseek(in, 0, SEEK_SET);
    data = malloc(file_len);
    fread(data, 1, file_len, in);
    fclose(in);
    data[file_len - 1] = 0;
  }

  long min_length = get_length_after_substitutions(data, file_len);
  printf("%ld\n", min_length);
  {
    min_length = LONG_MAX;
    for (char c = 'a'; c <= 'z'; c++)
    {
      long num_local_replacements = 0;
      FOR_EACH_N(c_ptr, data, file_len - 1)
      {
        if (tolower(*c_ptr) == c)
        {
          num_local_replacements += 1;
          *c_ptr |= CHAR_MIN;
        }
      }
      long length_after_sub = get_length_after_substitutions(data, file_len) - num_local_replacements;
      if (length_after_sub < min_length)
      {
        min_length = length_after_sub;
      }
    }
  }
  printf("%ld\n", min_length);

  free(data);
  return 0;
}
