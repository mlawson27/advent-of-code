#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "vector.h"

typedef int64_t coordinates_t[4];

MAKE_VECTOR_TYPE(coord, coordinates_t)

static inline size_t manhattan_distance(coordinates_t const* a, coordinates_t const* b)
{
  size_t ret = 0;
  for (size_t dim = 0; dim < ARRAY_LEN((coordinates_t){}); dim++)
  {
    ret += (size_t)((*a)[dim] > (*b)[dim] ? (*a)[dim] - (*b)[dim] : (*b)[dim] - (*a)[dim]);
  }
  return ret;
}

int main()
{
  int                 return_code  = 0;
  struct vector_coord coordinate_set;
  {
    size_t num_new_lines;
    FILE*  in            = open_and_count_newlines("dec25.txt", &num_new_lines);
    coordinate_set       = vector_coord_new(num_new_lines + 1, nullptr);
    {
      coordinates_t working_coordinates;
      while (fscanf(in, "%" PRId64 ", %" PRId64 ", %" PRId64", %" PRId64, &working_coordinates[0],
                                                                          &working_coordinates[1],
                                                                          &working_coordinates[2],
                                                                          &working_coordinates[3]) == 4)
      {
        vector_coord_push_back(&coordinate_set, (coordinates_t*)working_coordinates);
      }
    }
    fclose(in);
  }

  {
    struct constellation_node
    {
      struct constellation_node* next;
      coordinates_t const*       coordinates_ptr;
    };

    struct constellation_node** constellations     = malloc(sizeof(struct constellation_node*) * vector_coord_length(&coordinate_set));
    size_t                      num_constellations = 0;
    {
      size_t* matching_constellation_is = malloc(sizeof(size_t) * vector_coord_length(&coordinate_set));
      size_t  num_matching_constellations;

      VECTOR_FOR_EACH(coordinates_ptr, &coordinate_set)
      {
        num_matching_constellations = 0;
        for (size_t i_p1 = num_constellations, i = i_p1 - 1; i_p1 > 0; i_p1--, i = i_p1 - 1)
        {
          struct constellation_node* working_node;
          for (working_node = constellations[i]; working_node != nullptr && manhattan_distance(coordinates_ptr, working_node->coordinates_ptr) > 3; working_node = working_node->next);
          if (working_node != nullptr)
          {
            matching_constellation_is[num_matching_constellations] = i;
            num_matching_constellations++;
          }
        }

        constellations[num_constellations]                  = malloc(sizeof(struct constellation_node));
        constellations[num_constellations]->next            = nullptr;
        constellations[num_constellations]->coordinates_ptr = coordinates_ptr;
        num_constellations++;

        if (num_matching_constellations > 0)
        {
          struct constellation_node** node_to_merge = &constellations[num_constellations - 1];
          FOR_EACH_N(i_ptr, matching_constellation_is, num_matching_constellations)
          {
            struct constellation_node** place_to_put;
            for (place_to_put = &constellations[*i_ptr]; *place_to_put != nullptr; place_to_put = &(*place_to_put)->next);
            *place_to_put = *node_to_merge;
            memmove(node_to_merge, node_to_merge + 1, (num_constellations - (node_to_merge - constellations) - 1) * sizeof(struct constellation_node*));
            num_constellations--;
            node_to_merge = &constellations[*i_ptr];
          }
        }
      }
      free(matching_constellation_is);
    }

    FOR_EACH_N(constellation_ptr, constellations, num_constellations)
    {
      while (*constellation_ptr != nullptr)
      {
        struct constellation_node* next = (*constellation_ptr)->next;
        free(*constellation_ptr);
        *constellation_ptr = next;
      }
    }
    free(constellations);
    printf("%zu\n", num_constellations);
  }

  vector_coord_clean(&coordinate_set);
  return return_code;
}
