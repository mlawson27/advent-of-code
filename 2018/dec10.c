#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "vector.h"

typedef int vec2d[2];

MAKE_VECTOR_TYPE(2d, vec2d)

int main()
{
  struct vector_2d positions;
  struct vector_2d velocities;
  size_t num_points;
  {
    FILE* in = open_and_count_newlines("dec10.txt", &num_points);
    num_points++;
    positions  = vector_2d_new(num_points, nullptr);
    velocities = vector_2d_new(num_points, nullptr);
    vec2d pos, velocity;
    while (fscanf(in, " position=< %i, %i> velocity=< %i, %i>", &pos[0], &pos[1], &velocity[0], &velocity[1]) == 4)
    {
      vector_2d_push_back(&positions, (vec2d*)pos);
      vector_2d_push_back(&velocities, (vec2d*)velocity);
    }
    assert(vector_2d_length(&positions)  == num_points);
    assert(vector_2d_length(&velocities) == num_points);
    fclose(in);
  }

  {
    int   last_delta = INT_MAX;
    int   iter;
    vec2d mins;
    vec2d maxs;
    int   incr_amount = 1;
    for (iter = 0; ; iter += incr_amount)
    {
      for (size_t d = 0; d < ARRAY_LEN((vec2d){}); d++)
      {
        mins[d] = INT_MAX;
        maxs[d] = INT_MIN;
      }
      for (size_t i = 0; i < num_points; i++)
      {
        vec2d*       pos_ptr      = vector_2d_get_mut(&positions,  i);
        vec2d const* velocity_ptr = vector_2d_get    (&velocities, i);
        for (size_t d = 0; d < ARRAY_LEN((vec2d){}); d++)
        {
          (*pos_ptr)[d] += (*velocity_ptr)[d] * incr_amount;
          if ((*pos_ptr)[d] < mins[d])
          {
            mins[d] = (*pos_ptr)[d];
          }
          if ((*pos_ptr)[d] > maxs[d])
          {
            maxs[d] = (*pos_ptr)[d];
          }
        }
      }
      int delta = 0;
      for (size_t d = 0; d < ARRAY_LEN((vec2d){}); d++)
      {
        delta += maxs[d] - mins[d];
      }
      if (incr_amount < 0)
      {
        iter++;
        break;
      }
      if (delta > last_delta)
      {
        incr_amount = -1;
      }
      last_delta = delta;
    }

    size_t chars_per_row = (maxs[0] - mins[0] + 2);
    size_t text_len      = chars_per_row * (maxs[1] - mins[1] + 1) + 1;
    char*  text          = malloc(text_len * sizeof(char));
    memset(text, '.', text_len - 1);
    text[text_len - 1] = 0;
    for (size_t i = chars_per_row; i < text_len; i += chars_per_row)
    {
      text[i - 1] = '\n';
    }
    VECTOR_FOR_EACH(pos_ptr, &positions)
    {
      text[((*pos_ptr)[1] - mins[1]) * chars_per_row + ((*pos_ptr)[0] - mins[0])] = '#';
    }
    printf("%s", text);
    free(text);
    printf("%i\n", iter);
  }

  vector_2d_clean(&positions);
  vector_2d_clean(&velocities);
  return 0;
}
