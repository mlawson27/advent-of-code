#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

#include "vector.h"
#include "util.h"

struct claim
{
  unsigned int id;
  unsigned int left;
  unsigned int top;
  unsigned int width;
  unsigned int height;
  bool         any_overlap;
};

MAKE_VECTOR_TYPE(claim, struct claim)

int main()
{
  struct vector_claim claim_vec;
  uintptr_t*          board;
  unsigned int        max_left_excl = 0;
  unsigned int        max_top_excl  = 0;
  {
    char* data;
    FILE* in = fopen("dec03.txt", "r");
    fseek(in, 0, SEEK_END);
    long file_size = ftell(in);
    fseek(in, 0, SEEK_SET);

    data = malloc(file_size + 1);
    data[file_size] = 0;
    fread(data, file_size, 1, in);
    fclose(in);

    size_t num_claims = 1;
    FOR_EACH_N(c, (const char*)data, file_size)
    {
      num_claims += (*c == '\n');
    }

    claim_vec = vector_claim_new(num_claims, nullptr);
    int          result;
    int          num_chars_consumed;
    struct claim working_claim;
    char const*  working_data  = data;
    working_claim.any_overlap = false;
    while ((result = sscanf(working_data, " #%u @ %u,%u: %ux%u%n", &working_claim.id,
                                                                   &working_claim.left,
                                                                   &working_claim.top,
                                                                   &working_claim.width,
                                                                   &working_claim.height,
                                                                   &num_chars_consumed)) > 0)
    {
      unsigned int furthest_left = working_claim.left + working_claim.width;
      unsigned int furthest_top  = working_claim.top  + working_claim.height;
      max_left_excl = (furthest_left > max_left_excl) ? furthest_left : max_left_excl;
      max_top_excl  = (furthest_top  > max_top_excl)  ? furthest_top  : max_top_excl;
      vector_claim_push_back(&claim_vec, &working_claim);
      working_data += num_chars_consumed;
    }
    free(data);

    board = calloc(max_left_excl * max_top_excl, sizeof(*board));
  }

  {
    unsigned int multiple_claimed_spaces = 0;
    VECTOR_FOR_EACH_MUT(claim_ptr, &claim_vec)
    {
      for (unsigned int y = claim_ptr->top; y < claim_ptr->top + claim_ptr->height; y++)
      {
        for (unsigned int x = claim_ptr->left; x < claim_ptr->left + claim_ptr->width; x++)
        {
          size_t index = y * max_left_excl + x;
          if (board[index] == 0)
          {
            board[index] = (uintptr_t)claim_ptr;
          }
          else
          {
            claim_ptr->any_overlap = true;
            if (board[index] != UINTPTR_MAX)
            {
              multiple_claimed_spaces++;
              ((struct claim*)board[index])->any_overlap = true;
              board[index]                               = UINTPTR_MAX;
            }
          }
        }
      }
    }
    printf("%u\n", multiple_claimed_spaces);
  }

  VECTOR_FOR_EACH(claim_ptr, &claim_vec)
  {
    if (!claim_ptr->any_overlap)
    {
      printf("%u\n", claim_ptr->id);
      break;
    }
  }

  free(board);
  vector_claim_clean(&claim_vec);
  return 0;
}