#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashmap.h"
#include "hashset.h"
#include "priority_queue.h"
#include "util.h"
#include "vector.h"

typedef uint32_t position_pair_t[2];

struct sorted_item
{
  position_pair_t position;
  bool            is_goblin;
};

struct state
{
  position_pair_t current_position;
  position_pair_t starting_position;
  size_t          num_moves;
};

MAKE_VECTOR_TYPE(item,  struct sorted_item)

MAKE_PRIORITY_QUEUE_TYPE(state, struct state)

MAKE_HASHMAP_TYPE(player, position_pair_t, size_t)

MAKE_HASHSET_TYPE(pos, position_pair_t)

static const size_t HIT_POINTS = 200;

static inline int compare_position(position_pair_t const a, position_pair_t const b)
{
  if (a[1] != b[1])
  {
    return (a[1] > b[1]) - (a[1] < b[1]);
  }
  return (a[0] > b[0]) - (a[0] < b[0]);
}

static inline int compare_sorted_item(struct sorted_item const* a, struct sorted_item const* b)
{
  return compare_position(a->position, b->position);
}

static void get_surrounding_positions(position_pair_t const position, position_pair_t* out)
{
  struct adj { int16_t v[2]; };
  static constexpr struct adj ADJUSTMENTS[] = {
    { .v = {  0, -1 } },
    { .v = { -1,  0 } },
    { .v = {  1,  0 } },
    { .v = {  0,  1 } },
  };
  for (size_t i = 0; i < ARRAY_LEN(ADJUSTMENTS); i++)
  {
    for (size_t j = 0; j < ARRAY_LEN(out[i]); j++)
    {
      out[i][j] = position[j] + ADJUSTMENTS[i].v[j];
    }
  }
}

static bool try_get_move(position_pair_t const        position,
                         bool                         is_goblin,
                         struct hashmap_player const* elves_or_goblins,
                         struct hashset_pos const*    walls,
                         position_pair_t*             out)
{
  union position_uint_map
  {
    position_pair_t p;
    uint64_t        v;
  };

  struct hashset_pos          seen_positions = hashset_pos_new(nullptr);
  struct priority_queue_state state_queue    = priority_queue_state_new(nullptr);
  struct state                init_state;
  size_t                      min_moves      = SIZE_MAX;
  position_pair_t             min_end_position;
  memcpy(init_state.current_position, position, sizeof(position_pair_t));
  memset(init_state.starting_position, UINT8_MAX, sizeof(position_pair_t));
  init_state.num_moves = 0;
  priority_queue_state_push(&state_queue, 0, &init_state);
  while (!priority_queue_state_is_empty(&state_queue))
  {
    struct state working_state;
    priority_queue_state_pop(&state_queue, &working_state);
    working_state.num_moves += 1;
    position_pair_t surrounding_positions[4];
    get_surrounding_positions(working_state.current_position, surrounding_positions);
    FOR_EACH(next_position_ptr, surrounding_positions)
    {
      if (hashmap_player_try_find(&elves_or_goblins[!is_goblin], (position_pair_t*)*next_position_ptr) != nullptr &&
          (working_state.num_moves < min_moves ||
           (working_state.num_moves == min_moves &&
            (compare_position(min_end_position, working_state.current_position) > 0))))
      {
        memcpy(*out,             ((union position_uint_map*)working_state.starting_position)->v == UINT64_MAX ? position : working_state.starting_position, sizeof(position_pair_t));
        memcpy(min_end_position, working_state.current_position,                                                                                       sizeof(position_pair_t));
        min_moves = working_state.num_moves;
      }
      else if (hashmap_player_try_find(&elves_or_goblins[is_goblin], (position_pair_t*)*next_position_ptr) == nullptr &&
               !hashset_pos_contains(walls, (position_pair_t*)*next_position_ptr) &&
               !hashset_pos_contains(&seen_positions, (position_pair_t*)*next_position_ptr) &&
               working_state.num_moves < min_moves)
      {
        hashset_pos_insert(&seen_positions, (position_pair_t*)*next_position_ptr);
        struct state new_state;
        memcpy(new_state.current_position, *next_position_ptr, sizeof(position_pair_t));
        if (((union position_uint_map*)working_state.starting_position)->v == UINT64_MAX)
        {
          memcpy(new_state.starting_position, *next_position_ptr, sizeof(position_pair_t));
        }
        else
        {
          memcpy(new_state.starting_position, working_state.starting_position, sizeof(position_pair_t));
        }
        new_state.num_moves = working_state.num_moves;
        priority_queue_state_push(&state_queue, 0, &new_state);
      }
    }
  }
  hashset_pos_clean(&seen_positions);
  priority_queue_state_clean(&state_queue);
  return (min_moves < SIZE_MAX);
}

static size_t get_num_rounds(struct hashmap_player* elves_or_goblins, struct hashset_pos const* walls, size_t const attack_power[], bool allow_elf_deaths)
{
  size_t num_rounds     = 0;
  size_t num_characters = 0;
  for (size_t i = 0; i < 2; i++)
  {
    num_characters += hashmap_player_len(&elves_or_goblins[i]);
  }
  struct vector_item positions_sorted = vector_item_new(num_characters, nullptr);
  for (;;)
  {
    for (size_t i = 0; i < 2; i++)
    {
      for (struct hashmap_player_iterator it = hashmap_player_iterate(&elves_or_goblins[i]); hashmap_player_iterator_valid(&it); it = hashmap_player_iterate_next(&it))
      {
        position_pair_t const* position = hashmap_player_iterator_key(&it);
        struct sorted_item item;
        item.is_goblin = i;
        memcpy(item.position, *position, sizeof(item.position));
        vector_item_push_back(&positions_sorted, &item);
      }
    }
    vector_item_sort(&positions_sorted, compare_sorted_item);
    VECTOR_FOR_EACH_MUT(item_ptr, &positions_sorted)
    {
      for (size_t i = 0; i < 2; i++)
      {
        if (hashmap_player_len(&elves_or_goblins[i]) == 0)
        {
          vector_item_clean(&positions_sorted);
          return num_rounds;
        }
      }
      if (hashmap_player_try_find(&elves_or_goblins[item_ptr->is_goblin], (position_pair_t*)item_ptr->position) == nullptr)
      {
        continue;
      }
      position_pair_t move = { UINT32_MAX, UINT32_MAX };
      if (!try_get_move(item_ptr->position, item_ptr->is_goblin, elves_or_goblins, walls, &move))
      {
        continue;
      }
      if (memcmp(move, item_ptr->position, sizeof(position_pair_t)) != 0)
      {
        size_t const* hit_points = hashmap_player_try_find(&elves_or_goblins[item_ptr->is_goblin], (position_pair_t*)item_ptr->position);
        hashmap_player_insert(&elves_or_goblins[item_ptr->is_goblin], (position_pair_t*)move, hit_points);
        hashmap_player_remove(&elves_or_goblins[item_ptr->is_goblin], (position_pair_t*)item_ptr->position);
        memcpy(item_ptr->position, move, sizeof(position_pair_t));
      }
      position_pair_t attack_position = { UINT32_MAX, UINT32_MAX };
      size_t          min_hit_points  = SIZE_MAX;
      position_pair_t surrounding_positions[4];
      get_surrounding_positions(item_ptr->position, surrounding_positions);
      FOR_EACH(surrounding_position_ptr, surrounding_positions)
      {
        size_t const* hit_points_ptr;
        if ((hit_points_ptr = hashmap_player_try_find(&elves_or_goblins[!item_ptr->is_goblin], (position_pair_t*)*surrounding_position_ptr)) != nullptr &&
            (*hit_points_ptr < min_hit_points ||
              (*hit_points_ptr == min_hit_points &&
               compare_position(*surrounding_position_ptr, attack_position) < 0)))
        {
          memcpy(attack_position, *surrounding_position_ptr, sizeof(position_pair_t));
          min_hit_points  = *hit_points_ptr;
        }
      }
      if (min_hit_points < SIZE_MAX)
      {
        size_t* hit_points_ptr = hashmap_player_try_find_mut(&elves_or_goblins[!item_ptr->is_goblin], (position_pair_t*)attack_position);
        if (*hit_points_ptr <= attack_power[item_ptr->is_goblin])
        {
          if (item_ptr->is_goblin && !allow_elf_deaths)
          {
            vector_item_clean(&positions_sorted);
            return SIZE_MAX;
          }
          hashmap_player_remove(&elves_or_goblins[!item_ptr->is_goblin], (position_pair_t*)attack_position);
        }
        else
        {
          *hit_points_ptr -= attack_power[item_ptr->is_goblin];
        }
      }
    }
    vector_item_clear(&positions_sorted);
    num_rounds++;
  }
  vector_item_clean(&positions_sorted);
  return num_rounds;
}

int main()
{
  int                   return_code = 0;
  struct hashset_pos    walls;
  struct hashmap_player elves_or_goblins[2];
  {
    walls = hashset_pos_new(nullptr);
    FOR_EACH(dict_ptr, elves_or_goblins)
    {
      *dict_ptr = hashmap_player_new(nullptr, nullptr);
    }
    FILE* in = fopen("dec15.txt", "r");
    while (fgetc(in) != '\n');
    size_t line_len = ftell(in);
    fseek(in, 0, SEEK_SET);
    char* line = malloc(line_len);
    for(size_t y = 0; fscanf(in, "%s", line) == 1; y++)
    {
      for (size_t x = 0; x < strlen(line); x++)
      {
        position_pair_t position = { x, y };
        switch (line[x])
        {
          case '#': hashset_pos_insert(&walls, (position_pair_t*)position); break;
          case 'E':
          case 'G': hashmap_player_insert(&elves_or_goblins[line[x] == 'G'], (position_pair_t*)position, &HIT_POINTS); break;
        }
      }
    }
    free(line);
    fclose(in);
  }

  {
    static constexpr size_t ATTACK_POWER[] = { 3, 3 };
    struct hashmap_player local_elves_or_goblins[2];
    for (int i = 0; i < ARRAY_LEN(local_elves_or_goblins); i++)
    {
      local_elves_or_goblins[i] = hashmap_player_clone(&elves_or_goblins[i]);
    }
    size_t num_rounds           = get_num_rounds(local_elves_or_goblins, &walls, ATTACK_POWER, true);
    size_t hit_points_remaining = 0;
    FOR_EACH(dict_ptr, local_elves_or_goblins)
    {
      for (struct hashmap_player_iterator it = hashmap_player_iterate(dict_ptr); hashmap_player_iterator_valid(&it); it = hashmap_player_iterate_next(&it))
      {
        size_t const* hit_point_ptr = hashmap_player_iterator_value(&it);
        hit_points_remaining += *hit_point_ptr;
      }
      hashmap_player_clear(dict_ptr);
    }
    printf("%zu\n", num_rounds * hit_points_remaining);
  }

  {
    size_t                num_rounds = 0;
    struct hashmap_player local_elves_or_goblins[2];
    for (size_t elf_power = 4; ; elf_power++)
    {
      size_t attack_power[] = { elf_power, 3 };
      for (int i = 0; i < ARRAY_LEN(local_elves_or_goblins); i++)
      {
        local_elves_or_goblins[i] = hashmap_player_clone(&elves_or_goblins[i]);
      }
      size_t local_num_rounds = get_num_rounds(local_elves_or_goblins, &walls, attack_power, false);
      if (local_num_rounds != SIZE_MAX)
      {
        num_rounds = local_num_rounds;
        break;
      }
      FOR_EACH(dict_ptr, local_elves_or_goblins)
      {
        hashmap_player_clear(dict_ptr);
      }
    }
    size_t hit_points_remaining = 0;
    FOR_EACH(dict_ptr, local_elves_or_goblins)
    {
      for (struct hashmap_player_iterator it = hashmap_player_iterate(dict_ptr); hashmap_player_iterator_valid(&it); it = hashmap_player_iterate_next(&it))
      {
        size_t const* hit_point_ptr = hashmap_player_iterator_value(&it);
        hit_points_remaining += *hit_point_ptr;
      }
      hashmap_player_clear(dict_ptr);
    }
    printf("%zu\n", num_rounds * hit_points_remaining);
  }

  hashset_pos_clean(&walls);
  FOR_EACH(dict_ptr, elves_or_goblins)
  {
    hashmap_player_clean(dict_ptr);
  }
  return return_code;
}
