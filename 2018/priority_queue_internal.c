#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "priority_queue.h"

struct _priority_queue_internal_node_with_same_priority
{
  struct _priority_queue_internal_node_with_same_priority* next;
  void*                                                    data;
};

struct _priority_queue_internal_node
{
  struct _priority_queue_internal_node_with_same_priority*  next_with_same_priority;
  struct _priority_queue_internal_node_with_same_priority** to_insert_with_same_priority;
  struct _priority_queue_internal_node*                     next_with_lower_priority;
  size_t                                                    priority;
  void*                                                     data;
};

struct _priority_queue_internal _priority_queue_internal_new(size_t data_size,
                                                             void(*free_fn)(void*))
{
  return (struct _priority_queue_internal) {
    .data_size = data_size,
    .length    = 0,
    .head      = nullptr,
    .free_fn   = free_fn,
  };
}

void _priority_queue_internal_clean(struct _priority_queue_internal* q)
{
  while (q->head != nullptr)
  {
    while (q->head->next_with_same_priority != nullptr)
    {
      if (q->free_fn != nullptr)
      {
        q->free_fn(q->head->next_with_same_priority->data);
      }
      struct _priority_queue_internal_node_with_same_priority* next = q->head->next_with_same_priority->next;
      free(q->head->next_with_same_priority);
      q->head->next_with_same_priority = next;
    }
    if (q->free_fn != nullptr)
    {
      q->free_fn(q->head->data);
    }
    struct _priority_queue_internal_node* next = q->head->next_with_lower_priority;
    free(q->head);
    q->head = next;
  }
}

void _priority_queue_internal_push(struct _priority_queue_internal* q, size_t priority, void const* data)
{
  struct _priority_queue_internal_node** to_insert = &q->head;
  while ((*to_insert != nullptr) && (priority > (*to_insert)->priority))
  {
    to_insert = &(*to_insert)->next_with_lower_priority;
  }

  void* data_ptr;
  if ((*to_insert == nullptr) || (priority < (*to_insert)->priority))
  {
    struct _priority_queue_internal_node* prev = *to_insert;
    void* alloced_data                         = malloc(sizeof(struct _priority_queue_internal_node) + q->data_size);
    *to_insert                                 = alloced_data;
    (*to_insert)->priority                     = priority;
    (*to_insert)->next_with_lower_priority     = prev;
    (*to_insert)->next_with_same_priority      = nullptr;
    (*to_insert)->to_insert_with_same_priority = &(*to_insert)->next_with_same_priority;
    (*to_insert)->data                         = alloced_data + sizeof(struct _priority_queue_internal_node);
    data_ptr                                   = (*to_insert)->data;
  }
  else
  {
    assert(priority == (*to_insert)->priority);
    void* alloced_data                                  = malloc(sizeof(struct _priority_queue_internal_node_with_same_priority) + q->data_size);
    *(*to_insert)->to_insert_with_same_priority         = alloced_data;
    (*(*to_insert)->to_insert_with_same_priority)->data = alloced_data + sizeof(struct _priority_queue_internal_node_with_same_priority);
    (*(*to_insert)->to_insert_with_same_priority)->next = nullptr;
    data_ptr                                            = (*(*to_insert)->to_insert_with_same_priority)->data;
    (*to_insert)->to_insert_with_same_priority          = &(*(*to_insert)->to_insert_with_same_priority)->next;
  }
  memcpy(data_ptr, data, q->data_size);
  q->length++;
}

void _priority_queue_internal_pop(struct _priority_queue_internal* q, void* data)
{
  assert(q->head != nullptr);
  q->length--;
  memcpy(data, q->head->data, q->data_size);
  if (q->free_fn != nullptr)
  {
    q->free_fn(q->head->data);
  }
  if (q->head->next_with_same_priority != nullptr)
  {
    struct _priority_queue_internal_node_with_same_priority* to_remove = q->head->next_with_same_priority;
    memcpy(q->head->data, to_remove->data, q->data_size);
    q->head->next_with_same_priority = to_remove->next;
    if (q->head->next_with_same_priority == nullptr)
    {
      q->head->to_insert_with_same_priority = &q->head->next_with_same_priority;
    }
    free(to_remove);
  }
  else
  {
    struct _priority_queue_internal_node* to_remove = q->head;
    q->head = to_remove->next_with_lower_priority;
    free(to_remove);
  }
}
