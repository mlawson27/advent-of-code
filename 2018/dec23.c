#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "util.h"
#include "vector.h"

static constexpr int64_t ORIGIN[3] = {};

struct nanobot
{
  int64_t position[3];
  size_t  radius;
};

MAKE_VECTOR_TYPE(bot, struct nanobot)

static inline size_t distance_diff(int64_t const a[3], int64_t const b[3])
{
  size_t ret = 0;
  for (size_t i = 0; i < ARRAY_LEN(((struct nanobot){}).position); i++)
  {
    ret += (a[i] > b[i] ? a[i] - b[i] : b[i] - a[i]);
  }
  return ret;
}

static void get_position_intersecting_most_bots(int64_t boundary[2][3], struct vector_bot const* bots, int64_t max_intersection_position[3], size_t* intersection_count_at_max)
{
  int64_t halfway_points[3];
  for (size_t dim_i = 0; dim_i < ARRAY_LEN(halfway_points); dim_i++)
  {
    halfway_points[dim_i] = boundary[0][dim_i] + (boundary[1][dim_i] - boundary[0][dim_i]) / 2;
  }

#define MAYBE_ADD_ORIGIN(index) halfway_points[index] + (halfway_points[index] == boundary[0][index] && halfway_points[index] != boundary[1][index])
  int64_t octets[8][2][3] = { { { boundary[0][0],      boundary[0][1],      boundary[0][2]      },
                                { halfway_points[0],   halfway_points[1],   halfway_points[2]   } },
                              { { boundary[0][0],      boundary[0][1],      MAYBE_ADD_ORIGIN(2) },
                                { halfway_points[0],   halfway_points[1],   boundary[1][2]      } },
                              { { boundary[0][0],      MAYBE_ADD_ORIGIN(1), boundary[0][2]      },
                                { halfway_points[0],   boundary[1][1],      halfway_points[2]   } },
                              { { MAYBE_ADD_ORIGIN(0), boundary[0][1],      boundary[0][2]      },
                                { boundary[1][0],      halfway_points[1],   halfway_points[2]   } },
                              { { boundary[0][0],      MAYBE_ADD_ORIGIN(1), MAYBE_ADD_ORIGIN(2) },
                                { halfway_points[0],   boundary[1][1],      boundary[1][2]      } },
                              { { MAYBE_ADD_ORIGIN(0), boundary[0][1],      MAYBE_ADD_ORIGIN(2) },
                                { boundary[1][0],      halfway_points[1],   boundary[1][2]      } },
                              { { MAYBE_ADD_ORIGIN(0), MAYBE_ADD_ORIGIN(1), boundary[0][2]      },
                                { boundary[1][0],      boundary[1][1],      halfway_points[2]   } },
                              { { MAYBE_ADD_ORIGIN(0), MAYBE_ADD_ORIGIN(1), MAYBE_ADD_ORIGIN(2) },
                                { boundary[1][0],      boundary[1][1],      boundary[1][2]      } } };
#undef MAYBE_ADD_ORIGIN

  size_t max_octet_intersection_count = 0;
  size_t max_octet_i_count            = 0;
  size_t max_octet_is[ARRAY_LEN(octets)];
  for (size_t octet_i = 0; octet_i < ARRAY_LEN(octets); octet_i++)
  {
    size_t octet_min_max_distance_from_origin[2];
    for (size_t min_or_max_i = 0; min_or_max_i < ARRAY_LEN(octets[octet_i]); min_or_max_i++)
    {
      octet_min_max_distance_from_origin[min_or_max_i] = distance_diff(octets[octet_i][min_or_max_i], ORIGIN);
    }
    if (octet_min_max_distance_from_origin[0] > octet_min_max_distance_from_origin[1])
    {
      size_t temp                           = octet_min_max_distance_from_origin[0];
      octet_min_max_distance_from_origin[0] = octet_min_max_distance_from_origin[1];
      octet_min_max_distance_from_origin[1] = temp;
    }
    size_t octet_intersection_count = 0;
    VECTOR_FOR_EACH(bot, bots)
    {
      size_t bot_distance_from_origin = distance_diff(bot->position, ORIGIN);
      octet_intersection_count += (bot_distance_from_origin + bot->radius >= octet_min_max_distance_from_origin[0] &&
                                   bot_distance_from_origin <= octet_min_max_distance_from_origin[1] + bot->radius);
    }
    if (octet_intersection_count >= max_octet_intersection_count)
    {
      max_octet_i_count               = (max_octet_i_count + 1) * (octet_intersection_count == max_octet_intersection_count);
      max_octet_is[max_octet_i_count] = octet_i;
      max_octet_intersection_count    = octet_intersection_count;
    }
  }

  if (max_octet_intersection_count > *intersection_count_at_max)
  {
    FOR_EACH_N(max_octet_i_ptr, max_octet_is, max_octet_i_count + 1)
    {
      bool all_octet_dims_1 = true;
      for (size_t dim_i = 0; all_octet_dims_1 && dim_i < ARRAY_LEN(octets[*max_octet_i_ptr][0]); dim_i++)
      {
        all_octet_dims_1 = octets[*max_octet_i_ptr][0][dim_i] >= octets[*max_octet_i_ptr][1][dim_i];
      }
      if (all_octet_dims_1)
      {
        for (size_t dim_i = 0; dim_i < ARRAY_LEN(octets[*max_octet_i_ptr][1]); dim_i++)
        {
          max_intersection_position[dim_i] = octets[*max_octet_i_ptr][1][dim_i];
        }
        *intersection_count_at_max = max_octet_intersection_count;
      }
      else
      {
        get_position_intersecting_most_bots(octets[*max_octet_i_ptr], bots, max_intersection_position, intersection_count_at_max);
      }
    }
  }
}

int main()
{
  int               return_code  = 0;
  struct vector_bot bots;
  {
    size_t         num_new_lines;
    FILE*          in              = open_and_count_newlines("dec23.txt", &num_new_lines);
    bots                           = vector_bot_new(num_new_lines + 1, nullptr);
    struct nanobot working_bot;
    while (fscanf(in, " pos=<%" PRId64 ",%" PRId64 ",%" PRId64 ">, r=%zu", &working_bot.position[0],
                                                                           &working_bot.position[1],
                                                                           &working_bot.position[2],
                                                                           &working_bot.radius) == 4)
    {
      vector_bot_push_back(&bots, &working_bot);
    }
    fclose(in);
  }

  {
    struct nanobot const* bot_with_largest_radius = nullptr;
    VECTOR_FOR_EACH(bot, &bots)
    {
      if (bot_with_largest_radius == nullptr || bot->radius > bot_with_largest_radius->radius)
      {
        bot_with_largest_radius = bot;
      }
    }
    size_t num_bots_within_range_of_largest = 0;
    VECTOR_FOR_EACH(bot, &bots)
    {
      num_bots_within_range_of_largest += distance_diff(bot_with_largest_radius->position, bot->position) <= bot_with_largest_radius->radius;
    }
    printf("%zu\n", num_bots_within_range_of_largest);
  }

  {
    int64_t min_max_extents[2][3];
    for (size_t dim_i = 0; dim_i < ARRAY_LEN(min_max_extents[0]); dim_i++)
    {
      min_max_extents[0][dim_i] = INT64_MAX;
      min_max_extents[1][dim_i] = INT64_MIN;
    }
    VECTOR_FOR_EACH(bot, &bots)
    {
      for (size_t dim_i = 0; dim_i < ARRAY_LEN(bot->position); dim_i++)
      {
        min_max_extents[0][dim_i] = bot->position[dim_i] < min_max_extents[0][dim_i] ? bot->position[dim_i] : min_max_extents[0][dim_i];
        min_max_extents[1][dim_i] = bot->position[dim_i] > min_max_extents[1][dim_i] ? bot->position[dim_i] : min_max_extents[1][dim_i];
      }
    }
    int64_t max_intersection_position[3] = { INT32_MAX, INT32_MAX, INT32_MAX };
    size_t  intersection_count_at_max    = 0;
    get_position_intersecting_most_bots(min_max_extents, &bots, max_intersection_position, &intersection_count_at_max);
    printf("%zu\n", distance_diff(max_intersection_position, ORIGIN));
  }
  vector_bot_clean(&bots);
  return return_code;
}
