#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "util.h"

#define P2_NUM_ELVES      5
#define P2_SECOND_OFFSET  60

int main()
{
  uint_fast32_t dependencies['Z' - 'A' + 1] = {};
  bool          used        ['Z' - 'A' + 1] = {};
  {
    FILE* in = fopen("dec07.txt", "r");
    char step, step_dependency;
    while (fscanf(in, " Step %c must be finished before step %c can begin.", &step_dependency, &step) > 0)
    {
      dependencies[step - 'A'] |= (1 << (step_dependency - 'A'));
      used[step - 'A']            = true;
      used[step_dependency - 'A'] = true;
    }
    fclose(in);
  }

  {
    char  result['Z' - 'A' + 2] = {};
    char* next_result_ptr       = result;
    uint_fast32_t working['Z' - 'A' + 1];
    memcpy(working, dependencies, sizeof(working));
    bool is_remaining['Z' - 'A' + 1];
    memcpy(is_remaining, used, sizeof(is_remaining));
    int remaining_steps = 0;
    FOR_EACH(step_is_remaining_ptr, is_remaining)
    {
      remaining_steps += *step_is_remaining_ptr;
    }
    while (remaining_steps > 0)
    {
      int i;
      for (i = 0; i < ARRAY_LEN(working); i++)
      {
        if (is_remaining[i] && (working[i] == 0))
        {
          break;
        }
      }
      *next_result_ptr = 'A' + i;
      next_result_ptr++;
      is_remaining[i] = false;
      remaining_steps--;
      FOR_EACH(dependency_ptr, working)
      {
        *dependency_ptr &= ~((uint_fast32_t)1 << i);
      }
    }
    printf("%s\n", result);
  }

  {
    struct {
      bool is_working;
      int  current_i;
      int  time_remaining;
    } workers[P2_NUM_ELVES + 1] = {};

    uint_fast32_t working['Z' - 'A' + 1];
    memcpy(working, dependencies, sizeof(working));
    bool is_remaining['Z' - 'A' + 1];
    memcpy(is_remaining, used, sizeof(is_remaining));
    bool in_progress['Z' - 'A' + 1] = {};
    int remaining_steps = 0;
    FOR_EACH(step_is_remaining_ptr, is_remaining)
    {
      remaining_steps += *step_is_remaining_ptr;
    }
    int time;
    for (time = 0; remaining_steps > 0; time++)
    {
      FOR_EACH(worker_ptr, workers)
      {
        worker_ptr->time_remaining -= (worker_ptr->is_working);
        if (worker_ptr->time_remaining == 0 && worker_ptr->is_working)
        {
          worker_ptr->is_working              = false;
          is_remaining[worker_ptr->current_i] = false;
          in_progress[worker_ptr->current_i]  = true;
          remaining_steps--;
          FOR_EACH(dependency_ptr, working)
          {
            *dependency_ptr &= ~((uint_fast32_t)1 << worker_ptr->current_i);
          }
        }
      }
      FOR_EACH(worker_ptr, workers)
      {
        if (worker_ptr->time_remaining == 0)
        {
          bool found = false;
          int i;
          for (i = 0; i < ARRAY_LEN(working); i++)
          {
            if (is_remaining[i] && !in_progress[i] && (working[i] == 0))
            {
              found = true;
              break;
            }
          }
          if (found)
          {
            worker_ptr->is_working     = true;
            worker_ptr->current_i      = i;
            worker_ptr->time_remaining = (P2_SECOND_OFFSET + i + 1);
            in_progress[i]             = true;
          }
          else
          {
            break;
          }
        }
      }
    }
    time--;
    printf("%i\n", time);
  }

  return 0;
}
