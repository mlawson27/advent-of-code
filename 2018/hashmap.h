#ifndef __HASHMAP_H__
#define __HASHMAP_H__

#include "hashmap_internal.h"

#define MAKE_HASHMAP_TYPE(suffix, key_type, value_type) \
\
struct hashmap_##suffix \
{ \
  union \
  { \
    struct _hashmap_internal _internal; \
    key_type*                _dummy_key; /* for typeof uses in macros */ \
    value_type*              _dummy_val; /* for typeof uses in macros */ \
  }; \
}; \
\
static inline struct hashmap_##suffix hashmap_##suffix##_new_with_num_buckets(size_t num_buckets, \
                                                                              void(*custom_key_free_fn)  (key_type*),   \
                                                                              void(*custom_value_free_fn)(value_type*)) \
{ \
  return (struct hashmap_##suffix) { \
    ._internal = _hashmap_internal_new_with_num_buckets(num_buckets, \
                                                        sizeof(key_type),   \
                                                        sizeof(value_type), \
                                                        (_hashmap_internal_mem_free_fn)custom_key_free_fn, \
                                                        (_hashmap_internal_mem_free_fn)custom_value_free_fn) \
  }; \
} \
\
static inline struct hashmap_##suffix hashmap_##suffix##_new(void(*custom_key_free_fn)  (key_type*),   \
                                                             void(*custom_value_free_fn)(value_type*)) \
{ \
  return (struct hashmap_##suffix) { \
    ._internal = _hashmap_internal_new(sizeof(key_type),   \
                                       sizeof(value_type), \
                                       (_hashmap_internal_mem_free_fn)custom_key_free_fn, \
                                       (_hashmap_internal_mem_free_fn)custom_value_free_fn) \
  }; \
} \
\
static inline struct hashmap_##suffix hashmap_##suffix##_clone(struct hashmap_##suffix const* m) \
{ \
  return (struct hashmap_##suffix) { \
    ._internal = _hashmap_internal_clone(&m->_internal) \
  }; \
} \
\
static inline void hashmap_##suffix##_clean(struct hashmap_##suffix* m) \
{ \
  _hashmap_internal_clean(&m->_internal); \
} \
\
static inline size_t hashmap_##suffix##_len(struct hashmap_##suffix const* m)\
{ \
  return _hashmap_internal_len(&m->_internal); \
} \
\
static inline value_type const* hashmap_##suffix##_try_find(struct hashmap_##suffix const* m, key_type const* key) \
{ \
  return (value_type const*)_hashmap_internal_try_find(&m->_internal, key); \
} \
\
static inline value_type* hashmap_##suffix##_try_find_mut(struct hashmap_##suffix* m, key_type const* key) \
{ \
  return (value_type*)_hashmap_internal_try_find_mut(&m->_internal, key); \
} \
\
static inline bool hashmap_##suffix##_insert(struct hashmap_##suffix* m, key_type const* key, value_type const* value) \
{ \
  return _hashmap_internal_insert(&m->_internal, key, value); \
} \
\
static inline bool hashmap_##suffix##_remove(struct hashmap_##suffix* m, key_type const* key) \
{ \
  return _hashmap_internal_remove(&m->_internal, key); \
} \
\
static inline void hashmap_##suffix##_clear(struct hashmap_##suffix* m) \
{ \
  _hashmap_internal_clear(&m->_internal); \
} \
\
\
struct hashmap_##suffix##_iterator \
{ \
  struct _hashmap_internal_iterator _internal; \
}; \
\
static inline key_type const* hashmap_##suffix##_iterator_key(struct hashmap_##suffix##_iterator const* it) \
{ \
  return (key_type const*)_hashmap_internal_iterator_key(&it->_internal); \
} \
\
static inline value_type const* hashmap_##suffix##_iterator_value(struct hashmap_##suffix##_iterator const* it) \
{ \
  return (value_type const*)_hashmap_internal_iterator_value(&it->_internal); \
} \
\
static inline struct hashmap_##suffix##_iterator hashmap_##suffix##_iterate_next(struct hashmap_##suffix##_iterator* it) \
{ \
  return (struct hashmap_##suffix##_iterator) { \
    ._internal = _hashmap_internal_iterate_next(&it->_internal) \
  }; \
} \
\
static inline struct hashmap_##suffix##_iterator hashmap_##suffix##_iterate(struct hashmap_##suffix* m) \
{ \
  return (struct hashmap_##suffix##_iterator) { \
    ._internal = _hashmap_internal_iterate(&m->_internal) \
  }; \
} \
\
static inline bool hashmap_##suffix##_iterator_valid(struct hashmap_##suffix##_iterator const* it) \
{ \
  return _hashmap_internal_iterator_valid(&it->_internal); \
}

#endif