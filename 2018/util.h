#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define _STRINGIFY(x) #x
#define STRINGIFY(x) _STRINGIFY(x)

#define ARRAY_LEN(arr) sizeof(arr)/sizeof(arr[0])

#define BEGIN(buf) (buf)
#define END(buf)   (buf) + ARRAY_LEN(buf)

#define FOR_EACH(var, arr)      for (typeof_unqual(&(arr)[0]) var = BEGIN(arr); var != END(arr); var++)
#define FOR_EACH_N(var, arr, n) for (typeof_unqual(&(arr)[0]) var = BEGIN(arr); var != BEGIN(arr) + n; var++)


FILE* open_and_count_newlines(char const* filename, size_t* num_new_lines);


#define MAKE_COMPARE_FN(suffix, type) \
static inline int compare_##suffix(void const* a, void const* b)\
{\
  type const* a_typed = (type const*)a;\
  type const* b_typed = (type const*)b;\
  return (*a_typed > *b_typed) - (*b_typed > *a_typed);\
}

MAKE_COMPARE_FN(b, bool)
MAKE_COMPARE_FN(c, char)
MAKE_COMPARE_FN(uc, unsigned char)
MAKE_COMPARE_FN(s, short)
MAKE_COMPARE_FN(us, unsigned short)
MAKE_COMPARE_FN(i, int)
MAKE_COMPARE_FN(ui, unsigned int)
MAKE_COMPARE_FN(l, long)
MAKE_COMPARE_FN(ul, unsigned long)
MAKE_COMPARE_FN(ll, long long)
MAKE_COMPARE_FN(ull, unsigned long long)
MAKE_COMPARE_FN(f, float)
MAKE_COMPARE_FN(d, double)

#undef MAKE_COMPARE_FN

#endif