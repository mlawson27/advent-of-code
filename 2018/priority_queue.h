#ifndef __PRIORITY_QUEUE_H__
#define __PRIORITY_QUEUE_H__

#include "priority_queue_internal.h"

#define MAKE_PRIORITY_QUEUE_TYPE(suffix, elem_type) \
\
struct priority_queue_##suffix \
{ \
  union \
  { \
    struct _priority_queue_internal _internal; \
    elem_type*                      _dummy; /* for typeof uses in macros */ \
  }; \
}; \
\
static inline struct priority_queue_##suffix priority_queue_##suffix##_new(void(*custom_free_fn)(elem_type*)) \
{\
  return (struct priority_queue_##suffix) { \
    ._internal = _priority_queue_internal_new(sizeof(elem_type), (void(*)(void*))custom_free_fn) \
  }; \
}\
\
void priority_queue_##suffix##_clean(struct priority_queue_##suffix* q) \
{\
  _priority_queue_internal_clean(&q->_internal); \
}\
\
static inline bool priority_queue_##suffix##_is_empty(struct priority_queue_##suffix const* q) \
{ \
  return _priority_queue_internal_is_empty(&q->_internal); \
} \
\
static inline size_t priority_queue_##suffix##_length(struct priority_queue_##suffix const* q) \
{ \
  return _priority_queue_internal_length(&q->_internal); \
} \
\
static inline void priority_queue_##suffix##_push(struct priority_queue_##suffix* q, size_t priority, elem_type const* data) \
{ \
  _priority_queue_internal_push(&q->_internal, priority, data); \
} \
\
static inline void priority_queue_##suffix##_pop(struct priority_queue_##suffix* q, elem_type* data) \
{ \
  _priority_queue_internal_pop(&q->_internal, data); \
}

#endif