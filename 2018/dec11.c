#include <limits.h>
#include <stdio.h>
#include <string.h>

static constexpr int GRID_SERIAL = 9445;
static constexpr int GRID_SIZE   =  300;

int main()
{
  int grid[GRID_SIZE][GRID_SIZE];
  for (int y = 0; y < GRID_SIZE; y++)
  {
    for (int x = 0; x < GRID_SIZE; x++)
    {
      int rack_id = (x + 1) + 10;
      grid[y][x]  = (int)((((long long)((rack_id * (y + 1)) + GRID_SERIAL) * rack_id) / 100) % 10) - 5;
    }
  }

  {
    int max_coordinates[2];
    int max_square_size = 0;
    int max_sum         = INT_MIN;
    for (int square_size = 1; square_size <= GRID_SIZE; square_size++)
    {
      int working_coordinates[2];
      int working_max_sum = INT_MIN;
      for (int y = 0; y < GRID_SIZE - square_size + 1; y++)
      {
        int working_sum = 0;
        for (int sub_y = 0; sub_y < square_size; sub_y++)
        {
          for (int sub_x = 0; sub_x < square_size - 1; sub_x++)
          {
            working_sum += grid[y + sub_y][sub_x];
          }
        }
        for (int x = 0; x < GRID_SIZE - square_size + 1; x++)
        {
          for (int sub_y = 0; sub_y < square_size; sub_y++)
          {
            working_sum += grid[y + sub_y][x + square_size - 1];
          }
          if (working_sum > working_max_sum)
          {
            working_max_sum        = working_sum;
            working_coordinates[0] = y;
            working_coordinates[1] = x;
          }
          for (int sub_y = 0; sub_y < square_size; sub_y++)
          {
            working_sum -= grid[y + sub_y][x];
          }
        }
      }
      if (working_max_sum > max_sum)
      {
        max_sum         = working_max_sum;
        max_square_size = square_size;
        memcpy(max_coordinates, working_coordinates, sizeof(max_coordinates));
      }
      if (square_size == 3)
      {
        printf("%i,%i\n", max_coordinates[1] + 1, max_coordinates[0] + 1);
      }
    }
    printf("%i,%i,%i\n", max_coordinates[1] + 1, max_coordinates[0] + 1, max_square_size);
  }
  return 0;
}
