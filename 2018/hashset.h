#ifndef __HASHSET_H__
#define __HASHSET_H__

#include "hashmap.h"

#define MAKE_HASHSET_TYPE(suffix, key_type) \
\
struct hashset_##suffix \
{ \
  union \
  { \
    struct _hashmap_internal _internal; \
    key_type*                _dummy_key; /* for typeof uses in macros */ \
  }; \
}; \
\
static inline struct hashset_##suffix hashset_##suffix##_new_with_num_buckets(size_t num_buckets, \
                                                                              void(*custom_key_free_fn)(key_type*)) \
{ \
  return (struct hashset_##suffix) { \
    ._internal = _hashmap_internal_new_with_num_buckets(num_buckets, \
                                                        sizeof(key_type),   \
                                                        0, \
                                                        (_hashmap_internal_mem_free_fn)custom_key_free_fn, \
                                                        nullptr) \
  }; \
} \
\
static inline struct hashset_##suffix hashset_##suffix##_new(void(*custom_key_free_fn)(key_type*)) \
{ \
  return (struct hashset_##suffix) { \
    ._internal = _hashmap_internal_new(sizeof(key_type),   \
                                       0, \
                                       (_hashmap_internal_mem_free_fn)custom_key_free_fn, \
                                       nullptr) \
  }; \
} \
\
static inline void hashset_##suffix##_clean(struct hashset_##suffix* s) \
{ \
  _hashmap_internal_clean(&s->_internal); \
} \
\
static inline size_t hashset_##suffix##_len(struct hashset_##suffix const* s)\
{ \
  return _hashmap_internal_len(&s->_internal); \
} \
\
static inline bool hashset_##suffix##_contains(struct hashset_##suffix const* s, key_type const* key) \
{ \
  return _hashmap_internal_try_find(&s->_internal, key) != nullptr; \
} \
\
static inline bool hashset_##suffix##_insert(struct hashset_##suffix* s, key_type const* key) \
{ \
  return _hashmap_internal_insert(&s->_internal, key, nullptr); \
} \
\
static inline bool hashset_##suffix##_remove(struct hashset_##suffix* s, key_type const* key) \
{ \
  return _hashmap_internal_remove(&s->_internal, key); \
} \
\
static inline void hashset_##suffix##_clear(struct hashset_##suffix* s) \
{ \
  _hashmap_internal_clear(&s->_internal); \
} \
\
\
struct hashset_##suffix##_iterator \
{ \
  struct _hashmap_internal_iterator _internal; \
}; \
\
static inline key_type const* hashset_##suffix##_iterator_key(struct hashset_##suffix##_iterator const* it) \
{ \
  return (key_type const*)_hashmap_internal_iterator_key(&it->_internal); \
} \
\
static inline struct hashset_##suffix##_iterator hashset_##suffix##_iterate_next(struct hashset_##suffix##_iterator* it) \
{ \
  return (struct hashset_##suffix##_iterator) { \
    ._internal = _hashmap_internal_iterate_next(&it->_internal) \
  }; \
} \
\
static inline struct hashset_##suffix##_iterator hashset_##suffix##_iterate(struct hashset_##suffix* s) \
{ \
  return (struct hashset_##suffix##_iterator) { \
    ._internal = _hashmap_internal_iterate(&s->_internal) \
  }; \
} \
\
static inline bool hashset_##suffix##_iterator_valid(struct hashset_##suffix##_iterator const* it) \
{ \
  return _hashmap_internal_iterator_valid(&it->_internal); \
}

#endif