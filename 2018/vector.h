#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <stddef.h>
#include <stdint.h>

#include "vector_internal.h"

#define MAKE_VECTOR_TYPE(suffix, elem_type) \
\
struct vector_##suffix \
{ \
  union \
  { \
    struct _vector_internal _internal; \
    elem_type*              _dummy; /* for typeof uses in macros */ \
  }; \
}; \
\
static inline struct vector_##suffix vector_##suffix##_new(size_t num_elems,                  \
                                                           void(*custom_free_fn)(elem_type*)) \
{ \
  return (struct vector_##suffix) { \
    ._internal = _vector_internal_new(sizeof(elem_type), num_elems, (void(*)(void*))custom_free_fn) \
  }; \
} \
\
static inline void vector_##suffix##_clean(struct vector_##suffix* const v) \
{ \
  _vector_internal_clean(&v->_internal); \
} \
\
static inline size_t vector_##suffix##_length(struct vector_##suffix* const v) \
{ \
  return _vector_internal_length(&v->_internal); \
} \
\
elem_type const* vector_##suffix##_get(struct vector_##suffix const* v, size_t elem_index) \
{ \
  return (elem_type const*)_vector_internal_get(&v->_internal, elem_index); \
} \
\
elem_type* vector_##suffix##_get_mut(struct vector_##suffix* v, size_t elem_index) \
{ \
  return (elem_type*)_vector_internal_get_mut(&v->_internal, elem_index); \
} \
\
void vector_##suffix##_push_back(struct vector_##suffix* v, elem_type const* data) \
{ \
  _vector_internal_push_back(&v->_internal, data); \
} \
\
void vector_##suffix##_sort(struct vector_##suffix* v, int (*comp)(elem_type const*, const elem_type const*)) \
{ \
  _vector_internal_sort(&v->_internal, (int(*)(void const*, void const*))comp); \
} \
\
void vector_##suffix##_sort_N(struct vector_##suffix* v, size_t count, int (*comp)(elem_type const*, const elem_type const*)) \
{ \
  _vector_internal_sort_N(&v->_internal, count, (int(*)(void const*, void const*))comp); \
} \
\
void vector_##suffix##_clear(struct vector_##suffix* v) \
{ \
  _vector_internal_clear(&v->_internal); \
}

#define VECTOR_FOR_EACH(var, v)\
for (typeof(*(v)->_dummy) const* var = (typeof(*(v)->_dummy) const*)(v)->_internal.head_ptr; var != (v)->_internal.tail_ptr; var++)

#define VECTOR_FOR_EACH_MUT(var, v)\
for (typeof((v)->_dummy) var = (typeof((v)->_dummy))(v)->_internal.head_ptr; var != (v)->_internal.tail_ptr; var++)

#define VECTOR_FOR_EACH_N(var, v, count)\
for (typeof(*(v)->_dummy) const* var = (typeof(*(v)->_dummy) const*)(v)->_internal.head_ptr; var != (((v)->_internal.head_ptr + ((count) * sizeof(*(v)->_dummy)) > (v)->_internal.tail_ptr) ? (v)->_internal.tail_ptr : (v)->_internal.head_ptr + ((count) * sizeof(*(v)->_dummy))); var++)

#define VECTOR_FOR_EACH_N_MUT(var, v, count)\
for (typeof((v)->_dummy) var = (typeof((v)->_dummy))(v)->_internal.head_ptr; var != (((v)->_internal.head_ptr + ((count) * sizeof(*(v)->_dummy)) > (v)->_internal.tail_ptr) ? (v)->_internal.tail_ptr : (v)->_internal.head_ptr + ((count) * sizeof(*(v)->_dummy))); var++)

#endif
