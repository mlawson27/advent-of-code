#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "vector.h"
#include "util.h"

MAKE_VECTOR_TYPE(str, char const*)

int main()
{
  char*             data;
  struct vector_str string_vec = vector_str_new(500, nullptr);
  {
    FILE* in = fopen("dec02.txt", "r");
    fseek(in, 0, SEEK_END);
    long file_size = ftell(in);
    fseek(in, 0, SEEK_SET);

    data = malloc(file_size + 1);
    fread(data, file_size, 1, in);
    fclose(in);

    data[file_size] = 0;
    char* last_start = nullptr;
    FOR_EACH_N(c, data, file_size + 1)
    {
      if (!isalpha(*c))
      {
        *c         = 0;
        last_start = nullptr;
      }
      else if (last_start == nullptr)
      {
        char const* ro_c = c;
        vector_str_push_back(&string_vec, &ro_c);
        last_start = c;
      }
    }
  }

  {
    unsigned int count_by_char['z' - 'a' + 1];
    unsigned int exactly_two_count   = 0;
    unsigned int exactly_three_count = 0;
    VECTOR_FOR_EACH(line_ptr, &string_vec)
    {
      memset(count_by_char, 0, sizeof(count_by_char));
      size_t line_len = strlen(*line_ptr);
      FOR_EACH_N(c, *line_ptr, line_len)
      {
        count_by_char[*c - 'a']++;
      }
      bool any_two_count   = false;
      bool any_three_count = false;
      FOR_EACH(count, count_by_char)
      {
        any_two_count   = any_two_count   || (*count == 2);
        any_three_count = any_three_count || (*count == 3);
      }
      exactly_two_count   += any_two_count;
      exactly_three_count += any_three_count;
    }
    printf("%u\n", exactly_two_count * exactly_three_count);
  }

  {
    size_t string_vec_len = vector_str_length(&string_vec);
    for (int i = 0; i < string_vec_len - 1; i++)
    {
      char const* const* elem_i = vector_str_get(&string_vec, i);
      size_t             len_i  = strlen(*elem_i);
      bool               found  = false;
      for (int j = i + 1; !found && j < string_vec_len; j++)
      {
        char const* const* elem_j     = vector_str_get(&string_vec, j);
        size_t             diff_count = 0;
        for (int c = 0; c < len_i; c++)
        {
          diff_count += ((*elem_i)[c] != (*elem_j)[c]);
        }
        if (diff_count == 1)
        {
          for (int c = 0; c < len_i; c++)
          {
            if ((*elem_i)[c] == (*elem_j)[c])
            {
              printf("%c", (*elem_i)[c]);
            }
          }
          printf("\n");
          found = true;
          break;
        }
      }
    }
  }

  vector_str_clean(&string_vec);
  free(data);
  return 0;
}