#ifndef __VECTOR_INTERNAL_H__
#define __VECTOR_INTERNAL_H__

#ifndef __VECTOR_H__
#error "Only include vector_internal.h from vector.h"
#endif

#include <stddef.h>
#include <stdint.h>

struct _vector_internal
{
  size_t elem_size;
  size_t capacity;

  void*  buf;
  void*  head_ptr;
  void*  tail_ptr;

  void(*free_fn)(void*);
};

struct _vector_internal _vector_internal_new(size_t elem_size,
                                             size_t num_elems,
                                             void(*free_fn)(void*));

void _vector_internal_clean(struct _vector_internal* v);

static inline size_t _vector_internal_length(struct _vector_internal* const v)
{
  return (v->tail_ptr - v->head_ptr) / v->elem_size;
}

void const* _vector_internal_get(struct _vector_internal const* v, size_t elem_index);

static inline void* _vector_internal_get_mut(struct _vector_internal* v, size_t elem_index)
{
  return (void*)_vector_internal_get(v, elem_index);
}

void _vector_internal_push_back(struct _vector_internal* v, void const* data);

void _vector_internal_sort_N(struct _vector_internal* v, size_t count, int (*comp)(void const*, void const*));

static inline void _vector_internal_sort(struct _vector_internal* v, int (*comp)(void const*, void const*))
{
  _vector_internal_sort_N(v, _vector_internal_length(v), comp);
}

void _vector_internal_clear(struct _vector_internal* v);

#endif
