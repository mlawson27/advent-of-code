#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashmap.h"

typedef int32_t position_t[2];
static_assert(sizeof(position_t) == sizeof(void*));

MAKE_HASHMAP_TYPE(dist, position_t, size_t)

size_t distance_to_furthest_room(char const** regex_str, size_t starting_distance_so_far, position_t starting_position, struct hashmap_dist* min_distance_to_room)
{
  position_t current_position;
  memcpy(current_position, starting_position, sizeof(position_t));
  size_t distance_so_far     = starting_distance_so_far;
  size_t max_distance_so_far = distance_so_far;

  for (; **regex_str != '$';)
  {
    switch (**regex_str)
    {
      case '^':
        (*regex_str)++;
        break;
      case 'N':
        current_position[1]++;
        goto DIR;
      case 'S':
        current_position[1]--;
        goto DIR;
      case 'E':
        current_position[0]++;
        goto DIR;
      case 'W':
        current_position[0]--;
DIR:    {
          size_t const* distance_ptr;
          if ((distance_ptr = hashmap_dist_try_find(min_distance_to_room, (position_t*)current_position)) != nullptr)
          {
            distance_so_far = *distance_ptr;
          }
          else
          {
            distance_so_far++;
            hashmap_dist_insert(min_distance_to_room, (position_t*)current_position, &distance_so_far);
          }
          (*regex_str)++;
          break;
        }
      case '(':
        (*regex_str)++;
        distance_so_far = distance_to_furthest_room(regex_str, distance_so_far, current_position, min_distance_to_room);
        break;
      case ')':
        (*regex_str)++;
        return (distance_so_far > max_distance_so_far ? distance_so_far : max_distance_so_far);
      case '|':
        memcpy(current_position, starting_position, sizeof(position_t));
        max_distance_so_far = distance_so_far > max_distance_so_far ? distance_so_far : max_distance_so_far;
        distance_so_far     = starting_distance_so_far;
        (*regex_str)++;
        break;
      default:
        assert(false);
    }
  }
  return (distance_so_far > max_distance_so_far ? distance_so_far : max_distance_so_far);
}

int main()
{
  int         return_code  = 0;
  char const* regex_str;
  size_t      regex_str_len;
  {
    FILE* in = fopen("dec20.txt", "r");
    fseek(in, 0, SEEK_END);
    regex_str_len        = (size_t)(ftell(in) + 1);
    char* regex_str_read = malloc(regex_str_len);
    fseek(in, 0, SEEK_SET);
    fread(regex_str_read, regex_str_len - 1, sizeof(char), in);
    regex_str_read[regex_str_len - 1] = 0;
    regex_str = regex_str_read;
    fclose(in);
  }

  {
    struct hashmap_dist min_distance_to_room = hashmap_dist_new(nullptr, nullptr);
    position_t          current_position     = { 0, 0 };
    char const*         local_regex_str      = regex_str;
    printf("%zu\n", distance_to_furthest_room(&local_regex_str, 0, current_position, &min_distance_to_room));
    hashmap_dist_clean(&min_distance_to_room);
  }

  {
    struct hashmap_dist min_distance_to_room = hashmap_dist_new(nullptr, nullptr);
    position_t          current_position     = { 0, 0 };
    char const*         local_regex_str      = regex_str;
    distance_to_furthest_room(&local_regex_str, 0, current_position, &min_distance_to_room);
    size_t num_exceeding_1000 = 0;
    for (struct hashmap_dist_iterator it = hashmap_dist_iterate(&min_distance_to_room); hashmap_dist_iterator_valid(&it); it = hashmap_dist_iterate_next(&it))
    {
      num_exceeding_1000 += (*hashmap_dist_iterator_value(&it) >= 1000);
    }
    printf("%zu\n", num_exceeding_1000);
    hashmap_dist_clean(&min_distance_to_room);
  }
  free((void*)regex_str);
  return return_code;
}
