#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "vector.h"

struct unit_set
{
  size_t       count;
  size_t       per_unit_hit_points;
  char const** immunities;
  size_t       num_immunities;
  char const** weaknesses;
  size_t       num_weaknesses;
  char const*  attack_type;
  size_t       attack_power;
  size_t       initiative;
};

struct attack_state
{
  struct unit_set* unit_set;
  size_t           owner_i;
  struct unit_set* unit_set_to_attack;
  size_t           attack_modifier;
};

MAKE_VECTOR_TYPE(unit, struct unit_set)

static inline size_t unit_set_effective_power(struct unit_set const* unit_set_ptr)
{
  return unit_set_ptr->attack_power * unit_set_ptr->count;
}

static void free_unit_set(struct unit_set* unit_set_ptr)
{
  free(unit_set_ptr->immunities);
  free(unit_set_ptr->weaknesses);
}

static int compare_attack_states_by_initiative(void const* a_as_void, void const* b_as_void)
{
  struct attack_state const* const* a = a_as_void;
  struct attack_state const* const* b = b_as_void;
  return ((*b)->unit_set->initiative > (*a)->unit_set->initiative) - ((*b)->unit_set->initiative < (*a)->unit_set->initiative);
}

static int compare_attack_states_by_effective_power(void const* a_as_void, void const* b_as_void)
{
  struct attack_state const* const* a       = a_as_void;
  struct attack_state const* const* b       = b_as_void;
  size_t                            a_power = unit_set_effective_power((*a)->unit_set);
  size_t                            b_power = unit_set_effective_power((*b)->unit_set);
  return (a_power == b_power) ? compare_attack_states_by_initiative(a_as_void, b_as_void) : (b_power > a_power) - (b_power < a_power);
}

static size_t do_combat(struct vector_unit unit_sets[2])
{
  size_t                num_unit_sets        = vector_unit_length(&unit_sets[0]) + vector_unit_length(&unit_sets[1]);
  struct attack_state*  attack_states        = malloc(sizeof(struct attack_state)  * num_unit_sets);
  struct attack_state** attack_states_sorted = malloc(sizeof(struct attack_state*) * num_unit_sets);
  {
    size_t attack_state_i = 0;
    for (size_t i = 0; i < 2; i++)
    {
      VECTOR_FOR_EACH_MUT(unit_set_ptr, &unit_sets[i])
      {
        attack_states[attack_state_i].unit_set           = unit_set_ptr;
        attack_states[attack_state_i].owner_i            = i;
        attack_states[attack_state_i].unit_set_to_attack = nullptr;
        attack_states[attack_state_i].attack_modifier    = 0;
        attack_states_sorted[attack_state_i]             = &attack_states[attack_state_i];
        attack_state_i++;
      }
    }
  }
  unsigned char* claimed_for_attack = malloc(num_unit_sets);
  size_t         winner;
  for (;;)
  {
    memset(claimed_for_attack, 0, num_unit_sets);
    qsort(attack_states_sorted, num_unit_sets, sizeof(struct attack_state*), compare_attack_states_by_effective_power);
    FOR_EACH_N(attack_state_ptr_ptr, attack_states_sorted, num_unit_sets)
    {
      if ((*attack_state_ptr_ptr)->unit_set->count == 0)
      {
        continue;
      }
      size_t attack_state_to_attack_i      = 0;
      size_t attack_state_to_attack_damage = 0;
      size_t default_damage_taken          = unit_set_effective_power((*attack_state_ptr_ptr)->unit_set);
      for (size_t other_unit_set_i = 0; other_unit_set_i < vector_unit_length(&unit_sets[1 - (*attack_state_ptr_ptr)->owner_i]); other_unit_set_i++)
      {
        size_t                 other_attack_state_i = vector_unit_length(&unit_sets[0]) * ((*attack_state_ptr_ptr)->owner_i == 0) + other_unit_set_i;
        struct unit_set const* other_unit_set_ptr   = vector_unit_get(&unit_sets[1 - (*attack_state_ptr_ptr)->owner_i], other_unit_set_i);
        if (!claimed_for_attack[other_attack_state_i] && other_unit_set_ptr->count > 0)
        {
          size_t damage_taken = default_damage_taken;
          bool   is_immune    = false;
          for (size_t immunity_i = 0; !is_immune && immunity_i < other_unit_set_ptr->num_immunities; immunity_i++)
          {
            is_immune = (strcmp((*attack_state_ptr_ptr)->unit_set->attack_type, other_unit_set_ptr->immunities[immunity_i]) == 0);
          }
          if (is_immune)
          {
            damage_taken = 0;
          }
          else
          {
            bool is_weak = false;
            for (size_t weakness_i = 0; !is_weak && weakness_i < other_unit_set_ptr->num_weaknesses; weakness_i++)
            {
              is_weak = (strcmp((*attack_state_ptr_ptr)->unit_set->attack_type, other_unit_set_ptr->weaknesses[weakness_i]) == 0);
            }
            if (is_weak)
            {
              damage_taken *= 2;
            }
          }
          if (damage_taken > attack_state_to_attack_damage ||
              (damage_taken == attack_state_to_attack_damage &&
                (unit_set_effective_power(other_unit_set_ptr) > unit_set_effective_power(attack_states[attack_state_to_attack_i].unit_set) ||
                (unit_set_effective_power(other_unit_set_ptr) == unit_set_effective_power(attack_states[attack_state_to_attack_i].unit_set) &&
                  other_unit_set_ptr->initiative > attack_states[attack_state_to_attack_i].unit_set->initiative))))
          {
            attack_state_to_attack_i      = other_attack_state_i;
            attack_state_to_attack_damage = damage_taken;
          }
        }
      }
      if (attack_state_to_attack_damage > 0)
      {
        claimed_for_attack[attack_state_to_attack_i] = 1;
        (*attack_state_ptr_ptr)->unit_set_to_attack  = attack_states[attack_state_to_attack_i].unit_set;
        (*attack_state_ptr_ptr)->attack_modifier     = (attack_state_to_attack_damage / default_damage_taken);
      }
      else
      {
        (*attack_state_ptr_ptr)->unit_set_to_attack = nullptr;
      }
    }
    qsort(attack_states_sorted, num_unit_sets, sizeof(struct attack_state*), compare_attack_states_by_initiative);
    bool any_units_killed = false;
    FOR_EACH_N(attack_state_ptr_ptr, attack_states_sorted, num_unit_sets)
    {
      if ((*attack_state_ptr_ptr)->unit_set_to_attack == nullptr ||
          (*attack_state_ptr_ptr)->unit_set->count == 0)
      {
        continue;
      }
      size_t num_units_killed = ((*attack_state_ptr_ptr)->unit_set->attack_power * (*attack_state_ptr_ptr)->unit_set->count * (*attack_state_ptr_ptr)->attack_modifier) / (*attack_state_ptr_ptr)->unit_set_to_attack->per_unit_hit_points;
      any_units_killed        = (any_units_killed || (num_units_killed > 0));
      (*attack_state_ptr_ptr)->unit_set_to_attack->count = ((*attack_state_ptr_ptr)->unit_set_to_attack->count > num_units_killed ? (*attack_state_ptr_ptr)->unit_set_to_attack->count - num_units_killed : 0);
    }
    if (!any_units_killed)
    {
      winner = SIZE_MAX;
      break;
    }
    size_t any_all_units_defeated = 0;
    for (size_t army_i = 0; army_i < 2; army_i++)
    {
      size_t any_not_defeated = 0;
      VECTOR_FOR_EACH(unit_set_ptr, &unit_sets[army_i])
      {
        any_not_defeated = unit_set_ptr->count > 0;
        if (any_not_defeated)
        {
          break;
        }
      }
      if (!any_not_defeated)
      {
        any_all_units_defeated = 1;
        winner                 = 1 - army_i;
        break;
      }
    }
    if (any_all_units_defeated)
    {
      break;
    }
  }
  free(claimed_for_attack);
  free(attack_states_sorted);
  free(attack_states);
  return winner;
}

int main()
{
  int                return_code  = 0;
  struct vector_unit unit_sets[2];
  char const**       attack_types;
  size_t             num_attack_types;
  {
    FILE* in = fopen("dec24.txt", "r");
    {
      struct attack_type_node
      {
        struct attack_type_node* next;
        char*                    data;
      };
      struct attack_type_node* attack_type_set_head = nullptr;
      size_t                   attack_type_set_len  = 0;
      for (int i = 0; i < ARRAY_LEN(unit_sets); i++)
      {
        fscanf(in, "%*[^:]: ");
        size_t start_of_units_pos = ftell(in);
        size_t longest_line_len   = 0;
        size_t current_line_len   = 0;
        size_t num_lines          = 0;
        for (int c = fgetc(in), last_c = 0; c != EOF && (c != '\n' || last_c != '\n'); last_c = c, c = fgetc(in))
        {
          current_line_len++;
          if (c == '\n')
          {
            longest_line_len = (current_line_len > longest_line_len ? current_line_len : longest_line_len);
            current_line_len = 0;
            num_lines++;
          }
        }
        if (current_line_len != 0)
        {
            longest_line_len = (current_line_len > longest_line_len ? current_line_len : longest_line_len);
            current_line_len = 0;
            num_lines++;
        }
        fseek(in, start_of_units_pos, SEEK_SET);
        unit_sets[i]       = vector_unit_new(num_lines, free_unit_set);
        char* working_line = malloc(longest_line_len + 1);
        struct unit_set working_unit_set;
        while (fgets(working_line, longest_line_len + 1, in) != nullptr && working_line[0] != '\n')
        {
          size_t weakness_start, weakness_end, attack_type_start, attack_type_end;
          [[maybe_unused]] int count = sscanf(working_line, "%zu units each with %zu hit points %zn", &working_unit_set.count,
                                                                                                      &working_unit_set.per_unit_hit_points,
                                                                                                      &weakness_start);
          assert(count == 2);
          if (working_line[weakness_start] == '(')
          {
            weakness_start++;
            sscanf(working_line + weakness_start, "%*[^)])%zn", &weakness_end);
            weakness_end += weakness_start;
          }
          else
          {
            weakness_end = weakness_start;
          }
          count = sscanf(working_line + weakness_end, " with an attack that does %zu %zn%*s%zn damage at initiative %zu", &working_unit_set.attack_power,
                                                                                                                          &attack_type_start,
                                                                                                                          &attack_type_end,
                                                                                                                          &working_unit_set.initiative);
          assert(count == 2);
          attack_type_start += weakness_end;
          attack_type_end   += weakness_end;

          working_unit_set.num_immunities = 0;
          working_unit_set.num_weaknesses = 0;
          working_unit_set.immunities     = nullptr;
          working_unit_set.weaknesses     = nullptr;
          char const* working_type_start;
          for (int weakness_type_i = 0; weakness_type_i < 2; weakness_type_i++)
          {
            bool is_immune = working_line[weakness_start] == 'i';
            if (is_immune)
            {
              weakness_start += sizeof("immune to") - 1;
            }
            else
            {
              weakness_start += sizeof("weak to") - 1;
            }
            struct attack_type_node* this_set_head = nullptr;
            size_t                   this_set_len  = 0;
            working_type_start = working_line + weakness_start;
            for (char const* weakness_char_ptr = working_line + weakness_start; weakness_char_ptr <= working_line + weakness_end + 1; weakness_char_ptr++)
            {
              switch (*weakness_char_ptr)
              {
                case ' ':
                case '\t':
                  working_type_start++;
                  break;
                case ';':
                case ')':
                case ',':
                {
                  size_t weakness_type_len = weakness_char_ptr - working_type_start;
                  struct attack_type_node** node_to_check;
                  for (node_to_check = &attack_type_set_head; *node_to_check != nullptr; node_to_check = &(*node_to_check)->next)
                  {
                    if (memcmp(working_type_start, (*node_to_check)->data, weakness_type_len) == 0)
                    {
                      break;
                    }
                  }
                  if (*node_to_check == nullptr)
                  {
                    *node_to_check         = malloc(sizeof(struct attack_type_node));
                    (*node_to_check)->next = nullptr;
                    (*node_to_check)->data = malloc(weakness_type_len + 1);
                    memcpy((*node_to_check)->data, working_type_start, weakness_type_len);
                    (*node_to_check)->data[weakness_type_len] = 0;
                    attack_type_set_len++;
                  }
                  struct attack_type_node** this_set_to_add;
                  for (this_set_to_add = &this_set_head; *this_set_to_add != nullptr; this_set_to_add = &(*this_set_to_add)->next);
                  *this_set_to_add         = malloc(sizeof(struct attack_type_node));
                  (*this_set_to_add)->next = nullptr;
                  (*this_set_to_add)->data = (*node_to_check)->data;
                  working_type_start = weakness_char_ptr + 1;
                  this_set_len++;
                  if (*weakness_char_ptr == ';' || *weakness_char_ptr == ')')
                  {
                    weakness_start = weakness_char_ptr - working_line + 2;
                    goto LIST_DONE;
                  }
                  break;
                }
              }
            }
LIST_DONE:  if (this_set_len > 0)
            {
              size_t*       list_num_ptr;
              char const*** list_ptr;
              if (is_immune)
              {
                list_num_ptr = &working_unit_set.num_immunities;
                list_ptr     = &working_unit_set.immunities;
              }
              else
              {
                list_num_ptr = &working_unit_set.num_weaknesses;
                list_ptr     = &working_unit_set.weaknesses;
              }
              *list_num_ptr = this_set_len;
              *list_ptr     = malloc(sizeof(char const*) * this_set_len);
              this_set_len = 0;
              while (this_set_head != nullptr)
              {
                (*list_ptr)[this_set_len] = this_set_head->data;
                this_set_len++;
                struct attack_type_node* temp = this_set_head->next;
                free(this_set_head);
                this_set_head = temp;
              }
            }
          }
          {
            struct attack_type_node** node_to_check;
            char const*               attack_type_start_ptr = working_line + attack_type_start;
            size_t                    attack_type_len       = attack_type_end - attack_type_start;
            for (node_to_check = &attack_type_set_head; *node_to_check != nullptr; node_to_check = &(*node_to_check)->next)
            {
              if (memcmp(attack_type_start_ptr, (*node_to_check)->data, attack_type_len) == 0)
              {
                break;
              }
            }
            if (*node_to_check == nullptr)
            {
              *node_to_check         = malloc(sizeof(struct attack_type_node));
              (*node_to_check)->next = nullptr;
              (*node_to_check)->data = malloc(attack_type_len + 1);
              memcpy((*node_to_check)->data, attack_type_start_ptr, attack_type_len);
              (*node_to_check)->data[attack_type_len] = 0;
              attack_type_set_len++;
            }
            working_unit_set.attack_type = (*node_to_check)->data;
          }
          vector_unit_push_back(&unit_sets[i], &working_unit_set);
        }
        free(working_line);
      }
      num_attack_types    = attack_type_set_len;
      attack_types        = malloc(sizeof(char const*) * attack_type_set_len);
      attack_type_set_len = 0;
      while (attack_type_set_head != nullptr)
      {
        attack_types[attack_type_set_len] = attack_type_set_head->data;
        attack_type_set_len++;
        struct attack_type_node* temp = attack_type_set_head->next;
        free(attack_type_set_head);
        attack_type_set_head = temp;
      }
    }
    fclose(in);
  }

  {
    struct vector_unit local_unit_sets[2];
    for (size_t i = 0; i < ARRAY_LEN(local_unit_sets); i++)
    {
      local_unit_sets[i] = vector_unit_new(vector_unit_length(&unit_sets[i]), nullptr);
      VECTOR_FOR_EACH(unit_set_ptr, &unit_sets[i])
      {
        vector_unit_push_back(&local_unit_sets[i], unit_set_ptr);
      }
    }
    do_combat(local_unit_sets);
    size_t remaining_count = 0;
    FOR_EACH(army_ptr, local_unit_sets)
    {
      VECTOR_FOR_EACH(unit_set_ptr, army_ptr)
      {
        remaining_count += unit_set_ptr->count;
      }
    }
    FOR_EACH(unit_set_ptr, local_unit_sets)
    {
      vector_unit_clean(unit_set_ptr);
    }
    printf("%zu\n", remaining_count);
  }

  {
    size_t             boost_max = UINT8_MAX;
    size_t             boost_min = 0;
    struct vector_unit local_unit_sets[2];
    for (;;)
    {
      size_t        boost = boost_min + (boost_max - boost_min) / 2;
      for (size_t i = 0; i < ARRAY_LEN(local_unit_sets); i++)
      {
        local_unit_sets[i] = vector_unit_new(vector_unit_length(&unit_sets[i]), nullptr);
        VECTOR_FOR_EACH(unit_set_ptr, &unit_sets[i])
        {
          vector_unit_push_back(&local_unit_sets[i], unit_set_ptr);
        }
      }
      VECTOR_FOR_EACH_MUT(unit_set_ptr, &local_unit_sets[0])
      {
        unit_set_ptr->attack_power += boost;
      }
      if (do_combat(local_unit_sets) == 0)
      {
        boost_max -= (boost - boost_min) + (boost == boost_min);
      }
      else
      {
        boost_min += (boost - boost_min) + (boost == boost_min);
      }
      if (boost_min > boost_max)
      {
        break;
      }
      FOR_EACH(unit_set_ptr, local_unit_sets)
      {
        vector_unit_clean(unit_set_ptr);
      }
    }
    size_t remaining_count = 0;
    FOR_EACH(army_ptr, local_unit_sets)
    {
      VECTOR_FOR_EACH(unit_set_ptr, army_ptr)
      {
        remaining_count += unit_set_ptr->count;
      }
    }
    FOR_EACH(unit_set_ptr, local_unit_sets)
    {
      vector_unit_clean(unit_set_ptr);
    }
    printf("%zu\n", remaining_count);
  }

  FOR_EACH(unit_set_ptr, unit_sets)
  {
    vector_unit_clean(unit_set_ptr);
  }
  FOR_EACH_N(attack_type_ptr, attack_types, num_attack_types)
  {
    free((void*)*attack_type_ptr);
  }
  free(attack_types);
  return return_code;
}
