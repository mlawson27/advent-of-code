#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

struct circle_entry;

struct circle_entry
{
  struct circle_entry* next;
  struct circle_entry* prev;
};

static size_t high_score_after_rounds(size_t num_players, size_t last_marble)
{
  struct circle_entry* circle_data   = malloc((last_marble + 1) * sizeof(*circle_data));
  size_t*              player_scores = calloc(num_players, sizeof(*player_scores));
  circle_data[0].next = &circle_data[0];

  struct circle_entry* current_marble_ptr = circle_data;
  for (size_t marble = 1; marble <= last_marble; marble++)
  {
    if (marble % 23 == 0)
    {
      size_t current_player = (marble - 1) % num_players;
      player_scores[current_player] += marble;
      struct circle_entry* entry_to_remove = current_marble_ptr;
      for (int i = 0; i < 7; i++)
      {
        entry_to_remove = entry_to_remove->prev;
      }
      player_scores[current_player] += (entry_to_remove - circle_data);
      entry_to_remove->prev->next = entry_to_remove->next;
      entry_to_remove->next->prev = entry_to_remove->prev;
      current_marble_ptr          = entry_to_remove->next;
    }
    else
    {
      circle_data[marble].next             = current_marble_ptr->next->next;
      circle_data[marble].prev             = current_marble_ptr->next;
      current_marble_ptr->next->next->prev = circle_data + marble;
      current_marble_ptr->next->next       = circle_data + marble;
      current_marble_ptr                   = circle_data + marble;
    }
  }
  free(circle_data);

  size_t high_score = 0;
  FOR_EACH_N(player_score_ptr, player_scores, num_players)
  {
    if (*player_score_ptr > high_score)
    {
      high_score = *player_score_ptr;
    }
  }
  free(player_scores);

  return high_score;
}

int main()
{
  size_t num_players;
  size_t last_marble;
  {
    FILE* in     = fopen("dec09.txt", "r");
    int   result = fscanf(in, " %zu players; last marble is worth %zu points", &num_players, &last_marble);
    assert(result == 2);
    fclose(in);
  }

  printf("%zu\n", high_score_after_rounds(num_players, last_marble));
  printf("%zu\n", high_score_after_rounds(num_players, last_marble * 100));

  return 0;
}
