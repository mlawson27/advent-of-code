#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "vector.h"
#include "vector_internal.h"

struct _vector_internal _vector_internal_new(size_t elem_size,
                                             size_t num_elems,
                                             void(*free_fn)(void*))
{
  void* buf = malloc(elem_size * num_elems);
  return (struct _vector_internal) {
    .elem_size = elem_size,
    .capacity  = num_elems,
    .buf       = buf,
    .head_ptr  = buf,
    .tail_ptr  = buf,
    .free_fn   = free_fn
  };
}

void _vector_internal_clean(struct _vector_internal* v)
{
  for (void* i = v->head_ptr; i != v->tail_ptr; i += v->elem_size)
  {
    if (v->free_fn != nullptr)
    {
      v->free_fn(i);
    }
  }
  free(v->buf);
}

void const* _vector_internal_get(struct _vector_internal const* v, size_t elem_index)
{
  return v->head_ptr + (elem_index * v->elem_size);
}

void _vector_internal_push_back(struct _vector_internal* v, void const* data)
{
  assert(v->tail_ptr < v->buf + (v->capacity * v->elem_size));
  memcpy(v->tail_ptr, data, v->elem_size);
  v->tail_ptr += v->elem_size;
}

void _vector_internal_sort_N(struct _vector_internal* v, size_t count, int (*comp)(const void *, const void *))
{
  size_t actual_count = _vector_internal_length(v);
  qsort(v->head_ptr, (count <= actual_count ? count : actual_count), v->elem_size, comp);
}

void _vector_internal_clear(struct _vector_internal* v)
{
  v->head_ptr = v->buf;
  v->tail_ptr = v->buf;
}
