#include "util.h"

FILE* open_and_count_newlines(char const* filename, size_t* num_new_lines)
{
  FILE* in;
  in = fopen(filename, "r");

  int c;
  *num_new_lines = 0;
  while ((c = fgetc(in)) != EOF) {
    (*num_new_lines) += (c == '\n');
  }

  fseek(in, 0, SEEK_SET);
  return in;
}
