#include <stdlib.h>
#include <string.h>

#include "hashmap.h"
#include "hashmap_internal.h"
#include "util.h"

struct _hashmap_internal_node
{
  void* key_data;
  void* value_data;

  struct _hashmap_internal_node* next;
};

static void free_node(struct _hashmap_internal const* m, struct _hashmap_internal_node* n)
{
  if (n->next != nullptr)
  {
    free_node(m, n->next);
  }

  if (m->key_free_fn != nullptr)
  {
    m->key_free_fn(n->key_data);
  }
  if (m->value_free_fn != nullptr)
  {
    m->key_free_fn(n->value_data);
  }

  free(n);
}

static size_t hash(void const* data, size_t len, size_t bucket_count)
{
  size_t ret         = 0;
  size_t mult_factor = 17 << ((bucket_count / 256) - 1);
  FOR_EACH_N(b, (unsigned char const*)data, len)
  {
    ret = ((ret + *b) * mult_factor) % bucket_count;
  }
  return ret;
}

struct _hashmap_internal _hashmap_internal_new_with_num_buckets(size_t                        bucket_count,
                                                                size_t                        key_size,
                                                                size_t                        value_size,
                                                                _hashmap_internal_mem_free_fn custom_key_free_fn,
                                                                _hashmap_internal_mem_free_fn custom_value_free_fn)
{
  return (struct _hashmap_internal) {
    .buckets       = calloc(bucket_count, sizeof(struct _hashmap_internal_node**)),
    .bucket_count  = bucket_count,
    .count         = 0,
    .key_size      = key_size,
    .value_size    = value_size,
    .key_init_fn   = (key_size   <= sizeof(void*) ? _hashmap_internal_init_primitive : _hashmap_internal_init_copy),
    .key_get_fn    = (key_size   <= sizeof(void*) ? _hashmap_internal_get_primitive  : _hashmap_internal_get_copy),
    .key_free_fn   = custom_key_free_fn,
    .value_init_fn = (value_size <= sizeof(void*) ? _hashmap_internal_init_primitive : _hashmap_internal_init_copy),
    .value_get_fn  = (value_size <= sizeof(void*) ? _hashmap_internal_get_primitive  : _hashmap_internal_get_copy),
    .value_free_fn = custom_value_free_fn
  };
}

struct _hashmap_internal _hashmap_internal_clone(struct _hashmap_internal const* m)
{
  struct _hashmap_internal ret = (struct _hashmap_internal) {
    .buckets       = calloc(m->bucket_count, sizeof(struct _hashmap_internal_node**)),
    .bucket_count  = m->bucket_count,
    .count         = m->count,
    .key_size      = m->key_size,
    .value_size    = m->value_size,
    .key_init_fn   = m->key_init_fn,
    .key_get_fn    = m->key_get_fn,
    .key_free_fn   = m->key_free_fn,
    .value_init_fn = m->value_init_fn,
    .value_get_fn  = m->value_get_fn,
    .value_free_fn = m->value_free_fn,
  };

  size_t key_size_to_alloc = (m->key_size   > sizeof(m->buckets[0]->key_data)   ? m->key_size   : 0);
  size_t val_size_to_alloc = (m->value_size > sizeof(m->buckets[0]->value_data) ? m->value_size : 0);

  for (size_t i = 0; i < m->bucket_count; i++)
  {
    struct _hashmap_internal_node** node_ptr = &ret.buckets[i];
    for (struct _hashmap_internal_node const* node = m->buckets[i]; node != nullptr; node = node->next)
    {
      *node_ptr = malloc(sizeof(struct _hashmap_internal_node) + key_size_to_alloc + val_size_to_alloc);
      *(*node_ptr) = (struct _hashmap_internal_node){
        .next = nullptr
      };
      (*node_ptr)->key_data = (void*)*node_ptr + sizeof(struct _hashmap_internal_node);
      m->key_init_fn(&(*node_ptr)->key_data, &node->key_data, m->key_size);
      if (m->value_init_fn != nullptr)
      {
        (*node_ptr)->value_data = (void*)*node_ptr + sizeof(struct _hashmap_internal_node) + key_size_to_alloc;
        m->value_init_fn(&(*node_ptr)->value_data, &node->value_data, m->value_size);
      }
      node_ptr = &(*node_ptr)->next;
    }
  }
  return ret;
}

void _hashmap_internal_clean(struct _hashmap_internal* m)
{
  FOR_EACH_N(bucket_ptr, m->buckets, m->bucket_count)
  {
    if (*bucket_ptr != nullptr)
    {
      free_node(m, *bucket_ptr);
      *bucket_ptr = nullptr;
    }
  }
  free(m->buckets);
  m->count = 0;
}


void const* _hashmap_internal_try_find(struct _hashmap_internal const* m, void const* key)
{
  struct _hashmap_internal_node const* node;
  for (node = m->buckets[hash(key, m->key_size, m->bucket_count)]; node != nullptr; node = node->next)
  {
    if (memcmp(key, m->key_get_fn((void const* const*)&node->key_data), m->key_size) == 0)
    {
      return m->value_get_fn((void const**)&node->value_data);
    }
  }
  return nullptr;
}

bool _hashmap_internal_insert(struct _hashmap_internal* m, void const* key, void const* value)
{
  struct _hashmap_internal_node** node_ptr;
  for (node_ptr = &m->buckets[hash(key, m->key_size, m->bucket_count)]; *node_ptr != nullptr; node_ptr = &(*node_ptr)->next)
  {
    if (memcmp(key, m->key_get_fn((void const* const*)&(*node_ptr)->key_data), m->key_size) == 0)
    {
      if ((*node_ptr)->value_data != value)
      {
        if (m->value_free_fn != nullptr)
        {
          m->value_free_fn((*node_ptr)->value_data);
        }
        m->value_init_fn(&(*node_ptr)->value_data, value, m->value_size);
      }
      return false;
    }
  }

  size_t key_size_to_alloc = (m->key_size   > sizeof((*node_ptr)->key_data)   ? m->key_size   : 0);
  size_t val_size_to_alloc = (m->value_size > sizeof((*node_ptr)->value_data) ? m->value_size : 0);
  *node_ptr = malloc(sizeof(struct _hashmap_internal_node) + key_size_to_alloc + val_size_to_alloc);
  *(*node_ptr) = (struct _hashmap_internal_node){
    .next = nullptr
  };
  (*node_ptr)->key_data = (void*)*node_ptr + sizeof(struct _hashmap_internal_node);
  m->key_init_fn(&(*node_ptr)->key_data, key, m->key_size);
  if (m->value_init_fn != nullptr)
  {
    (*node_ptr)->value_data = (void*)*node_ptr + sizeof(struct _hashmap_internal_node) + key_size_to_alloc;
    m->value_init_fn(&(*node_ptr)->value_data, value, m->value_size);
  }
  m->count++;
  return true;
}

bool _hashmap_internal_remove(struct _hashmap_internal* m, void const* key)
{
  for (struct _hashmap_internal_node** node = &m->buckets[hash(key, m->key_size, m->bucket_count)]; *node != nullptr; node = &(*node)->next)
  {
    if (memcmp(key, m->key_get_fn((void const* const*)&(*node)->key_data), m->key_size) == 0)
    {
      struct _hashmap_internal_node* to_delete = *node;
      *node = to_delete->next;
      to_delete->next = nullptr;
      free_node(m, to_delete);
      m->count--;
      return true;
    }
  }
  return false;
}

void const* _hashmap_internal_iterator_key(struct _hashmap_internal_iterator const* it)
{
  return it->map->key_get_fn((void const**)&it->bucket_item_ptr->key_data);
}

void const* _hashmap_internal_iterator_value(struct _hashmap_internal_iterator const* it)
{
  return it->map->value_get_fn((void const**)&it->bucket_item_ptr->value_data);
}

struct _hashmap_internal_iterator _hashmap_internal_iterate_next(struct _hashmap_internal_iterator* it)
{
  struct _hashmap_internal_iterator ret = *it;
  if (ret.bucket_item_ptr != nullptr)
  {
    ret.bucket_item_ptr = ret.bucket_item_ptr->next;
  }
  if (ret.bucket_item_ptr != nullptr)
  {
    return ret;
  }
  do
  {
    ret.bucket_ptr++;
  } while (ret.bucket_ptr < &it->map->buckets[it->map->bucket_count] && *ret.bucket_ptr == nullptr);
  if (ret.bucket_ptr >= &it->map->buckets[it->map->bucket_count])
  {
    ret.bucket_ptr      = nullptr;
    ret.bucket_item_ptr = nullptr;
  }
  else
  {
    ret.bucket_item_ptr = *ret.bucket_ptr;
  }
  return ret;
}


void _hashmap_internal_init_primitive(void** result, void const* src, size_t size)
{
  memcpy((void*)result, src, size);
  memset((void*)result + size, 0, sizeof(result) - size);
}

void _hashmap_internal_init_copy(void** result, void const* src, size_t size)
{
  memcpy(*result, src, size);
}
