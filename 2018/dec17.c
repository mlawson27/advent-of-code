#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "hashset.h"

typedef uint32_t point_t[2];

MAKE_HASHSET_TYPE(pt, point_t)

static bool traverse(point_t starting_point, struct hashset_pt const* clay_points, struct hashset_pt* encountered_water_points, struct hashset_pt* ending_water_points, uint32_t max_y)
{
  if (hashset_pt_contains(encountered_water_points, (point_t*)starting_point))
  {
    return true;
  }
  hashset_pt_insert(encountered_water_points, (point_t*)starting_point);

  point_t working_point;
  memcpy(working_point, starting_point, sizeof(point_t));
  for (point_t next_point = { starting_point[0], starting_point[1] };;)
  {
    next_point[1]++;
    if (next_point[1] > max_y)
    {
      return true;
    }
    if (hashset_pt_contains(clay_points, (point_t*)next_point) || hashset_pt_contains(ending_water_points, (point_t*)next_point))
    {
      break;
    }
    memcpy(working_point, next_point, sizeof(point_t));
    hashset_pt_insert(encountered_water_points, (point_t*)working_point);
  }

  for(;;)
  {
    if (memcmp(working_point, starting_point, sizeof(point_t)) == 0)
    {
      return false;
    }
    uint32_t left_x, right_x;
    bool     left_in_clay, right_in_clay;
    {
      point_t left_right, left_right_down;
      memcpy(left_right,      working_point, sizeof(point_t));
      memcpy(left_right_down, working_point, sizeof(point_t));
      left_right_down[1]++;
      while (!(left_in_clay = hashset_pt_contains(clay_points, (point_t*)left_right)) && (hashset_pt_contains(clay_points, (point_t*)left_right_down) || hashset_pt_contains(ending_water_points, (point_t*)left_right_down)))
      {
        hashset_pt_insert(encountered_water_points, (point_t*)left_right);
        left_right[0]--;
        left_right_down[0]--;
      }
      left_x             = left_right[0];
      left_right[0]      = working_point[0];
      left_right_down[0] = working_point[0];
      while (!(right_in_clay = hashset_pt_contains(clay_points, (point_t*)left_right)) && (hashset_pt_contains(clay_points, (point_t*)left_right_down) || hashset_pt_contains(ending_water_points, (point_t*)left_right_down)))
      {
        hashset_pt_insert(encountered_water_points, (point_t*)left_right);
        left_right[0]++;
        left_right_down[0]++;
      }
      right_x = left_right[0];
    };

    bool fell_off = false;
    if (left_in_clay)
    {
      if (right_in_clay)
      {
        for (point_t left_right = { left_x + 1, working_point[1] }; left_right[0] < right_x; left_right[0]++)
        {
          hashset_pt_insert(ending_water_points, (point_t*)left_right);
        }
        working_point[1]--;
      }
    }
    else
    {
      point_t left_right = { left_x, working_point[1] };
      fell_off           = traverse(left_right, clay_points, encountered_water_points, ending_water_points, max_y);
    }
    if (!right_in_clay)
    {
      point_t left_right = { right_x, working_point[1] };
      fell_off           = traverse(left_right, clay_points, encountered_water_points, ending_water_points, max_y) || fell_off;
    }
    if (fell_off)
    {
      return true;
    }
  }
}

int main()
{
  int               return_code = 0;
  struct hashset_pt clay_points;
  uint32_t          min_y       = UINT32_MAX;
  uint32_t          max_y       = 0;
  {
    FILE* in    = fopen("dec17.txt", "r");
    clay_points = hashset_pt_new(nullptr);
    uint32_t left, right_low, right_high;
    char left_xy, right_xy;
    while (fscanf(in, " %c=%u, %c=%u..%u", &left_xy, &left, &right_xy, &right_low, &right_high) == 5)
    {
      point_t point;
      point[left_xy == 'y'] = left;
      for (uint32_t right = right_low; right <= right_high; right++)
      {
        point[right_xy == 'y'] = right;
        hashset_pt_insert(&clay_points, (point_t*)point);
        uint32_t y = right_xy == 'y' ? right : left;
        min_y = y < min_y ? y : min_y;
        max_y = y > max_y ? y : max_y;
      }
    }
    fclose(in);
  }

  {
    struct hashset_pt ending_water_points      = hashset_pt_new(nullptr);
    struct hashset_pt encountered_water_points = hashset_pt_new(nullptr);
    point_t           starting_point           = { 500, min_y };
    traverse(starting_point, &clay_points, &encountered_water_points, &ending_water_points, max_y);
    printf("%zu\n", hashset_pt_len(&encountered_water_points));
    hashset_pt_clean(&ending_water_points);
    hashset_pt_clean(&encountered_water_points);
  }

  {
    struct hashset_pt ending_water_points      = hashset_pt_new(nullptr);
    struct hashset_pt encountered_water_points = hashset_pt_new(nullptr);
    point_t           starting_point           = { 500, min_y };
    traverse(starting_point, &clay_points, &encountered_water_points, &ending_water_points, max_y);
    printf("%zu\n", hashset_pt_len(&ending_water_points));
    hashset_pt_clean(&ending_water_points);
    hashset_pt_clean(&encountered_water_points);
  }

  hashset_pt_clean(&clay_points);
  return return_code;
}
