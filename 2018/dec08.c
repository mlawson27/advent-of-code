#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

struct node;

struct node
{
  struct node* child_nodes;
  size_t       num_child_nodes;
  int*         metadata_entries;
  size_t       num_metadata_entries;
};

static void parse_subtree(FILE* in, struct node* n)
{
  int result = fscanf(in, " %zu %zu", &n->num_child_nodes, &n->num_metadata_entries);
  assert(result == 2);
  n->child_nodes = malloc(n->num_child_nodes      * sizeof(n->child_nodes[0]) +
                          n->num_metadata_entries * sizeof(n->metadata_entries[0]));
  n->metadata_entries = (int*)(n->child_nodes + n->num_child_nodes);
  for (size_t i = 0; i < n->num_child_nodes; i++)
  {
    parse_subtree(in, n->child_nodes + i);
  }
  for (size_t i = 0; i < n->num_metadata_entries; i++)
  {
    int result = fscanf(in, " %i", n->metadata_entries + i);
    assert(result == 1);
  }
}

static void free_node_tree(struct node* n)
{
  FOR_EACH_N(subnode, n->child_nodes, n->num_child_nodes)
  {
    free_node_tree(subnode);
  }
  free(n->child_nodes);
}

static int node_tree_metadata_sum_p1(struct node* n)
{
  int result = 0;
  FOR_EACH_N(subnode, n->child_nodes, n->num_child_nodes)
  {
    result += node_tree_metadata_sum_p1(subnode);
  }
  FOR_EACH_N(metadata_value_ptr, n->metadata_entries, n->num_metadata_entries)
  {
    result += *metadata_value_ptr;
  }
  return result;
}

static int node_tree_metadata_sum_p2(struct node* n)
{
  if (n->num_child_nodes == 0)
  {
    return node_tree_metadata_sum_p1(n);
  }

  int result = 0;
  FOR_EACH_N(metadata_value_ptr, n->metadata_entries, n->num_metadata_entries)
  {
    int index = *metadata_value_ptr - 1;
    if (index >= 0 && index < n->num_child_nodes)
    {
      result += node_tree_metadata_sum_p2(n->child_nodes + index);
    }
  }
  return result;
}

int main()
{
  struct node head;
  {
    FILE* in = fopen("dec08.txt", "r");
    parse_subtree(in, &head);
    fclose(in);
  }

  printf("%i\n", node_tree_metadata_sum_p1(&head));
  printf("%i\n", node_tree_metadata_sum_p2(&head));

  free_node_tree(&head);
  return 0;
}
