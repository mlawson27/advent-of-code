#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashmap.h"
#include "hashset.h"
#include "priority_queue.h"
#include "util.h"

enum tool
{
  NONE,
  TORCH,
  CLIMBING_GEAR,
  _TOOL_COUNT
};

typedef uint32_t position_t[2];

struct p2_state
{
  position_t position;
  uint16_t   time_so_far;
  enum tool  tool;
};

struct p2_seen_state
{
  position_t position;
  enum tool tool;
};

MAKE_HASHMAP_TYPE(el,  position_t,           uint_fast16_t)
MAKE_HASHMAP_TYPE(tts, struct p2_seen_state, uint16_t)

MAKE_PRIORITY_QUEUE_TYPE(p2, struct p2_state)

static inline bool try_get_erosion_level_base(uint32_t const target[], size_t depth, uint32_t x, uint32_t y, uint_fast16_t* ret)
{
  if (x == target[0] && y == target[1])
  {
    *ret = (uint_fast16_t)(depth % 20183);
    return true;
  }
  if (y == 0)
  {
    *ret = (uint_fast16_t)(((uint64_t)x * 16807 + depth) % 20183);
    return true;
  }
  if (x == 0)
  {
    *ret = (uint_fast16_t)(((uint64_t)y* 48271 + depth) % 20183);
    return true;
  }
  return false;
}

static inline uint_fast16_t get_erosion_level_arr(uint_fast16_t const* erosion_levels, uint32_t const target[], size_t depth, uint32_t x, uint32_t y)
{
  uint_fast16_t base_ret;
  if (try_get_erosion_level_base(target, depth, x, y, &base_ret))
  {
    return base_ret;
  }

  uint64_t base = 1;
  if (x - 1 <= target[0] && y <= target[1])
  {
    base *= erosion_levels[y * (target[0] + 1) + (x - 1)];
  }
  else
  {
    base *= get_erosion_level_arr(erosion_levels, target, depth, x - 1, y);
  }
  if (y - 1 <= target[1] && x <= target[0])
  {
    base *= erosion_levels[(y - 1) * (target[0] + 1) + x];
  }
  else
  {
    base *= get_erosion_level_arr(erosion_levels, target, depth, x, y - 1);
  }
  return (uint_fast16_t)((base + depth) % 20183);
}

static inline uint_fast16_t get_erosion_level_map(struct hashmap_el* erosion_levels, uint32_t const target[], size_t depth, uint32_t x, uint32_t y)
{
  uint_fast16_t base_ret;
  if (try_get_erosion_level_base(target, depth, x, y, &base_ret))
  {
    return base_ret;
  }

  position_t test_pos = { x, y };
  uint_fast16_t const* working;
  if ((working = hashmap_el_try_find(erosion_levels, (position_t*)test_pos)) != nullptr)
  {
    return *working;
  }
  uint_fast16_t ret = (uint_fast16_t)((((uint64_t)get_erosion_level_map(erosion_levels, target, depth, x - 1, y) *
                                        (uint64_t)get_erosion_level_map(erosion_levels, target, depth, x, y - 1)) + depth) % 20183);
  hashmap_el_insert((struct hashmap_el*)erosion_levels, (position_t*)test_pos, &ret);
  return ret;
}

static inline uint32_t distance_between_points(uint32_t current[2], uint32_t target[2])
{
  return ((current[0] > target[0] ? current[0] - target[0] : target[0] - current[0]) +
          (current[1] > target[1] ? current[1] - target[1] : target[1] - current[1]));
}

int main()
{
  int      return_code  = 0;
  uint32_t target[2];
  size_t   depth;
  {
    FILE*  in          = fopen("dec22.txt", "r");
    size_t depth_read  = fscanf(in, " depth: %zu", &depth);
    size_t target_read = fscanf(in, " target: %u, %u", &target[0], &target[1]);
    assert(depth_read == 1);
    assert(target_read == 2);
    fclose(in);
  }

  {
    uint_fast16_t* erosion_levels = malloc((target[0] + 1) * (target[1] + 1) * sizeof(uint_fast16_t));
    for (uint32_t y = 0; y <= target[1]; y++)
    {
      for (uint32_t x = 0; x <= target[0]; x++)
      {
        erosion_levels[y * (target[0] + 1) + x] = get_erosion_level_arr(erosion_levels, target, depth, x, y);
      }
    }
    size_t risk_level_sum = 0;
    for (uint32_t y = 0; y <= target[1]; y++)
    {
      for (uint32_t x = 0; x <= target[0]; x++)
      {
        risk_level_sum += erosion_levels[y * (target[0] + 1) + x] % 3;
      }
    }
    free(erosion_levels);
    printf("%zu\n", risk_level_sum);
  }

  {
    inline size_t to_priority(uint32_t distance_to_target, uint16_t time_so_far)
    {
      assert(distance_to_target < UINT16_MAX);
      return (time_so_far << 16) | (uint16_t)distance_to_target;
    }

    static constexpr enum tool DISALLOWED_TOOL_FOR_REGION[] = { NONE, TORCH, CLIMBING_GEAR };

    struct hashmap_el        erosion_levels       = hashmap_el_new(nullptr, nullptr);
    struct priority_queue_p2 state_queue          = priority_queue_p2_new(nullptr);
    struct hashmap_tts       lowest_time_to_state = hashmap_tts_new(nullptr, nullptr);
    struct p2_state          working_state        = { .position = {}, .time_so_far = 0, .tool = TORCH };
    size_t                   initial_distance     = distance_between_points(working_state.position, target);
    size_t                   min_distance         = initial_distance;
    uint16_t const*          time_to_neighbor     = nullptr;
    priority_queue_p2_push(&state_queue, 0, &working_state);
    hashmap_tts_insert(&lowest_time_to_state, &(struct p2_seen_state){ .position = {}, .tool = working_state.tool }, &working_state.time_so_far);
    while (!priority_queue_p2_is_empty(&state_queue))
    {
      struct p2_seen_state ss;
      priority_queue_p2_pop(&state_queue, &working_state);
      if (memcmp(working_state.position, target, sizeof(target)) == 0 && working_state.tool == TORCH)
      {
        break;
      }
      uint32_t distance_remaining = distance_between_points(working_state.position, target);
      if (distance_remaining < min_distance)
      {
        min_distance = distance_remaining;
      }
      else if (distance_remaining > min_distance + (initial_distance / 10))
      {
        continue;
      }
      working_state.time_so_far += 7;
      enum tool current_tool = working_state.tool;
      for (enum tool next_tool = (enum tool)0; next_tool < _TOOL_COUNT; next_tool++)
      {
        if (next_tool == current_tool ||
            next_tool == DISALLOWED_TOOL_FOR_REGION[get_erosion_level_map(&erosion_levels, target, depth, working_state.position[0], working_state.position[1]) % 3])
        {
          continue;
        }
        memcpy(ss.position, working_state.position, sizeof(ss.position));
        ss.tool = next_tool;
        if ((time_to_neighbor = hashmap_tts_try_find(&lowest_time_to_state, &ss)) == nullptr || *time_to_neighbor > working_state.time_so_far)
        {
          working_state.tool = next_tool;
          hashmap_tts_insert(&lowest_time_to_state, &ss, &working_state.time_so_far);
          priority_queue_p2_push(&state_queue, to_priority(distance_remaining, working_state.time_so_far), &working_state);
        }
      }
      working_state.time_so_far    -= 6;
      working_state.tool            = current_tool;
      uint32_t next_positions[][2]  = { { working_state.position[0] - 1, working_state.position[1] },
                                        { working_state.position[0] + 1, working_state.position[1] },
                                        { working_state.position[0],     working_state.position[1] - 1 },
                                        { working_state.position[0],     working_state.position[1] + 1 } };
      FOR_EACH(next_position_ptr, next_positions)
      {
        if ((*next_position_ptr)[0] < INT32_MAX &&
            (*next_position_ptr)[1] < INT32_MAX &&
            working_state.tool != DISALLOWED_TOOL_FOR_REGION[get_erosion_level_map(&erosion_levels, target, depth, (*next_position_ptr)[0], (*next_position_ptr)[1]) % 3])
        {
          memcpy(ss.position, *next_position_ptr, sizeof(ss.position));
          ss.tool = working_state.tool;
          if ((time_to_neighbor = hashmap_tts_try_find(&lowest_time_to_state, &ss)) == nullptr || *time_to_neighbor > working_state.time_so_far)
          {
            memcpy(working_state.position, *next_position_ptr, sizeof(working_state.position));
            hashmap_tts_insert(&lowest_time_to_state, &ss, &working_state.time_so_far);
            priority_queue_p2_push(&state_queue, to_priority(distance_between_points(working_state.position, target), working_state.time_so_far), &working_state);
          }
        }
      }
    }
    hashmap_el_clean(&erosion_levels);
    priority_queue_p2_clean(&state_queue);
    hashmap_tts_clean(&lowest_time_to_state);
    printf("%u\n", working_state.time_so_far);
  }
  return return_code;
}
