#include <stdlib.h>
#include <string.h>

#include "hashmap.h"
#include "vector.h"
#include "util.h"

const char FALLS_ASLEEP_STR[] = "falls asleep";
const char WAKES_UP_STR[]     = "wakes up";

enum log_entry_type
{
  LOG_ENTRY_BEGIN_SHIFT  = 0,
  LOG_ENTRY_FALLS_ASLEEP = 1,
  LOG_ENTRY_WAKES_UP     = 2
};

struct log_entry
{
  enum log_entry_type entry_type;
  unsigned short      year;
  unsigned short      month;
  unsigned short      day;
  unsigned short      hour;
  unsigned short      minute;
  struct
  {
    unsigned int guard_id;
  } begins_shift;
};

struct guard_data
{
  unsigned int per_minute[60];
  unsigned int total;
};

MAKE_VECTOR_TYPE(entry,     struct log_entry)
MAKE_VECTOR_TYPE(entry_ptr, struct log_entry const*)

MAKE_HASHMAP_TYPE(guard, unsigned int, struct guard_data)

static inline long long entry_to_int(struct log_entry const* e)
{
  return ((long long)e->year   * 1'00'00'00'00) +
         ((long long)e->month  *    1'00'00'00) +
         ((long long)e->day    *       1'00'00) +
         ((long long)e->hour   *          1'00) +
         ((long long)e->minute *             1);
}

static int compare_log_entries(struct log_entry const* const* e1_ptr, struct log_entry const* const* e2_ptr)
{
  long long e1_int = entry_to_int(*e1_ptr);
  long long e2_int = entry_to_int(*e2_ptr);

  return (e1_int > e2_int) - (e2_int > e1_int);
}

static const struct guard_data EMPTY_GUARD_DATA = {
  .per_minute = {},
  .total      = 0
};

int main()
{
  struct vector_entry     entry_vec;
  struct vector_entry_ptr sorted_entry_vec;
  struct hashmap_guard    guard_to_time_map;
  {
    size_t num_new_lines;
    FILE* in = open_and_count_newlines("dec04.txt", &num_new_lines);

    entry_vec         = vector_entry_new    (num_new_lines + 1, nullptr);
    sorted_entry_vec  = vector_entry_ptr_new(num_new_lines + 1, nullptr);
    guard_to_time_map = hashmap_guard_new(nullptr, nullptr);

    struct log_entry working_entry;
    int              result;
    while ((result = fscanf(in, " [%hu-%hu-%hu %hu:%hu] ", &working_entry.year,
                                                           &working_entry.month,
                                                           &working_entry.day,
                                                           &working_entry.hour,
                                                           &working_entry.minute)) > 0)
    {
      char rest_of_log[64];
      fgets(rest_of_log, sizeof(rest_of_log), in);
      size_t rest_of_log_len = strlen(rest_of_log);
      if (rest_of_log[rest_of_log_len - 1] == '\n')
      {
        rest_of_log[rest_of_log_len - 1] = 0;
      }
      unsigned int guard_id;
      if ((result = sscanf(rest_of_log, "Guard #%u begins shift", &guard_id)) > 0)
      {
        working_entry.entry_type            = LOG_ENTRY_BEGIN_SHIFT;
        working_entry.begins_shift.guard_id = guard_id;
        if (hashmap_guard_try_find(&guard_to_time_map, &guard_id) == nullptr)
        {
          hashmap_guard_insert(&guard_to_time_map, &guard_id, &EMPTY_GUARD_DATA);
        }
      }
      else if (strncmp(rest_of_log, FALLS_ASLEEP_STR, sizeof(FALLS_ASLEEP_STR)) == 0)
      {
        working_entry.entry_type = LOG_ENTRY_FALLS_ASLEEP;
      }
      else if (strncmp(rest_of_log, WAKES_UP_STR, sizeof(WAKES_UP_STR)) == 0)
      {
        working_entry.entry_type = LOG_ENTRY_WAKES_UP;
      }
      else
      {
        printf("Bad entry type\n");
      }
      vector_entry_push_back(&entry_vec, &working_entry);
    }

    fclose(in);

    VECTOR_FOR_EACH(entry_ptr, &entry_vec)
    {
      vector_entry_ptr_push_back(&sorted_entry_vec, &entry_ptr);
    }

    vector_entry_ptr_sort(&sorted_entry_vec, compare_log_entries);
  }

  {
    unsigned int             max_guard_id_p1   = 0;
    unsigned int             max_guard_id_p2   = 0;
    struct guard_data const* max_guard_data    = nullptr;
    unsigned int             max_minute_val_p2 = 0;
    unsigned int             max_minute_p2     = 0;
    struct log_entry  const* last_begin_shift  = nullptr;
    struct log_entry  const* last_falls_asleep = nullptr;
    VECTOR_FOR_EACH(entry_ptr_ptr, &sorted_entry_vec)
    {
      switch ((*entry_ptr_ptr)->entry_type)
      {
        case LOG_ENTRY_BEGIN_SHIFT:
          last_begin_shift = *entry_ptr_ptr;
          break;
        case LOG_ENTRY_FALLS_ASLEEP:
          last_falls_asleep = *entry_ptr_ptr;
          break;
        case LOG_ENTRY_WAKES_UP:
        {
          struct guard_data* guard_data = hashmap_guard_try_find_mut(&guard_to_time_map, &last_begin_shift->begins_shift.guard_id);
          for (unsigned short min = last_falls_asleep->minute; min != (*entry_ptr_ptr)->minute; min = (min + 1) % 60)
          {
            guard_data->per_minute[min]++;
            guard_data->total++;
            if (guard_data->per_minute[min] > max_minute_val_p2)
            {
              max_guard_id_p2   = last_begin_shift->begins_shift.guard_id;
              max_minute_p2     = min;
              max_minute_val_p2 = guard_data->per_minute[min];
            }
          }
          if (max_guard_data == nullptr || guard_data->total > max_guard_data->total)
          {
            max_guard_data  = guard_data;
            max_guard_id_p1 = last_begin_shift->begins_shift.guard_id;
          }
          break;
        }
      }
    }
    unsigned int max_minute_val_p1 = 0;
    unsigned int max_minute_p1     = 0;
    for (unsigned int min = 0; min < ARRAY_LEN(max_guard_data->per_minute); min++)
    {
      if (max_guard_data->per_minute[min] > max_minute_val_p1)
      {
        max_minute_p1     = min;
        max_minute_val_p1 = max_guard_data->per_minute[min];
      }
    }
    printf("%u\n", max_minute_p1 * max_guard_id_p1);
    printf("%u\n", max_minute_p2 * max_guard_id_p2);
  }

  vector_entry_clean    (&entry_vec);
  vector_entry_ptr_clean(&sorted_entry_vec);
  hashmap_guard_clean(&guard_to_time_map);
  return 0;
}