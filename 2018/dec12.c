#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

#define INITIAL_STATE_STR "initial state:"

int main()
{
  char*  initial_state;
  size_t initial_state_len;
  char   rules[1 << 5];
  {
    FILE* in = fopen("dec12.txt", "r");
    int   prefix_len;
    int   result = fscanf(in, INITIAL_STATE_STR " %n%*s%zn", &prefix_len, &initial_state_len);
    assert(result == 0);
    initial_state_len -= prefix_len;
    initial_state = malloc(initial_state_len + 1);
    fseek(in, 0, SEEK_SET);
    result = fscanf(in, INITIAL_STATE_STR " %s", initial_state);
    assert(result == 1);
    memset(rules, '.', sizeof(rules));
    char rule_in[5];
    char rule_out;
    while (fscanf(in, " %5c => %c", rule_in, &rule_out) == 2)
    {
      size_t rule_v = 0;
      for (int i = 0; i < ARRAY_LEN(rule_in); i++)
      {
        rule_v |= ((rule_in[ARRAY_LEN(rule_in) - i - 1]) == '#') << i;
      }
      rules[rule_v] = rule_out;
    }
    fclose(in);
  }

  {
    char* working_state = malloc(initial_state_len);
    memcpy(working_state, initial_state, initial_state_len);
    int    last_plant_sum        = 0;
    int    last_plant_sum_diff   = 0;
    int    plant_diff_same_count = 0;
    size_t round;
    for (round = 1; ; round++)
    {
      size_t working_state_len = initial_state_len + 4 * round;
      char*  new_working_state = malloc(working_state_len);
      size_t working_sum = 0;
      for (int i = 0; i < working_state_len - 4; i++)
      {
        working_sum          = ((working_sum & 0xF) << 1) | (working_state[i] == '#');
        new_working_state[i] = rules[working_sum];
      }
      for (int i = working_state_len - 4; i < working_state_len; i++)
      {
        working_sum          = ((working_sum & 0xF) << 1);
        new_working_state[i] = rules[working_sum];
      }
      free(working_state);
      working_state = new_working_state;
      int plant_sum = 0;
      for (int i = -(int)(round * 2); i < (int)(initial_state_len + round * 2); i++)
      {
        plant_sum += i * (working_state[i + (int)round * 2] == '#');
      }
      if (round == 20)
      {
        printf("%i\n", plant_sum);
      }
      int plant_sum_diff    = plant_sum - last_plant_sum;
      plant_diff_same_count = (plant_diff_same_count + 1) * (plant_sum_diff == last_plant_sum_diff);
      last_plant_sum        = plant_sum;
      if (plant_diff_same_count == 10)
      {
        break;
      }
      last_plant_sum_diff = plant_sum_diff;
    }
    free(working_state);
    printf("%" PRIi64 "\n", last_plant_sum + (INT64_C(50000000000) - (int)round) * last_plant_sum_diff);
  }

  free(initial_state);
  return 0;
}
