#ifndef __HASHMAP_INTERNAL_H__
#define __HASHMAP_INTERNAL_H__

#ifndef __HASHMAP_H__
#error "Only include hashmap_internal.h from hashmap.h"
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct _hashmap_internal_node;

typedef void(*_hashmap_internal_mem_init_fn)(void**, void const*, size_t);
typedef void(*_hashmap_internal_mem_free_fn)(void*);

struct _hashmap_internal
{
  struct _hashmap_internal_node** buckets;
  size_t bucket_count;
  size_t count;

  size_t key_size;
  size_t value_size;

  _hashmap_internal_mem_init_fn key_init_fn;
  void const*(*key_get_fn)(void const* const*);
  _hashmap_internal_mem_free_fn key_free_fn;
  _hashmap_internal_mem_init_fn value_init_fn;
  void const*(*value_get_fn)(void const* const*);
  _hashmap_internal_mem_free_fn value_free_fn;
};

struct _hashmap_internal _hashmap_internal_new_with_num_buckets(size_t                        bucket_count,
                                                                size_t                        key_size,
                                                                size_t                        value_size,
                                                                _hashmap_internal_mem_free_fn custom_key_free_fn,
                                                                _hashmap_internal_mem_free_fn custom_value_free_fn);

static inline struct _hashmap_internal _hashmap_internal_new(size_t                        key_size,
                                                             size_t                        value_size,
                                                             _hashmap_internal_mem_free_fn custom_key_free_fn,
                                                             _hashmap_internal_mem_free_fn custom_value_free_fn)
{
  return _hashmap_internal_new_with_num_buckets(256,
                                                key_size,
                                                value_size,
                                                custom_key_free_fn,
                                                custom_value_free_fn);
}

struct _hashmap_internal _hashmap_internal_clone(struct _hashmap_internal const* m);

void _hashmap_internal_clean(struct _hashmap_internal* m);

static inline size_t _hashmap_internal_len(struct _hashmap_internal const* m)
{
  return m->count;
}

void const* _hashmap_internal_try_find(struct _hashmap_internal const* m, void const* key);

static inline void* _hashmap_internal_try_find_mut(struct _hashmap_internal* m, void const* key)
{
  return (void**)_hashmap_internal_try_find(m, key);
}

bool _hashmap_internal_insert(struct _hashmap_internal* m, void const* key, void const* value);

bool _hashmap_internal_remove(struct _hashmap_internal* m, void const* key);

static inline void _hashmap_internal_clear(struct _hashmap_internal* m)
{
  _hashmap_internal_clean(m);
}


struct _hashmap_internal_iterator
{
  struct _hashmap_internal*       map;
  struct _hashmap_internal_node** bucket_ptr;
  struct _hashmap_internal_node*  bucket_item_ptr;
};

void const* _hashmap_internal_iterator_key(struct _hashmap_internal_iterator const* it);
void const* _hashmap_internal_iterator_value(struct _hashmap_internal_iterator const* it);

struct _hashmap_internal_iterator _hashmap_internal_iterate_next(struct _hashmap_internal_iterator* it);

static inline struct _hashmap_internal_iterator _hashmap_internal_iterate(struct _hashmap_internal* m)
{
  return _hashmap_internal_iterate_next(&(struct _hashmap_internal_iterator) {
    .map             = m,
    .bucket_ptr      = (struct _hashmap_internal_node**)(((uintptr_t)m->buckets) - sizeof(void*)),
    .bucket_item_ptr = nullptr,
  });
}

static inline bool _hashmap_internal_iterator_valid(struct _hashmap_internal_iterator const* it)
{
  return it->bucket_ptr != nullptr;
}


void _hashmap_internal_init_primitive(void** result, void const* src, size_t size);
void _hashmap_internal_init_copy     (void** result, void const* src, size_t size);

static inline void const* _hashmap_internal_get_primitive(void const* const* data)
{
  return data;
}

static inline void const* _hashmap_internal_get_copy(void const* const* data)
{
  return *data;
}

#endif