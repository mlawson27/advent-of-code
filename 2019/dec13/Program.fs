﻿let program = System.IO.File.ReadAllText("2019/dec13.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map

let current = System.DateTime.Now
printfn "%i" (
    let mutable tiles       = Map.empty
    let mutable currentX    = 0L
    let mutable currentY    = 0L
    let mutable outputIndex = 0
    let onOutput value      =
        match outputIndex with
        | 0 -> currentX <- value
        | 1 -> currentY <- value
        | 2 -> tiles    <- if value = 2 then tiles.Add ((currentX, currentY), value) else tiles
        | x -> assert(false)
        outputIndex <- (outputIndex + 1) % 3
    Intcode.runProgram program (fun _ -> 0L) onOutput |> ignore
    tiles.Count
)
let postP1 = System.DateTime.Now
printfn "%i" (
    let mutable currentX    = 0L
    let mutable currentY    = 0L
    let mutable outputIndex = 0
    let mutable ballX       = 0L
    let mutable paddleX     = 0L
    let mutable score       = 0L
    let getInput ()         = System.Convert.ToInt64(paddleX > ballX) - System.Convert.ToInt64(paddleX < ballX)
    let onOutput value      =
        match outputIndex with
        | 0 -> currentX <- value
        | 1 -> currentY <- value
        | 2 ->
            if currentX = -1 && currentY = 0 then
                score <- value
            else if value = 3 then
                ballX <- currentX
            else if value = 4 then
                paddleX <- currentX
        | x -> assert(false)
        outputIndex <- (outputIndex + 1) % 3
    Intcode.runProgram (program |> Map.add 0 2) getInput onOutput |> ignore
    score
)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
