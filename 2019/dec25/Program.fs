﻿let program = System.IO.File.ReadAllText("2019/dec25.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map
let current = System.DateTime.Now

printfn "%i" (
    let mutable outputSB = System.Text.StringBuilder(500)
    let runUntilCondition (program: System.Collections.Generic.Dictionary<int64, int64>) pc relBase getInput storeOutput shouldExit =
        let mutable (newPCOrNone, newRelBase) = (Some(pc), relBase)
        while newPCOrNone.IsSome && not (shouldExit program newPCOrNone.Value) do
            let (localPCOrNone, localRelBase) = Intcode.stepProgram program newPCOrNone.Value newRelBase getInput storeOutput
            newPCOrNone <- localPCOrNone
            newRelBase  <- localRelBase
        newPCOrNone |> Option.map (fun newPC -> (newPC, newRelBase))
    let runUntilNextInput (program: System.Collections.Generic.Dictionary<int64, int64>) pc relBase =
        outputSB.Clear() |> ignore
        let resultsOrNone = runUntilCondition program pc relBase (fun () -> assert false; 0) (fun value -> outputSB.Append(char value) |> ignore) (fun newProgram newPC -> newProgram[newPC] % 100L = 3L || outputSB.Length >= 500)
        (resultsOrNone, outputSB.ToString())

    let         program           = System.Collections.Generic.Dictionary program
    let mutable seen              = System.Collections.Generic.Dictionary<string, System.Collections.Generic.HashSet<string * uint32>>()
    let mutable seenItems         = Map.empty
    let mutable commandsForOutput = System.Collections.Generic.Dictionary()
    let mutable stateQueue        = System.Collections.Generic.Queue([ let (vals, output) = runUntilNextInput program 0L 0L in (program, vals.Value, 0u, output) ])
    let mutable result            = None

    while result.IsNone do
        let (program, (pc, relBase), itemMask, lastOutput) = stateQueue.Dequeue()
        let outputLines                                    = lastOutput.Substring(lastOutput.LastIndexOf("\n\n\n") + 3).Split('\n')
        let mutable commandsForThisOutput = ref ([||])
        if not (commandsForOutput.TryGetValue (outputLines[0], commandsForThisOutput)) then
            commandsForThisOutput.Value <- outputLines |> Seq.skipWhile (fun line -> line <> "Doors here lead:")
                                                       |> Seq.skip 1
                                                       |> Seq.takeWhile (fun line -> not (System.String.IsNullOrEmpty line))
                                                       |> Seq.map (fun line -> (line[2..] + "\n", itemMask))
                                                       |> Seq.append (outputLines |> Seq.skipWhile (fun line -> line <> "Items here:")
                                                                                  |> Seq.indexed |> Seq.skipWhile (fun (i, _) -> i < 1)
                                                                                  |> Seq.map (fun (_, line) -> line)
                                                                                  |> Seq.takeWhile (fun line -> not (System.String.IsNullOrEmpty line))
                                                                                  |> Seq.map (fun line -> ("take " + line[2..] + "\n", 1u <<< (seenItems |> Map.tryFind line[2..] |> Option.orElseWith (fun () -> seenItems <- seenItems |> Map.add line[2..] seenItems.Count; Some(seenItems.Count - 1)) |> Option.get))))
                                                       |> Seq.toArray
            commandsForOutput.Add (outputLines[0], commandsForThisOutput.Value)
        for (command, addlItemMask) in commandsForThisOutput.Value do
            let newItemMask = itemMask ||| addlItemMask
            if not (seen.ContainsKey outputLines[0]) || not (seen[outputLines[0]].Contains (command, newItemMask)) then
                if not (seen.ContainsKey outputLines[0]) then
                    seen.Add (outputLines[0], System.Collections.Generic.HashSet())
                seen[outputLines[0]].Add (command, newItemMask) |> ignore
                let mutable commandI         = 0
                let         thisProgram      = System.Collections.Generic.Dictionary (program)
                let         valuesAfterInput = runUntilCondition thisProgram pc relBase (fun () -> commandI <- commandI + 1; int64 (command[commandI - 1])) (fun _ -> assert false) (fun _ _ -> commandI = command.Length)
                assert valuesAfterInput.IsSome
                let (pcAfterInput,      relBaseAfterInput) = valuesAfterInput.Value
                let (valuesAtNextInput, output)            = runUntilNextInput thisProgram pcAfterInput relBaseAfterInput
                if valuesAtNextInput.IsSome && output.Length < 500 && not (output.Contains "You can't move!!") then
                    let outputToUse = if output.StartsWith "\nYou take the" then lastOutput else output
                    stateQueue.Enqueue (thisProgram, valuesAtNextInput.Value, newItemMask, outputToUse)
                else if valuesAtNextInput.IsNone && output.Contains "Analysis complete!" then
                    result <- Some(int64 (System.Text.RegularExpressions.Regex.Match (output, @"\d+")).Groups[0].Value)
    result.Value
)
let post = System.DateTime.Now
printfn "%d us" (int64 (post - current).TotalMicroseconds)
