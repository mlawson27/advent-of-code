﻿let rec fuelRequired (recurse: bool, value: int64) =
    let mutable ret    = 0L
    let mutable value  = value
    let mutable doNext = true
    while doNext && value > 0 do
        let localValue = value / 3L - 2L
        doNext <- localValue >= 0
        if doNext then
            ret   <- ret + localValue
            value <- localValue
        doNext <- doNext && recurse
    ret

let values = System.IO.File.ReadAllLines("2019/dec01.txt") |> Array.map(int64)

let current = System.DateTime.Now
printfn "%i" (values |> Seq.map(fun v -> fuelRequired(false, v)) |> Seq.sum)
let postP1 = System.DateTime.Now
printfn "%i" (values |> Seq.map(fun v -> fuelRequired(true,  v)) |> Seq.sum)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
