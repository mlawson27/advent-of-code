﻿module Intcode

let stepProgram (program: System.Collections.Generic.Dictionary<int64, int64>) (workingPos: int64) (relativeBase: int64) (getInput) (storeOutput) =
    let mutable program = program

    let getParameter mode offset =
        let valueGetters = [| (fun (value: int64) (relativeBase: int64) -> if program.ContainsKey value then program[value] else 0L);
                              (fun (value: int64) (relativeBase: int64) -> value);
                              (fun (value: int64) (relativeBase: int64) -> if program.ContainsKey (value + relativeBase) then program[value + relativeBase] else 0L) |]
        valueGetters[mode] program[workingPos + offset] relativeBase
    let setParameter mode offset value =
        program[program[workingPos + offset] + (if (mode) = 2 then relativeBase else 0L)] <- value

    let mutable ret             = Some(workingPos)
    let mutable newRelativeBase = relativeBase
    let         p1Mode          = int (program[workingPos] /   100L) % 10
    let         p2Mode          = int (program[workingPos] /  1000L) % 10
    let         p3Mode          = int (program[workingPos] / 10000L) % 10
    match int (program[workingPos] % 100L) with
    | 1 ->
        setParameter p3Mode 3L ((getParameter p1Mode 1L) + (getParameter p2Mode 2L))
        ret <- Some(workingPos + 4L)
    | 2 ->
        setParameter p3Mode 3L ((getParameter p1Mode 1L) * (getParameter p2Mode 2L))
        ret <- Some(workingPos + 4L)
    | 3 ->
        setParameter p1Mode 1L (getInput())
        ret <- Some(workingPos + 2L)
    | 4 ->
        storeOutput (getParameter p1Mode 1L)
        ret <- Some(workingPos + 2L)
    | 5 ->
        if (getParameter p1Mode 1L) <> 0 then
            ret <- Some(int (getParameter p2Mode 2L))
        else
            ret <- Some(workingPos + 3L)
    | 6 ->
        if (getParameter p1Mode 1L) = 0 then
            ret <- Some(int (getParameter p2Mode 2L))
        else
            ret <- Some(workingPos + 3L)
    | 7 ->
        setParameter p3Mode 3L (if (getParameter p1Mode 1L) < (getParameter p2Mode 2L) then 1L else 0L)
        ret <- Some(workingPos + 4L)
    | 8 ->
        setParameter p3Mode 3L (if (getParameter p1Mode 1L) = (getParameter p2Mode 2L) then 1L else 0L)
        ret <- Some(workingPos + 4L)
    | 9 ->
        newRelativeBase <- relativeBase + (getParameter p1Mode 1L)
        ret <- Some(workingPos + 2L)
    | 99 -> ret <- None
    | x -> assert(false)
    (ret, newRelativeBase)

let runProgram (program: Map<int64, int64>) (getInput) (storeOutput) =
    let mutable program      = System.Collections.Generic.Dictionary program
    let mutable workingPos   = 0L
    let mutable relativeBase = 0L
    let mutable doNext       = true
    while doNext do
        let (newPosOrNone, newRelativeBase) = stepProgram program workingPos relativeBase getInput storeOutput
        if newPosOrNone.IsSome then
            workingPos   <- newPosOrNone.Value
            relativeBase <- newRelativeBase
        else
            doNext <- false
    program[0]
