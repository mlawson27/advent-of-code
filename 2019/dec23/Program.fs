﻿let runPrograms (program: Map<int64, int64>) onPacketSentTo255 shouldContinue onAllWaiting =
    let mutable programsPCsRelBases        = Seq.init 50 (fun _ -> Some (System.Collections.Generic.Dictionary program, 0L, 0L)) |> Seq.toArray
    let mutable packetQueuesRxITxITxInProg = seq { 0..(programsPCsRelBases.Length - 1) } |> Seq.map (fun _ -> (System.Collections.Generic.Queue(), 0, 0, [| 0L; 0L |])) |> Seq.toArray
    let mutable currentProgramI            = 0
    let mutable waitingMask                = 0UL
    let         allWaitingMask             = (1UL <<< programsPCsRelBases.Length) - 1UL

    let mutable getInputs = Array.create programsPCsRelBases.Length (fun () -> 0L)
    let getFromPacketQueue () =
        let (packetQueue, rxI, txI, txInProg) = packetQueuesRxITxITxInProg[currentProgramI]
        if packetQueue.Count = 0 then
            waitingMask <- waitingMask ||| (1UL <<< currentProgramI)
            -1L
        else
            waitingMask <- waitingMask &&& ~~~(1UL <<< currentProgramI)
            packetQueuesRxITxITxInProg[currentProgramI] <- (packetQueue, 1 - rxI, txI, txInProg)
            if rxI = 0 then
                fst (packetQueue.Peek())
            else
                snd (packetQueue.Dequeue())
    let getNicAddress () = getInputs[currentProgramI] <- getFromPacketQueue; int64 currentProgramI
    getInputs <- Array.create getInputs.Length getNicAddress

    let sendOutput value =
        let (packetQueue, rxI, txI, txInProg) = packetQueuesRxITxITxInProg[currentProgramI]
        if txI < 2 then
            txInProg[txI]                               <- value
            packetQueuesRxITxITxInProg[currentProgramI] <- (packetQueue, rxI, txI + 1, txInProg)
        else
            if txInProg[0] = 255 then
                onPacketSentTo255 (txInProg[1], value)
            else
                let (rxQueue, _, _, _) = packetQueuesRxITxITxInProg[int txInProg[0]]
                rxQueue.Enqueue (txInProg[1], value)
            packetQueuesRxITxITxInProg[currentProgramI] <- (packetQueue, rxI, 0, txInProg)

    while shouldContinue() do
        if currentProgramI = 0 && waitingMask = allWaitingMask then
            waitingMask <- onAllWaiting packetQueuesRxITxITxInProg waitingMask
        if programsPCsRelBases[currentProgramI].IsSome then
            let (program, pc, relBase)    = programsPCsRelBases[currentProgramI].Value
            let (newPCOrNone, newRelBase) = Intcode.stepProgram program pc relBase getInputs[currentProgramI] sendOutput
            programsPCsRelBases[currentProgramI] <- newPCOrNone |> Option.map (fun newPC -> (program, newPC, newRelBase))
        currentProgramI <- (currentProgramI + 1) % programsPCsRelBases.Length

let program = System.IO.File.ReadAllText("2019/dec23.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map
let current = System.DateTime.Now

printfn "%i" (
    let mutable packetSentToAddr255 = None
    runPrograms program (fun packet -> packetSentToAddr255 <- Some(packet)) (fun () -> packetSentToAddr255.IsNone) (fun _ waitingMask -> waitingMask)
    snd packetSentToAddr255.Value
)
let postP1 = System.DateTime.Now

printfn "%i" (
    let mutable natPacket = None
    let mutable lastYSent = None
    let mutable doNext    = true
    let onAllWaiting (packetQueuesRxITxITxInProg: (System.Collections.Generic.Queue<int64 * int64> * 'a * 'b * 'c)[]) waitingMask =
        if natPacket.IsSome then
            if lastYSent.IsSome && (snd natPacket.Value) = lastYSent.Value then
                doNext <- false
                waitingMask
            else
                let (rxQueue, _, _, _) = packetQueuesRxITxITxInProg[0]
                rxQueue.Enqueue natPacket.Value
                lastYSent <- Some (snd natPacket.Value)
                waitingMask &&& ~~~1UL
        else
            waitingMask
    runPrograms program (fun packet -> natPacket <- Some(packet)) (fun () -> doNext) onAllWaiting
    lastYSent.Value
)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
