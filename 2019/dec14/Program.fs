﻿let getOreRequiredForXFuel (reactions: Map<string, (int64 * (int64 * string)[])>) desiredFuel =
    let mutable numTimesUsedAsOperand = reactions.Values |> Seq.collect (fun operands -> snd operands)
                                                         |> Seq.fold (fun map (_, operand) -> map |> Map.change operand (fun v -> Some((v |> Option.defaultValue(0L)) + 1L))) Map.empty
    let mutable neededIngredients     = Map (["FUEL", desiredFuel])
    let mutable alreadyHandled        = Set.empty
    while not numTimesUsedAsOperand.IsEmpty do
        let mutable newIngredients            = Map.empty
        let mutable numTimesConsumedAsOperand = Map.empty
        for KeyValue(currentIngredientName, currentIngredientAmount) in neededIngredients do
            if not (alreadyHandled.Contains currentIngredientName) && reactions.ContainsKey (currentIngredientName) && not (numTimesUsedAsOperand.ContainsKey (currentIngredientName)) then
                let (requiredCurrentAmount, reactionRequirements) = reactions[currentIngredientName]
                let multiplier                                    = max (currentIngredientAmount / requiredCurrentAmount + System.Convert.ToInt64 (currentIngredientAmount % requiredCurrentAmount <> 0L)) 1L
                for (reactionIngredientAmount, reactionIngredientName) in reactionRequirements do
                    let amountOfNew = reactionIngredientAmount * multiplier
                    newIngredients            <- newIngredients.Change (reactionIngredientName, (fun currentAmountOrNone -> Some((currentAmountOrNone |> Option.defaultValue (0L)) + amountOfNew)))
                    numTimesConsumedAsOperand <- numTimesConsumedAsOperand.Change (reactionIngredientName, fun valueOrNone -> Some((valueOrNone |> Option.defaultValue(0L)) + 1L))
            else
                newIngredients <- newIngredients.Change (currentIngredientName, (fun currentAmountOrNone -> Some((currentAmountOrNone |> Option.defaultValue 0L) + currentIngredientAmount)))
        neededIngredients     <- newIngredients
        numTimesUsedAsOperand <- numTimesUsedAsOperand |> Seq.map    (fun (KeyValue(key, value)) -> ((key, value), (numTimesConsumedAsOperand |> Map.tryFind (key) |> Option.defaultValue 0L)))
                                                       |> Seq.filter (fun ((_, remaining), used) ->  used <> remaining)
                                                       |> Seq.map    (fun ((key, remaining), used) -> (key, remaining - used))
                                                       |> Map
    neededIngredients["ORE"]

let reactions = System.IO.File.ReadAllLines("2019/dec14.txt") |> Seq.map (fun line -> (fun (equationSides: string[]) -> (equationSides[1].Split(' ')[1], ((int64 (equationSides[1].Split(' ')[0])), (equationSides[0].Split(", ") |> Array.map (fun part -> (fun (parts: string[]) -> (int64 parts[0], parts[1])) (part.Split(' '))))))) (line.Split(" => ")))
                                                              |> Map

let current = System.DateTime.Now
printfn "%i" (getOreRequiredForXFuel reactions 1)
let postP1 = System.DateTime.Now
printfn "%i" (
    let mutable fuelMin = 0L
    let mutable fuelMax = int64 System.UInt32.MaxValue
    while fuelMin <= fuelMax do
        let fuelMid       = fuelMin + (fuelMax - fuelMin) / 2L
        let oreRequired   = getOreRequiredForXFuel reactions fuelMid
        let compareResult = oreRequired.CompareTo(1_000_000_000_000L)
        match compareResult with
            | -1 -> fuelMin <- fuelMid + 1L
            |  0 -> fuelMin <- fuelMid; fuelMax <- fuelMid
            |  1 -> fuelMax <- fuelMid - 1L
            |  x -> assert false
    fuelMax
)

let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
