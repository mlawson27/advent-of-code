﻿let doFFT (inputSignal: byte[]) =
    let pattern = [| 0; 1; 0; -1 |]
    seq { 0 .. (inputSignal.Length - 1) } |> Seq.map (fun i ->
        let mutable resultI = 0
        for j in i..(inputSignal.Length - 1) do
            resultI <- resultI + (int inputSignal[j]) * pattern[((j + 1) / (i + 1)) % pattern.Length]
        byte ((abs resultI) % 10)
    ) |> Seq.toArray

let doFFTWithOptimization (inputSignal: byte[]) =
    let mutable result     = Array.zeroCreate inputSignal.Length
    let mutable workingSum = 0L
    for i in (inputSignal.Length - 1) .. -1 .. 0 do
        workingSum <- workingSum + int64 inputSignal[i]
        result[i]  <- byte(workingSum % 10L)
    result

let run100Get8 signal fftFn = seq { 1..100 } |> Seq.fold (fun signal _ -> fftFn signal) signal |> Seq.take 8 |> Seq.map (fun value -> char (char value + '0')) |> Seq.toArray |> System.String

let inputSignal = System.IO.File.ReadAllText("2019/dec16.txt") |> Seq.map (fun c -> byte(c - '0')) |> Seq.toArray
let current = System.DateTime.Now

printfn "%s" (run100Get8 inputSignal doFFT)
let postP1 = System.DateTime.Now

let offset = inputSignal |> Seq.take 7 |> Seq.mapi (fun i value -> pown 10 (7 - i - 1) * int value) |> Seq.sum
assert (offset >= inputSignal.Length / 2)
printfn "%s" (run100Get8 (seq { for _ in 1..10000 do yield! inputSignal } |> Seq.skip(offset) |> Seq.toArray) doFFTWithOptimization)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
