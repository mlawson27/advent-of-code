﻿let grid = System.IO.File.ReadAllLines "2019/dec24.txt" |> Array.map (fun line -> line |> Seq.map (fun c -> c = '#') |> Seq.toArray)
let current = System.DateTime.Now

printfn "%i" (
    let         toSeenMask (grid: bool[][]) = grid |> Seq.rev |> Seq.fold (fun workingMask line -> workingMask <<< line.Length ||| (line |> Seq.indexed |> Seq.fold (fun lineMask (i, b) -> lineMask ||| ((System.Convert.ToUInt64 b) <<< i)) 0UL)) 0UL
    let         charOrDefaultAt grid x y    = grid |> Array.tryItem y |> Option.defaultValue [||] |> Array.tryItem x |> Option.defaultValue false
    let mutable workingGrid                 = grid |> Array.map (fun line -> line |> Array.copy)
    let mutable seenStates                  = System.Collections.Generic.HashSet([toSeenMask workingGrid])
    let mutable doNext                      = true
    while doNext do
        let mutable newGrid = workingGrid |> Array.map (fun line -> Array.create line.Length false)
        for y in 0..(workingGrid.Length - 1) do
            for x in 0..(workingGrid[y].Length - 1) do
                newGrid[y][x] <- match [| (x, y - 1); (x, y + 1); (x - 1, y); (x + 1, y) |] |> Seq.map (fun (innerX, innerY) -> System.Convert.ToUInt16 (charOrDefaultAt workingGrid innerX innerY)) |> Seq.sum with
                                    | 0us -> false
                                    | 1us -> true
                                    | 2us -> not (workingGrid[y][x])
                                    | 3us -> false
                                    | 4us -> false
                                    | _   -> assert false; false
        doNext      <- seenStates.Add (toSeenMask newGrid)
        workingGrid <- newGrid
    toSeenMask workingGrid
)
let postP1 = System.DateTime.Now

printfn "%i" (
    let         charOrDefaultAt grid x y = grid |> Array.tryItem y |> Option.defaultValue [||] |> Array.tryItem x |> Option.defaultValue false
    let mutable layers                   = Map ([0L, grid |> Array.map Array.copy])
    let rec getLayers (currentLayers: Map<int64, bool[][]>) =
        let mutable currentLayerI = (layers.Keys |> Seq.max) + 1L
        let mutable doNextLayer   = true
        let mutable newLayers     = Map.empty
        while doNextLayer do
            let         lastGrid  = currentLayers |> Map.tryFind currentLayerI |> Option.defaultValue [||]
            let mutable newGrid   = grid |> Array.map (fun line -> Array.create line.Length false)
            let mutable anyPoints = false
            for y in 0..(newGrid.Length - 1) do
                for x in 0..(newGrid[y].Length - 1) do
                    let knownSum = [ (x, y - 1); (x, y + 1); (x - 1, y); (x + 1, y) ] |> Seq.filter (fun (innerX, innerY) -> (innerX <> newGrid[y].Length / 2 || innerY <> newGrid.Length / 2))
                                                                                      |> Seq.map    (fun (innerX, innerY) -> System.Convert.ToUInt16 (charOrDefaultAt lastGrid innerX innerY)) |> Seq.sum

                    let (additionalToFindLayer, additionalToFindPoints) =
                        match (x, y) with
                            | (0, 0)                                                        -> (currentLayerI - 1L, [| (grid[y].Length / 2, grid.Length / 2 - 1); (grid[y].Length / 2 - 1, grid.Length / 2) |])
                            | (a, 0) when a = grid[y].Length - 1                            -> (currentLayerI - 1L, [| (grid[y].Length / 2, grid.Length / 2 - 1); (grid[y].Length / 2 + 1, grid.Length / 2) |])
                            | (0, b) when b = grid.Length - 1                               -> (currentLayerI - 1L, [| (grid[y].Length / 2 - 1, grid.Length / 2); (grid[y].Length / 2, grid.Length / 2 + 1) |])
                            | (a, b) when a = grid[y].Length - 1 && b = grid.Length - 1     -> (currentLayerI - 1L, [| (grid[y].Length / 2 + 1, grid.Length / 2); (grid[y].Length / 2, grid.Length / 2 + 1) |])
                            | (_, 0)                                                        -> (currentLayerI - 1L, [| (grid.Length / 2, grid.Length / 2 - 1) |])
                            | (_, b) when b = grid.Length - 1                               -> (currentLayerI - 1L, [| (grid.Length / 2, grid.Length / 2 + 1) |])
                            | (0, _)                                                        -> (currentLayerI - 1L, [| (grid.Length / 2 - 1, grid.Length / 2) |])
                            | (a, _) when a = grid[y].Length - 1                            -> (currentLayerI - 1L, [| (grid.Length / 2 + 1, grid.Length / 2) |])
                            | (a, b) when a = grid[y].Length / 2 && b = grid.Length / 2 - 1 -> (currentLayerI + 1L, [| 0..(grid[y].Length - 1)|] |> Array.map(fun x -> (x, 0)))
                            | (a, b) when a = grid[y].Length / 2 && b = grid.Length / 2 + 1 -> (currentLayerI + 1L, [| 0..(grid[y].Length - 1)|] |> Array.map(fun x -> (x, grid.Length - 1)))
                            | (a, b) when a = grid[y].Length / 2 - 1 && b = grid.Length / 2 -> (currentLayerI + 1L, [| 0..(grid.Length - 1)|]    |> Array.map(fun y -> (0, y)))
                            | (a, b) when a = grid[y].Length / 2 + 1 && b = grid.Length / 2 -> (currentLayerI + 1L, [| 0..(grid.Length - 1)|]    |> Array.map(fun y -> (grid[y].Length - 1, y)))
                            | _                                                             -> (0L, [||])

                    let otherLayerSum = layers |> Map.tryFind additionalToFindLayer
                                               |> Option.map (fun otherGrid -> additionalToFindPoints |> Seq.map (fun (x, y) -> System.Convert.ToUInt16 (charOrDefaultAt otherGrid x y)) |> Seq.sum)
                                               |> Option.defaultValue 0us

                    if x <> newGrid[0].Length / 2 || y <> newGrid.Length / 2 then
                        newGrid[y][x] <- match knownSum + otherLayerSum with
                                            | 0us -> false
                                            | 1us -> true
                                            | 2us -> not (charOrDefaultAt lastGrid x y)
                                            | _   -> false
                    anyPoints <- anyPoints || newGrid[y][x]
            if anyPoints then
                newLayers <- newLayers |> Map.add currentLayerI newGrid
            currentLayerI <- currentLayerI - 1L
            doNextLayer   <- anyPoints || (layers.ContainsKey currentLayerI)
        newLayers

    for _ in 0..(200 - 1) do
        layers <- getLayers layers
    layers.Values |> Seq.map (fun layerGrid -> layerGrid |> Seq.map (fun line -> line |> Seq.map System.Convert.ToInt64 |> Seq.sum) |> Seq.sum) |> Seq.sum
)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
