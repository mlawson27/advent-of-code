﻿let runSpringDroidProgram program (springDroidProgram: string) showOutput =
    let mutable currentInputI = 0
    let mutable output        = 0L
    Intcode.runProgram program (fun () -> let ret = int64 springDroidProgram[currentInputI] in currentInputI <- currentInputI + 1; ret) (fun value -> if value > int64 System.Char.MaxValue then output <- value else if showOutput then printf "%c" (char value)) |> ignore
    output

let program = System.IO.File.ReadAllText("2019/dec21.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map
let current = System.DateTime.Now

printfn "%i" (runSpringDroidProgram program "NOT A J\nNOT B T\nOR T J\nNOT C T\nOR T J\nAND D J\nWALK\n" false)
let postP1 = System.DateTime.Now

printfn "%i" (runSpringDroidProgram program "NOT A J\nNOT B T\nOR T J\nNOT C T\nAND H T\nOR T J\nAND D J\nRUN\n" false)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
