﻿type Operation =
    | DealIntoNewStack
    | Cut of int
    | DealWithIncrement of int
let parseOperation opStr =
    let mutable m = System.Text.RegularExpressions.Regex.Match(opStr, @"deal\s+into\s+new\s+stack")
    if m.Success then
        Operation.DealIntoNewStack
    else
        m <- System.Text.RegularExpressions.Regex.Match(opStr, @"cut\s+(-?\d+)")
        if m.Success then
            Operation.Cut (int m.Groups[1].Value)
        else
            m <- System.Text.RegularExpressions.Regex.Match(opStr, @"deal\s+with\s+increment\s+(\d+)")
            if m.Success then
                Operation.DealWithIncrement (int m.Groups[1].Value)
            else
                assert false
                Operation.DealIntoNewStack

let instructions = System.IO.File.ReadAllLines "2019/dec22.txt" |> Array.map parseOperation

let current = System.DateTime.Now

printfn "%i" (
    let handleOperation cards operation =
        let dealIntoNewStackCards (cards: int[]) = cards |> Array.rev

        let cutCards (cards: int[]) (amount: int) =
            let mutable ret = Array.zeroCreate cards.Length
            for i in 0..(cards.Length - 1) do
                ret[i] <- cards[(i + amount + cards.Length) % cards.Length]
            ret

        let dealWithIncrement (cards: int[]) (amount: int) =
            let mutable ret = Array.zeroCreate cards.Length
            for i in 0..(cards.Length - 1) do
                ret[(i * amount) % cards.Length] <- cards[i]
            ret

        match operation with
            | DealIntoNewStack -> dealIntoNewStackCards cards
            | DealWithIncrement (incr) -> dealWithIncrement cards incr
            | Cut(amount) -> cutCards cards amount

    instructions |> Seq.fold handleOperation [| 0..(10007 - 1) |] |> Seq.findIndex (fun card -> card = 2019)
)
let postP1 = System.DateTime.Now

printfn "%i" (
    let i128 i64 = System.Int128 ((if i64 < 0L then System.UInt64.MaxValue else 0UL), uint64 i64)

    let NUM_CARDS    = i128 119315717514047L
    let NUM_SHUFFLES = i128 101741582076661L

    let modExp (bas: System.Int128) (exponent: System.Int128) (modulus: System.Int128) =
        assert ((modulus - System.Int128.One) < (System.Int128.MaxValue / (modulus - System.Int128.One)))
        let mutable result   = System.Int128.One
        let mutable bas      = bas % modulus
        let mutable exponent = exponent
        while exponent > System.Int128.Zero do
            if exponent % (System.Int128.One + System.Int128.One) = System.Int128.One then
                result <- (result * bas) % modulus
            exponent <- exponent >>> 1
            bas      <- (bas * bas) % modulus
        result

    let (incrementMul, offsetDiff) = instructions |> Seq.rev
                                                  |> Seq.fold (fun (workingI, workingO) operation ->
                                                        let (newI, newO) = match operation with
                                                                            | DealIntoNewStack -> (-workingI, -workingO - System.Int128.One)
                                                                            | Cut amount -> (workingI, workingO + i128 amount)
                                                                            | DealWithIncrement amount -> let mult = modExp (i128 amount) (NUM_CARDS - (i128 2L)) NUM_CARDS in (workingI * mult, workingO * mult)
                                                        (newI % NUM_CARDS, newO % NUM_CARDS)
                                                     ) (System.Int128.One, System.Int128.Zero)

    int64 ((((i128 2020L) * (modExp incrementMul NUM_SHUFFLES NUM_CARDS) % NUM_CARDS) +
            (offsetDiff * (((modExp incrementMul NUM_SHUFFLES NUM_CARDS) - System.Int128.One) * (modExp (incrementMul - System.Int128.One) (NUM_CARDS - (i128 2L)) NUM_CARDS) % NUM_CARDS))) % NUM_CARDS)
)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
