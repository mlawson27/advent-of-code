﻿let minStepsRequired (grid: char[][]) portals (startingX, startingY) endingPosition (emulateLevels: bool) =
    let tryFindInDictionary key (dict: System.Collections.Generic.Dictionary<'a, 'b>) = if dict.ContainsKey key then Some(dict[key]) else None

    let mutable stateQueue = System.Collections.Generic.Queue ([((int16 startingX, int16 startingY), 0us, 0us)])
    let mutable minMoves   = None
    let mutable seen       = System.Collections.Generic.HashSet [(fun (pos, _, level) -> (pos, level)) (stateQueue.Peek())]
    while minMoves.IsNone do
        let ((currentX, currentY), currentMovesSoFar, currentLevel) = stateQueue.Dequeue()
        if (currentX, currentY) = endingPosition && currentLevel = 0us then
            minMoves <- Some(int currentMovesSoFar)
        else
            for (newX, newY) in [| (currentX, currentY - 1s); (currentX, currentY + 1s); (currentX - 1s, currentY); (currentX + 1s, currentY) |] do
                let ((isValid, (newX, newY)), newLevel) =
                    let gridCharAtNew () = grid |> Array.tryItem (int newY) |> Option.defaultValue [||] |> Array.tryItem (int newX)
                    portals |> tryFindInDictionary (newX, newY)
                            |> Option.map (fun (portalX, portalY) -> if not emulateLevels then ((true, (int16 portalX, int16 portalY)), 0us) else let inside = gridCharAtNew().IsSome in ((currentLevel <> 0us || inside, (int16 portalX, int16 portalY)), currentLevel + (if inside then 1us else 0xFFFFus)))
                            |> Option.orElseWith(fun () -> Some((match gridCharAtNew () |> Option.defaultValue '#' with
                                                                                | '#' | '@' -> (false, (0s, 0s))
                                                                                | '.'       -> (true,  (newX, newY))
                                                                                | x         -> assert(false); (false, (0s, 0s))), currentLevel))
                            |> Option.get
                if isValid && seen.Add ((newX, newY), newLevel) then
                    stateQueue.Enqueue ((newX, newY), currentMovesSoFar + 1us, newLevel)
    minMoves.Value

let (grid, portals, startingPosition, endingPosition) =
    let nextFromPortal (isEntry: bool) ((x, y), orientationInt) =
        match orientationInt with
            | 0 -> (int16 (x), int16 (y - (orientationInt % 3) - System.Convert.ToInt32 isEntry))
            | 1 -> (int16 (x), int16 (y - (orientationInt % 3) + System.Convert.ToInt32 (not isEntry)))
            | 2 -> (int16 (x), int16 (y - (orientationInt % 3) - System.Convert.ToInt32 (not isEntry)))
            | 3 -> (int16 (x - (orientationInt % 3) - System.Convert.ToInt32 isEntry),       int16 y)
            | 4 -> (int16 (x - (orientationInt % 3) + System.Convert.ToInt32 (not isEntry)), int16 y)
            | 5 -> (int16 (x - (orientationInt % 3) - System.Convert.ToInt32 (not isEntry)), int16 y)
            | x -> assert(false); (int16 x, int16 y)

    let mutable paths = Map.empty
    let initialGrid  = System.IO.File.ReadAllLines "2019/dec20.txt" |> Array.map (fun line -> line.ToCharArray())
    assert (initialGrid[1..] |> Seq.forall (fun line -> line.Length = initialGrid[0].Length))
    for x in 2..(initialGrid[0].Length - 3) do
        for y in 0..(initialGrid.Length - 2) do
            if (seq { y .. (y + 1)} |> Seq.forall (fun y1 -> System.Char.IsLetter (initialGrid[y1][x]))) then
                paths <- paths.Change (System.String [| initialGrid[y][x]; initialGrid[y + 1][x] |], (fun listOrNone -> Some (((x - 2, y), if y = 0 then 0 else if y = initialGrid.Length - 2 || System.Char.IsWhiteSpace (initialGrid[y + 2][x]) then 2 else 1)::(listOrNone |> Option.defaultValue(List.empty)) )))
                initialGrid[y][x]     <- ' '
                initialGrid[y + 1][x] <- ' '
    for y in 2..(initialGrid.Length - 3) do
        for x in 0..(initialGrid[y].Length - 2) do
            if initialGrid[y][x..(x+1)] |> Seq.forall System.Char.IsLetter then
                paths <- paths.Change (System.String (initialGrid[y][x..(x + 1)]), (fun listOrNone -> Some(((x, y - 2), if x = 0 then 3 else if x = initialGrid[y].Length - 2 || System.Char.IsWhiteSpace (initialGrid[y][x + 2]) then 5 else 4)::(listOrNone |> Option.defaultValue(List.empty)))))
                initialGrid[y][x]     <- ' '
                initialGrid[y][x + 1] <- ' '

    let newGrid = initialGrid[2..(initialGrid.Length - 3)] |> Array.map(fun line -> line[2..(line.Length - 3)])
    (newGrid,
     paths.Values |> Seq.filter (fun list -> list.Length = 2) |> Seq.fold (fun (state: System.Collections.Generic.Dictionary<int16 * int16, int16 * int16>) list -> state.Add ((nextFromPortal true list.Head), (nextFromPortal false list.Tail.Head)); state.Add ((nextFromPortal true list.Tail.Head), (nextFromPortal false list.Head)); state) (System.Collections.Generic.Dictionary()),
     paths["AA"].Head |> nextFromPortal false,
     paths["ZZ"].Head |> nextFromPortal false)
assert (grid.Length <= int System.UInt16.MaxValue && grid |> Seq.forall (fun line -> line.Length < int System.UInt16.MaxValue))
let current = System.DateTime.Now

printfn "%i" (minStepsRequired grid portals startingPosition endingPosition false)
let postP1 = System.DateTime.Now

printfn "%i" (minStepsRequired grid portals startingPosition endingPosition true)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
