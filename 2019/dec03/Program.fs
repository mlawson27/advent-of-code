﻿let foldOverAllPoints mapFn (seenState, (currentX: int64, currentY: int64), stepsSoFar: int64) (dir: char, amount: int64) =
    let toRepeat =
        match dir with
            | 'L' -> (-1L,  0L)
            | 'R' -> ( 1L,  0L)
            | 'U' -> ( 0L, -1L)
            | 'D' -> ( 0L,  1L)
            | _   ->
                assert(false)
                (0, 0)
    (Seq.replicate(int amount) toRepeat) |> Seq.fold mapFn (seenState, (currentX, currentY), stepsSoFar)

let directions = System.IO.File.ReadAllLines("2019/dec03.txt") |> Array.map(fun line -> line.Split(',') |> Array.map(fun token -> (token[0], int64 token[1..])))

let current = System.DateTime.Now
let mapFnP1 (seenSet: Set<int64 * int64>, (workingX: int64, workingY: int64), stepsSoFar: int64) (adjX: int64, adjY: int64) =
    let newPos = (workingX + adjX, workingY + adjY)
    (seenSet.Add(newPos), newPos, stepsSoFar + 1L)
printfn "%i" (directions |> Array.map(fun arr -> Array.fold (fun state elem -> foldOverAllPoints mapFnP1 state elem) (Set.empty, (0L, 0L), 0L) arr)
                         |> Array.map(fun (set, _, _) -> set)
                         |> Array.reduce(Set.intersect)
                         |> Seq.map(fun (x, y) -> abs x + abs y)
                         |> Seq.min)
let postP1 = System.DateTime.Now
let mapFnP2 (seenPoints: Map<int64 * int64, int64>, (workingX: int64, workingY: int64), stepsSoFar: int64) (adjX: int64, adjY: int64) =
    let newPos = (workingX + adjX, workingY + adjY)
    ((if seenPoints.ContainsKey(newPos) then seenPoints else seenPoints.Add(newPos, stepsSoFar)), newPos, stepsSoFar + 1L)
printfn "%i" (directions |> Array.map(fun arr -> Array.fold (fun state elem -> foldOverAllPoints mapFnP2 state elem) (Map.empty, (0L, 0L), 1L) arr)
                         |> Array.map(fun (map, _, _) -> map)
                         |> Array.reduce(fun map1 map2 -> map1 |> Map.filter(fun key v -> map2.ContainsKey(key))
                                                               |> Map.map(fun key v -> v + map2[key]))
                         |> Map.values
                         |> Seq.min)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
