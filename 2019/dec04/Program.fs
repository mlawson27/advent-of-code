﻿let MIN_VAL = 146810
let MAX_VAL = 612564

let isValidValue (longerChainsAllowed: bool) (value: int) =
    let mutable anyRepeat        = false
    let mutable currentRepeatLen = 0
    let mutable isValid          = true
    let mutable lastDigit        = 10
    let mutable value            = value
    while isValid && value > 0 do
        let thisDigit = value % 10
        isValid          <- isValid   && thisDigit <= lastDigit
        anyRepeat        <- anyRepeat || (if longerChainsAllowed then thisDigit = lastDigit else currentRepeatLen = 1 && thisDigit <> lastDigit)
        currentRepeatLen <- if thisDigit = lastDigit then currentRepeatLen + 1 else 0
        value            <- value / 10
        lastDigit        <- thisDigit
    isValid && (anyRepeat || (not longerChainsAllowed && currentRepeatLen = 1))

let current = System.DateTime.Now
printfn "%i" (seq { MIN_VAL .. MAX_VAL } |> Seq.filter(isValidValue true)
                                         |> Seq.length)
let postP1 = System.DateTime.Now
printfn "%i" (seq { MIN_VAL .. MAX_VAL } |> Seq.filter(isValidValue false)
                                         |> Seq.length)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
