let getPaintedPoints program includeInList initialPoints =
    let mutable (workingX, workingY) = (0L, 0L)
    let mutable currentDir           = 0
    let mutable valueIsPaintColor    = true
    let mutable paintedBlocks        = initialPoints
    let         getInput ()          = (paintedBlocks |> Map.tryFind (workingX, workingY) |> Option.orElse (Some(0L))).Value
    let         storeOutput value    =
        if not valueIsPaintColor then
            currentDir <- (currentDir + [| 3; 1 |][int (value % 2L)]) % 4
            workingX   <- workingX + [|  0L; 1L; 0L; -1L |][currentDir]
            workingY   <- workingY + [| -1L; 0L; 1L;  0L |][currentDir]
        else if includeInList(value) then
            paintedBlocks <- paintedBlocks.Add ((workingX, workingY), value)
        valueIsPaintColor <- not valueIsPaintColor
    Intcode.runProgram program getInput storeOutput |> ignore
    paintedBlocks

let program = System.IO.File.ReadAllText("2019/dec11.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map

let current = System.DateTime.Now

(getPaintedPoints program (fun _  -> true) Map.empty) |> Map.count |> (printfn "%i")
let postP1 = System.DateTime.Now
(
    let whitePoints                  = getPaintedPoints program (fun value -> value = 1) (Map ([((0L, 0L), 1L)])) |> Map.remove (0L, 0L)
    let ((minX, maxX), (minY, maxY)) = whitePoints.Keys |> Seq.fold (fun ((workingMinX, workingMaxX), (workingMinY, workingMaxY)) (x, y) -> (((min x workingMinX), (max x workingMaxX)), ((min y workingMinY), (max y workingMaxY)))) ((System.Int64.MaxValue, System.Int64.MinValue), (System.Int64.MaxValue, System.Int64.MinValue))
    {minY ..maxY} |> Seq.iter (fun y ->
        seq {minX ..maxX} |> Seq.map (fun x -> if whitePoints.ContainsKey (x, y) then '#' else ' ') |> Seq.iter (printf "%c")
        printfn "")
)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
