﻿let WIDTH  = 25
let HEIGHT =  6

let layers = System.IO.File.ReadAllText("2019/dec08.txt") |> Seq.mapi (fun i c -> (i, c))
                                                          |> Seq.groupBy (fun (i, _) -> i / WIDTH)
                                                          |> Seq.mapi (fun i (_, elems) -> (i, elems |> Seq.map (fun (_, c) -> c) |> Seq.toArray |> System.String |> string))
                                                          |> Seq.groupBy (fun (i, _) -> i / HEIGHT)
                                                          |> Seq.map (fun (_, strsAndIndices) -> strsAndIndices |> Seq.map (fun (_, str) -> str) |> Seq.toArray)
                                                          |> Seq.toArray

let current = System.DateTime.Now
let numInstancesInLayer charToFind layer =
    layer |> Seq.map (fun str -> str |> Seq.filter (fun c -> c = charToFind) |> Seq.length) |> Seq.sum
let layerWithFewest0s = layers |> Seq.minBy (numInstancesInLayer '0')
printfn "%i"  ((numInstancesInLayer '1' layerWithFewest0s) * (numInstancesInLayer '2' layerWithFewest0s))
let postP1 = System.DateTime.Now
let mergeLayers a b =
    let pickChar (current, other) =
        match current with
            | '0' | ' ' -> ' '
            | '1' | '#' -> '#'
            | '2'       -> match other with
                               | '0' -> ' '
                               | '1' -> '#'
                               | x -> other
            | x   -> x

    b |> Seq.zip a
      |> Seq.map (fun (working, other) -> other |> Seq.zip working
                                                |> Seq.map pickChar
                                                |> Seq.toArray
                                                |> System.String
                                                |> string)
      |> Seq.toArray
let displayedImage = layers |> Seq.reduce mergeLayers
displayedImage |> Seq.iter (printfn "%s")
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
