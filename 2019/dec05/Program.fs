let program = System.IO.File.ReadAllText("2019/dec05.txt").Split(',') |> Array.mapi (fun i value -> (int64 i, int64 value)) |> Map

let current = System.DateTime.Now
let mutable p1Output = 0L
Intcode.runProgram program (fun _ -> 1) (fun value -> (p1Output <- value)) |> ignore
printfn "%i" p1Output
let postP1 = System.DateTime.Now
Intcode.runProgram program (fun _ -> 5) (printfn "%i") |> ignore
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
