﻿let NUM_STEPS = 1000

let doStep (positionsAndVelocities: (int64[] * int64[])[]) =
    let mutable newPositionsAndVelocities = positionsAndVelocities |> Array.map (fun (pos, vel) -> (pos.Clone() :?> int64[], vel.Clone() :?> int64[]))
    for i in 0..(positionsAndVelocities.Length - 1) do
        let mutable (iModPosition, iModVelocity) = newPositionsAndVelocities[i]
        let         (iPosition,    iVelocity)    = positionsAndVelocities[i]
        for j in 0..(positionsAndVelocities.Length - 1) do
            if i <> j then
                let (jPosition, _) = positionsAndVelocities[j]
                for k in 0..(iPosition.Length - 1) do
                    iModVelocity[k] <- iModVelocity[k] + (System.Convert.ToInt64(jPosition[k] > iPosition[k]) - System.Convert.ToInt64(iPosition[k] > jPosition[k]))
        for k in 0..(iPosition.Length - 1) do
            iModPosition[k] <- iModPosition[k] + iModVelocity[k]
    newPositionsAndVelocities

let positionsAndVelocities = System.IO.File.ReadAllLines("2019/dec12.txt") |> Seq.map (fun line -> ((System.Text.RegularExpressions.Regex.Match(line, @"^<x=\s*(-?\d+),\s*y=\s*(-?\d+),\s*z=\s*(-?\d+)>$").Groups |> Seq.skip 1
                                                                                                                                                                                                                  |> Seq.map (fun g -> int64 g.Value)
                                                                                                                                                                                                                  |> Seq.toArray), [| 0L; 0L; 0L |]))
                                                                           |> Seq.toArray

let current = System.DateTime.Now
printfn "%i" (seq { 1..NUM_STEPS } |> Seq.fold (fun workingPositionsAndVelocities _ -> doStep workingPositionsAndVelocities) positionsAndVelocities
                                   |> Seq.sumBy (fun (pos, vel) -> [|pos; vel|] |> Seq.map(fun arr -> (arr |> Seq.sumBy (fun elem -> abs elem))) |> Seq.reduce ( * )))
let postP1 = System.DateTime.Now
printfn "%i" (
    let mutable dimCycleLen                   = (fst positionsAndVelocities[0]) |> Array.map (fun _ -> None)
    let mutable numCyclesToFind               = dimCycleLen.Length
    let mutable workingPositionsAndVelocities = positionsAndVelocities
    let mutable seenDims                      = dimCycleLen |> Array.mapi(fun dim _ -> Map [((positionsAndVelocities |> Array.map (fun (pos, vel) -> (pos[dim], vel[dim]))), 0L)])
    let mutable currentCycle                  = 0L
    while numCyclesToFind > 0 do
        workingPositionsAndVelocities <- doStep workingPositionsAndVelocities
        currentCycle                  <- currentCycle + 1L
        for i in 0..(dimCycleLen.Length - 1) do
            if dimCycleLen[i].IsNone then
                let allAtDim              = workingPositionsAndVelocities |> Seq.map (fun (pos, vel) -> (pos[i], vel[i])) |> Seq.toArray
                let prevCycleForDimOrNone = seenDims[i] |> Map.tryFind allAtDim
                if prevCycleForDimOrNone.IsSome then
                    dimCycleLen[i]  <- Some(currentCycle - prevCycleForDimOrNone.Value)
                    numCyclesToFind <- numCyclesToFind - 1
                seenDims[i] <- seenDims[i].Add (allAtDim, currentCycle)
    let rec gcd x y =
        if y = 0L then x
        else gcd y (x % y)
    let lcm a b = a * b / (gcd a b)
    dimCycleLen |> Seq.map Option.get |> Seq.reduce lcm
)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
