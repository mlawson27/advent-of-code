﻿let minStepsRequired grid startingPositions =
    let         allKeysMask = grid |> Array.collect (fun line -> line)
                                   |> Seq.choose(fun c -> if System.Char.IsAsciiLetterLower c then Some(c) else None)
                                   |> Seq.fold (fun workingMask c -> workingMask ||| (1u <<< (int c - int 'a'))) 0u
    let mutable stateQueue  = System.Collections.Generic.Queue ([((startingPositions |> Array.rev |> Array.fold (fun mask (x, y) -> (mask <<< 16) ||| (uint64 x <<< 8) ||| (uint64 y)) 0UL), 0, 0u, -1)])
    let mutable minMoves    = None
    let mutable seen        = System.Collections.Generic.HashSet [(fun (pos, _, mask, _) -> (pos, mask)) (stateQueue.Peek())]
    while minMoves.IsNone do
        let (currentPositionMask, currentMovesSoFar, currentKeyMask, lastMovedI) = stateQueue.Dequeue()
        if currentKeyMask = allKeysMask then
            minMoves <- Some(currentMovesSoFar)
        else
            for i in (if lastMovedI < 0 then seq { 0 .. (startingPositions.Length - 1) } else seq { lastMovedI .. lastMovedI }) do
                let (currentX, currentY) = (int ((currentPositionMask >>> (i * 16) + 8) &&& 0xFFUL), int ((currentPositionMask >>> (i * 16)) &&& 0xFFUL))
                for (posAdjX, posAdjY) in [ (0, -1); (0, 1); (-1, 0); (1, 0) ] do
                    let (newX,    newY)        = (currentX + posAdjX, currentY + posAdjY)
                    let (isValid, addlKeyMask) = match grid[newY][newX] with
                                                    | '#'                                      -> (false, 0u)
                                                    | '.' | '@'                                -> (true,  0u)
                                                    | l when System.Char.IsAsciiLetterUpper(l) -> (currentKeyMask &&& (1u <<< (int l - int 'A')) <> 0u, 0u)
                                                    | l when System.Char.IsAsciiLetterLower(l) -> (true, (1u <<< (int l - int 'a')))
                                                    | x                                        -> assert(false); (false, 0u)
                    if isValid then
                        let newPositionMask = (currentPositionMask &&& ~~~(0xFFFFUL <<< (i * 16))) ||| (uint64 (newX) <<< ((i * 16) + 8)) ||| (uint64 (newY) <<< (i * 16))
                        let newKeysMask     = currentKeyMask ||| addlKeyMask
                        if seen.Add (newPositionMask, newKeysMask) then
                            stateQueue.Enqueue (newPositionMask, currentMovesSoFar + 1, newKeysMask, if addlKeyMask = 0u || (currentKeyMask &&& addlKeyMask) = addlKeyMask then i else -1)
    minMoves.Value

let grid             = System.IO.File.ReadAllLines("2019/dec18.txt") |> Array.map (fun line -> line.ToCharArray())
let startingPosition = grid |> Seq.mapi (fun y line -> (y, line))
                            |> Seq.collect (fun (y, line) -> line |> Seq.mapi (fun x c -> (x, y, c)))
                            |> Seq.pick (fun (x, y, c) -> if c = '@' then Some(x, y) else None)

assert (grid.Length <= int System.Byte.MaxValue && grid |> Seq.forall (fun line -> line.Length < int System.Byte.MaxValue))
let current = System.DateTime.Now

printfn "%i" (minStepsRequired grid [| startingPosition |])
let postP1 = System.DateTime.Now

printfn "%i" (
    let         newGrid           = grid |> Array.map(fun line -> line.Clone() :?> char[])
    let mutable startingPositions = Array.zeroCreate 4
    for y in (snd startingPosition - 1)..(snd startingPosition + 1) do
        for x in (fst startingPosition - 1)..(fst startingPosition + 1) do
            if x = fst startingPosition || y = snd startingPosition then
                newGrid[y][x] <- '#'
            else
                newGrid[y][x] <- '@'
                startingPositions[System.Convert.ToInt32(y > snd startingPosition) * 2 + System.Convert.ToInt32(x > fst startingPosition)] <- (x, y)
    minStepsRequired newGrid startingPositions
)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
