﻿let program = System.IO.File.ReadAllText("2019/dec17.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map
let (scaffoldingPoints, (startingPoint, startingDirection)) =
    let mutable result        = Set.empty
    let mutable startingPoint = ((0L, 0L), char 0)
    let mutable currentX      = 0L
    let mutable currentY      = 0L
    Intcode.runProgram program (fun () -> 0L) (fun value ->
        match char value with
            | '\n'                  -> currentY <- currentY + 1L; currentX <- 0L;
            | '#'                   -> result <- result.Add (currentX, currentY); currentX <- currentX + 1L;
            | '.'                   -> currentX <- currentX + 1L
            | '^' | 'v' | '<' | '>' -> startingPoint <- ((currentX, currentY), char value); currentX <- currentX + 1L
            | x   -> assert(false)
    ) |> ignore
    (result, startingPoint)
let current = System.DateTime.Now
printfn "%i" (scaffoldingPoints |> Seq.filter (fun (x, y) -> [| (x, y - 1L); (x, y + 1L); (x - 1L, y); (x + 1L, y) |] |> Seq.forall (fun (xAdj, yAdj) -> scaffoldingPoints.Contains (xAdj, yAdj)))
                                |> Seq.sumBy  (fun (x, y) -> (int64 x) * (int64 y)))
let postP1 = System.DateTime.Now

printfn "%i" (
    let totalPathStr =
        let mutable remainingPoints            = scaffoldingPoints
        let mutable path                       = System.Collections.Generic.LinkedList()
        let mutable currentPoint               = startingPoint
        let mutable direction                  = startingDirection
        let         nextPoint direction (x, y) =
            match direction with
                | '^' -> (x,      y - 1L)
                | 'v' -> (x,      y + 1L)
                | '<' -> (x - 1L, y)
                | '>' -> (x + 1L, y)
                | x   -> assert(false); (0L, 0L)
        while not remainingPoints.IsEmpty do
            let mutable straightAmount = 0L
            let mutable nextPt         = nextPoint direction currentPoint
            while scaffoldingPoints.Contains (nextPt) do
                remainingPoints <- remainingPoints.Remove (nextPt)
                straightAmount  <- straightAmount + 1L
                currentPoint    <- nextPt
                nextPt          <- nextPoint direction currentPoint
            let mutable newPathStr = System.String.Empty
            if straightAmount > 0 then
                newPathStr <- straightAmount.ToString()
            else
                let (left, right) = match direction with
                                        | '^' -> ('<', '>')
                                        | 'v' -> ('>', '<')
                                        | '<' -> ('v', '^')
                                        | '>' -> ('^', 'v')
                                        | x   -> assert(false); (char 0, char 0)
                for (newDirection, turnStr ) in [| (left, "L"); (right, "R") |] do
                    if scaffoldingPoints.Contains (nextPoint newDirection currentPoint) then
                        direction  <- newDirection
                        newPathStr <- turnStr
            path.AddLast(newPathStr) |> ignore
        System.String.Join (',', path |> Seq.toArray)

    let functionRoutines =
        let mutable ret = Array.create 4 System.String.Empty
        ret[0] <- totalPathStr
        for fnI in 1..(ret.Length - 1) do
            let startI = seq{ 0..(ret[0].Length - 2) } |> Seq.skipWhile(fun i -> [| 'A'; 'B'; 'C'; ',' |] |> Seq.exists (fun c -> ret[0][i] = c)) |> Seq.head
            let endI   = (seq{ startI + 1..(ret[0].Length - 1) } |> Seq.skipWhile (fun i -> System.Text.RegularExpressions.Regex.Matches(ret[0].Substring(i), ret[0].Substring(startI, i)).Count > 1) |> Seq.head) - 2
            ret[fnI] <- ret[0].Substring(startI, endI)
            ret[0]   <- ret[0].Replace(ret[fnI], [| "A"; "B"; "C"|][fnI - 1])
        ret

    let mutable ret        = 0L
    let mutable (eqI, eqX) = (0, 0)
    Intcode.runProgram (program |> Map.add 0L 2L) (fun () ->
        int64 (if eqI = functionRoutines.Length then
                   let ret = if eqX = 0 then 'n' else '\n'
                   eqX <- eqX + 1
                   ret
               else if eqX = functionRoutines[eqI].Length then
                   eqX <- 0
                   eqI <- eqI + 1
                   '\n'
               else
                   let ret = functionRoutines[eqI][eqX]
                   eqX <- eqX + 1
                   ret)
    ) (fun value -> ret <- value) |> ignore
    ret
)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
