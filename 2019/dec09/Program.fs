﻿let program = System.IO.File.ReadAllText("2019/dec09.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map

let current = System.DateTime.Now
Intcode.runProgram program (fun _ -> 1L) (printfn "%i") |> ignore
let postP1 = System.DateTime.Now
Intcode.runProgram program (fun _ -> 2L) (printfn "%i") |> ignore
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
