﻿let program = System.IO.File.ReadAllText("2019/dec02.txt").Split(',') |> Array.mapi(fun i value -> (int64 i, int64 value)) |> Map

let current = System.DateTime.Now
printfn "%i" (Intcode.runProgram (program |> Map.add 1L 12 |> Map.add 2L 2) (fun _ -> 0L) (fun _ -> ()))
let postP1 = System.DateTime.Now
printfn "%i" (
    let mapFn (v1, v2) =
        let mutable workingProgram = program
        workingProgram <- workingProgram.Add(1L, v1)
        workingProgram <- workingProgram.Add(2L, v2)
        if Intcode.runProgram workingProgram (fun _ -> 0L) (fun _ -> ()) = 19690720 then
            Some (v1, v2)
        else
            None

    seq { 0L .. int64 (program.Count - 1) } |> Seq.allPairs(seq { 0L .. int64(program.Count - 1) })
                                            |> Seq.pick(mapFn)
                                            |> fun (v1, v2) -> v1 * 100L + v2)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
