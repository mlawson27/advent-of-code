﻿let doBFS (program: Map<int64, int64>) startPos isPart1 =
    let mutable ret           = if isPart1 then System.Int64.MinValue else 0L
    let mutable endPoint      = None
    let mutable programAtEnd  = None
    let mutable stateQueue    = System.Collections.Generic.Queue([((System.Collections.Generic.Dictionary program, 0L, 0L), (startPos, 0L))])
    let mutable seenPositions = Set.empty
    let mutable keepRunning   = true
    while keepRunning && stateQueue.Count <> 0 do
        let         thisState                                        = stateQueue.Dequeue ()
        let         (currentX, currentY), movesSoFar                 = snd thisState
        let mutable (workingProgram, workingPC, workingRelativeBase) = fst thisState
        if not isPart1 then
            ret <- movesSoFar
        while workingProgram[workingPC] % 100L <> 3 do
            let (pc, relbase) = Intcode.stepProgram workingProgram workingPC workingRelativeBase (fun _ -> 0L) (fun _ -> ())
            workingPC           <- pc.Value
            workingRelativeBase <- relbase
        for dir in 1L..4L do
            let mutable returnCode                                       = None
            let mutable (thisDirProgram, thisDirPC, thisDirRelativeBase) = (System.Collections.Generic.Dictionary workingProgram, workingPC, workingRelativeBase)
            while returnCode.IsNone do
                let (pc, relbase) = Intcode.stepProgram thisDirProgram thisDirPC thisDirRelativeBase (fun () -> dir) (fun value -> returnCode <- Some(value))
                thisDirPC           <- pc.Value
                thisDirRelativeBase <- relbase
            let getNewPos () = match dir with
                                | 1L -> (currentX,      currentY - 1L)
                                | 2L -> (currentX,      currentY + 1L)
                                | 3L -> (currentX - 1L, currentY)
                                | 4L -> (currentX + 1L, currentY)
                                | x  -> assert(false); (0L, 0L)
            match returnCode.Value with
                | 0L -> ()
                | 1L ->
                    let newPos = getNewPos ()
                    if not (seenPositions.Contains newPos) then
                        seenPositions <- seenPositions.Add newPos
                        stateQueue.Enqueue ((thisDirProgram, thisDirPC, thisDirRelativeBase), (newPos, movesSoFar + 1L))
                | 2L ->
                    if isPart1 then
                        ret          <- movesSoFar + 1L
                        endPoint     <- Some(getNewPos())
                        programAtEnd <- Some(thisDirProgram)
                        keepRunning  <- false
                | x  -> assert(false)
    (ret, endPoint, programAtEnd |> Option.map (fun d -> d |> Seq.map (fun p -> p.Key, p.Value) |> Map))

let program = System.IO.File.ReadAllText("2019/dec15.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map

let current = System.DateTime.Now
let (minDistance, endPos, programAtEnd) = doBFS program (0L, 0L) true
printfn "%i" minDistance

let postP1 = System.DateTime.Now
printfn "%i" ((fun (value, _, _) -> value) (doBFS programAtEnd.Value endPos.Value false))
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
