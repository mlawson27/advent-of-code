﻿let TO_REMOVE = 200

let asteroidPositions = System.IO.File.ReadAllLines("2019/dec10.txt") |> Seq.mapi (fun y line -> (y, line))
                                                                      |> Seq.collect (fun (y, line) -> line |> Seq.mapi (fun x c -> (x, y, c))
                                                                                                            |> Seq.choose (fun (x, y, c) -> if c = '#' then Some(x, y) else None))
                                                                      |> Set

let current = System.DateTime.Now
let ((mostSeenX, mostSeenY), mostSeenCount) =
    let rec gcd x y =
        if y = 0 then x
        else gcd y (x % y)
    asteroidPositions |> Seq.map (fun (curX, curY) ->
        ((curX, curY), (asteroidPositions |> Seq.filter (fun (otherX, otherY) ->
            if curX <> otherX || curY <> otherY then
                let run        = otherX - curX
                let rise       = otherY - curY
                let riseRunGcd = gcd (abs rise) (abs run)
                let stepX      = run  / riseRunGcd
                let stepY      = rise / riseRunGcd
                seq { 1..((if stepX <> 0 then (otherX - curX) / stepX else (otherY - curY) / stepY) - 1) } |> Seq.map (fun mult -> (curX + stepX * mult, curY + stepY * mult))
                                                                                                           |> Seq.forall (fun pos -> not (asteroidPositions.Contains (pos)))
            else
                false
        ) |> Seq.length))
    ) |> Seq.maxBy (fun (_, seenCount) -> seenCount)
printfn "%i"  mostSeenCount
let postP1 = System.DateTime.Now

let (nthRemovedX, nthRemovedY) =
    let         quadrantFilters      = [| (fun (x, y) -> x >=  mostSeenX && y < mostSeenY);
                                          (fun (x, y) -> x > mostSeenX && y >=  mostSeenY);
                                          (fun (x, y) -> x <=  mostSeenX && y > mostSeenY);
                                          (fun (x, y) -> x < mostSeenX && y <=  mostSeenY) |]
    let mutable asteroidsToBeRemoved = asteroidPositions.Remove ((mostSeenX, mostSeenY))
    let mutable currentQuadrant      = 0
    let mutable removedCount         = 0
    let mutable nthRemoved           = None
    while nthRemoved.IsNone && not asteroidsToBeRemoved.IsEmpty do
        let toRemove = asteroidsToBeRemoved |> Seq.filter (quadrantFilters[currentQuadrant])
                                            |> Seq.groupBy (fun (x, y) -> (if x = mostSeenX then (if y < mostSeenY then -infinity else infinity) else (float (y - mostSeenY)) / (float (x - mostSeenX))))
                                            |> Seq.sortBy (fun (slope, _) -> slope)
                                            |> Seq.map(fun (_, group) -> group |> Seq.sortBy (fun (x, y) -> abs(x - mostSeenX) + abs(y - mostSeenY)) |> Seq.head)
                                            |> Seq.truncate(TO_REMOVE - removedCount)
                                            |> Seq.toList
        removedCount <- removedCount + toRemove.Length
        if removedCount < TO_REMOVE then
            asteroidsToBeRemoved <- toRemove |> Seq.fold (fun set elem -> set.Remove (elem)) asteroidsToBeRemoved
        else
            nthRemoved <- Some(toRemove |> List.last)
        currentQuadrant <- (currentQuadrant + 1) % quadrantFilters.Length
    nthRemoved.Value
printfn "%i"  (nthRemovedX * 100 + nthRemovedY)
let postP2 = System.DateTime.Now

printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
