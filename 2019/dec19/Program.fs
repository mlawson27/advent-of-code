﻿let runUntilPass program initialState maxX maxY shouldContinue onInRange onNewLine =
    let mutable (x, y)      = (0L, 0L)
    let mutable lastInRange = false
    let mutable startingX   = 0L
    let mutable lastLen     = 0L
    let mutable state       = initialState
    while y < maxY && shouldContinue state do
        let mutable askingForY = false
        let mutable isInRange  = false
        Intcode.runProgram (program) (fun () ->
            let ret = if askingForY then y else x
            askingForY <- not askingForY
            ret
        ) (fun value -> isInRange <- value = 1L) |> ignore
        if (isInRange || (x < 2L * y && not lastInRange)) && x < maxX then
            if isInRange then
                let mutable numInRange = 0L
                if not lastInRange then
                    startingX  <- x
                    numInRange <- min (max lastLen 1L) (max (maxX - x) 0L)
                else
                    numInRange <- 1L
                state <- onInRange state numInRange
                x     <- x + numInRange
            else
                lastLen <- lastLen - 1L
                x       <- x + 1L
            lastInRange <- isInRange
        else
            state       <- onNewLine state x startingX lastInRange
            lastLen     <- (x - startingX + 1L) * System.Convert.ToInt64 (lastInRange)
            y           <- y + 1L
            x           <- startingX
            lastInRange <- false
    state

let program = System.IO.File.ReadAllText("2019/dec19.txt").Split(',') |> Array.mapi(fun i valStr -> (int64 i, int64 valStr)) |> Map
let current = System.DateTime.Now

printfn "%i" (runUntilPass program 0L 50L 50L (fun _ -> true) (fun numInRange numNewInRange -> numInRange + numNewInRange) (fun numInRange _ _ _ -> numInRange))
let postP1 = System.DateTime.Now

printfn "%i" (
    let (startAndLengths, numRemoved) = runUntilPass program
                                                     (System.Collections.Generic.LinkedList<int64 * int64>(), 0L)
                                                     System.Int64.MaxValue
                                                     System.Int64.MaxValue
                                                     (fun (startAndLengths, _) -> startAndLengths.Count < 100 || ((snd startAndLengths.First.Value - (fst startAndLengths.Last.Value - fst startAndLengths.First.Value) < 100L)))
                                                     (fun state _ -> state)
                                                     (fun (startAndLengths, numRemoved) x startingX lastInRange ->
                                                        startAndLengths.AddLast (System.Collections.Generic.LinkedListNode (startingX, if lastInRange then (x - startingX) else 0L))
                                                        let mutable localNumRemoved = 0L
                                                        while startAndLengths.Count > 100 do
                                                            startAndLengths.RemoveFirst ()
                                                            localNumRemoved <- localNumRemoved + 1L
                                                        (startAndLengths, (numRemoved + localNumRemoved)))
    (fst startAndLengths.Last.Value) * 10000L + numRemoved
)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
