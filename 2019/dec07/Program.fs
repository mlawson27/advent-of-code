﻿let program = System.IO.File.ReadAllText("2019/dec07.txt").Split(',') |> Array.mapi(fun i value -> (int64 i, int64 value)) |> Map

let current = System.DateTime.Now
printfn "%i" (
    let mutable maxSignal = 0L
    for i in 0L..4L do
        for j in seq { 0L..4L } |> Seq.filter(fun v -> v <> i) do
            for k in seq { 0L..4L } |> Seq.filter(fun v -> v <> i && v <> j) do
                for l in seq { 0L..4L } |> Seq.filter(fun v -> v <> i && v <> j && v <> k) do
                    for m in seq { 0L..4L } |> Seq.filter(fun v -> v <> i && v <> j && v <> k && v <> l) do
                        let         thisVal    = (i * 10000L) + (j * 1000L) + (k * 100L) + (l * 10L) + m
                        let mutable lastOutput = 0L
                        let mutable inputDiv   = 10000L
                        for _ in 0..4 do
                            let mutable isFirst      = true
                            let         nextInput () =
                                if isFirst then
                                    let ret = (thisVal / inputDiv) % 10L
                                    inputDiv <- inputDiv / 10L
                                    isFirst  <- false
                                    ret
                                else
                                    lastOutput
                            Intcode.runProgram program nextInput (fun value -> (lastOutput <- value)) |> ignore
                        if lastOutput > maxSignal then
                            maxSignal <- lastOutput
    maxSignal)
let postP1 = System.DateTime.Now
printfn "%i" (
    let mutable maxSignal = 0L
    for i in 5L..9L do
        for j in seq { 5L..9L } |> Seq.filter(fun v -> v <> i) do
            for k in seq { 5L..9L } |> Seq.filter(fun v -> v <> i && v <> j) do
                for l in seq { 5L..9L } |> Seq.filter(fun v -> v <> i && v <> j && v <> k) do
                    for m in seq { 5L..9L } |> Seq.filter(fun v -> v <> i && v <> j && v <> k && v <> l) do
                        let mutable inputs         = [|Some(i); Some(j); Some(k); Some(l); Some(m)|]
                        let mutable lastOutput     = 0L
                        let mutable programsAndPCs = inputs |> Array.map (fun _ -> (System.Collections.Generic.Dictionary program, 0L))
                        let mutable doNext         = true
                        let mutable currentAmp     = 0
                        let nextInput () =
                            if inputs[currentAmp].IsSome then
                                let ret = inputs[currentAmp].Value
                                inputs[currentAmp] <- None
                                ret
                            else
                                lastOutput
                        while doNext do
                            let mutable stepNext = true
                            let onOutput value =
                                lastOutput <- value
                                stepNext   <- false
                            let mutable (program, pc) = programsAndPCs[currentAmp]
                            while stepNext do
                                let (newPCOrNone, _) = Intcode.stepProgram program pc 0L nextInput onOutput
                                if newPCOrNone.IsSome then
                                    pc <- newPCOrNone.Value
                                else
                                    stepNext <- false
                                    doNext   <- false
                            programsAndPCs[currentAmp] <- (program, pc)
                            currentAmp                 <- (currentAmp + 1) % programsAndPCs.Length
                        if lastOutput > maxSignal then
                            maxSignal <- lastOutput
    maxSignal)
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
