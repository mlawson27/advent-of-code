﻿let orbits = System.IO.File.ReadAllLines("2019/dec06.txt") |> Array.map(fun line -> line.Split(')'))
                                                           |> Array.fold (fun (map: Map<string, string>) elem -> map.Add (elem[1], elem[0])) Map.empty

let current = System.DateTime.Now
let rec countOrbits (planet: string) (seen: Set<string>) =
    let mutable ret = seen
    if orbits.ContainsKey planet then
        let thisOrbits = orbits[planet]
        if not (seen.Contains thisOrbits) then
            ret <- ret.Add(planet)
            ret <- countOrbits thisOrbits ret
    ret
printfn "%i" (orbits.Keys |> Seq.map (fun planet -> (countOrbits planet Set.empty).Count)
                          |> Seq.sum)
let postP1 = System.DateTime.Now
let minDistanceTo (first: string) (last: string) =
    let addToSet (this: string) (other: string) (set: Map<string, Set<string>>) =
        set.Change (this, (fun valOpt -> if valOpt.IsSome then Some(valOpt.Value.Add (other)) else Some(Set([other]))))

    let orbitsAndOrbitedBy = orbits |> (Seq.fold (fun map (KeyValue(planet, other)) -> addToSet other planet (addToSet planet other map)) Map.empty)

    let mutable working = [(orbits[first], 0L)]
    let mutable doNext  = true
    let mutable ret     = 0L
    let mutable seen    = Set.empty
    while doNext do
        let (current, stepsSoFar) = working.Head
        working <- working[1..]
        doNext  <- current <> orbits[last]
        if doNext then
            for other in orbitsAndOrbitedBy[current] do
                if not (seen.Contains other) then
                    seen <- seen.Add(other)
                    working <- working @ [(other, stepsSoFar + 1L)]
        else
            ret <- stepsSoFar
    ret
printfn "%i" (minDistanceTo "YOU" "SAN")
let postP2 = System.DateTime.Now
printfn "p1: %d us" (int64 (postP1 - current).TotalMicroseconds)
printfn "p2: %d us" (int64 (postP2 - postP1) .TotalMicroseconds)
