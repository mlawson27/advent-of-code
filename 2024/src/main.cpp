#include <array>
#include <chrono>
#include <cstring>
#include <print>
#include <span>

#include "helpers.h"

int dec01();
int dec02();
int dec03();
int dec04();
int dec05();
int dec06();
int dec07();
int dec08();
int dec09();
int dec10();
int dec11();
int dec12();
int dec13();
int dec14();
int dec15();
int dec16();
int dec17();
int dec18();
int dec19();
int dec20();
int dec21();
int dec22();
int dec23();
int dec24();
int dec25();

static constexpr auto DAY_TO_RUN_FN = std::array {
  dec01,
  dec02,
  dec03,
  dec04,
  dec05,
  dec06,
  dec07,
  dec08,
  dec09,
  dec10,
  dec11,
  dec12,
  dec13,
  dec14,
  dec15,
  dec16,
  dec17,
  dec18,
  dec19,
  dec20,
  dec21,
  dec22,
  dec23,
  dec24,
  dec25,
};

auto main(int argc, char const* argv[]) -> int
{
  std::optional<char const*> arg = at_or_none(std::span(argv, argc), 1);
  if (!arg.has_value())
  {
    std::println("Specify a day of the month to run or --all to run all days");
    return 1;
  }
  if (strcmp(*arg, "--all") == 0)
  {
    std::chrono::time_point start = std::chrono::steady_clock::now();
    for (std::size_t i = 0; i < DAY_TO_RUN_FN.size(); i++)
    {
      auto ret = DAY_TO_RUN_FN[i]();
      if (ret != 0)
      {
        std::println("Error running day {}", (i + 1));
        return ret;
      }
      else
      {
        std::println();
      }
    }
    std::chrono::time_point end = std::chrono::steady_clock::now();
    std::println("total time: {}", std::chrono::duration_cast<std::chrono::microseconds>(end - start));
    return 0;
  }
  std::expected<std::size_t, std::error_code> day = parse<std::size_t>(std::string_view(*arg));
  if (!day.has_value())
  {
    std::println("Invalid day: {}", day.error().message());
    return 2;
  }
  if ((*day > DAY_TO_RUN_FN.size()) || (*day == 0))
  {
    std::println("Day out of range: {}", *day);
    return 3;
  }
  return DAY_TO_RUN_FN[*day - 1]();
}
