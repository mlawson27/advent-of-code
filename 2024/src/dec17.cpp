#include <chrono>
#include <cinttypes>
#include <cstdio>
#include <expected>
#include <fstream>
#include <print>
#include <span>
#include <utility>
#include <vector>

using register_set = std::array<uint_fast64_t, 3>;

template <typename Callable>
static constexpr void run_program(register_set const& initial_reg_values, std::span<uint8_t> program, Callable&& handle_output)
{
  std::size_t  ic                = 0;
  register_set regs              = initial_reg_values;
  const auto   get_combo_operand = [&program, &ic, &regs]() -> register_set::value_type {
                                     switch (program[ic + 1])
                                     {
                                       case 0:
                                       case 1:
                                       case 2:
                                       case 3:
                                         return program[ic + 1];
                                       case 4:
                                       case 5:
                                       case 6:
                                         return regs[program[ic + 1] - 4];
                                       case 7:
                                         return 0; // Invalid
                                       default:
                                         std::unreachable();
                                     }
                                   };
  while (ic < program.size())
  {
    switch (program[ic])
    {
      case 0:
        regs['A' - 'A'] = regs['A' - 'A'] / (1u << get_combo_operand());
        break;
      case 1:
        regs['B' - 'A'] = regs['B' - 'A'] ^ static_cast<register_set::value_type>(program[ic + 1]);
        break;
      case 2:
        regs['B' - 'A'] = get_combo_operand() & 0b111;
        break;
      case 3:
        if (regs['A' - 'A'] != 0)
        {
          ic = program[ic + 1];
          continue;
        }
        break;
      case 4:
        regs['B' - 'A'] = regs['B' - 'A'] ^ regs['C' - 'A'];
        break;
      case 5:
        handle_output(get_combo_operand() & 0b111);
        break;
      case 6:
        regs['B' - 'A'] = regs['A' - 'A'] / (1u << get_combo_operand());
        break;
      case 7:
        regs['C' - 'A'] = regs['A' - 'A'] / (1u << get_combo_operand());
        break;
      default:
        std::unreachable();
    }
    ic += 2;
  }
}

static void part1(register_set const& initial_reg_values, std::span<uint8_t> program)
{
  char prefix = 0;
  run_program(initial_reg_values, program, [&prefix](register_set::value_type v) {
                                             std::print("{}{}", prefix, v);
                                             prefix = ',';
                                           });
  std::println();
}

static void part2(register_set const& initial_reg_values, std::span<uint8_t> program)
{
  auto const find_inner = [&initial_reg_values, &program](register_set::value_type working, auto&& it_cur, register_set::value_type to_test, auto&& find_inner) -> std::optional<register_set::value_type>
  {
    if (it_cur == program.rend())
    {
      return working;
    }
    to_test = (to_test << 3) | (*it_cur);
    for (register_set::value_type next_v = 0b000; next_v < 0b111; next_v++)
    {
      register_set::value_type result   = 0;
      std::size_t              shift    = 0;
      register_set             tmp_regs = initial_reg_values;
      tmp_regs['A' - 'A']               = (working << 3) | next_v;
      run_program(tmp_regs, program, [&result, &shift](register_set::value_type v) {
                                       result |= (v << shift);
                                       shift  += 3;
                                     });
      if (std::optional<register_set::value_type> inner_ret;
          ((result == to_test) &&
           (inner_ret = find_inner(tmp_regs['A' - 'A'], std::next(it_cur), to_test, find_inner)).has_value()))
      {
        return inner_ret;
      }
    }
    return std::nullopt;
  };
  std::println("{}", *find_inner(0, program.rbegin(), 0, find_inner));
}

int dec17()
{
  namespace chr = std::chrono;

  auto reg_values_and_program_or_err = []() -> std::expected<std::pair<register_set, std::vector<uint8_t>>, std::string_view>
  {
    std::unique_ptr<FILE, void(*)(FILE*)> f(fopen("input/dec17.txt", "r"),
                                            [](FILE* f) { fclose(f); });
    register_set regs;
    for (std::size_t i = 0; i < regs.size(); i++)
    {
      char reg_name;
      if (fscanf(f.get(), "Register %c: %" PRIuFAST64 " ", &reg_name, &regs[i]) != 2)
      {
        return std::unexpected("Can't parse initial register value line");
      }
      if (reg_name != static_cast<char>('A' + i))
      {
        return std::unexpected("Register inital values specified out of order");
      }
    }
    uint8_t program_value[2];
    if (fscanf(f.get(), "Program: %1[0-9]", program_value) != 1)
    {
      return std::unexpected("Can't parse first program value");
    }
    long current = ftell(f.get());
    fseek(f.get(), 0, SEEK_END);
    std::vector<uint8_t> program;
    program.reserve(((ftell(f.get()) - current) / 2) + 1);
    fseek(f.get(), current, SEEK_SET);
    while (true)
    {
      program.push_back(program_value[0] - '0');
      int ret = fscanf(f.get(), ",%1[0-9]", program_value);
      if (ret == EOF)
      {
        break;
      }
      if (ret != 1)
      {
        return std::unexpected("Can't parse program value");
      }
    }
    if (program.capacity() != program.size())
    {
      return std::unexpected("File has more data");
    }
    return std::pair{ std::move(regs), std::move(program) };
  }();
  if (!reg_values_and_program_or_err.has_value())
  {
    std::println("Error: {}", reg_values_and_program_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *reg_values_and_program_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *reg_values_and_program_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
