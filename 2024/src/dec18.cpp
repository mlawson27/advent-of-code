#include <algorithm>
#include <bitset>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <queue>
#include <span>

using position = std::array<uint32_t, 2>;

static constexpr position BOUNDS = { 70, 70 };

using position_set = std::bitset<(BOUNDS[0] + 1) * (BOUNDS[1] + 1)>;

template <typename T>
requires std::is_same_v<std::remove_const_t<std::remove_reference_t<T>>, position_set>
static constexpr auto get_at(T&& set, position const& pos) -> decltype(std::declval<T>()[0])
{
  return std::forward<T>(set)[pos[0] * (BOUNDS[1] + 1) + pos[1]];
}

static constexpr auto find_exit(position_set const& byte_positions)
{
  std::queue<std::pair<position, std::size_t>> position_queue;
  position_set                                 seen_set;
  std::optional<std::size_t>                   ret;
  position_queue.emplace(position{ 0, 0 }, 0zu);
  get_at(seen_set, { 0, 0 }) = true;
  while (!position_queue.empty())
  {
    auto [working_pos, moves_so_far] = std::move(position_queue.front());
    position_queue.pop();
    if (working_pos == BOUNDS)
    {
      ret = moves_so_far;
      break;
    }
    for (position const& other_pos : { position{ working_pos[0],     working_pos[1] + 1 },
                                       position{ working_pos[0] + 1, working_pos[1] },
                                       position{ working_pos[0],     working_pos[1] - 1 },
                                       position{ working_pos[0] - 1, working_pos[1] } })
    {
      if (std::ranges::all_of(std::views::zip_transform(std::less_equal{}, other_pos, BOUNDS),
                              [](bool v) { return v; }) &&
          !get_at(seen_set, other_pos) &&
          !get_at(byte_positions, other_pos))
      {
        get_at(seen_set, other_pos) = true;
        position_queue.emplace(other_pos, moves_so_far + 1);
      }
    }
  }
  return ret;
}

static void part1(std::span<position> byte_position_list)
{
  std::span<position> byte_position_sublist = byte_position_list.subspan(0, 1024);
  position_set        byte_positions;
  std::ranges::for_each(byte_position_sublist, [&byte_positions](auto const& pos) { get_at(byte_positions, pos) = true; });
  std::println("{}", *find_exit(byte_positions));
}

static void part2(std::span<position> byte_position_list)
{
  std::span<position> byte_position_sublist = byte_position_list.subspan(0, 1024zu + 1);
  position_set        byte_positions;
  std::ranges::for_each(byte_position_sublist, [&byte_positions](auto const& pos) { get_at(byte_positions, pos) = true; });
  std::size_t         first_cant_exit_pos = *std::ranges::partition_point(std::views::iota(byte_position_sublist.size(), byte_position_list.size()),
                                                                          [&byte_positions, &byte_position_list, &byte_position_sublist](std::size_t pos_to_try) {
                                                                            if (pos_to_try >= byte_positions.count())
                                                                            {
                                                                              byte_position_sublist = byte_position_list.subspan(byte_positions.count(), pos_to_try + 1 - byte_positions.count());
                                                                              std::ranges::for_each(byte_position_sublist, [&byte_positions](auto const& pos) { get_at(byte_positions, pos) = true; });
                                                                            }
                                                                            else
                                                                            {
                                                                              byte_position_sublist = byte_position_list.subspan(pos_to_try + 1, byte_positions.count() - pos_to_try);
                                                                              std::ranges::for_each(byte_position_sublist, [&byte_positions](auto const& pos) { get_at(byte_positions, pos) = false; });
                                                                            }
                                                                            return find_exit(byte_positions).has_value();
                                                                          });
  std::println("{},{}", byte_position_list[first_cant_exit_pos][1], byte_position_list[first_cant_exit_pos][0]);
}

int dec18()
{
  namespace chr = std::chrono;

  auto byte_positions_or_err = []() -> std::expected<std::vector<position>, std::string_view>
  {
    std::ifstream f("input/dec18.txt");
    std::size_t num_lines = 1;
    for (char c; f.get(c);)
    {
      num_lines += (c == '\n');
    }
    f.clear(std::ios::eofbit);
    f.seekg(0, std::ios::beg);
    std::vector<position> ret_vec;
    ret_vec.reserve(num_lines);
    while (true)
    {
      position new_pos;
      char     sep;
      f >> new_pos[1] >> sep >> new_pos[0];
      if (!f)
      {
        break;
      }
      if (sep != ',')
      {
        return std::unexpected("Unable to parse line");
      }
      ret_vec.push_back(std::move(new_pos));
    }
    if (ret_vec.size() != num_lines)
    {
      return std::unexpected("Reserved wrong size");
    }
    return ret_vec;
  }();
  if (!byte_positions_or_err.has_value())
  {
    std::println("Error: {}", byte_positions_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*byte_positions_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*byte_positions_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
