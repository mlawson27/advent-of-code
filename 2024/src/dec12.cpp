#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <queue>
#include <ranges>
#include <span>

#include "helpers.h"

using char_grid = grid<char>;

template <typename Callable1, typename Callable2, typename Callable3>
static constexpr auto find_total_price(char_grid const& garden_map, Callable1&& on_queued_value, Callable2&& before_queue_value, Callable3&& post_boundary_find) -> std::size_t
{
  std::size_t ret = 0;

  std::queue<std::array<std::size_t, 2>> seen_queue;
  grid<uint16_t> seen_map { garden_map.num_rows(), garden_map.num_cols(), std::vector<uint16_t>(garden_map.num_rows() * garden_map.num_cols(), 0) };
  uint16_t       seen_count = 1;

  for (auto&& [row, col] : std::views::cartesian_product(std::views::iota(0zu, garden_map.num_rows()),
                                                         std::views::iota(0zu, garden_map.num_cols())))
  {
    if (seen_map[row, col] != 0)
    {
      continue;
    }

    std::size_t area = 0, other_v = 0;

    seen_queue.push({ row, col });
    seen_map[row, col] = seen_count;
    while (!seen_queue.empty())
    {
      auto [w_row, w_col] = std::move(seen_queue.front());
      seen_queue.pop();

      area++;
      std::forward<Callable1>(on_queued_value)(other_v, w_row, w_col);

      for (auto&& pos_i : { std::array{ w_row - 1, w_col },
                            std::array{ w_row,     w_col + 1 },
                            std::array{ w_row + 1, w_col },
                            std::array{ w_row,     w_col - 1 } })
      {
        auto&& [row_i, col_i] = pos_i;
        if (auto seen_char_or_none = seen_map.get(row_i, col_i);
            seen_char_or_none.has_value() &&
            (garden_map[row_i, col_i] == garden_map[w_row, w_col]))
        {
          std::forward<Callable2>(before_queue_value)(other_v);
          if (seen_char_or_none->get() == 0)
          {
            seen_map[row_i, col_i] = seen_count;
            seen_queue.push(std::move(pos_i));
          }
        }
      }
    }
    std::forward<Callable3>(post_boundary_find)(other_v, area, row, col, seen_map, seen_count);
    ret += (area * other_v);
    seen_count++;
  }
  return ret;
}

static void part1(char_grid const& garden_map)
{
  std::println("{}", find_total_price(garden_map,
                                      [](std::size_t& perimeter, auto&&...) { perimeter += 4; },
                                      [](std::size_t& perimeter)            { perimeter--; },
                                      [](auto&&...)                         {}));
}

static void part2(char_grid const& garden_map)
{
  std::size_t min_row = garden_map.num_rows(), max_row = 0;
  std::size_t min_col = garden_map.num_cols(), max_col = 0;

  auto const find_num_sides = [&min_row, &max_row, &min_col, &max_col, &garden_map](std::size_t& num_sides, std::size_t area, std::size_t first_row, std::size_t first_col, auto const& seen_map, std::size_t seen_count) {
    num_sides = 4;
    std::size_t row_bound = max_row - min_row;
    std::size_t col_bound = max_col - min_col;
    if (area != ((row_bound + 1) * (col_bound + 1)))
    {
      char_grid sub_seen { (row_bound + 1), (col_bound + 1), std::vector<char>((row_bound + 1) * (col_bound + 1), 0) };
      for (auto&& [outer_bound, inner_bound, outer_is_row] : { std::tuple{ row_bound, col_bound, true },
                                                               std::tuple{ col_bound, row_bound, false } })
      {
        auto const get_from_grid = [&min_row, &min_col, &outer_is_row](auto& grid, std::size_t w_out, std::size_t w_in) {
          return grid[(outer_is_row ? w_out : w_in) + min_row, (outer_is_row ? w_in : w_out) + min_col];
        };

        for (std::size_t w_out = 0; w_out <= outer_bound; w_out++)
        {
          for (std::size_t w_in = 0; w_in < inner_bound; w_in++)
          {
            std::size_t const& w_row = (outer_is_row ? w_out : w_in);
            std::size_t const& w_col = (outer_is_row ? w_in  : w_out);
            if (bool curr_is_c = (garden_map[w_row + min_row, w_col + min_col] == garden_map[first_row, first_col]);
                (seen_map[w_row + min_row + (!outer_is_row && !curr_is_c), w_col + min_col + (outer_is_row && !curr_is_c)] == seen_count) &&
                (curr_is_c != (garden_map[w_row + min_row + !outer_is_row, w_col + min_col + outer_is_row] == garden_map[first_row, first_col])) &&
                ((sub_seen[w_row + (!outer_is_row && curr_is_c), w_col + (outer_is_row && curr_is_c)] & (1 << ((outer_is_row << 1) | curr_is_c))) == 0))
            {
              num_sides++;
              std::size_t not_eq_row = w_row + (!outer_is_row && curr_is_c);
              std::size_t not_eq_col = w_col + ( outer_is_row && curr_is_c);
              sub_seen[not_eq_row, not_eq_col] |= (1 << ((outer_is_row << 1) | curr_is_c));
              for (auto&& mult : { 1z, -1z })
              {
                for (std::size_t adj = 1;
                    (sub_seen.get(not_eq_row + (mult * adj * outer_is_row), not_eq_col + (mult * adj * !outer_is_row)).has_value() &&
                      (garden_map[w_row + min_row + (!outer_is_row && !curr_is_c) + (mult * adj * outer_is_row), w_col + min_col + (outer_is_row && !curr_is_c) + (mult * adj * !outer_is_row)] == garden_map[first_row, first_col]) &&
                      (garden_map[not_eq_row + min_row + (mult * adj * outer_is_row), not_eq_col + min_col + (mult * adj * !outer_is_row)] != garden_map[first_row, first_col]));
                    adj++)
                {
                  sub_seen[not_eq_row + (mult * adj * outer_is_row), not_eq_col + (mult * adj * !outer_is_row)] |= (1 << ((outer_is_row << 1) | curr_is_c));
                }
              }
            }
          }
          for (auto&& bound : { 0zu, inner_bound })
          {
            if ((w_out > 0) &&
                (w_out < outer_bound) &&
                (get_from_grid(garden_map, w_out - 1, bound) == garden_map[first_row, first_col]) &&
                (get_from_grid(seen_map,   w_out - 1, bound) == seen_count))
            {
              std::size_t sub_out;
              for (sub_out = w_out; sub_out < outer_bound && get_from_grid(garden_map, sub_out, bound) != garden_map[first_row, first_col]; sub_out++);
              num_sides += ((sub_out > w_out) &&
                            (get_from_grid(garden_map, sub_out, bound) == garden_map[first_row, first_col]) &&
                            (get_from_grid(seen_map,   sub_out, bound) == seen_count));
            }
          }
        }
      }
    }
    min_row = garden_map.num_rows();
    max_row = 0;
    min_col = garden_map.num_cols();
    max_col = 0;
  };
  std::println("{}", find_total_price(garden_map,
                                      [&min_row, &max_row, &min_col, &max_col](std::size_t&, std::size_t row, std::size_t col) {
                                        min_row = std::min(min_row, row);
                                        max_row = std::max(max_row, row);
                                        min_col = std::min(min_col, col);
                                        max_col = std::max(max_col, col);
                                      },
                                      [](std::size_t&) {},
                                      find_num_sides));
}

int dec12()
{
  namespace chr = std::chrono;

  auto garden_map_or_err = []() -> std::expected<char_grid, std::string_view>
  {
    std::ifstream f("input/dec12.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::vector<char> ret_vec;
    std::size_t       num_rows = (total_len + 1) / (line_len + 1);
    ret_vec.reserve(num_rows * line_len);
    ret_vec.insert(ret_vec.end(),
                   std::istream_iterator<char>{ f },
                   std::istream_iterator<char>{});
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return char_grid{ num_rows, line_len, std::move(ret_vec) };
  }();
  if (!garden_map_or_err.has_value())
  {
    std::println("Error: {}", garden_map_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*garden_map_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*garden_map_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
