#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <span>
#include <string>
#include <unordered_map>

#include "helpers.h"

using char_grid = grid<char>;

struct pos_hasher
{
  constexpr auto operator()(std::array<uint32_t, 2> const& v) const -> uint64_t
  {
    return std::bit_cast<uint64_t>(v);
  }
};

template <bool ALL_POSSIBLE_WAYS>
static constexpr auto get_scores(char_grid const& topography_map) -> std::size_t
{
  std::unordered_map<std::array<uint32_t, 2>, std::size_t, pos_hasher> seen_positions;
  auto const get_score = [&topography_map, &seen_positions](std::size_t row, std::size_t col) {
    if (topography_map[row, col] != '0')
    {
      return 0zu;
    }
    if constexpr (!ALL_POSSIBLE_WAYS)
    {
      seen_positions.clear();
    }
    auto const get_score_inner = [&topography_map, &seen_positions](std::array<uint32_t, 2> const& pos, std::size_t working_score, auto&& get_score_inner) {
      if (auto it = seen_positions.find(pos);
          it != seen_positions.end())
      {
        return it->second * ALL_POSSIBLE_WAYS;
      }
      auto current_c = std::apply([&topography_map](std::size_t const& row, std::size_t const& col) {
                                    return topography_map[row, col];
                                  }, pos);
      if (current_c == '9')
      {
        seen_positions.emplace(pos, 1zu);
        return 1zu;
      }
      auto&& [row, col] = pos;
      using pos_type = std::remove_const_t<std::remove_reference_t<decltype(pos)>>;
      auto ret = std::ranges::fold_left(std::array{ pos_type{ row - 1, col },
                                                    pos_type{ row,     col + 1 },
                                                    pos_type{ row + 1, col },
                                                    pos_type{ row,     col - 1 } } | std::views::filter([&topography_map, &current_c](pos_type const& sub_pos) {
                                                                                                          return std::apply([&topography_map](std::size_t const& row, std::size_t const& col) {
                                                                                                                              return topography_map.get(row, col).transform([](auto const& c_ref) { return c_ref.get(); });
                                                                                                                            }, sub_pos).value_or(current_c) == (current_c + 1);
                                                                                                        })
                                                                                   | std::views::transform([&working_score, &get_score_inner](pos_type const& sub_pos) {
                                                                                                             return get_score_inner(sub_pos, working_score, get_score_inner);
                                                                                                           }),
                                        0zu,
                                        std::plus{});
      seen_positions.emplace(pos, ret);
      return ret;
    };
    return get_score_inner(std::array{ static_cast<uint32_t>(row), static_cast<uint32_t>(col) }, 0, get_score_inner);
  };

  return std::ranges::fold_left(std::views::cartesian_product(std::views::iota(0zu, topography_map.num_rows()),
                                                              std::views::iota(0zu, topography_map.num_cols())) | std::views::transform([&get_score](auto const& pos_as_size) {
                                                                                                                                          return std::apply([&get_score](auto&&... sub_v) {
                                                                                                                                                              return get_score(sub_v...);
                                                                                                                                                            }, pos_as_size);
                                                                                                                                        }),
                                0zu,
                                std::plus{});
}

static void part1(char_grid const& topography_map)
{
  std::println("{}", get_scores<false>(topography_map));
}

static void part2(char_grid const& topography_map)
{
  std::println("{}", get_scores<true>(topography_map));
}

int dec10()
{
  namespace chr = std::chrono;

  auto topography_map_or_err = []() -> std::expected<char_grid, std::string_view>
  {
    std::ifstream f("input/dec10.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::vector<char> ret_vec;
    std::size_t       num_rows = (total_len + 1) / (line_len + 1);
    ret_vec.reserve(num_rows * line_len);
    ret_vec.insert(ret_vec.end(),
                   std::istream_iterator<char>{ f },
                   std::istream_iterator<char>{});
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return char_grid{ num_rows, line_len, std::move(ret_vec) };
  }();
  if (!topography_map_or_err.has_value())
  {
    std::println("Error: {}", topography_map_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*topography_map_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*topography_map_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
