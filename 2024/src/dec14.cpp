#include <algorithm>
#include <chrono>
#include <cstdio>
#include <expected>
#include <print>
#include <ranges>
#include <span>

using pos_and_velo = std::array<std::array<std::ptrdiff_t, 2>, 2>;

static constexpr pos_and_velo::value_type GRID_DIMS = { 101, 103 };

static void part1(std::span<pos_and_velo const> pos_and_velos)
{
  std::vector<pos_and_velo::value_type> final_positions;
  final_positions.reserve(pos_and_velos.size());
  std::ranges::transform(pos_and_velos,
                         std::back_inserter(final_positions),
                         [](auto const& pos_and_velo) {
                           auto&& [pos, velo] = pos_and_velo;
                           std::remove_const_t<decltype(pos)> new_pos;
                           std::ranges::copy(std::views::zip_transform([](auto&& p, auto&& d, auto&& v) { return (((p + (v * 100)) % d) + d) % d; },
                                                                       pos, GRID_DIMS, velo),
                                             new_pos.begin());
                           return new_pos;
                         });

  auto const quadrant_counts = std::ranges::fold_left(final_positions,
                                                      std::array<std::size_t, 4>{},
                                                      [](auto&& arr, auto&& pos) {
                                                        arr[(pos[1] > GRID_DIMS[1] / 2) * 2 +
                                                            (pos[0] > GRID_DIMS[0] / 2)] += ((pos[0] != GRID_DIMS[0] / 2) && (pos[1] != GRID_DIMS[1] / 2));
                                                        return arr;
                                                      });

  std::println("{}", std::ranges::fold_left(quadrant_counts, 1zu, std::multiplies{}));
}

static void part2(std::span<pos_and_velo const> pos_and_velos)
{
  static constexpr std::ptrdiff_t TREE_BOX_SIDE_LEN = 32;

  std::vector<pos_and_velo::value_type> working_positions;
  working_positions.reserve(pos_and_velos.size());
  std::ranges::transform(pos_and_velos,
                         std::back_inserter(working_positions),
                         [](auto const& pos_and_velo) { return std::get<0>(pos_and_velo); });

  std::array<std::array<std::optional<std::size_t>, 2>, GRID_DIMS.size()> patterns;

  std::ranges::for_each(std::views::iota(1zu, std::numeric_limits<std::size_t>::max()) |
                          std::views::take_while([&working_positions, &pos_and_velos, &patterns](std::size_t i) {
                                                   std::remove_reference_t<decltype(working_positions[0])> average_pos = {};
                                                   std::ranges::for_each(std::views::zip(working_positions,
                                                                                         pos_and_velos | std::views::values),
                                                                         [&average_pos](auto&& pos_and_velo) {
                                                                           auto&& [pos, velo] = pos_and_velo;
                                                                           std::ranges::transform(std::views::zip(pos, GRID_DIMS, velo, average_pos),
                                                                                                  pos.begin(),
                                                                                                  [](auto&& pos_dim_velo_elem) {
                                                                                                    auto&& [p, d, v, a] = pos_dim_velo_elem;
                                                                                                    auto ret = (p + d + v) % d;
                                                                                                    a += ret;
                                                                                                    return ret;
                                                                                                  });
                                                                         });
                                                   std::ranges::for_each(average_pos,
                                                                         [&working_positions](auto&& pos_elem) { pos_elem /= working_positions.size(); });

                                                   bool all_dims_this_iter = true;
                                                   for (std::size_t dim = 0; dim < patterns.size(); dim++)
                                                   {
                                                     std::size_t out_of_range_count = 0;
                                                     for (auto&& working_pos : working_positions)
                                                     {
                                                       out_of_range_count += (std::abs(working_pos[dim] - average_pos[dim]) > (TREE_BOX_SIDE_LEN / 2));
                                                       if (out_of_range_count >= (working_positions.size() / 2))
                                                       {
                                                         all_dims_this_iter = false;
                                                         break;
                                                       }
                                                     }
                                                     if (decltype(patterns[dim].begin()) next_pat_it;
                                                         (out_of_range_count < (working_positions.size() / 2)) &&
                                                         ((next_pat_it = std::ranges::find_if(patterns[dim], [](auto&& x) { return !x.has_value(); })) != patterns[dim].end()))
                                                     {
                                                       *next_pat_it = i;
                                                     }
                                                   }
                                                   return !all_dims_this_iter && std::ranges::any_of(patterns,
                                                                                                     [](auto const& pattern_dim) { return std::ranges::any_of(pattern_dim, [](auto const& opt) { return !opt.has_value(); }); });
                           }), [](auto&&...){});
  std::println("{}", [&patterns]() {
                       for (std::size_t n = 0; n < patterns[0].size(); n++)
                       {
                         if (std::all_of(std::next(patterns.begin()),
                                         patterns.end(),
                                         [&n, &patterns](auto&& p) { return p[n] == patterns[0][n]; }))
                         {
                           return *patterns[0][n];
                         }
                       }
                       std::array<decltype(patterns)::value_type::value_type::value_type, patterns.size()> slopes;
                       std::ranges::transform(patterns,
                                              slopes.begin(),
                                              [](auto&& dim) { return *dim[1] - *dim[0]; });
                       std::size_t ret;
                       for (ret = *patterns[0][0]; (ret - *patterns[1][0]) % slopes[1] != 0; ret += slopes[0]);
                       return ret;
                     }());
}

int dec14()
{
  namespace chr = std::chrono;

  auto pos_and_velos_or_err = []() -> std::expected<std::vector<pos_and_velo>, std::string_view>
  {
    std::unique_ptr<FILE, void(*)(FILE*)> f(fopen("input/dec14.txt", "r"),
                                            [](FILE* f) { fclose(f); });
    std::size_t num_lines = 1;
    for (int c; (c = fgetc(f.get())) != EOF; num_lines += (c == '\n'));
    fseek(f.get(), 0, SEEK_SET);
    std::vector<pos_and_velo> ret_vec;
    ret_vec.reserve(num_lines);
    while (!feof(f.get()))
    {
      auto& [p, v] = ret_vec.emplace_back();
      if (fscanf(f.get(), " p=%zd,%zd v=%zd,%zd", &p[0], &p[1], &v[0], &v[1]) != 4)
      {
        return std::unexpected("Couldn't parse line");
      }
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return ret_vec;
  }();
  if (!pos_and_velos_or_err.has_value())
  {
    std::println("Error: {}", pos_and_velos_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*pos_and_velos_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*pos_and_velos_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
