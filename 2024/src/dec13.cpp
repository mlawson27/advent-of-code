#include <algorithm>
#include <array>
#include <chrono>
#include <cinttypes>
#include <cmath>
#include <cstdio>
#include <expected>
#include <iterator>
#include <optional>
#include <print>
#include <ranges>
#include <span>

#include "helpers.h"

enum
{
  BUTTON_A = 0,
  BUTTON_B = 1,
  PRIZE    = 2,
};

using machine = std::array<std::array<uint32_t, 2>, 3>;

template <arithmetic T, std::size_t N, arithmetic U>
static constexpr auto operator*(std::array<T, N> const& l, U r) -> std::array<decltype (T{} * U{}), N>
{
  std::array<decltype (T{} * U{}), N> ret;
  std::ranges::transform(l,
                         ret.begin(),
                         [&r](T v) { return v * r; });
  return ret;
}

template <arithmetic T, std::size_t N, arithmetic U>
static constexpr auto operator+(std::array<T, N> const& l, std::array<U, N> const& r) -> std::array<decltype (T{} + U{}), N>
{
  std::array<decltype (T{} + U{}), N> ret;
  std::ranges::copy(std::views::zip_transform(std::plus{}, l, r),
                    ret.begin());
  return ret;
}

template <bool PART_1>
static constexpr auto get_total_tokens_required(std::span<machine const> machine_list) -> std::size_t
{
  static constexpr int_fast64_t MAX_PRESSES_ALLOWED = 100;
  static constexpr int_fast64_t PRIZE_ADJUSTMENT    = 10000000000000 * !PART_1;

  static constexpr auto get_presses_required = [](machine const& m) -> std::optional<std::array<std::size_t, 2>> {
    std::array<int_fast64_t, std::size(m[PRIZE])> prize_adjusted;
    std::ranges::transform(m[PRIZE],
                           prize_adjusted.begin(),
                           [](uint32_t prize) { return prize + PRIZE_ADJUSTMENT; });

    auto eqns = std::array{ std::to_array<int_fast64_t>({ m[BUTTON_A][0], m[BUTTON_B][0], prize_adjusted[0] }),
                            std::to_array<int_fast64_t>({ m[BUTTON_A][1], m[BUTTON_B][1], prize_adjusted[1] }) };

    auto new_eq        = (eqns[0] * eqns[1][BUTTON_A]) + (eqns[1] * -eqns[0][BUTTON_A]);
    auto b_presses_div = std::div(new_eq[2], new_eq[1]);
    if ((b_presses_div.rem != 0) ||
        (b_presses_div.quot < 0) ||
        (PART_1 && (b_presses_div.quot > MAX_PRESSES_ALLOWED)))
    {
      return std::nullopt;
    }
    auto a_presses_div = std::div(eqns[0][PRIZE] - (eqns[0][BUTTON_B] * b_presses_div.quot), eqns[0][BUTTON_A]);
    return bool_obj((a_presses_div.rem == 0) &&
                    (a_presses_div.quot >= 0) &&
                    (!PART_1 || (a_presses_div.quot < MAX_PRESSES_ALLOWED))).then([&a_presses_div, &b_presses_div]() {
      return std::array{ static_cast<std::size_t>(a_presses_div.quot), static_cast<std::size_t>(b_presses_div.quot) };
    });
  };

  return std::ranges::fold_left(machine_list | std::views::transform(get_presses_required)
                                             | std::views::filter([](auto const& opt) { return opt.has_value(); })
                                             | std::views::transform([](auto const& opt) { auto&& [a, b] = *opt; return (a * 3) + b; }),
                                0zu,
                                std::plus{});
}

static void part1(std::span<machine const> machine_list)
{
  std::println("{}", get_total_tokens_required<true>(machine_list));
}

static void part2(std::span<machine const> machine_list)
{
  std::println("{}", get_total_tokens_required<false>(machine_list));
}

int dec13()
{
  namespace chr = std::chrono;

  auto machine_list_or_err = []() -> std::expected<std::vector<machine>, std::string_view>
  {
    std::unique_ptr<FILE, void(*)(FILE*)> f(fopen("input/dec13.txt", "r"),
                                            [](FILE* f) { fclose(f); });
    std::size_t num_machines = 1;
    for (int c, last_c = 0; (c = fgetc(f.get())) != EOF; last_c = c)
    {
      num_machines += ((c == '\n') && (last_c == '\n'));
    }
    fseek(f.get(), 0, SEEK_SET);
    std::vector<machine> ret_vec;
    ret_vec.reserve(num_machines);
    while (!feof(f.get()))
    {
      machine& m = ret_vec.emplace_back();
      for (auto&& [i, format_str] : std::array{ " Button A: X%" PRIu32 ", Y%" PRIu32,
                                                " Button B: X%" PRIu32 ", Y%" PRIu32,
                                                " Prize: X=%" PRIu32 ", Y=%" PRIu32 } | std::views::enumerate)
      {
        if (fscanf(f.get(), format_str, &m[i][0], &m[i][1]) != 2)
        {
          return std::unexpected("Couldn't parse error line");
        }
      }
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return ret_vec;
  }();
  if (!machine_list_or_err.has_value())
  {
    std::println("Error: {}", machine_list_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*machine_list_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*machine_list_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
