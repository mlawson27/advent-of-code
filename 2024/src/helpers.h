#pragma once

#include <algorithm>
#include <cassert>
#include <charconv>
#include <concepts>
#include <expected>
#include <optional>
#include <ranges>
#include <tuple>
#include <type_traits>

template <typename T>
concept arithmetic = std::is_integral_v<T> || std::is_floating_point_v<T>;

namespace _internal
{

template <arithmetic T, typename Arg>
constexpr auto parse(std::string_view const& str, Arg&& arg) -> std::expected<T, std::error_code>
{
  T ret;
  std::from_chars_result result = std::from_chars(str.begin(), str.end(), ret, std::forward<Arg>(arg));
  return result ? std::expected<T, std::error_code>(ret) : std::unexpected(std::make_error_code(result.ec));
}

}

class bool_obj
{
  public:
    constexpr bool_obj(bool v) : _inner(v) {}

    template <typename Callable>
    constexpr auto then(Callable&& c) const -> std::optional<decltype(c())>
    {
      return _inner ? std::optional(c()) : std::nullopt;
    }

    template <typename T>
    constexpr auto then_some(T&& value) const -> std::optional<std::remove_reference_t<T>>
    {
      return then([&value]() { return std::forward<T>(value); });
    }

    constexpr operator bool() const { return _inner; }

  private:
    bool _inner;
};

template <typename T, template <typename> typename Container = std::vector>
class grid
{
  public:
    grid(std::size_t num_rows, std::size_t num_cols, Container<T>&& data = {})
      : _data(std::move(data))
      , _num_rows(num_rows)
      , _num_cols(num_cols)
    {
      assert(_data.size() == _num_rows * _num_cols);
    }

  constexpr auto num_rows() const -> std::size_t { return _num_rows; }
  constexpr auto num_cols() const -> std::size_t { return _num_cols; }

  template <typename Self>
  constexpr auto operator[](this Self&& self, std::size_t row, std::size_t col) -> decltype(std::forward<Self>(self)._data[0])
  {
#ifdef NDEBUG
    return std::forward<Self>(self)._data[row * std::forward<Self>(self)._num_cols + col];
#else
    return std::forward<Self>(self)._data.at(row * std::forward<Self>(self)._num_cols + col);
#endif
  }

  template <typename Self>
  constexpr auto get(this Self&& self, std::size_t row, std::size_t col) -> std::optional<decltype(std::ref(std::forward<Self>(self)._data[0]))>
  {
    return bool_obj((row < std::forward<Self>(self)._num_rows) &&
                    (col < std::forward<Self>(self)._num_cols)).then([&self, &row, &col](){ return std::ref(std::forward<Self>(self)[row, col]); });
  }

  template <typename Self>
  constexpr auto begin(this Self&& self) -> decltype(std::forward<Self>(self)._data.begin())
  {
    return std::forward<Self>(self)._data.begin();
  }

  template <typename Self>
  constexpr auto end(this Self&& self) -> decltype(std::forward<Self>(self)._data.end())
  {
    return std::forward<Self>(self)._data.end();
  }

  private:
    Container<T> _data;
    std::size_t  _num_rows;
    std::size_t  _num_cols;
};

template <std::ranges::random_access_range T>
constexpr auto at_or_none(T&& container, std::size_t index)
{
  return index < container.size() ? std::optional(std::forward<T>(container)[index]) : std::nullopt;
}

template <std::integral T>
constexpr auto parse(std::string_view const& str, int base = 10) -> std::expected<T, std::error_code>
{
  return _internal::parse<T>(str, base);
}

template <std::floating_point T>
constexpr auto parse(std::string_view const& str, std::chars_format fmt = std::chars_format::general) -> std::expected<T, std::error_code>
{
  return _internal::parse<T>(str, fmt);
}

template <typename T>
auto maybe_find(T& map, typename T::key_type const& key)
  -> std::optional<std::reference_wrapper<std::conditional_t<std::is_const_v<T>, typename T::mapped_type const, typename T::mapped_type>>>
{
  auto it = map.find(key);
  return bool_obj(it != map.end()).then_some(std::ref(it->second));
}

template <typename T>
constexpr auto ptr_value_or(T const* ptr, T const& default_value) -> T const&
{
  return (ptr == nullptr) ? default_value : *ptr;
}

template <std::ranges::forward_range Range, typename T, typename BinaryOp>
constexpr auto try_fold_iter(Range&& r, T&& init, BinaryOp&& op)
  -> std::ranges::in_value_result<decltype(r.begin()), std::optional<T>>
{
  std::ranges::in_value_result<decltype(r.begin()), std::optional<T>> ret = { r.begin(), std::move(init) };
  for (; ret.in != r.end(); ret.in = std::next(ret.in))
  {
    ret.value = op(std::move(*ret.value), std::forward<decltype(*ret.in)>(*ret.in));
    if (!ret.value.has_value())
    {
      break;  // Exit early if the condition is met
    }
  }
  return ret;
}

template <std::ranges::forward_range Range, typename T, typename BinaryOp>
constexpr auto try_fold(Range&& r, T&& init, BinaryOp&& op) -> std::optional<T>
{
  return try_fold_iter(std::forward<Range>(r), std::forward<T>(init), std::forward<BinaryOp>(op)).value;
}

template <std::ranges::forward_range Range, typename BinaryOp>
constexpr auto try_fold_with_first(Range&& r, BinaryOp&& op) -> std::optional<decltype(*r.begin())>
{
  auto begin = r.begin();
  return try_fold(std::ranges::subrange(std::next(begin), std::end(begin), *begin, std::forward<BinaryOp>(op)));
}
