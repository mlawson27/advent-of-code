#include <algorithm>
#include <bitset>
#include <chrono>
#include <concepts>
#include <execution>
#include <expected>
#include <fstream>
#include <iterator>
#include <numeric>
#include <print>
#include <ranges>
#include <span>
#include <thread>

template <std::unsigned_integral T>
static constexpr auto next_secret_number(T secret_number) -> T
{
  T ret = ((secret_number <<  6) ^ secret_number) & 0xFFFFFF;
  ret   = ((ret           >>  5) ^ ret)           & 0xFFFFFF;
  return  ((ret           << 11) ^ ret)           & 0xFFFFFF;
}

static void part1(std::span<uint_fast64_t const> secret_numbers)
{
  std::println("{}", std::transform_reduce(std::execution::par_unseq,
                                           secret_numbers.begin(),
                                           secret_numbers.end(),
                                           0zu,
                                           std::plus{},
                                           [](uint_fast64_t secret_number) {
                                             return std::ranges::fold_left(std::views::iota(0, 2000),
                                                                           secret_number,
                                                                           [](uint_fast64_t working_number, auto&&) {
                                                                             return next_secret_number(working_number);
                                                                           });
                                           }));
}

static void part2(std::span<uint_fast64_t const> secret_numbers)
{
  static constexpr auto at_deltas = []<std::unsigned_integral T>(auto& map, std::array<T, 4> const& deltas) -> std::remove_reference_t<decltype(map)>::reference {
    return map[(((((deltas[0] * 19zu) + deltas[1]) * 19zu) + deltas[2]) * 19zu) + deltas[3]];
  };

  std::size_t const                  num_threads = std::min<std::size_t>(std::thread::hardware_concurrency(), 8);
  std::vector<std::vector<uint16_t>> seen_deltas_per_thread(num_threads, std::vector<uint16_t>(19 * 19 * 19 * 19));
  std::vector<std::thread>           threads;
  threads.reserve(num_threads);
  std::size_t numbers_per_thread = (secret_numbers.size() + num_threads - 1) / num_threads;
  for (std::size_t i = 0; i < seen_deltas_per_thread.size(); i++)
  {
    std::size_t offset = i * numbers_per_thread;
    threads.emplace_back([](std::vector<uint16_t>& seen_deltas, std::span<uint_fast64_t const> secret_numbers) {
                           std::for_each(secret_numbers.begin(),
                                         secret_numbers.end(),
                                         [&seen_deltas](auto secret_number) {
                                           std::array<uint8_t, 4>         deltas;
                                           std::bitset<19 * 19 * 19 * 19> this_seen = {};
                                           auto const                     do_next   = [&secret_number, &deltas]() {
                                             decltype(secret_number) new_number = next_secret_number(secret_number);
                                             auto new_price = static_cast<decltype(deltas)::value_type>(new_number % 10);
                                             deltas         = std::bit_cast<decltype(deltas)>((std::bit_cast<uint32_t>(deltas) << std::numeric_limits<decltype(deltas)::value_type>::digits) | (new_price + 9 - static_cast<decltype(deltas)::value_type>(secret_number % 10)));
                                             secret_number  = new_number;
                                             return new_price;
                                           };
                                           for (std::size_t i = 0; i < 3; i++)
                                           {
                                             do_next();
                                           }
                                           for (std::size_t i = 3; i < 2000; i++)
                                           {
                                             auto new_price = do_next();
                                             if (auto&& s = at_deltas(this_seen, deltas);
                                                 !s)
                                             {
                                               at_deltas(seen_deltas, deltas) += new_price;
                                               s = true;
                                             }
                                           }
                                         });
                         },
                         std::ref(seen_deltas_per_thread[i]),
                         secret_numbers.subspan(offset, std::min(offset + numbers_per_thread, secret_numbers.size()) - offset));
  }
  threads.front().join();
  std::ranges::for_each(std::views::zip(threads, seen_deltas_per_thread) | std::views::drop(1),
                        [&seen_deltas_per_thread](auto&& thread_and_deltas) {
                          auto&& [thread, deltas] = thread_and_deltas;
                          thread.join();
                          std::ranges::copy(std::views::zip_transform(std::plus{}, seen_deltas_per_thread.front(), deltas),
                                            seen_deltas_per_thread.front().begin());
                        });
  std::println("{}", *std::ranges::max_element(seen_deltas_per_thread.front()));
}

int dec22()
{
  namespace chr = std::chrono;

  auto secret_numbers_or_err = []() -> std::expected<std::vector<uint_fast64_t>, std::string_view>
  {
    std::ifstream f("input/dec22.txt");
    std::size_t num_lines = 1;
    for (char c; f.get(c); num_lines += (c == '\n'));
    f.clear();
    f.seekg(0, std::ios::beg);
    std::vector<uint_fast64_t> ret_vec;
    ret_vec.reserve(num_lines);
    std::copy(std::istream_iterator<uint_fast64_t>{ f },
              std::istream_iterator<uint_fast64_t>{},
              std::back_inserter(ret_vec));
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return ret_vec;
  }();
  if (!secret_numbers_or_err.has_value())
  {
    std::println("Error: {}", secret_numbers_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*secret_numbers_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*secret_numbers_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
