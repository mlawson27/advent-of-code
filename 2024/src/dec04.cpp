#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <span>
#include <string>

#include "helpers.h"

using char_grid = grid<char>;

static void part1(char_grid const& word_search)
{
  namespace rn = std::ranges;
  namespace vw = std::views;

  static constexpr std::string_view TO_FIND = "XMAS";

  auto const do_search = [&word_search](std::size_t min_col, std::size_t max_row, std::size_t max_col, int row_incr, int col_incr) {
    return rn::count_if(vw::cartesian_product(vw::iota(0zu,     max_row),
                                              vw::iota(min_col, max_col)),
                        [&word_search, &row_incr, &col_incr](auto const& index_pair) {
                          auto&& [row, col] = index_pair;
                          auto   range      = vw::iota(0zu, TO_FIND.size()) | vw::transform([&](std::size_t i){ return word_search[row + (row_incr * i), col + (col_incr * i)]; });
                          return rn::equal(TO_FIND, range) || rn::equal(TO_FIND | vw::reverse, range);
                        });
  };

  std::println("{}", rn::fold_left((std::array{ std::tuple{ 0zu,                word_search.num_rows(),                      word_search.num_cols() - TO_FIND.size() + 1, 0,  1 },
                                                std::tuple{ 0zu,                word_search.num_rows() - TO_FIND.size() + 1, word_search.num_cols(),                      1,  0 },
                                                std::tuple{ 0zu,                word_search.num_rows() - TO_FIND.size() + 1, word_search.num_cols() - TO_FIND.size() + 1, 1,  1 },
                                                std::tuple{ TO_FIND.size() - 1, word_search.num_rows() - TO_FIND.size() + 1, word_search.num_cols(),                      1, -1 } } |
                                    vw::transform([&do_search](auto const& tup) { return std::apply(do_search, tup); })),
                                   0zu,
                                   std::plus{}));
}

static void part2(char_grid const& word_search)
{
  namespace rn = std::ranges;
  namespace vw = std::views;

  std::println("{}", rn::count_if(vw::cartesian_product(vw::iota(0zu, word_search.num_rows() - 2),
                                                        vw::iota(0zu, word_search.num_cols() - 2)),
                                  [&word_search](std::tuple<std::size_t, std::size_t> const& coordinates) {
                                    static constexpr auto are_m_and_s = [](char c1, char c2) {
                                      return (c1 == 'M' && c2 == 'S') || (c1 == 'S' && c2 == 'M');
                                    };
                                    auto&& [row, col] = coordinates;
                                    return ((word_search[row + 1, col + 1] == 'A') &&
                                            are_m_and_s(word_search[row,     col], word_search[row + 2, col + 2]) &&
                                            are_m_and_s(word_search[row + 2, col], word_search[row,     col + 2]));
                                  }));
}

int dec04()
{
  namespace chr = std::chrono;

  auto word_search_or_err = []() -> std::expected<char_grid, std::string_view>
  {
    std::ifstream f("input/dec04.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::vector<char> ret_vec;
    std::size_t       num_rows = (total_len + 1) / (line_len + 1);
    ret_vec.reserve(num_rows * line_len);
    ret_vec.insert(ret_vec.end(),
                   std::istream_iterator<char>{ f },
                   std::istream_iterator<char>{});
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return char_grid{ num_rows, line_len, std::move(ret_vec) };
  }();
  if (!word_search_or_err.has_value())
  {
    std::println("Error: {}", word_search_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*word_search_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*word_search_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
