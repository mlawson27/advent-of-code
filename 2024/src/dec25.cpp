#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <span>
#include <vector>

int dec25()
{
  namespace chr = std::chrono;

  auto key_and_lock_bits_or_err = []() -> std::expected<std::pair<std::vector<uint64_t>, std::vector<uint64_t>>, std::string_view>
  {
    std::ifstream f("input/dec25.txt");
    std::size_t    group_size = 0;
    std::size_t    num_groups = 1;
    for (char c, last_c = 0; f.get(c); last_c = c)
    {
      if (c == '\n')
      {
        num_groups += (last_c == '\n');
        group_size += ((num_groups == 1) && (last_c != '\n'));
      }
    }
    if (group_size > std::numeric_limits<uint64_t>::digits)
    {
      return std::unexpected("Group size too large");
    }
    f.clear();
    f.seekg(0, std::ios::beg);

    std::vector<uint64_t> key_bits, lock_bits;
    key_bits.reserve(num_groups);
    lock_bits.reserve(num_groups);
    for (std::size_t i = 0; !f.eof() && i < num_groups; i++)
    {
      std::array<char, 5 + 1> line;
      f.getline(line.data(), line.size()) >> std::ws;
      auto& new_bits = ((std::ranges::count(line, '#') == 0) ? &key_bits : &lock_bits)->emplace_back();
      new_bits       = *std::ranges::fold_left_first(std::views::zip_transform([](auto&& i, auto&& c) { return static_cast<uint64_t>(c == '#') << i; }, std::views::iota(0, 5), line),
                                                     std::bit_or{});
      for (std::size_t j = 1; j < group_size; j++)
      {
        if (!f.getline(line.data(), line.size()))
        {
          return std::unexpected("Could not read line");
        }
        new_bits |= *std::ranges::fold_left_first(std::views::zip_transform([](auto&& i, auto&& c) { return static_cast<uint64_t>(c == '#') << i; }, std::views::iota(j * 5, (j + 1) * 5), line),
                                                  std::bit_or{});
        f >> std::ws;
      }
    }

    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (key_bits.size() + lock_bits.size() != num_groups)
    {
      return std::unexpected("Reserved wrong size");
    }

    key_bits.shrink_to_fit();
    lock_bits.shrink_to_fit();
    return std::pair{ std::move(key_bits), std::move(lock_bits) };
  }();
  if (!key_and_lock_bits_or_err.has_value())
  {
    std::println("Error: {}", key_and_lock_bits_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply([](std::span<uint64_t const> key_heights, std::span<uint64_t const> lock_heights) {
    std::println("{}", std::ranges::count_if(std::views::cartesian_product(key_heights, lock_heights),
                                             [](auto&& key_lock_heights) { return std::apply(std::bit_and{}, key_lock_heights) == 0; }));
  }, *key_and_lock_bits_or_err);
  chr::time_point post = chr::steady_clock::now();
  std::println("{}", chr::duration_cast<chr::microseconds>(post - current));
  return 0;
}
