#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <unordered_map>
#include <unordered_set>

using position = std::array<uint32_t, 2>;

template <bool COUNT_ALL>
static constexpr auto count_antinodes(std::unordered_map<char, std::vector<position>> const& map, position const& bounds) -> std::size_t
{
  static constexpr auto all_positions_iter = [](std::tuple<position const&, position const&> p1_and_p2, position const& bounds) {
    auto&& [p1, p2] = p1_and_p2;
    std::array<ptrdiff_t, std::size(p1)> diffs;
    std::ranges::copy(std::views::zip_transform(std::minus<std::ptrdiff_t>{}, p1, p2),
                      diffs.begin());
    constexpr std::ptrdiff_t start = COUNT_ALL ? 0                                          : 1;
    constexpr std::ptrdiff_t end   = COUNT_ALL ? std::numeric_limits<std::ptrdiff_t>::max() : 2;
    return std::views::iota(start, end) | std::views::transform([&p1, diffs=std::move(diffs)](std::ptrdiff_t mult) {
                                            decltype(diffs) ret;
                                            std::ranges::copy(std::views::zip_transform([&mult](auto v, auto diff) { return v + (diff * mult); }, p1, diffs),
                                                              ret.begin());
                                            return ret;
                                          })
                                        | std::views::take_while([&bounds](auto&& pos) {
                                            return std::ranges::all_of(std::views::zip_transform([](auto&& pos, auto&& bound) { return pos >= 0 && pos < bound; }, pos, bounds),
                                                                       [](bool v) { return v; });
                                          })
                                        | std::views::transform([](auto&& pos_extended) {
                                            position antinode_pos;
                                            std::ranges::transform(pos_extended,
                                                                   antinode_pos.begin(),
                                                                   [](auto v) { return static_cast<position::value_type>(v); });
                                            return antinode_pos;
                                          });
  };

  return std::ranges::fold_left(map | std::views::values
                                    | std::views::transform([](auto const& positions) { return std::views::cartesian_product(positions, positions); })
                                    | std::views::join
                                    | std::views::filter([](auto&& p1_and_p2) { return std::get<0>(p1_and_p2) != std::get<1>(p1_and_p2); })
                                    | std::views::transform([&bounds](auto&& p1_and_p2) { return all_positions_iter(p1_and_p2, bounds); })
                                    | std::views::join,
                                std::unordered_set<uint64_t>{},
                                [](std::unordered_set<uint64_t> found_positions, auto&& pos) {
                                  found_positions.insert(std::bit_cast<uint64_t>(pos));
                                  return found_positions;
                                }).size();
}

static void part1(std::unordered_map<char, std::vector<position>> const& map, position const& bounds)
{
  std::println("{}", count_antinodes<false>(map, bounds));
}

static void part2(std::unordered_map<char, std::vector<position>> const& map, position const& bounds)
{
  std::println("{}", count_antinodes<true>(map, bounds));
}

int dec08()
{
  namespace chr = std::chrono;

  auto map_and_bounds_or_error = []() -> std::expected<std::pair<std::unordered_map<char, std::vector<position>>, position>, std::string_view>
  {
    std::ifstream f("input/dec08.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::unordered_map<char, std::vector<position>> ret_map;
    std::size_t num_rows = (total_len + 1) / (line_len + 1);
    std::string tmp_line;
    tmp_line.reserve(line_len);
    for (std::size_t r = 0; r < num_rows; r++)
    {
      std::getline(f, tmp_line);
      if (tmp_line.size() != line_len)
      {
        return std::unexpected("Line has incorrect length");
      }
      for (std::size_t c = 0; c < line_len; c++)
      {
        if (tmp_line[c] != '.')
        {
          ret_map[tmp_line[c]].push_back({ static_cast<uint32_t>(r), static_cast<uint32_t>(c) });
        }
      }
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    return std::pair{ ret_map, position{ static_cast<uint32_t>(num_rows), static_cast<uint32_t>(line_len) } };
  }();
  if (!map_and_bounds_or_error.has_value())
  {
    std::println("Error: {}", map_and_bounds_or_error.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *map_and_bounds_or_error);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *map_and_bounds_or_error);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
