#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <queue>
#include <string>
#include <utility>
#include <vector>

#include "helpers.h"

using char_grid = grid<char>;
using position  = std::array<uint_fast16_t, 2>;

enum class Direction: uint32_t
{
  North = 0,
  East  = 1,
  South = 2,
  West  = 3,
};
static constexpr auto Direction_Values = std::array{ Direction::North, Direction::East, Direction::South, Direction::West };
template <std::integral T>
static constexpr Direction operator+(Direction d, T val)
{
  return static_cast<Direction>((std::to_underlying(d) + val) % Direction_Values.size());
}

struct compare_queue_elems
{
  template <typename... Args1, typename... Args2>
  constexpr auto operator()(std::tuple<Args1...> const& a, std::tuple<Args1...> const& b)
  {
    return std::get<0>(a) > std::get<0>(b);
  }
  template <typename... Args1, typename... Args2>
  constexpr auto operator()(std::unique_ptr<std::tuple<Args1...>> const& a, std::unique_ptr<std::tuple<Args2...>> const& b)
  {
    return (*this)(*a, *b);
  }
};

class path_container
{
  public:
    constexpr path_container(std::size_t num_elems)
      : _elems((num_elems + std::numeric_limits<std::size_t>::digits - 1) / std::numeric_limits<std::size_t>::digits)
    {
    }

    constexpr auto set(std::size_t index) -> void
    {
      _elems[index / std::numeric_limits<std::size_t>::digits] |= (1zu << (index % std::numeric_limits<std::size_t>::digits));
    }

    constexpr auto count() const -> std::size_t
    {
      return std::ranges::fold_left(_elems,
                                    0zu,
                                    [](std::size_t working, std::size_t elem) { return working + std::popcount(elem); });
    }

    constexpr auto operator|=(path_container const& other) -> path_container&
    {
      std::ranges::copy(std::views::zip_transform(std::bit_or{}, _elems, other._elems),
                        _elems.begin());
      return *this;
    }

  private:
    std::vector<std::size_t> _elems;
};

static void part1(char_grid const& maze_map, position const& start_pos, position const& end_pos)
{
  using queue_elem = std::tuple<std::size_t, position::value_type, position::value_type, Direction>;
  std::optional<std::size_t> ret;
  std::priority_queue<queue_elem, std::vector<queue_elem>, compare_queue_elems> position_queue;
  grid<uint8_t> seen_set(maze_map.num_rows(), maze_map.num_cols(), std::vector<uint8_t>(maze_map.num_rows() * maze_map.num_cols()));
  position_queue.emplace(0zu, start_pos[0], start_pos[1], Direction::East);
  seen_set[start_pos[0], start_pos[1]] = 1 << std::to_underlying(Direction::East);
  while (!position_queue.empty())
  {
    auto const [score, orig_row, orig_col, dir] = position_queue.top();
    if ((orig_row == end_pos[0]) && (orig_col == end_pos[1]))
    {
      ret = score;
      break;
    }
    for (Direction other_dir : Direction_Values)
    {
      if (dir == (other_dir + (Direction_Values.size() / 2)))
      {
        continue;
      }
      auto row = orig_row, col = orig_col;
      switch (other_dir)
      {
        case Direction::North: row -= 1; break;
        case Direction::East:  col += 1; break;
        case Direction::South: row += 1; break;
        case Direction::West:  col -= 1; break;
      }
      if (uint8_t* seen_pos;
          (maze_map[row, col] != '#') &&
          ((*(seen_pos = &seen_set[row, col]) & (1 << std::to_underlying(other_dir))) == 0))
      {
        position_queue.emplace(score + ((dir != other_dir) * 1000) + 1, row, col, other_dir);
        *seen_pos |= 1 << std::to_underlying(other_dir);
      }
    }
    position_queue.pop();
  }
  std::println("{}", *ret);
}

static void part2(char_grid const& maze_map, position const& start_pos, position const& end_pos)
{
  using queue_elem = std::tuple<std::size_t, position::value_type, position::value_type, Direction, path_container>;
  std::optional<std::pair<std::size_t, path_container>> ret;
  std::priority_queue<std::unique_ptr<queue_elem>, std::vector<std::unique_ptr<queue_elem>>, compare_queue_elems> position_queue;
  path_container start_path(maze_map.num_rows() * maze_map.num_cols());
  start_path.set(start_pos[0] * maze_map.num_cols() + start_pos[1]);
  grid<std::array<queue_elem*, Direction_Values.size()>> seen_set(maze_map.num_rows(),
                                                                  maze_map.num_cols(),
                                                                  std::vector<std::array<queue_elem*, Direction_Values.size()>>(maze_map.num_rows() * maze_map.num_cols()));
  position_queue.emplace(std::make_unique<queue_elem>(0zu, start_pos[0], start_pos[1], Direction::East, std::move(start_path)));
  seen_set[start_pos[0], start_pos[1]][std::to_underlying(Direction::East)] = std::bit_cast<queue_elem*>(std::numeric_limits<std::uintptr_t>::max());
  while (!position_queue.empty())
  {
    auto const& [score, orig_row, orig_col, dir, path] = *position_queue.top();
    if ((orig_row != end_pos[0]) || (orig_col != end_pos[1])) [[likely]]
    {
      seen_set[orig_row, orig_col][std::to_underlying(dir)] = std::bit_cast<queue_elem*>(std::numeric_limits<uintptr_t>::max());
      for (Direction other_dir : Direction_Values)
      {
        if (dir == (other_dir + (Direction_Values.size() / 2)))
        {
          continue;
        }
        auto row = orig_row, col = orig_col;
        switch (other_dir)
        {
          case Direction::North: row -= 1; break;
          case Direction::East:  col += 1; break;
          case Direction::South: row += 1; break;
          case Direction::West:  col -= 1; break;
        }
        if (queue_elem** current_at_ptr;
            (maze_map[row, col] != '#') &&
            (std::bit_cast<uintptr_t>(*(current_at_ptr = &seen_set[row, col][std::to_underlying(other_dir)])) != std::numeric_limits<uintptr_t>::max()))
        {
          std::size_t new_score = (score + ((dir != other_dir) * 1000) + 1);
          if (ret.has_value() && (new_score > ret->first))
          {
            continue;
          }
          if ((*current_at_ptr == nullptr) ||
              (std::get<0>(**current_at_ptr) > new_score))
          {
            path_container new_path = path;
            new_path.set(row * maze_map.num_cols() + col);
            auto new_elem = std::make_unique<queue_elem>(new_score, row, col, other_dir, std::move(new_path));
            *current_at_ptr = new_elem.get();
            position_queue.emplace(std::move(new_elem));
          }
          else if (std::get<0>(**current_at_ptr) == new_score)
          {
            std::get<path_container>(**current_at_ptr) |= path;
          }
        }
      }
    }
    else if (ret.has_value())
    {
      ret->second |= path;
    }
    else
    {
      ret.emplace(score, std::move(path));
    }
    position_queue.pop();
  }
  std::println("{}", ret->second.count());
}

int dec16()
{
  namespace chr = std::chrono;

  auto maze_map_start_end_or_err = []() -> std::expected<std::tuple<char_grid, position, position>, std::string_view>
  {
    std::ifstream f("input/dec16.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::vector<char> ret_vec;
    std::size_t       num_rows = (total_len + 1) / (line_len + 1);
    ret_vec.reserve(num_rows * line_len);
    std::optional<position> start_pos, end_pos;
    for (auto it = std::istream_iterator<char>{ f }; it != std::istream_iterator<char>{}; it = std::next(it))
    {
      char c;
      if (*it == 'S')
      {
        start_pos = std::array{ static_cast<position::value_type>(ret_vec.size() / num_rows),
                                static_cast<position::value_type>(ret_vec.size() % line_len) };
        c         = '.';
      }
      else if (*it == 'E')
      {
        end_pos = std::array{ static_cast<position::value_type>(ret_vec.size() / num_rows),
                              static_cast<position::value_type>(ret_vec.size() % line_len) };
        c       = '.';
      }
      else
      {
        c = *it;
      }
      ret_vec.push_back(c);
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    for (auto&& [pos, err] : { std::pair{ &start_pos, "Unable to find start position" },
                               std::pair{ &end_pos,   "Unable to find end position"   } })
    {
      if (!pos->has_value())
      {
        return std::unexpected(err);
      }
    }
    return std::tuple{ char_grid{ num_rows, line_len, std::move(ret_vec) }, *start_pos, *end_pos };
  }();
  if (!maze_map_start_end_or_err.has_value())
  {
    std::println("Error: {}", maze_map_start_end_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *maze_map_start_end_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *maze_map_start_end_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
