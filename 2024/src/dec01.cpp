#include <algorithm>
#include <array>
#include <chrono>
#include <print>
#include <ranges>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "helpers.h"

using val_pair_t = std::pair<std::size_t, std::size_t>;

static void part1(std::vector<val_pair_t> const& pairs)
{
  auto singles = [&pairs]<std::size_t... Is>(std::index_sequence<Is...>) {
    static constexpr auto helper = [](decltype(pairs) pairs, auto fn) {
      std::vector<std::size_t> list;
      list.reserve(pairs.size());
      std::ranges::transform(pairs, std::back_inserter(list), fn);
      std::ranges::sort(list);
      return list;
    };
    return std::array{ helper(pairs, static_cast<auto (*)(val_pair_t const&) -> std::tuple_element_t<Is, val_pair_t> const&>(std::get<Is>))... };
  }(std::make_index_sequence<std::tuple_size_v<val_pair_t>>{});
  std::println("{}", std::apply([](auto const&... s) {
      return std::ranges::fold_left(std::views::zip_transform([](std::size_t a, std::size_t b) { return (a > b) ? a - b : b - a; },
                                                              s...),
                                    0zu,
                                    std::plus{});
    }, singles));
}

static void part2(std::vector<val_pair_t> const& pairs)
{
  std::unordered_map<std::size_t, std::size_t> val_to_count;
  val_to_count.reserve(pairs.size());
  std::ranges::for_each(pairs, [&val_to_count](val_pair_t const& p) { val_to_count.try_emplace(p.second, 0).first->second++; });
  std::println("{}", std::ranges::fold_left(pairs | std::views::transform([&val_to_count](val_pair_t const& p) { return maybe_find(val_to_count, p.first).transform([&p](auto const& ref) { return p.first * ref.get(); }).value_or(0zu); }),
                                            0zu,
                                            std::plus{}));
}

int dec01()
{
  namespace chr = std::chrono;

  std::vector<val_pair_t> pairs = []()
  {
    FILE*       f         = fopen("input/dec01.txt", "r");
    std::size_t num_pairs = 1;
    for (int c = fgetc(f); c != EOF; c = fgetc(f))
    {
      num_pairs += (c == '\n');
    }
    fseek(f, 0, SEEK_SET);
    std::vector<val_pair_t> ret;
    ret.reserve(num_pairs);
    for (val_pair_t p; fscanf(f, "%zu %zu", &p.first, &p.second) == 2;)
    {
      ret.push_back(p);
    }
    fclose(f);
    return ret;
  }();

  chr::time_point current = chr::steady_clock::now();
  part1(pairs);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(pairs);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
