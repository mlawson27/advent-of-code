#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <span>
#include <unordered_set>
#include <vector>

static void part1(std::vector<std::array<char, 2>> const& idx_to_name, std::span<std::vector<uint16_t> const> connections_per_idx)
{
  struct hasher
  {
    constexpr auto operator()(std::array<uint16_t, 3> const& v) const -> uint64_t
    {
      std::array<uint16_t, 4> new_v;
      std::copy_n(v.begin(), new_v.size(), new_v.begin());
      new_v.back() = 0;
      return std::bit_cast<uint64_t>(new_v);
    }
  };

  std::unordered_set<std::array<uint16_t, 3>, hasher> seen_3_sets;
  seen_3_sets.reserve(connections_per_idx.size() * 10);
  for (uint16_t i = 0; i < connections_per_idx.size(); i++)
  {
    for (auto a_it = connections_per_idx[i].begin(); a_it != std::prev(connections_per_idx[i].end()); a_it = std::next(a_it))
    {
      for (auto b_it = std::next(a_it); b_it != connections_per_idx[i].end(); b_it = std::next(b_it))
      {
        if (std::ranges::contains(connections_per_idx[*a_it], *b_it))
        {
          std::array<uint16_t, 3> indices = std::array{ i, *a_it, *b_it };
          std::ranges::sort(indices);
          if (std::ranges::any_of(indices,
                                  [&idx_to_name](std::size_t idx) { return idx_to_name[idx].front() == 't'; }) &&
              !seen_3_sets.contains(indices))
          {
            seen_3_sets.emplace(indices);
          }
        }
      }
    }
  }
  std::println("{}", seen_3_sets.size());
}

static void part2(std::vector<std::array<char, 2>> const& idx_to_name, std::span<std::vector<uint16_t> const> connections_per_idx)
{
  struct hasher
  {
    constexpr auto operator()(std::vector<uint16_t> const& v) const
    {
      return std::hash<std::string_view>{}(std::string_view(reinterpret_cast<char const*>(v.data()), v.size() * sizeof(uint16_t)));
    }
  };

  std::unordered_set<std::vector<uint16_t>, hasher> seen_sets;
  seen_sets.reserve(connections_per_idx.size() * 10);
  for (uint16_t i = 0; i < connections_per_idx.size(); i++)
  {
    for (auto a_it = connections_per_idx[i].begin(); std::next(a_it) != connections_per_idx[i].end(); a_it = std::next(a_it))
    {
      for (auto b_it = std::next(a_it); b_it != connections_per_idx[i].end(); b_it = std::next(b_it))
      {
        if (std::ranges::contains(connections_per_idx[*a_it], *b_it))
        {
          std::vector<uint16_t> indices = { i, *a_it, *b_it };
          std::ranges::sort(indices);
          if (std::ranges::any_of(indices,
                                  [&idx_to_name](std::size_t idx) { return idx_to_name[idx].front() == 't'; }) &&
              !seen_sets.contains(indices))
          {
            seen_sets.insert(std::move(indices));
          }
        }
      }
    }
  }

  while (seen_sets.size() > 1)
  {
    decltype(seen_sets) new_seen_sets;
    new_seen_sets.reserve(seen_sets.size());
    while (!seen_sets.empty())
    {
      auto working_set = std::move(seen_sets.extract(seen_sets.begin()).value());
      for (auto i : connections_per_idx[working_set[0]])
      {
        if (!std::ranges::binary_search(working_set, i) &&
            std::ranges::all_of(working_set,
                                [&connections_per_idx, &i](auto&& set_elem) { return std::ranges::contains(connections_per_idx[set_elem], i); }))
        {
          working_set.insert(std::ranges::upper_bound(working_set, i), i);
          new_seen_sets.emplace(std::move(working_set));
          break;
        }
      }
    }
    seen_sets = std::move(new_seen_sets);
  }

  auto max_set = std::move(seen_sets.extract(seen_sets.begin()).value());
  std::ranges::sort(max_set, [&idx_to_name](auto&& a, auto&& b) { return idx_to_name[a] < idx_to_name[b]; });
  std::print("{}", std::string_view(idx_to_name[max_set[0]]));
  std::for_each(std::next(max_set.begin()),
                max_set.end(),
                [&idx_to_name](auto&& i) { std::print(",{}", std::string_view(idx_to_name[i])); });
  std::println();
}

int dec23()
{
  namespace chr = std::chrono;

  auto idx_to_name_and_connections_per_idx_or_err = []() -> std::expected<std::pair<std::vector<std::array<char, 2>>, std::vector<std::vector<uint16_t>>>, std::string_view>
  {
    std::ifstream f("input/dec23.txt");
    std::size_t num_lines = 1;
    for (char c; f.get(c); num_lines += (c == '\n'));
    f.clear();
    f.seekg(0, std::ios::beg);
    std::array<std::array<uint16_t, 'z' - 'a' + 1>, 'z' - 'a' + 1> name_to_idx;
    std::vector<std::array<char, 2>>                               idx_to_name;
    std::vector<std::array<uint16_t, 2>>                           connections_vec;
    std::ranges::for_each(name_to_idx, [](auto& elem) { return elem.fill(std::numeric_limits<decltype(name_to_idx)::value_type::value_type>::max()); });
    idx_to_name.reserve(num_lines / 2);
    connections_vec.reserve(num_lines);
    std::size_t working_idx = 0;
    for (std::size_t i = 0; f && i < num_lines; i++)
    {
      char line[6];
      f.getline(line, sizeof(line));
      auto& connection = connections_vec.emplace_back();
      for (auto&& [l_i, r_i] : { std::pair{ 0zu, 1zu }, std::pair{ 3zu, 4zu } })
      {
        if (name_to_idx[line[l_i] - 'a'][line[r_i] - 'a'] == std::numeric_limits<decltype(name_to_idx)::value_type::value_type>::max())
        {
          name_to_idx[line[l_i] - 'a'][line[r_i] - 'a'] = working_idx;
          idx_to_name.push_back({ line[l_i], line[r_i] });
          working_idx++;
        }
        connection[l_i / 2] = name_to_idx[line[l_i] - 'a'][line[r_i] - 'a'];
      }
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (connections_vec.size() != connections_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    idx_to_name.shrink_to_fit();
    std::vector<std::vector<uint16_t>> connections_per_idx;
    connections_per_idx.resize(idx_to_name.size());
    for (auto const& connection_pair : connections_vec)
    {
      for (std::size_t i = 0; i < connection_pair.size(); i++)
      {
        if (connections_per_idx[connection_pair[i]].empty())
        {
          connections_per_idx[connection_pair[i]].reserve(idx_to_name.size());
        }
        connections_per_idx[connection_pair[i]].push_back(connection_pair[1 - i]);
      }
    }
    return std::pair{ std::move(idx_to_name), std::move(connections_per_idx) };
  }();
  if (!idx_to_name_and_connections_per_idx_or_err.has_value())
  {
    std::println("Error: {}", idx_to_name_and_connections_per_idx_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *idx_to_name_and_connections_per_idx_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *idx_to_name_and_connections_per_idx_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
