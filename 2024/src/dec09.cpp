#include <algorithm>
#include <chrono>
#include <fstream>
#include <list>
#include <optional>
#include <print>
#include <ranges>
#include <string>

struct node
{
  std::optional<uint_fast32_t> id;
  std::size_t                  len;
};

static void part1(std::span<node> fs_map)
{
  auto        fwd_it   = fs_map.begin();
  auto        rev_it   = fs_map.rbegin();
  std::size_t pos      = 0;
  std::size_t result   = 0;
  std::size_t used_fwd = 0;
  std::size_t used_rev = 0;
  while (fwd_it != rev_it.base())
  {
    if (fwd_it == std::next(rev_it).base())
    {
      used_fwd = used_rev;
    }
    if (fwd_it->id.has_value())
    {
      result += (((pos + (fwd_it->len - used_fwd) - 1) * (pos + (fwd_it->len - used_fwd))) - ((pos - 1) * pos)) / 2 * *fwd_it->id;
      pos    += fwd_it->len;
      fwd_it  = std::next(fwd_it);
    }
    else if ((fwd_it->len - used_fwd) <= (rev_it->len - used_rev))
    {
      used_rev += (fwd_it->len - used_fwd);
      result   += (((pos + (fwd_it->len - used_fwd) - 1) * (pos + (fwd_it->len - used_fwd))) - ((pos - 1) * pos)) / 2 * *rev_it->id;
      pos      += (fwd_it->len - used_fwd);
      fwd_it   = std::next(fwd_it);
      used_fwd = 0;
      if (used_rev == rev_it->len)
      {
        while (fwd_it != rev_it.base())
        {
          rev_it = std::next(rev_it);
          if (rev_it->id.has_value())
          {
            break;
          }
        }
        used_rev = 0;
      }
    }
    else
    {
      used_fwd += (rev_it->len - used_rev);
      result   += (((pos + (rev_it->len - used_rev) - 1) * (pos + (rev_it->len - used_rev))) - ((pos - 1) * pos)) / 2 * *rev_it->id;
      pos      += (rev_it->len - used_rev);
      if (used_fwd == fwd_it->len)
      {
        fwd_it   = std::next(fwd_it);
        used_fwd = 0;
      }
      while (fwd_it != rev_it.base())
      {
        rev_it = std::next(rev_it);
        if (rev_it->id.has_value())
        {
          break;
        }
      }
      used_rev = 0;
    }
  }
  std::println("{}", result);
}

static void part2(std::span<node> fs_map)
{
  auto node_used = std::vector<uint8_t>(fs_map.size(), UINT8_C(0));
  std::array<std::optional<std::pair<decltype(fs_map)::iterator, std::size_t>>, 9> next_for_block;
  auto const find_next_of_size = [&next_for_block, &fs_map, &node_used](std::size_t len_to_find, std::size_t pos, decltype(fs_map)::iterator start_it, decltype(fs_map)::iterator end_it) {
    for (auto it = start_it; it != end_it; it = std::next(it))
    {
      if (!it->id.has_value())
      {
        for (std::size_t len = 1; len <= it->len - node_used[std::distance(fs_map.begin(), it)]; len++)
        {
          if (!next_for_block[len - 1].has_value())
          {
            next_for_block[len - 1] = std::pair{ it, pos };
          }
        }
        if (it->len - node_used[std::distance(fs_map.begin(), it)] >= len_to_find)
        {
          break;
        }
      }
      pos += it->len;
    }
  };
  find_next_of_size(next_for_block.size(), 0, fs_map.begin(), fs_map.end());

  std::size_t rpos   = std::ranges::fold_left(fs_map,
                                              0zu,
                                              [](std::size_t ret, node const& n) { return n.len + ret; }) - fs_map.rbegin()->len;
  std::size_t result = 0;
  for (auto rev_it = fs_map.rbegin(); rev_it != fs_map.rend();)
  {
    if (auto it_and_pos = next_for_block[rev_it->len - 1];
        it_and_pos.has_value() && it_and_pos->second < rpos)
    {
      auto&&   [it, pos] = *it_and_pos;
      uint8_t& used      = node_used[std::distance(fs_map.begin(), it)];
      result += (((pos + rev_it->len + used - 1) * (pos + rev_it->len + used)) - ((pos + used - 1) * (pos + used))) / 2 * *rev_it->id;
      used   += rev_it->len;
      for (std::size_t len = it->len - used; len < it->len && next_for_block[len].has_value(); len++)
      {
        if (next_for_block[len]->first == it)
        {
          next_for_block[len].reset();
        }
      }
      find_next_of_size(it->len, pos + it->len, std::next(it), rev_it.base());
    }
    else
    {
      result += (((rpos + rev_it->len - 1) * (rpos + rev_it->len)) - ((rpos - 1) * rpos)) / 2 * *rev_it->id;
    }
    do
    {
      rev_it = std::next(rev_it);
      if (rev_it == fs_map.rend())
      {
        break;
      }
      rpos -= rev_it->len;
    } while (!rev_it->id.has_value());
  }
  std::println("{}", result);
}

int dec09()
{
  namespace chr = std::chrono;

  std::vector<node> fs_map = []()
  {
    std::ifstream f("input/dec09.txt");
    f.seekg(0, std::ios::end);
    std::size_t len = f.tellg();
    f.seekg(0, std::ios::beg);
    std::string ret;
    ret.reserve(len + 1);
    std::getline(f, ret, '\0');

    std::vector<node> fs_map;
    std::size_t       working_id    = 0;
    std::size_t       num_fs_blocks = 0;
    std::ranges::transform(ret | std::views::enumerate
                               | std::views::filter([](std::pair<std::size_t, char> const& p) { return p.second != '0'; }),
                           std::back_inserter(fs_map),
                           [&working_id, &num_fs_blocks](std::pair<std::size_t, char> const& p) {
                             bool is_fs = (p.first % 2);
                             node ret = {
                               .id  = is_fs ? std::nullopt : std::optional(working_id),
                               .len = static_cast<std::size_t>(p.second - '0'),
                             };
                             working_id    += !is_fs;
                             num_fs_blocks += is_fs;
                             return ret;
                           });
    if (fs_map.rbegin() != fs_map.rend() && !fs_map.rbegin()->id.has_value())
    {
      fs_map.pop_back();
    }
    return fs_map;
  }();

  chr::time_point current = chr::steady_clock::now();
  part1(fs_map);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(fs_map);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
