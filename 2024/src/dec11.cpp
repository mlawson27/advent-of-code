#include <algorithm>
#include <array>
#include <chrono>
#include <expected>
#include <fstream>
#include <iterator>
#include <optional>
#include <print>
#include <ranges>
#include <unordered_map>

template <std::size_t TIMES_TO_BLINK>
static constexpr auto stone_count_after(std::span<uint_fast64_t> stone_list) -> std::size_t
{
  using seen_map = std::unordered_map<uint_fast64_t, std::array<std::size_t, TIMES_TO_BLINK>>;
  static constexpr auto stone_count = [](uint_fast64_t v, std::size_t blink_count, seen_map& seen_results, auto&& stone_count_fn) {
    if (blink_count == 0)
    {
      return 1zu;
    }

    blink_count--;

    auto const add_to_map_and_return = [&seen_results, &v, &blink_count](std::size_t ret) {
      seen_results[v][blink_count] = ret;
      return ret;
    };

    if (v == 0)
    {
      blink_count -= (blink_count != 0);
      return add_to_map_and_return(stone_count_fn(2024, blink_count, seen_results, stone_count_fn));
    }

    if (auto it = seen_results.find(v);
        it != seen_results.end() && it->second[blink_count])
    {
      return it->second[blink_count];
    }

    std::size_t digit_count = 1;
    for (std::size_t working_v = v / 10; working_v; working_v /= 10, digit_count++);
    if (digit_count % 2 == 0)
    {
      std::ptrdiff_t dividend = 10z;
      for (std::size_t i = 1; i < digit_count / 2; i++, dividend *= 10);
      auto div_result = std::div(v, dividend);
      return add_to_map_and_return(stone_count_fn(div_result.quot, blink_count, seen_results, stone_count_fn) +
                                   stone_count_fn(div_result.rem , blink_count, seen_results, stone_count_fn));
    }
    return add_to_map_and_return(stone_count_fn(v * 2024, blink_count, seen_results, stone_count_fn));
  };
  return std::ranges::fold_left(stone_list,
                                std::pair{ seen_map{}, 0zu },
                                [](auto&& working_pair, uint_fast64_t elem) {
                                  auto&& [seen_results, working_ret] = working_pair;
                                  auto   addl_count                  = stone_count(elem, TIMES_TO_BLINK, seen_results, stone_count);
                                  return std::pair { std::move(seen_results),
                                                     working_ret + addl_count };
                                }).second;
}

static void part1(std::span<uint_fast64_t> stone_list)
{
  std::println("{}", stone_count_after<25>(stone_list));
}

static void part2(std::span<uint_fast64_t> stone_list)
{
  std::println("{}", stone_count_after<75>(stone_list));
}

int dec11()
{
  namespace chr = std::chrono;

  auto stone_list_or_err = []() -> std::expected<std::vector<uint_fast64_t>, std::string_view>
  {
    std::ifstream f("input/dec11.txt");
    std::size_t num_values = 1;
    for (char c; f.get(c);)
    {
      num_values += !!std::isspace(c);
    }
    f.clear(std::ios::eofbit);
    f.seekg(0, std::ios::beg);
    std::vector<uint_fast64_t> ret_vec;
    ret_vec.reserve(num_values);
    std::copy(std::istream_iterator<uint_fast64_t>{ f },
              std::istream_iterator<uint_fast64_t>{},
              std::back_inserter(ret_vec));
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return ret_vec;
  }();
  if (!stone_list_or_err.has_value())
  {
    std::println("Error: {}", stone_list_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*stone_list_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*stone_list_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
