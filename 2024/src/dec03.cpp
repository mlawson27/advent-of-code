#include <chrono>
#include <fstream>
#include <optional>
#include <print>
#include <string>

#include "helpers.h"

template <bool PARSE_DO_DONT>
static constexpr auto parse(std::string_view line) -> std::ptrdiff_t
{
  static constexpr auto advance_if_starts_with = [](std::string_view& v, auto&& s) {
    bool ret = v.starts_with(s);
    v        = v.substr(ret * (sizeof(s) - std::is_array_v<std::remove_reference_t<decltype(s)>>));
    return ret;
  };

  static constexpr auto try_parse_mul = [](std::string_view& v) -> std::optional<std::ptrdiff_t>
  {
    static constexpr auto parse_int = []<std::integral T>(std::string_view&v, T& t) {
      if (std::from_chars_result res = std::from_chars(v.begin(), v.end(), t);
          res)
      {
        v = { res.ptr, v.end() };
        return true;
      }
      return false;
    };

    std::ptrdiff_t l, r;
    return bool_obj(advance_if_starts_with(v, "mul(") &&
                    parse_int(v, l)                   &&
                    advance_if_starts_with(v, ',')    &&
                    parse_int(v, r)                   &&
                    advance_if_starts_with(v, ')')).then([l, r]() { return l * r; });
  };

  std::string_view line_working = line;
  std::ptrdiff_t   result       = 0;
  bool             mult_enabled = true;
  while (!line_working.empty())
  {
    if (std::optional<std::ptrdiff_t> mul_result;
        (!PARSE_DO_DONT || mult_enabled) &&
        (mul_result = try_parse_mul(line_working)).has_value())
    {
      result += *mul_result;
    }
    else if (PARSE_DO_DONT &&
             advance_if_starts_with(line_working, "do"))
    {
      static constexpr auto to_check = std::to_array<std::string_view>({ "n't()", "()" });
      if (auto it = std::ranges::find_if(to_check,
                                         [&line_working](std::string_view token) { return line_working.starts_with(token); });
          it != to_check.end())
      {
          mult_enabled = !!std::distance(it, to_check.begin());
          line_working = line_working.substr(it->size());
      }
    }
    else
    {
      line_working = line_working.substr(1);
    }
  }
  return result;
}

static void part1(std::string_view line)
{
  std::println("{}", parse<false>(line));
}

static void part2(std::string_view line)
{
  std::println("{}", parse<true>(line));
}

int dec03()
{
  namespace chr = std::chrono;

  std::string line = []()
  {
    std::ifstream f("input/dec03.txt");
    f.seekg(0, std::ios::end);
    std::size_t len = f.tellg();
    f.seekg(0, std::ios::beg);
    std::string ret;
    ret.reserve(len + 1);
    std::getline(f, ret, '\0');
    return ret;
  }();

  chr::time_point current = chr::steady_clock::now();
  part1(line);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(line);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
