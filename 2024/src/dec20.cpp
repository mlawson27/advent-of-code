#include <chrono>
#include <execution>
#include <expected>
#include <fstream>
#include <numeric>
#include <print>
#include <ranges>
#include <utility>
#include <vector>

#include "helpers.h"

using char_grid = grid<char>;
using position  = std::array<uint32_t, 2>;

template <typename Callable>
static constexpr auto for_all_adjacent(position const& current_pos, Callable&& fn) -> void
{
  for (auto const& pos_adj : { std::array{  0,  1 },
                               std::array{  1,  0 },
                               std::array{  0, -1 },
                               std::array{ -1,  0 } })
  {
    position new_pos;
    std::ranges::copy(std::views::zip_transform(std::plus{}, current_pos, pos_adj),
                      new_pos.begin());
    if (!fn(new_pos))
    {
      return;
    }
  }
}

static constexpr auto get_path_and_ps_to_map(char_grid const& maze_map, position const& start_pos, position const& end_pos) -> std::pair<grid<uint32_t>, std::vector<position>>
{
  grid<uint32_t>        ps_to(maze_map.num_rows(), maze_map.num_cols(), std::vector<uint32_t>(maze_map.num_rows() * maze_map.num_cols()));
  uint32_t              ps_so_far = 0;
  std::vector<position> path;
  path.reserve((maze_map.num_rows() - 2) * (maze_map.num_cols() - 2));
  path.push_back(start_pos);
  for (position current_pos = start_pos, last_pos = start_pos; current_pos != end_pos;)
  {
    ps_so_far++;
    for_all_adjacent(current_pos,
                     [&last_pos, &current_pos, &ps_to, &maze_map, &ps_so_far](position const& new_pos) {
                       auto&& [new_row, new_col] = new_pos;
                       if ((maze_map[new_row, new_col] == '.') &&
                           (new_pos != last_pos))
                       {
                         ps_to[new_row, new_col] = ps_so_far;
                         last_pos                = current_pos;
                         current_pos             = new_pos;
                         return false;
                       }
                       return true;
                     });
    path.push_back(current_pos);
  }
  path.shrink_to_fit();
  return std::pair{ std::move(ps_to), std::move(path) };
}

template <std::size_t MAX_CHEAT_LEN>
static constexpr auto find_num_cheats(grid<uint32_t> const& ps_to, std::span<position const> path) -> std::size_t
{
  static constexpr uint32_t DIFF_TO_TEST = 100;

  uint32_t ps_to_end = std::apply([&ps_to](auto&&... dims) { return ps_to[dims...]; }, path.back());
  if (ps_to_end <= DIFF_TO_TEST)
  {
    return 0;
  }

  return std::transform_reduce([]() { if constexpr (MAX_CHEAT_LEN >= 10) { return std::execution::par_unseq; } else { return std::execution::unseq; } }(),
                               path.begin(),
                               std::prev(path.end(), DIFF_TO_TEST),
                               0zu,
                               std::plus{},
                               [&ps_to](position const& pos) {
                                 auto&&      [row, col] = pos;
                                 std::size_t ret        = 0;
                                 for (std::size_t r = std::max<std::size_t>(row, MAX_CHEAT_LEN) - MAX_CHEAT_LEN; r <= std::min(row + MAX_CHEAT_LEN, ps_to.num_rows() - 1); r++)
                                 {
                                   std::size_t row_dist = row > r ? row - r : r - row;
                                   for (std::size_t c = std::max<std::size_t>(col, (MAX_CHEAT_LEN - row_dist)) - (MAX_CHEAT_LEN - row_dist); c <= std::min(col + (MAX_CHEAT_LEN - row_dist), ps_to.num_cols() - 1); c++)
                                   {
                                     std::size_t dist = row_dist + (col > c ? col - c : c - col);
                                     ret += (ps_to[r, c] >= (ps_to[row, col] + DIFF_TO_TEST + dist));
                                   }
                                 }
                                 return ret;
                               });
}

static void part1(grid<uint32_t> const& ps_to, std::span<position const> path)
{
  std::println("{}", find_num_cheats<2>(ps_to, path));
}

static void part2(grid<uint32_t> const& ps_to, std::span<position const> path)
{
  std::println("{}", find_num_cheats<20>(ps_to, path));
}

int dec20()
{
  namespace chr = std::chrono;

  auto maze_map_start_end_or_err = []() -> std::expected<std::tuple<char_grid, position, position>, std::string_view>
  {
    std::ifstream f("input/dec20.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::vector<char> ret_vec;
    std::size_t       num_rows = (total_len + 1) / (line_len + 1);
    ret_vec.reserve(num_rows * line_len);
    std::optional<position> start_pos, end_pos;
    for (auto it = std::istream_iterator<char>{ f }; it != std::istream_iterator<char>{}; it = std::next(it))
    {
      char c;
      if ((*it == 'S') || (*it == 'E'))
      {
        (*it == 'S' ? start_pos : end_pos) = std::array{ static_cast<position::value_type>(ret_vec.size() / num_rows),
                                                         static_cast<position::value_type>(ret_vec.size() % line_len) };
        c = '.';
      }
      else
      {
        c = *it;
      }
      ret_vec.push_back(c);
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    for (auto&& [pos, err] : { std::pair{ &start_pos, "Unable to find start position" },
                               std::pair{ &end_pos,   "Unable to find end position"   } })
    {
      if (!pos->has_value())
      {
        return std::unexpected(err);
      }
    }
    return std::tuple{ char_grid{ num_rows, line_len, std::move(ret_vec) }, *start_pos, *end_pos };
  }();
  if (!maze_map_start_end_or_err.has_value())
  {
    std::println("Error: {}", maze_map_start_end_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  auto&& [ps_to, path] = std::apply(get_path_and_ps_to_map, *maze_map_start_end_or_err);
  part1(ps_to, path);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(ps_to, path);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
