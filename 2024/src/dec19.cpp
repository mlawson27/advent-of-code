#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <vector>
#include <utility>

static constexpr auto get_num_possibilities(std::span<std::string const> patterns, std::span<std::string const> designs) -> std::vector<std::size_t>
{
  struct node
  {
    bool                                 present  = false;
    std::array<std::unique_ptr<node>, 5> children = {};
  };

  static constexpr auto find_child_node = []<typename Node>(Node&& node, char c) -> decltype(std::forward<Node>(node).children[0])& {
    std::size_t index;
    switch (c)
    {
      case 'b': index = 0; break;
      case 'g': index = 1; break;
      case 'r': index = 2; break;
      case 'u': index = 3; break;
      case 'w': index = 4; break;
      default: std::unreachable();
    }
    return std::forward<Node>(node).children[index];
  };

  node top_node;
  for (std::string_view pattern : patterns)
  {
    node* node_ptr = &top_node;
    for (char c : pattern)
    {
      std::unique_ptr<node>& child_node = find_child_node(*node_ptr, c);
      if (child_node == nullptr)
      {
        child_node = std::make_unique<node>();
      }
      node_ptr = child_node.get();
    }
    node_ptr->present = true;
  }

  auto const inner_fn = [&patterns, &top_node](std::string_view design, std::vector<std::pair<std::size_t, std::size_t>>& seen_design_lengths, auto&& inner_fn) -> std::size_t {
    node const* node_ptr = find_child_node(top_node, design[0]).get();
    std::size_t ret      = 0;
    for (std::size_t i = 1; (i < design.size()) && (node_ptr != nullptr); i++)
    {
      if (node_ptr->present)
      {
        auto it = std::ranges::find_if(seen_design_lengths,
                                        [&design, &i](auto&& pair) { return pair.first == design.size() - i; });
        if (it == seen_design_lengths.end())
        {
          seen_design_lengths.emplace_back(design.size() - i, inner_fn(design.substr(i), seen_design_lengths, inner_fn));
          it = std::prev(seen_design_lengths.end());
        }
        ret += it->second;
      }
      node_ptr = find_child_node(*node_ptr, design[i]).get();
    }
    return ret += ((node_ptr != nullptr) && node_ptr->present);
  };

  std::vector<std::size_t> ret;
  ret.reserve(designs.size());
  std::vector<std::pair<std::size_t, std::size_t>> seen_design_lengths;
  seen_design_lengths.reserve(designs.front().size());
  std::ranges::transform(designs,
                         std::back_inserter(ret),
                         [&inner_fn, &seen_design_lengths](std::string_view design) {
                           seen_design_lengths.clear();
                           return inner_fn(design, seen_design_lengths, inner_fn);
                         });
  return ret;
}

static void part1(std::vector<std::size_t> const& possibility_counts)
{
  std::println("{}", std::ranges::count_if(possibility_counts,
                                           [](std::size_t v) { return !!v; }));
}

static void part2(std::vector<std::size_t> const& possibility_counts)
{
  std::println("{}", std::ranges::fold_left(possibility_counts,
                                            0zu,
                                            std::plus{}));
}

int dec19()
{
  namespace chr = std::chrono;

  auto patterns_and_designs_or_err = []() -> std::expected<std::pair<std::vector<std::string>, std::vector<std::string>>, std::string_view>
  {
    std::ifstream f("input/dec19.txt");
    std::size_t token_len = 0, max_token_len = 0, num_tokens = 1;
    for (char c; f.get(c) && c != '\n';)
    {
      if (c == ',')
      {
        max_token_len = std::max(max_token_len, token_len);
        token_len     = 0;
        num_tokens++;
        f >> std::ws;
      }
      else
      {
        token_len++;
      }
    }
    max_token_len = std::max(max_token_len, token_len);
    f.clear(std::ios::eofbit);
    f.seekg(0, std::ios::beg);
    std::vector<std::string> patterns;
    patterns.reserve(num_tokens);
    while (f && (patterns.size() + 1 < num_tokens))
    {
      std::getline(f, patterns.emplace_back(), ',');
      f >> std::ws;
    }
    std::getline(f, patterns.emplace_back());
    f >> std::ws;
    std::streampos current_pos = f.tellg();
    num_tokens    = 1;
    token_len     = 0;
    max_token_len = 0;
    for (char c; f.get(c);)
    {
      if (c == '\n')
      {
        max_token_len = std::max(max_token_len, token_len);
        token_len     = 0;
        num_tokens++;
        f >> std::ws;
      }
      else
      {
        token_len++;
      }
    }
    f.clear(std::ios::eofbit);
    f.seekg(current_pos, std::ios::beg);
    std::vector<std::string> designs;
    while (f && (designs.size() < num_tokens))
    {
      std::getline(f, designs.emplace_back());
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    return std::pair{ std::move(patterns), std::move(designs) };
  }();
  if (!patterns_and_designs_or_err.has_value())
  {
    std::println("Error: {}", patterns_and_designs_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::vector<std::size_t> possibility_counts = std::apply(get_num_possibilities, *patterns_and_designs_or_err);
  part1(possibility_counts);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(possibility_counts);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
