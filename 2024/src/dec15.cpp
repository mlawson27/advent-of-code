#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <span>
#include <string>
#include <utility>
#include <vector>

#include "helpers.h"

using char_grid = grid<char>;

template <arithmetic T, std::size_t N, arithmetic U, typename Op>
static constexpr auto apply_operator(std::array<T, N> const& l, std::array<U, N> const& r, Op&& op) -> std::array<decltype(op(T{}, U{})), N>
{
  std::array<decltype (T{} + U{}), N> ret;
  std::ranges::copy(std::views::zip_transform(op, l, r),
                    ret.begin());
  return ret;
}

template <arithmetic T, std::size_t N, arithmetic U>
static constexpr auto operator+(std::array<T, N> const& l, std::array<U, N> const& r) -> std::array<decltype(T{} + U{}), N>
{
  return apply_operator(l, r, std::plus{});
}

template <arithmetic T, std::size_t N, arithmetic U>
static constexpr auto operator-(std::array<T, N> const& l, std::array<U, N> const& r) -> std::array<decltype(T{} - U{}), N>
{
  return apply_operator(l, r, std::minus{});
}

template <arithmetic T, std::size_t N, arithmetic U>
static constexpr auto operator+=(std::array<T, N>& l, std::array<U, N> const& r) -> std::array<T, N>&
{
  l = l + r;
  return l;
}

template <arithmetic T, std::size_t N, arithmetic U>
static constexpr auto operator-=(std::array<T, N>& l, std::array<U, N> const& r) -> std::array<T, N>&
{
  l = l - r;
  return l;
}

static constexpr auto get_at_pos(auto&& map, auto const& pos) -> auto&
{
  return std::apply([&map](auto&&... dims) -> auto& { return map[dims...]; }, pos);
};

static void part1(char_grid const& warehouse_map, std::string_view movements)
{
  std::remove_const_t<std::remove_reference_t<decltype(warehouse_map)>> new_map = warehouse_map;
  std::array<uint32_t, 2> working_pos = [&warehouse_map]() {
    for (uint32_t r = 0; r < warehouse_map.num_rows(); r++)
    {
      for (uint32_t c = 0; c < warehouse_map.num_cols(); c++)
      {
        if (warehouse_map[r, c] == '@')
        {
          return std::array{ r, c };
        }
      }
    }
    return std::array{ 0u, 0u};
  }();

  for (char c : movements)
  {
    std::array<int16_t, 2> to_add;
    switch (c)
    {
      case '^':
        to_add = { -1,  0 };
        break;
      case '>':
        to_add = {  0,  1 };
        break;
      case 'v':
        to_add = {  1,  0 };
        break;
      case '<':
        to_add = {  0, -1 };
        break;
      default:
        std::unreachable();
    }
    decltype(working_pos) pos_to_test = working_pos + to_add;
    while ((get_at_pos(new_map, pos_to_test) != '#') && (get_at_pos(new_map, pos_to_test) == 'O'))
    {
      pos_to_test += to_add;
    }
    if (get_at_pos(new_map, pos_to_test) == '#')
    {
      continue;
    }
    working_pos += to_add;
    for (decltype(working_pos) p = pos_to_test; p != working_pos; p -= to_add)
    {
      get_at_pos(new_map, p - to_add) = '.';
      get_at_pos(new_map, p)          = 'O';
    }
  }

  std::println("{}", std::ranges::fold_left(std::views::cartesian_product(std::views::iota(0zu, new_map.num_rows()),
                                                                          std::views::iota(0zu, new_map.num_cols())) | std::views::filter([&new_map](auto&& pos) { return get_at_pos(new_map, pos) == 'O'; }),
                                            0zu,
                                            [](std::size_t ret, auto&& pos) {
                                              auto&& [r, c] = pos;
                                              return ret + ((r * 100) + c);
                                            }));
}

static void part2(char_grid const& warehouse_map, std::string_view movements)
{
  char_grid new_map(warehouse_map.num_rows(), warehouse_map.num_cols() * 2, std::vector<char>(warehouse_map.num_rows() * warehouse_map.num_cols() * 2));
  std::array<uint32_t, 2> working_pos = {};
  std::size_t             num_boxes   = 0;
  for (uint32_t r = 0; r < warehouse_map.num_rows(); r++)
  {
    for (uint32_t c = 0; c < warehouse_map.num_cols(); c++)
    {
      uint32_t c2 = c * 2;
      switch (warehouse_map[r, c])
      {
        case '#':
          new_map[r, c2]     = '#';
          new_map[r, c2 + 1] = '#';
          break;
        case 'O':
          new_map[r, c2]     = 'O';
          new_map[r, c2 + 1] = '.';
          num_boxes++;
          break;
        case '@':
          working_pos        = { r, c2 };
          new_map[r, c2]     = '.';
          new_map[r, c2 + 1] = '.';
          break;
        default:
          new_map[r, c2]     = warehouse_map[r, c];
          new_map[r, c2 + 1] = warehouse_map[r, c];
          break;
      }
    }
  }

  std::vector<decltype(working_pos)> boxes_to_move;
  boxes_to_move.reserve(num_boxes);
  std::vector<decltype(working_pos)> up_down_queue;
  up_down_queue.reserve(warehouse_map.num_cols());

  for (char c : movements)
  {
    int16_t col_adj, row_adj;
    switch (c)
    {
      case '^':
      case 'v':
      {
        row_adj = 1 - 2 * (c == '^');
        col_adj = 0;
        decltype(working_pos) pos_to_test = { working_pos[0] + row_adj, working_pos[1] };
        if ((get_at_pos(new_map, pos_to_test) == '#') || [&new_map, &up_down_queue, &boxes_to_move, &pos_to_test, &row_adj]() {
                                                           std::size_t queue_index = 0;
                                                           up_down_queue.clear();
                                                           for (auto&& pos : { pos_to_test, std::array{ pos_to_test[0], pos_to_test[1] - 1 } })
                                                           {
                                                             if (get_at_pos(new_map, pos) == 'O')
                                                             {
                                                               up_down_queue.push_back(pos);
                                                               boxes_to_move.push_back(pos);
                                                               break;
                                                             }
                                                           }
                                                           while (queue_index < up_down_queue.size())
                                                           {
                                                             auto&& next_pos = up_down_queue[queue_index];
                                                             queue_index++;
                                                             for (auto&& col_adj : { 0, 1 })
                                                             {
                                                               if (new_map[next_pos[0] + row_adj, next_pos[1] + col_adj] == '#')
                                                               {
                                                                 boxes_to_move.clear();
                                                                 return true;
                                                               }
                                                             }
                                                             for (auto&& col_adj : { -1, 0, 1 })
                                                             {
                                                               decltype(working_pos) box_above_pos = { next_pos[0] + row_adj, next_pos[1] + col_adj };
                                                               if (get_at_pos(new_map, box_above_pos) == 'O')
                                                               {
                                                                 boxes_to_move.push_back(box_above_pos);
                                                                 up_down_queue.push_back(box_above_pos);
                                                               }
                                                             }
                                                           }
                                                           return false;
                                                         }())
        {
          continue;
        }
        working_pos = pos_to_test;
        break;
      }
      case '>':
      case '<':
      {
        row_adj = 0;
        col_adj = 1 - 2 * (c == '<');
        decltype(working_pos) pos_to_test = { working_pos[0], working_pos[1] + col_adj };
        if (get_at_pos(new_map, pos_to_test) == '#')
        {
          continue;
        }
        decltype(working_pos) new_working_pos = pos_to_test;
        pos_to_test[1] += col_adj * (col_adj < 0);
        bool set_working = true;
        while (get_at_pos(new_map, pos_to_test) == 'O')
        {
          if (new_map[pos_to_test[0], pos_to_test[1] + col_adj + (col_adj > 0)] == '#')
          {
            boxes_to_move.clear();
            set_working = false;
            break;
          }
          boxes_to_move.push_back(pos_to_test);
          pos_to_test[1] += col_adj * 2;
        }
        if (set_working)
        {
          working_pos = new_working_pos;
        }
        break;
      }
      default:
        std::unreachable();
    }
    for (auto&& pos : boxes_to_move | std::views::reverse)
    {
      get_at_pos(new_map, pos)                    = '.';
      new_map[pos[0] + row_adj, pos[1] + col_adj] = 'O';
    }
    boxes_to_move.clear();
  }

  std::println("{}", std::ranges::fold_left(std::views::cartesian_product(std::views::iota(0zu, new_map.num_rows()),
                                                                          std::views::iota(0zu, new_map.num_cols())) | std::views::filter([&new_map](auto&& pos) { return get_at_pos(new_map, pos) == 'O'; }),
                                            0zu,
                                            [](std::size_t ret, auto&& pos) {
                                              auto&& [r, c] = pos;
                                              return ret + ((r * 100) + c);
                                            }));
}

int dec15()
{
  namespace chr = std::chrono;

  auto warehouse_map_movements_or_err = []() -> std::expected<std::pair<char_grid, std::string>, std::string_view>
  {
    std::ifstream f("input/dec15.txt");
    std::size_t current_line_len = 0;
    std::size_t line_len         = std::numeric_limits<std::size_t>::max();
    std::size_t num_lines        = 0;
    for (char c, last_c = 0; f.get(c) && (c != '\n' || last_c != '\n'); last_c = c)
    {
      if (c == '\n')
      {
        num_lines++;
        if (line_len != std::numeric_limits<std::size_t>::max() && line_len != current_line_len)
        {
          return std::unexpected("Not all lines in map are the same length");
        }
        line_len         = current_line_len;
        current_line_len = 0;
      }
      else
      {
        current_line_len++;
      }
    }
    f.seekg(0, std::ios::beg);
    std::vector<char> ret_vec;
    ret_vec.reserve(num_lines * line_len);
    std::copy_n(std::istream_iterator<char>{ f },
                num_lines * line_len,
                std::back_inserter(ret_vec));
    f.seekg(2, std::ios::cur);
    std::streampos current_pos    = f.tellg();
    std::size_t    num_directions = 0;
    for (char c = 0; f.get(c);)
    {
      num_directions += (c != '\n');
    }
    std::string movements;
    movements.reserve(num_directions);
    f.clear(std::ios::eofbit);
    f.seekg(current_pos);
    std::copy(std::istream_iterator<char>{ f },
              std::istream_iterator<char>{},
              std::back_inserter(movements));
    if (movements.size() != num_directions)
    {
      return std::unexpected("Reserved wrong size");
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    return std::pair{ char_grid(num_lines, line_len, std::move(ret_vec)), std::move(movements) };
  }();
  if (!warehouse_map_movements_or_err.has_value())
  {
    std::println("Error: {}", warehouse_map_movements_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *warehouse_map_movements_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *warehouse_map_movements_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
