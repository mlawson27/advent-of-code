#include <algorithm>
#include <chrono>
#include <fstream>
#include <forward_list>
#include <print>
#include <ranges>
#include <vector>

#include "helpers.h"

template <std::ranges::forward_range Range>
static constexpr auto is_valid(Range&& report) -> bool
{
  return try_fold(report | std::views::adjacent_transform<2>([](std::size_t a, std::size_t b) { return a > b ? static_cast<std::ptrdiff_t>(a - b) : -static_cast<std::ptrdiff_t>(b - a); }),
                  0z,
                  [](std::ptrdiff_t last, std::ptrdiff_t v) -> std::optional<std::ptrdiff_t> {
                    return bool_obj(((last == 0) || ((v < 0) == (last < 0))) && (v != 0) && (v <= 3) && (v >= -3)).then_some(v);
                  }).has_value();
}

static void part1(std::vector<std::vector<std::size_t>> const& reports)
{
  std::println("{}", std::ranges::count_if(reports, is_valid<std::vector<std::size_t> const&>));
}

static void part2(std::vector<std::vector<std::size_t>> const& reports)
{
  std::println("{}", std::ranges::count_if(reports, [](std::vector<std::size_t> const& report) {
                                                      return std::ranges::any_of(std::views::iota(0zu, report.size()),
                                                                                 [&report](std::size_t i) {
                                                                                   return is_valid(report | std::views::enumerate
                                                                                                          | std::views::filter([i](std::pair<std::size_t, std::size_t> i_elem) { return i_elem.first != i; })
                                                                                                          | std::views::values);
                                                                                 });
                                                    }));
}

int dec02()
{
  namespace chr = std::chrono;

  std::vector<std::vector<std::size_t>> reports = []()
  {
    std::ifstream f("input/dec02.txt");
    std::size_t num_lines        = 1;
    std::size_t max_line_len     = 0;
    std::size_t current_line_len = 0;
    for (char c; f.get(c);)
    {
      if (c == '\n')
      {
        max_line_len     = std::max(max_line_len, current_line_len);
        current_line_len = 0;
        num_lines++;
      }
      else
      {
        current_line_len++;
      }
    }
    max_line_len += 2; // new line char + null terminator
    f.clear(std::ios::eofbit);
    f.seekg(0, std::ios::beg);
    std::vector<std::vector<std::size_t>> ret;
    ret.reserve(num_lines);
    std::string line_str;
    line_str.reserve(max_line_len);
    while (std::getline(f, line_str))
    {
      std::size_t num_values = 1;
      for (auto c_it = line_str.begin(); *c_it != '\n' && *c_it != '\0'; c_it = std::next(c_it))
      {
        num_values += !!std::isspace(*c_it);
      }
      std::vector<std::size_t>& line_vec = ret.emplace_back();
      line_vec.reserve(num_values);
      std::istringstream line_stream(line_str);
      for (std::size_t v; (line_stream >> v);)
      {
        line_vec.push_back(v);
      }
    }
    return ret;
  }();

  chr::time_point current = chr::steady_clock::now();
  part1(reports);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(reports);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
