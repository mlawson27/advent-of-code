#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <optional>
#include <print>
#include <ranges>
#include <utility>

#include "helpers.h"

template <std::unsigned_integral T>
static constexpr auto ends_with(T l, T r) -> std::optional<T>
{
  for (T working_r = r; working_r != 0; working_r /= 10)
  {
    if ((l % 10) != (working_r % 10))
    {
      return std::nullopt;
    }
    l /= 10;
  }
  return l;
}

template <bool TRY_CONCAT, std::unsigned_integral T>
static constexpr auto can_accumulate_to2(T result, std::span<T const> operands, T working_val) -> bool
{
  if (working_val > result)
  {
    return false;
  }
  if (operands.empty())
  {
    return result == working_val;
  }
  return (can_accumulate_to<TRY_CONCAT>(result, operands.subspan(1), working_val + operands[0]) ||
          can_accumulate_to<TRY_CONCAT>(result, operands.subspan(1), working_val * operands[0]) ||
          (TRY_CONCAT &&
           can_accumulate_to<TRY_CONCAT>(result, operands.subspan(1), concat(working_val, operands[0]))));
}

template <bool TRY_CONCAT, typename It>
static constexpr auto can_accumulate_to(It begin, It end, typename It::value_type working_val) -> bool
{
  if (std::next(begin) == end)
  {
    return *begin == working_val;
  }
  return (((*begin < working_val) &&
           can_accumulate_to<TRY_CONCAT>(std::next(begin), end, working_val - *begin)) ||
          ((working_val % *begin == 0) &&
           can_accumulate_to<TRY_CONCAT>(std::next(begin), end, working_val / *begin)) ||
          (TRY_CONCAT &&
           [&begin, &end, &working_val]() {
             auto opt = ends_with(working_val, *begin);
             return opt.has_value() && can_accumulate_to<TRY_CONCAT>(std::next(begin), end, *opt);
           }()));
}

static void part1(std::span<std::pair<uint_fast64_t, std::vector<uint_fast64_t>>> equations)
{
  std::println("{}", std::ranges::fold_left(equations | std::views::filter([](auto const& pair) {
                                                                             return std::apply([](uint_fast64_t result, std::span<uint_fast64_t const> operands) {
                                                                               return can_accumulate_to<false>(operands.rbegin(), operands.rend(), result);
                                                                             }, pair);
                                                                           })
                                                      | std::views::transform(static_cast<uint_fast64_t const&(*)(decltype(equations)::const_reference)>(std::get<0>)),
                                            0z,
                                            std::plus{}));
}

static void part2(std::span<std::pair<uint_fast64_t, std::vector<uint_fast64_t>>> equations)
{
  std::println("{}", std::ranges::fold_left(equations | std::views::filter([](auto const& pair) {
                                                                             return std::apply([](uint_fast64_t result, std::span<uint_fast64_t const> operands) {
                                                                               return can_accumulate_to<true>(operands.rbegin(), operands.rend(), result);
                                                                             }, pair);
                                                                           })
                                                      | std::views::transform(static_cast<uint_fast64_t const&(*)(decltype(equations)::const_reference)>(std::get<0>)),
                                            0z,
                                            std::plus{}));
}

int dec07()
{
  namespace chr = std::chrono;

  auto equations_or_error = []() -> std::expected<std::vector<std::pair<uint_fast64_t, std::vector<uint_fast64_t>>>, std::string_view>
  {
    std::ifstream f("input/dec07.txt");
    std::size_t line_count   = 0;
    std::size_t line_len     = 0;
    std::size_t max_line_len = 0;
    for (char c; f.get(c);)
    {
      if (c == '\n')
      {
        max_line_len = std::max(max_line_len, line_len);
        line_len     = 0;
        line_count++;
      }
      else
      {
        line_len++;
      }
    }
    max_line_len = std::max(max_line_len, line_len);
    line_count++;
    f.clear(std::ios::eofbit);
    f.seekg(0, std::ios::beg);
    std::vector<std::pair<uint_fast64_t, std::vector<uint_fast64_t>>> ret_vec;
    ret_vec.reserve(line_count);
    std::string tmp_line;
    tmp_line.reserve(max_line_len);
    for (std::size_t i = 0; i < line_count && f; i++)
    {
      std::getline(f, tmp_line);
      if (tmp_line.size() > max_line_len)
      {
        return std::unexpected("Line str allocated too small");
      }
      auto&& [result, operands] = ret_vec.emplace_back();
      auto   line_it            = std::as_const(tmp_line).data();
      auto   line_end           = std::as_const(tmp_line).data() + tmp_line.length();
      auto   parse_result       = std::from_chars(line_it, line_end, result);
      if (!parse_result)
      {
        return std::unexpected("Can't parse line result value");
      }
      line_it  = parse_result.ptr;
      line_it += (*line_it == ':');
      operands.reserve(std::count(line_it, line_end, ' '));
      for (std::size_t j = 0; j < operands.capacity(); j++)
      {
        line_it += (*line_it == ' ');
        parse_result = std::from_chars(line_it, line_end, operands.emplace_back());
        if (!parse_result)
        {
          return std::unexpected("Can't parse line result value");
        }
        line_it = parse_result.ptr;
      }
      if (line_it != line_end)
      {
        return std::unexpected("Equation line has more data");
      }
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return ret_vec;
  }();
  if (!equations_or_error.has_value())
  {
    std::println("Error: {}", equations_or_error.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*equations_or_error);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*equations_or_error);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
