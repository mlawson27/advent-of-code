#include <algorithm>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <span>
#include <unordered_map>
#include <utility>

enum direction_button : uint8_t
{
  DIR_BUTTON_A = 0,
  DIR_BUTTON_U = 1,
  DIR_BUTTON_D = 2,
  DIR_BUTTON_R = 3,
  DIR_BUTTON_L = 4,
};
static constexpr auto direction_button_values = { DIR_BUTTON_A,
                                                  DIR_BUTTON_U,
                                                  DIR_BUTTON_D,
                                                  DIR_BUTTON_R,
                                                  DIR_BUTTON_L };

template <std::size_t max_inclusive, typename Callable1, typename Callable2>
static constexpr auto build_presses_to_table(Callable1&& starting_pos_for, Callable2&& value_at_pos)
{
  std::array<std::array<std::tuple<std::array<std::array<direction_button,
                                                         max_inclusive>,
                                              max_inclusive>,
                                   std::size_t,
                                   std::size_t>,
                        max_inclusive + 1>,
             max_inclusive + 1> ret = {};
  std::array<std::size_t, max_inclusive + 1> seen;

  for (uint8_t i = 0; i <= max_inclusive; i++)
  {
    auto& this_elem                     = ret[i];
    auto& [paths, path_len, path_count] = this_elem[i];
    paths[0][0]                         = DIR_BUTTON_A;
    path_len                            = 1;
    path_count                          = 1;

    std::array<std::ptrdiff_t, 2> pos_on_keypad = std::forward<Callable1>(starting_pos_for)(i);

    using queue_elem = std::tuple<decltype(pos_on_keypad), std::array<direction_button, max_inclusive>, std::size_t>;
    std::array<queue_elem, 11> pos_queue;
    std::size_t                             pos_queue_head = 0;
    std::size_t                             pos_queue_tail = 1;
    pos_queue[pos_queue_head] = queue_elem{ pos_on_keypad, {}, 1};
    seen.fill(std::numeric_limits<std::size_t>::max());
    seen[i]                   = 1;
    while (pos_queue_head != pos_queue_tail)
    {
      auto  [new_pos, path_so_far, path_len_so_far] = pos_queue[pos_queue_head];
      auto& [row,     col]                          = new_pos;
      pos_queue_head                                = (pos_queue_head + 1) % pos_queue.size();
      for (auto&& [row_adj, col_adj, dir_button]: { std::tuple(row,     col + 1, DIR_BUTTON_R),
                                                    std::tuple(row + 1, col,     DIR_BUTTON_D),
                                                    std::tuple(row,     col - 1, DIR_BUTTON_L),
                                                    std::tuple(row - 1, col,     DIR_BUTTON_U) })
      {
        if (uint8_t j = std::forward<Callable2>(value_at_pos)(row_adj, col_adj);
            (j <= max_inclusive) &&
            (path_len_so_far <= seen[j]))
        {
          auto& [new_new_pos, new_path_so_far, new_path_len_so_far] = pos_queue[pos_queue_tail];
          pos_queue_tail                                            = (pos_queue_tail + 1) % pos_queue.size();

          new_new_pos                              = { row_adj, col_adj };
          new_path_so_far                          = path_so_far;
          new_path_len_so_far                      = path_len_so_far + 1;
          new_path_so_far[path_len_so_far - 1] = dir_button;

          auto& [paths, path_len, path_count] = this_elem[j];
          paths[path_count] = new_path_so_far;
          path_len          = new_path_len_so_far;
          path_count++;
          seen[j] = path_len;
        }
      }
    }
  }
  return ret;
}

static constexpr auto num_keypad_presses_to      = build_presses_to_table<0xA>([](uint8_t i) -> std::array<std::ptrdiff_t, 2> {
                                                                                 switch (i)
                                                                                 {
                                                                                   case 0:   return { 3,           1 };
                                                                                   case 0xA: return { 3,           2 };
                                                                                   default:  return { (9 - i) / 3, (i - 1) % 3 };
                                                                                 }
                                                                               },
                                                                               [](std::ptrdiff_t row, std::ptrdiff_t col) -> uint8_t {
                                                                                 constexpr uint8_t                               U      = std::numeric_limits<uint8_t>::max();
                                                                                 constexpr std::array<std::array<uint8_t, 3>, 4> keypad = { std::to_array<uint8_t>({ 7, 8, 9   }),
                                                                                                                                            std::to_array<uint8_t>({ 4, 5, 6   }),
                                                                                                                                            std::to_array<uint8_t>({ 1, 2, 3   }),
                                                                                                                                            std::to_array<uint8_t>({ U, 0, 0xA }) };
                                                                                 return ((row >= 0 && static_cast<std::size_t>(row) < keypad.size())      &&
                                                                                         (col >= 0 && static_cast<std::size_t>(col) < keypad[row].size()))
                                                                                        ? keypad[row][col]
                                                                                        : U;
                                                                               });
static constexpr auto num_directional_presses_to = build_presses_to_table<direction_button_values.size() - 1>([](uint8_t i) -> std::array<std::ptrdiff_t, 2> {
                                                                                                                switch (i)
                                                                                                                {
                                                                                                                  case DIR_BUTTON_A: return { 0, 2 };
                                                                                                                  case DIR_BUTTON_U: return { 0, 1 };
                                                                                                                  case DIR_BUTTON_D: return { 1, 1 };
                                                                                                                  case DIR_BUTTON_R: return { 1, 2 };
                                                                                                                  case DIR_BUTTON_L: return { 1, 0 };
                                                                                                                  default: std::unreachable();
                                                                                                                }
                                                                                                              },
                                                                                                              [](std::ptrdiff_t row, std::ptrdiff_t col) -> uint8_t {
                                                                                                                switch (row)
                                                                                                                {
                                                                                                                  case 0:
                                                                                                                  {
                                                                                                                    switch (col)
                                                                                                                    {
                                                                                                                      case 1: return DIR_BUTTON_U;
                                                                                                                      case 2: return DIR_BUTTON_A;
                                                                                                                    }
                                                                                                                    break;
                                                                                                                  }
                                                                                                                  case 1:
                                                                                                                  {
                                                                                                                    switch (col)
                                                                                                                    {
                                                                                                                      case 0: return DIR_BUTTON_L;
                                                                                                                      case 1: return DIR_BUTTON_D;
                                                                                                                      case 2: return DIR_BUTTON_R;
                                                                                                                    }
                                                                                                                  }
                                                                                                                  break;
                                                                                                                }
                                                                                                                return std::numeric_limits<uint8_t>::max();
                                                                                                              });

static constexpr auto complexity_sum(std::span<std::array<char, 4> const> codes, uint32_t num_directional_keypads_robots_using) -> std::size_t
{
  struct hasher
  {
    constexpr auto operator()(std::tuple<direction_button, direction_button, uint32_t> const& v) const -> uint64_t
    {
      struct elem
      {
        uint32_t         to_recurse;
        direction_button l;
        direction_button r;
        uint16_t         unused;
      };
      return std::bit_cast<uint64_t>(elem{ .to_recurse = std::get<2>(v), .l = std::get<0>(v), .r = std::get<1>(v), .unused = 0 });
    }
  };
  std::unordered_map<std::tuple<direction_button, direction_button, uint32_t>, std::size_t, hasher> result_cache;

  static constexpr auto min_paths_len = []<std::size_t N>(std::span<std::array<direction_button, N> const> paths, std::size_t path_len, uint32_t to_recurse, decltype(result_cache)& result_cache, auto&& min_paths_len, auto&& min_path_len) -> std::size_t {
    std::size_t ret = std::ranges::fold_left(paths,
                                             std::numeric_limits<std::size_t>::max(),
                                             [&path_len, &to_recurse, &result_cache, &min_paths_len, &min_path_len](std::size_t working, auto const& path) {
                                               return std::min(working, min_path_len(std::span(path.begin(), path_len), to_recurse, result_cache, min_paths_len, min_path_len));
                                             });
    return ret;
  };

  static constexpr auto min_path_len = [](std::span<direction_button const> path, uint32_t to_recurse, decltype(result_cache)& result_cache, auto&& min_paths_len, auto&& min_path_len) -> std::size_t {
    if (to_recurse == 0)
    {
      return path.size();
    }
    std::size_t      ret  = 0;
    direction_button last = DIR_BUTTON_A;
    for (direction_button i : path)
    {
      auto it = result_cache.find({ last, i, to_recurse - 1 });
      if (it == result_cache.end())
      {
        auto const& [inner_paths, inner_path_len, inner_path_count] = num_directional_presses_to[last][i];
        it = result_cache.emplace(std::tuple{ last, i, to_recurse - 1 },
                                  min_paths_len(std::span(inner_paths.begin(), inner_path_count), inner_path_len, to_recurse - 1, result_cache, min_paths_len, min_path_len)).first;
      }
      ret += it->second;
      last = i;
    }
    return ret;
  };

  std::size_t ret = 0;
  for (auto const& code : codes)
  {
    uint8_t     last          = 0xA;
    std::size_t ret_this_code = 0;
    std::size_t numeric_value = 0;
    for (char c : code)
    {
      uint8_t     i                             = (c == 'A') ? 0xA : (c - '0');
      auto const& [paths, path_len, path_count] = num_keypad_presses_to[last][i];
      ret_this_code += min_paths_len(std::span(paths.begin(), path_count), path_len, num_directional_keypads_robots_using, result_cache, min_paths_len, min_path_len);
      numeric_value  = ((i > 9) ? numeric_value : ((numeric_value * 10) + i));
      last = i;
    }
    ret += (ret_this_code * numeric_value);
  }
  return ret;
}

static void part1(std::span<std::array<char, 4> const> codes)
{
  std::println("{}", complexity_sum(codes, 2));
}

static void part2(std::span<std::array<char, 4> const> codes)
{
  std::println("{}", complexity_sum(codes, 25));
}

int dec21()
{
  namespace chr = std::chrono;

  auto codes_or_err = []() -> std::expected<std::array<std::array<char, 4>, 5>, std::string_view>
  {
    std::ifstream f("input/dec21.txt");
    std::array<std::array<char, 4>, 5> ret;
    for (auto& code : ret)
    {
      f.read(code.data(), code.size());
      if (!f)
      {
        return std::unexpected("Could not read line");
      }
      f >> std::ws;
    }
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    return ret;
  }();
  if (!codes_or_err.has_value())
  {
    std::println("Error: {}", codes_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  part1(*codes_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  part2(*codes_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
