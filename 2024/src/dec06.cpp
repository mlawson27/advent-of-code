#include <algorithm>
#include <bit>
#include <chrono>
#include <expected>
#include <fstream>
#include <print>
#include <ranges>
#include <utility>

#include "helpers.h"

using position = std::array<uint32_t, 2>;

enum class Direction
{
  North = 0,
  East  = 1,
  South = 2,
  West  = 3,
};
static constexpr auto Direction_Values = std::array{ Direction::North, Direction::East, Direction::South, Direction::West };
template <std::integral T>
static constexpr Direction& operator+=(Direction& d, T val)
{
  return (d = static_cast<Direction>((std::to_underlying(d) + val) % 4));
}
static constexpr Direction& operator++(Direction& d, int)
{
  return d += 1;
}
template <std::integral T>
static constexpr Direction operator+(Direction d, T val)
{
  auto ret = d;
  ret += val;
  return ret;
}

static constexpr auto next_pos(position pos, Direction direction, uint32_t amount = 1) -> position
{
  return std::apply([&direction, &amount](uint32_t x, uint32_t y) {
                      switch (direction)
                      {
                        case Direction::North: y -= amount; break;
                        case Direction::East:  x += amount; break;
                        case Direction::South: y += amount; break;
                        case Direction::West:  x -= amount; break;
                      }
                      return std::array {x, y};
                    }, pos);
}

using distance_to_next_obstacle_grid_elem = std::array<std::optional<uint32_t>, Direction_Values.size()>;

class traverse_map
{
  public:
    class iterator
    {
      public:
        using difference_type = std::ptrdiff_t;
        using value_type      = std::tuple<position, Direction>;

        constexpr iterator() = default;

        constexpr auto operator==(iterator const& other) const -> bool { return _map == other._map; }

        constexpr auto operator++() -> iterator&
        {
          uint32_t amount_to_move = 1;
          if (_map->_distance_to_next_crate != nullptr)
          {
            bool     run_to_start;
            position block_pos = next_pos(_map->_starting_pos, _map->_direction);
            switch (_direction)
            {
              case Direction::North:
                run_to_start = (_working_pos[0] == block_pos[0]) && (_working_pos[1] > block_pos[1]);
                break;
              case Direction::East:
                run_to_start = (_working_pos[1] == block_pos[1]) && (_working_pos[0] < block_pos[0]);
                break;
              case Direction::South:
                run_to_start = (_working_pos[0] == block_pos[0]) && (_working_pos[1] < block_pos[1]);
                break;
              case Direction::West:
                run_to_start = (_working_pos[1] == block_pos[1]) && (_working_pos[0] > block_pos[0]);
                break;
              default: std::unreachable();
            }
            uint32_t distance_to_crate = get_at_pos(*_map->_distance_to_next_crate, _working_pos)->get()[std::to_underlying(_direction)].value_or(std::numeric_limits<uint16_t>::max());
            if (run_to_start)
            {
              uint32_t distance_to_start = std::ranges::fold_left(std::views::zip_transform([](auto&& l, auto&& r) { return ((l < r) ? (r - l) : (l - r)); }, _working_pos, block_pos),
                                                                  0u,
                                                                  std::plus{});
              if (distance_to_start < distance_to_crate)
              {
                amount_to_move = distance_to_start;
              }
              else
              {
                amount_to_move = distance_to_crate;
              }
            }
            else
            {
              amount_to_move = distance_to_crate;
            }
            amount_to_move = std::max(amount_to_move - 1, 1u);
          }
          position next_pos_attempt = next_pos(_working_pos, _direction, amount_to_move);
          if (auto next_c_or_none = get_at_pos(_map->_working_map, next_pos_attempt);
              !next_c_or_none.has_value())
          {
            _map = nullptr;
          }
          else if ((*next_c_or_none == std::numeric_limits<uint8_t>::max()) ||
                   ((_map->_distance_to_next_crate != nullptr) &&
                     (next_pos_attempt == next_pos(_map->_starting_pos, _map->_direction))))
          {
            _direction++;
          }
          else
          {
            _working_pos = next_pos_attempt;
          }
          return *this;
        }

        constexpr auto operator++(int) -> iterator&
        {
          return ++(*this);
        }

        template <typename Self>
        constexpr auto operator*(this Self&& self)
        {
          return std::tuple(self._working_pos,
                            self._direction);
        }

      private:
        friend class traverse_map;

        constexpr iterator(traverse_map const& map, position working_pos, Direction direction)
          : _map(&map)
          , _working_pos(working_pos)
          , _direction(direction)
        {
        }

        template <typename T>
        static constexpr auto get_at_pos(T&& map, position pos) -> decltype(std::forward<T>(map).get(0, 0))
        {
          return std::apply([&map](uint32_t x, uint32_t y) { return std::forward<T>(map).get(y, x); }, pos);
        };

        traverse_map const* _map;
        position            _working_pos;
        Direction           _direction;
    };

    constexpr traverse_map(grid<uint8_t> const& working_map, position starting_pos, Direction direction = Direction::North, grid<distance_to_next_obstacle_grid_elem> const* distance_to_next_crate = nullptr)
      : _working_map           (working_map)
      , _starting_pos          (starting_pos)
      , _direction             (direction)
      , _distance_to_next_crate(distance_to_next_crate)
    {
    }

    template <typename Self>
    constexpr auto begin(this Self&& self)
    {
      if (!iterator::get_at_pos(std::forward<Self>(self)._working_map, std::forward<Self>(self)._starting_pos).has_value())
      {
        return iterator{};
      }
      return iterator{ self, std::forward<Self>(self)._starting_pos, std::forward<Self>(self)._direction };
    }

    constexpr auto end() const
    {
      return iterator{};
    }

  private:
    grid<uint8_t> const&                             _working_map;
    position                                         _starting_pos;
    Direction                                        _direction;
    grid<distance_to_next_obstacle_grid_elem> const* _distance_to_next_crate;
};

static void part1(grid<uint8_t> const& map, position const& start_pos)
{
  grid<uint8_t> updated_map = map;
  std::println("{}", std::ranges::count_if(traverse_map(updated_map, start_pos),
                                           [&updated_map](auto&& args_tup) {
                                             auto&& [col, row] = std::get<0>(args_tup);
                                             auto&  c          = updated_map[row, col];
                                             bool   ret        = (static_cast<uint8_t>(c) == 0);
                                             c                 = 1;
                                             return ret;
                                           }));
}

static void part2(grid<uint8_t> const& map, position const& start_pos)
{
  grid<distance_to_next_obstacle_grid_elem> distance_to_next_crate(map.num_rows(),
                                                                   map.num_cols(),
                                                                   std::vector<distance_to_next_obstacle_grid_elem>(map.num_rows() * map.num_cols()));
  for (auto&& [outer_bound, inner_bound, outer_is_row, fwd_dir] : { std::tuple{ map.num_rows(), map.num_cols(), true,  Direction::East },
                                                                    std::tuple{ map.num_cols(), map.num_rows(), false, Direction::South }, })
  {
    auto const row_for = [&outer_is_row](std::size_t outer, std::size_t inner) { return outer_is_row ? outer : inner; };
    auto const col_for = [&outer_is_row](std::size_t outer, std::size_t inner) { return outer_is_row ? inner : outer; };
    for (std::size_t outer = 0; outer < outer_bound; outer++)
    {
      std::optional<std::size_t> last_crate_fwd, last_crate_rev;
      for (std::size_t inner_fwd = 0, inner_rev = inner_bound - 1; inner_fwd < inner_bound; inner_fwd++, inner_rev--)
      {
        last_crate_fwd = (map[row_for(outer, inner_fwd), col_for(outer, inner_fwd)] == std::numeric_limits<uint8_t>::max()) ? std::optional(inner_fwd) : last_crate_fwd;
        last_crate_rev = (map[row_for(outer, inner_rev), col_for(outer, inner_rev)] == std::numeric_limits<uint8_t>::max()) ? std::optional(inner_rev) : last_crate_rev;
        distance_to_next_crate[row_for(outer, inner_fwd), col_for(outer, inner_fwd)][std::to_underlying(fwd_dir + 2)] = last_crate_fwd.transform([&inner_fwd](std::size_t inner) { return inner_fwd - inner; });
        distance_to_next_crate[row_for(outer, inner_rev), col_for(outer, inner_rev)][std::to_underlying(fwd_dir)]     = last_crate_rev.transform([&inner_rev](std::size_t inner) { return inner - inner_rev; });
      }
    }
  }
  grid<uint8_t> working_map = map;
  grid<uint8_t> tried_places(map.num_rows(), map.num_cols(), std::vector<uint8_t>(map.num_rows() * map.num_cols()));
  std::println("{}", std::ranges::count_if(traverse_map(working_map, start_pos),
                                           [&tried_places, &distance_to_next_crate, &working_map](auto const& args_tup) {
                                             auto&& [pos,       d]         = args_tup;
                                             auto&& [tried_col, tried_row] = next_pos(pos, d);
                                             if (auto tried_place_v_or_none = tried_places.get(tried_row, tried_col);
                                                 tried_place_v_or_none.transform([](uint8_t v) { return !v; }).value_or(false))
                                             {
                                               tried_place_v_or_none->get() = 1;
                                               grid<uint8_t> test_map       = working_map;
                                               return std::ranges::any_of(traverse_map(test_map, pos, d, &distance_to_next_crate),
                                                                          [&test_map](auto const& args_tup) {
                                                                            auto&& [inner_p, inner_d] = args_tup;
                                                                            auto& inner_c = test_map[inner_p[1], inner_p[0]];
                                                                            if (inner_c & (1 << std::to_underlying(inner_d)))
                                                                            {
                                                                              return true;
                                                                            }
                                                                            inner_c = static_cast<uint8_t>(inner_c | (1 << std::to_underlying(inner_d)));
                                                                            return false;
                                                                          });
                                             }
                                             auto& c = working_map[pos[1], pos[0]];
                                             c = static_cast<uint8_t>(c | (1 << std::to_underlying(d)));
                                             return false;
                                           }));
}

int dec06()
{
  namespace chr = std::chrono;

  auto map_and_start_or_error = []() -> std::expected<std::pair<grid<uint8_t>, position>, std::string_view>
  {
    std::ifstream f("input/dec06.txt");
    std::size_t line_len = 0;
    for (char c; f.get(c) && c != '\n';)
    {
      line_len++;
    }
    f.seekg(0, std::ios::end);
    std::size_t total_len = f.tellg();
    if ((total_len + 1) % (line_len + 1) != 0)
    {
      return std::unexpected("Incorrect total len");
    }
    f.seekg(0, std::ios::beg);
    std::vector<uint8_t> ret_vec;
    std::size_t          num_rows = (total_len + 1) / (line_len + 1);
    ret_vec.reserve(num_rows * line_len);
    ret_vec.insert(ret_vec.end(),
                   std::istream_iterator<char>{ f },
                   std::istream_iterator<char>{});
    if (!f.eof())
    {
      return std::unexpected("File has more data");
    }
    if (ret_vec.size() != ret_vec.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    std::optional<position> start_pos = [&ret_vec, &line_len]() {
      auto it = std::ranges::find(ret_vec, '^');
      return bool_obj(it != ret_vec.end()).then([&ret_vec, &it, &line_len]() -> position {
        auto total_pos = std::distance(ret_vec.begin(), it);
        *it = '.';
        return { static_cast<uint32_t>(total_pos % line_len), static_cast<uint32_t>(total_pos / line_len) };
      });
    }();
    if (!start_pos.has_value())
    {
      return std::unexpected("Start position not found");
    }
    std::ranges::for_each(ret_vec, [](uint8_t& v) { v = (v == '#') * std::numeric_limits<uint8_t>::max(); });
    return std::tuple{ grid<uint8_t>{ num_rows, line_len, std::move(ret_vec) }, *start_pos };
  }();
  if (!map_and_start_or_error.has_value())
  {
    std::println("Error: {}", map_and_start_or_error.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *map_and_start_or_error);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *map_and_start_or_error);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
