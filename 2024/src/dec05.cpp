#include <algorithm>
#include <bitset>
#include <chrono>
#include <expected>
#include <fstream>
#include <numeric>
#include <print>
#include <ranges>
#include <span>
#include <string>

#include "helpers.h"

static constexpr std::size_t MAX_PAGE_NUM = 100;

static constexpr auto update_is_valid_iter(std::span<std::bitset<MAX_PAGE_NUM>> rules, std::span<uint_fast8_t> update)
{
  std::bitset<MAX_PAGE_NUM> used_so_far;
  return try_fold_iter(update,
                       &used_so_far,
                       [&rules](decltype(used_so_far)* p_used_so_far, uint_fast8_t v) {
                         return bool_obj(((*p_used_so_far) & ~rules[v]).none()).then([&p_used_so_far, &v]() {
                             p_used_so_far->set(v);
                             return p_used_so_far;
                         });
                       });
};

static void part1(std::span<std::bitset<MAX_PAGE_NUM>> rules, std::span<std::vector<uint_fast8_t>> updates)
{
  std::println("{}", std::ranges::fold_left(updates | std::views::filter([&rules](std::span<uint_fast8_t> update) { return update_is_valid_iter(rules, update).value.has_value(); })
                                                    | std::views::transform([](std::span<uint_fast8_t> update) { return update[update.size() / 2]; }),
                                            0zu,
                                            std::plus{}));
}

static void part2(std::span<std::bitset<MAX_PAGE_NUM>> rules, std::span<std::vector<uint_fast8_t>> updates)
{
  auto const make_valid_and_get_mid = [&rules](std::span<uint_fast8_t> update) -> std::size_t {
    auto&& is_valid_result = update_is_valid_iter(rules, update);
    if (is_valid_result.value.has_value())
    {
      return 0zu;
    }

    auto        last_valid_it    = std::prev(is_valid_result.in);
    std::size_t position_to_find = update.size() / 2;
    if (std::saturate_cast<std::size_t>(std::distance(update.begin(), last_valid_it)) > position_to_find)
    {
      return update[position_to_find];
    }

    auto                      new_rule = std::vector(update.begin(), update.end());
    std::bitset<MAX_PAGE_NUM> used_so_far;
    for (auto it = new_rule.begin(); it != new_rule.begin() + position_to_find + 1; it++)
    {
      auto other_it = std::find_if(it,
                                   new_rule.end(),
                                   [&new_rule, &rules, &used_so_far](uint_fast8_t v) {
                                     return std::ranges::all_of(new_rule,
                                                                [&rules, &used_so_far, &v](uint_fast8_t other) { return (~rules[v] | used_so_far).test(other); });
                                   });
      std::swap(*it, *other_it);
      used_so_far.set(*it);
    }
    return new_rule[position_to_find];
  };
  std::println("{}", std::ranges::fold_left(updates | std::views::transform(make_valid_and_get_mid),
                                            0zu,
                                            std::plus{}));
}

int dec05()
{
  namespace chr = std::chrono;

  auto rules_and_updates_or_err = []() -> std::expected<std::pair<std::array<std::bitset<MAX_PAGE_NUM>, MAX_PAGE_NUM>, std::vector<std::vector<uint_fast8_t>>>, std::string_view>
  {
    std::ifstream f("input/dec05.txt");
    std::size_t   num_rules = 0, num_updates = 0, max_update_line_len = 0;
    {
      auto        arr              = std::array { &num_rules, &num_updates };
      auto        to_increment     = arr.begin();
      std::size_t current_line_len = 0;
      for (char c, last = '\0'; f.get(c); last = c)
      {
        if (c == '\n')
        {
          to_increment        += (last == '\n');
          *(*to_increment)    += (last != '\n');
          max_update_line_len  = std::max(max_update_line_len, (current_line_len) * (to_increment != arr.begin()));
          current_line_len     = 0;
        }
        else
        {
          current_line_len++;
        }
      }
      (*(*to_increment))++;
    }
    f.clear(std::ios::eofbit);
    f.seekg(0, std::ios::beg);
    std::pair<std::array<std::bitset<MAX_PAGE_NUM>, MAX_PAGE_NUM>, std::vector<std::vector<uint_fast8_t>>> ret;
    auto& [rules, updates] = ret;
    for (std::size_t i = 0; i < num_rules; i++)
    {
      unsigned int left, right;
      char         c;
      if (!(f >> left >> c >> right) ||
          (left  >= MAX_PAGE_NUM)    ||
          (right >= MAX_PAGE_NUM)    ||
          (c != '|'))
      {
        return std::unexpected("Bad rule line");
      }
      rules[right].set(left);
    }
    for (char count = 0, c; count < 2 && f.get(c); count += (c == '\n'));
    updates.reserve(num_updates);
    {
      std::string line;
      line.reserve(max_update_line_len);
      for (std::size_t i = 0; i < num_updates; i++)
      {
        if (!std::getline(f, line))
        {
          return std::unexpected("Not enough update lines");
        }
        auto& update = updates.emplace_back();
        update.reserve(std::ranges::count(line, ',') + 1);
        std::ranges::copy(line | std::views::split(',')
                               | std::views::transform([](auto const& v) { return parse<uint_fast8_t>(std::string_view(v)); })
                               | std::views::filter([](std::expected<uint_fast8_t, std::error_code> const& v) { return v.has_value(); })
                               | std::views::transform([](std::expected<uint_fast8_t, std::error_code> const& v) { return *v; }),
                          std::back_inserter(update));
        if (update.size() != update.capacity())
        {
          return std::unexpected("Bad update line");
        }
      }
    }
    return ret;
  }();
  if (!rules_and_updates_or_err.has_value())
  {
    std::println("Error: {}", rules_and_updates_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *rules_and_updates_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *rules_and_updates_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
