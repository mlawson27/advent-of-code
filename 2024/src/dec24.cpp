#include <algorithm>
#include <cassert>
#include <chrono>
#include <expected>
#include <fstream>
#include <list>
#include <numeric>
#include <queue>
#include <print>
#include <ranges>
#include <span>
#include <utility>
#include <vector>

enum class bit_op
{
  OR  = 0,
  AND = 1,
  XOR = 2,
};

struct gate
{
  uint16_t l;
  uint16_t r;
  bit_op   op;
  uint16_t v;
};

static void part1(std::span<std::optional<bool> const> initial_values, std::span<gate const> gates, std::span<std::array<char, 3> const> idx_to_name)
{
  auto z00_i = std::distance(idx_to_name.begin(),
                             std::ranges::partition_point(idx_to_name,
                                                          [](auto&& name_arr) { return name_arr[0] < 'z' || std::any_of(std::next(name_arr.begin()), name_arr.end(), [](char c) { return !std::isdigit(c); }); }));

  std::vector<std::optional<bool>> values(initial_values.begin(), initial_values.end());
  std::queue<gate const*>          remaining_gates;
  std::ranges::for_each(gates | std::views::filter([&values](auto const& gate) { return !values[gate.v].has_value(); }),
                        [&remaining_gates](auto const& gate) { remaining_gates.push(&gate); });
  while (!remaining_gates.empty())
  {
    gate const* p_gate = remaining_gates.front();
    remaining_gates.pop();
    auto&& [l_i, r_i, op, v_i] = *p_gate;
    if (values[l_i].has_value() && values[r_i].has_value())
    {
      switch (op)
      {
        case bit_op::AND: values[v_i] = *values[l_i] && *values[r_i]; break;
        case bit_op::OR:  values[v_i] = *values[l_i] || *values[r_i]; break;
        case bit_op::XOR: values[v_i] = *values[l_i] != *values[r_i]; break;
        default: std::unreachable();
      }
    }
    else
    {
      remaining_gates.push(p_gate);
    }
  }
  std::println("{}", std::accumulate(values.rbegin(),
                                     std::next(values.rbegin(), (values.size() - z00_i)),
                                     0zu,
                                     [](std::size_t l, auto const& r) { return (l << 1) | *r; }));
}

static void part2(std::span<std::optional<bool> const>, std::span<gate const> gates, std::span<std::array<char, 3> const> idx_to_name)
{
  static constexpr auto prefix_chars = std::array{ 'x', 'y', 'z' };
  std::array<uint16_t, prefix_chars.size()> prefix_is;
  std::ranges::transform(prefix_chars,
                         prefix_is.begin(),
                         [&idx_to_name](char c) { return std::distance(idx_to_name.begin(),
                                                                       std::ranges::partition_point(idx_to_name,
                                                                                                    [&c](auto&& name_arr) { return name_arr[0] < c || std::any_of(std::next(name_arr.begin()), name_arr.end(), [](char c) { return !std::isdigit(c); }); })); });

  std::list gates_list(gates.begin(), gates.end());
  auto const find_ops = [&gates_list](uint16_t op1, uint16_t op2, bit_op op) {
    return std::ranges::find_if(gates_list,
                                [&op1, &op2, &op](auto&& gate) { return (gate.op == op) &&
                                                                         ((gate.l == op1 && gate.r == op2) ||
                                                                          (gate.l == op2 && gate.r == op1)); });
  };
  auto const find_result = [&gates_list](uint16_t result) {
    return std::ranges::find_if(gates_list,
                                [&result](auto&& gate) { return gate.v == result; });
  };

  std::array<uint16_t, 8> swapped_is;
  std::size_t             swapped_i_count = 0;

  auto&& [x_i, y_i, z_i] = prefix_is;
  auto sum_it_ops        = find_ops(x_i, y_i, bit_op::XOR);
  if (sum_it_ops->v != z_i)
  {
    auto sum_it_result              = find_result(z_i);
    swapped_is[swapped_i_count]     = sum_it_ops->v;
    swapped_is[swapped_i_count + 1] = sum_it_result->v;
    swapped_i_count += 2;
    std::swap(sum_it_ops->v, sum_it_result->v);
  }
  gates_list.erase(sum_it_ops);

  auto carry_it_ops = find_ops(x_i, y_i, bit_op::AND);
  while (swapped_i_count < swapped_is.size())
  {
    x_i++;
    y_i++;
    z_i++;

    auto sum_it_base   = find_ops(x_i, y_i, bit_op::XOR);
    sum_it_ops         = find_ops(sum_it_base->v, carry_it_ops->v, bit_op::XOR);
    if (sum_it_ops == gates_list.end())
    {
      auto sum_it_result = find_result(z_i);
      auto&& [l, r, op, v] = *sum_it_result;
      assert(op == bit_op::XOR);
      auto& incorrect_it       = ((l != sum_it_base->v) && (r != sum_it_base->v)) ? sum_it_base : carry_it_ops;
      auto  incorrect_other_it = find_result(((&incorrect_it == &sum_it_base ? carry_it_ops : sum_it_base)->v != l) ? l : r);
      swapped_is[swapped_i_count]     = incorrect_it->v;
      swapped_is[swapped_i_count + 1] = incorrect_other_it->v;
      swapped_i_count += 2;
      std::swap(incorrect_it->v, incorrect_other_it->v);
      sum_it_ops = find_ops(sum_it_base->v, carry_it_ops->v, bit_op::XOR);
    }
    else if (sum_it_ops->v != z_i)
    {
      auto sum_it_result              = find_result(z_i);
      swapped_is[swapped_i_count]     = sum_it_ops->v;
      swapped_is[swapped_i_count + 1] = sum_it_result->v;
      swapped_i_count += 2;
      std::swap(sum_it_ops->v, sum_it_result->v);
    }
    gates_list.erase(sum_it_ops);

    auto carry_it_base = find_ops(x_i, y_i, bit_op::AND);
    auto carry_it_x    = find_ops(carry_it_ops->v, sum_it_base->v, bit_op::AND);
    if (carry_it_x == gates_list.end())
    {
      assert(false); // Unimplemented
    }
    gates_list.erase(sum_it_base);
    gates_list.erase(carry_it_ops);

    carry_it_ops = find_ops(carry_it_x->v, carry_it_base->v, bit_op::OR);
    if (carry_it_ops == gates_list.end())
    {
      assert(false); // Unimplemented
    }
    gates_list.erase(carry_it_x);
  }
  std::ranges::sort(swapped_is, [&idx_to_name](auto&& l_i, auto&& r_i) { return idx_to_name[l_i] < idx_to_name[r_i]; });
  std::print("{}", std::string_view(idx_to_name[swapped_is.front()].begin(), idx_to_name[swapped_is.front()].end()));
  std::for_each(std::next(swapped_is.begin()),
                swapped_is.end(),
                [&idx_to_name](auto&& i) { std::print(",{}", std::string_view(idx_to_name[i].begin(), idx_to_name[i].end())); });
  std::println();
}

int dec24()
{
  namespace chr = std::chrono;

  auto initial_values_rules_and_idx_to_name_or_err = []() -> std::expected<std::tuple<std::vector<std::optional<bool>>, std::vector<gate>, std::vector<std::array<char, 3>>>, std::string_view>
  {
    std::ifstream f("input/dec24.txt");
    std::size_t    num_lines         = 1;
    std::size_t    num_lines_pre_sep = 0;
    std::streampos separator_blank;
    for (char c, last_c = 0; f.get(c); last_c = c)
    {
      if (c == '\n')
      {
        if (last_c == '\n')
        {
          num_lines_pre_sep = num_lines;
          separator_blank   = f.tellg();
        }
        else
        {
          num_lines++;
        }
      }
    }
    f.clear();

    f.seekg(0, std::ios::beg);
    std::vector<std::array<char, 3>> idx_to_name;
    idx_to_name.reserve(num_lines);
    while (f.tellg() < separator_blank)
    {
      std::array<char, 7> line;
      f.getline(line.data(), line.size()) >> std::ws;
      auto& name = idx_to_name.emplace_back();
      std::copy_n(line.begin(),
                  name.size(),
                  name.begin());
    }
    while (!f.eof())
    {
      std::array<char, 19> line;
      f.getline(line.data(), line.size());
      auto& name = idx_to_name.emplace_back();
      std::copy_n(std::prev(line.end(), 4 + (line[line.size() - 2] == 0)),
                  name.size(),
                  name.begin());
    }
    if (idx_to_name.size() != idx_to_name.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    std::ranges::sort(idx_to_name);
    auto const idx_for_name = [&idx_to_name](auto&& name) {
      auto name_sv = std::string_view(name.begin(), std::size(decltype(idx_to_name)::value_type{}));
      return std::distance(idx_to_name.begin(),
                           std::ranges::partition_point(idx_to_name,
                                                        [&name_sv](auto const& name_elem) { return std::string_view(name_elem.begin(), name_elem.end()) < name_sv; }));
    };

    std::vector<std::optional<bool>> initial_values;
    initial_values.resize(num_lines);
    f.clear();
    f.seekg(0, std::ios::beg);
    while (f.tellg() < separator_blank)
    {
      std::array<char, 7> line;
      f.getline(line.data(), line.size()) >> std::ws;
      initial_values[idx_for_name(line | std::views::take(3))]= (line[line.size() - 2] == '1');
    }

    std::vector<gate> gates;
    gates.reserve(num_lines - num_lines_pre_sep + 1);
    while (!f.eof())
    {
      std::array<char, 19> line;
      f.getline(line.data(), line.size());
      auto  line_ws_split       = std::views::split(line, ' ');
      auto  it                  = line_ws_split.begin();
      auto& [l_i, r_i, op, v_i] = gates.emplace_back();
      l_i = idx_for_name(*it);
      it++;
      switch ((*it)[0])
      {
        case 'A': op = bit_op::AND; break;
        case 'O': op = bit_op::OR;  break;
        case 'X': op = bit_op::XOR; break;
        default:
          return std::unexpected("Invalid binary operation");
      }
      it++;
      r_i = idx_for_name(*it);
      it  = std::next(it, 2);
      v_i = idx_for_name(*it);
    }
    if (gates.size() != gates.capacity())
    {
      return std::unexpected("Reserved wrong size");
    }
    return std::tuple{ std::move(initial_values), std::move(gates), std::move(idx_to_name) };
  }();
  if (!initial_values_rules_and_idx_to_name_or_err.has_value())
  {
    std::println("Error: {}", initial_values_rules_and_idx_to_name_or_err.error());
    return 1;
  }

  chr::time_point current = chr::steady_clock::now();
  std::apply(part1, *initial_values_rules_and_idx_to_name_or_err);
  chr::time_point post_p1 = chr::steady_clock::now();
  std::apply(part2, *initial_values_rules_and_idx_to_name_or_err);
  chr::time_point post_p2 = chr::steady_clock::now();
  std::println("p1: {}", chr::duration_cast<chr::microseconds>(post_p1 - current));
  std::println("p2: {}", chr::duration_cast<chr::microseconds>(post_p2 - post_p1));
  return 0;
}
