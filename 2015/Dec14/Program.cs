﻿using System.Text.RegularExpressions;

IDictionary<string, (long distance, long points)> simulate(IList<(string name, long speed, long runTime, long restTime)> speedsRestTimes)
{
  IDictionary<string, (long distance, long points)> distancesPointsSoFar = speedsRestTimes.ToDictionary(p => p.name, p => ((long)0, (long)0));

  for (int i = 0; i < 2503; i++)
  {
    (string? leader, long leaderDistance, long leaderPoints) = (null, 0, 0);
    foreach ((string name, long speed, long runTime, long restTime) in speedsRestTimes)
    {
      (long currentDistance, long currentPoints) = distancesPointsSoFar[name];
      currentDistance += (Convert.ToInt64(i % (runTime + restTime) < runTime) * speed);
      distancesPointsSoFar[name] = (currentDistance, currentPoints);
      if (currentDistance > leaderDistance)
      {
        leader         = name;
        leaderDistance = currentDistance;
        leaderPoints   = currentPoints;
      }
    }
    distancesPointsSoFar[leader!] = (leaderDistance, leaderPoints + 1);
  }
  return distancesPointsSoFar;
}

long part1(IList<(string name, long speed, long runTime, long restTime)> speedsRestTimes)
{
  return simulate(speedsRestTimes).Max(p => p.Value.distance);
}

long part2(IList<(string name, long speed, long runTime, long restTime)> speedsRestTimes)
{
  return simulate(speedsRestTimes).Max(p => p.Value.points);
}

IList<(string, long, long, long)> speedsRestTimes = File.ReadAllLines("2015/dec14.txt").Select(str => Regex.Match(str, @"(\w+)\s+can\s+fly\s+(\d+)\s+km/s\s+for\s+(\d+)\s+seconds,\s+but\s+then\s+must\s+rest\s+for\s+(\d+)\s+seconds."))
                                                                                       .Select(m => (m.Groups[1].Value, long.Parse(m.Groups[2].Value), long.Parse(m.Groups[3].Value), long.Parse(m.Groups[4].Value))).ToList();

Console.WriteLine(part1(speedsRestTimes));
Console.WriteLine(part2(speedsRestTimes));
