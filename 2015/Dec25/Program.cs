﻿using System.Text.RegularExpressions;

(long row, long column) = ((Func<Match, (long, long)>)(m => (long.Parse(m.Groups[1].Value),
                                                             long.Parse(m.Groups[2].Value))))(Regex.Match(File.ReadAllText("2015/dec25.txt"),
                                                                                                          @"row (\d+), column (\d+)"));

(long workingRow, long workingColumn) = (1, 1);
long currentCode                      = 20151125;
do
{
  if (workingRow == 1)
  {
    workingRow     = workingColumn + 1;
    workingColumn  = 1;
  }
  else
  {
    workingRow--;
    workingColumn++;
  }
  currentCode = ((currentCode * 252533) % 33554393);
} while (workingRow != row || workingColumn != column);

Console.WriteLine(currentCode);
