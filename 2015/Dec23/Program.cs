﻿void runProgram(IList<(string instr, string[] args)> instructions, IDictionary<string, long> regs)
{
  IDictionary<string, Func<string[], IDictionary<string, long>, int, int>> INSTRUCTION_FUNCTIONS = new Dictionary<string, Func<string[], IDictionary<string, long>, int, int>>()
  {
    { "hlf", ((args, regs, pc) => { regs[args[0]] /= 2; return pc + 1; }) },
    { "tpl", ((args, regs, pc) => { regs[args[0]] *= 3; return pc + 1; }) },
    { "inc", ((args, regs, pc) => { regs[args[0]] += 1; return pc + 1; }) },
    { "jmp", ((args, regs, pc) => pc + int.Parse(args[0])) },
    { "jie", ((args, regs, pc) => pc + ((regs[args[0]] % 2 == 0) ? int.Parse(args[1]) : 1)) },
    { "jio", ((args, regs, pc) => pc + ((regs[args[0]]     == 1) ? int.Parse(args[1]) : 1)) },
  };

  int pc = 0;
  while (pc >= 0 && pc < instructions.Count)
  {
    (string instr, string[] args) = instructions[pc];
    pc = INSTRUCTION_FUNCTIONS[instr](args, regs, pc);
  }
}

long part1(IList<(string instr, string[] args)> instructions)
{
  IDictionary<string, long> regs = new Dictionary<string, long>() { { "a", 0 }, { "b", 0 } };
  runProgram(instructions, regs);
  return regs["b"];
}

long part2(IList<(string instr, string[] args)> instructions)
{
  IDictionary<string, long> regs = new Dictionary<string, long>() { { "a", 1 }, { "b", 0 } };
  runProgram(instructions, regs);
  return regs["b"];
}

IList<(string instr, string[] args)> instructions = File.ReadAllLines("2015/dec23.txt").Select(str => (str.Substring(0, 3), str.Substring(4).Split(", "))).ToList();

Console.WriteLine(part1(instructions));
Console.WriteLine(part2(instructions));
