﻿using System.Text.RegularExpressions;

string getNextPassword(string line)
{
  long lineInt = 0;
  for (int i = 0; i < line.Length; i++)
  {
    lineInt = lineInt * 26 + (line[i] - 'a');
  }

  bool valid = false;
  do
  {
    lineInt++;
    line = "";
    long  lineIntWorking      = lineInt;
    int   consecIncreasing    = 1;
    int   maxConsecIncreasing = 0;
    char? lastChar            = null;
    while (lineIntWorking > 0)
    {
      char workingC = Convert.ToChar((lineIntWorking % 26) + 'a');
      if ("iol".Contains(workingC))
      {
        break;
      }
      consecIncreasing    = (consecIncreasing * Convert.ToInt32(lastChar.HasValue && ((lastChar!.Value - workingC) == 1)) + 1);
      maxConsecIncreasing = Math.Max(maxConsecIncreasing, consecIncreasing);
      line                = (workingC + line);
      lineIntWorking      = lineIntWorking / 26;
      lastChar            = workingC;
    }
    valid = (Regex.Matches(line, @"([a-z])\1").Count >= 2) && (maxConsecIncreasing >= 3);
  } while(!valid);

  return line;
}

string part1(string line)
{
  return getNextPassword(line);
}

string part2(string line)
{
  return getNextPassword(getNextPassword(line));
}

string line = File.ReadAllText("2015/dec11.txt");

Console.WriteLine(part1(line));
Console.WriteLine(part2(line));
