﻿long part1(IList<string> strs)
{
  bool isNice(string str)
  {
    bool consec = false;
    for (int i = 0; !consec && i < str.Length - 1; i++)
    {
      consec = (str[i] == str[i + 1]);
    }

    return ("aeoiu".Sum(c => str.Count(cc => cc == c)) >= 3 &&
            consec &&
            !(new string[]{ "ab", "cd", "pq", "xy" }.Any(badStr => str.Contains(badStr))));
  }

  return strs.Count(isNice);
}

long part2(IList<string> strs)
{
  bool isNice(string str)
  {
    ISet<string> knownPairs     = new HashSet<string>();
    string       prevPair       = "";
    bool         multiplePairs  = false;
    bool         sameWithMiddle = false;
    for (int i = 0; !multiplePairs && i < str.Length - 1; i++)
    {
      string pair = str.Substring(i, 2);
      if (pair != prevPair)
      {
        multiplePairs = knownPairs.Contains(pair);
        knownPairs.Add(pair);
      }
      prevPair = pair;
    }
    for (int i = 0; !sameWithMiddle && i < str.Length - 2; i++)
    {
      sameWithMiddle = (str[i] == str[i + 2]);
    }
    return multiplePairs && sameWithMiddle;
  }

  return strs.Count(isNice);
}

IList<string> strs = File.ReadAllLines("2015/dec05.txt");

Console.WriteLine(part1(strs));
Console.WriteLine(part2(strs));
