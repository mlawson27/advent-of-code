﻿static class Dec21
{
  struct Item
  {
    public long cost;
    public long damage;
    public long armor;
  }

  static readonly IDictionary<string, Item> WEAPONS = new Dictionary<string, Item>
  {
    { "Dagger",     new Item{ cost =  8, damage = 4, armor = 0 } },
    { "Shortsword", new Item{ cost = 10, damage = 5, armor = 0 } },
    { "Warhammer",  new Item{ cost = 25, damage = 6, armor = 0 } },
    { "Longsword",  new Item{ cost = 40, damage = 7, armor = 0 } },
    { "Greataxe",   new Item{ cost = 74, damage = 8, armor = 0 } },
  };

  static readonly IDictionary<string, Item> ARMOR = new Dictionary<string, Item>
  {
    { "Leather",    new Item{ cost =  13, damage = 0, armor = 1 } },
    { "Chainmail",  new Item{ cost =  31, damage = 0, armor = 2 } },
    { "Splintmail", new Item{ cost =  53, damage = 0, armor = 3 } },
    { "Bandedmail", new Item{ cost =  75, damage = 0, armor = 4 } },
    { "Platemail",  new Item{ cost = 102, damage = 0, armor = 5 } },
  };

  static readonly IDictionary<string, Item> RINGS = new Dictionary<string, Item>
  {
    { "Damage +1",  new Item{ cost =  25, damage = 1, armor = 0 } },
    { "Damage +2",  new Item{ cost =  50, damage = 2, armor = 0 } },
    { "Damage +3",  new Item{ cost = 100, damage = 3, armor = 0 } },
    { "Defense +1", new Item{ cost =  20, damage = 0, armor = 1 } },
    { "Defense +2", new Item{ cost =  40, damage = 0, armor = 2 } },
    { "Defense +3", new Item{ cost =  80, damage = 0, armor = 3 } },
  };

  static long getCost(long playerHitPoints, long bossHitPoints, long bossDamage, long bossArmor, bool playerWins)
  {
    IEnumerable<Item?[]> getEquipmentCombinations()
    {
      IEnumerable<Item?> ringsWithNull = new Item?[2].Concat(RINGS.Values.Cast<Item?>());

      foreach (Item? weapon in WEAPONS.Values.Cast<Item?>())
      {
        foreach (Item? armor in new Item?[1].Concat(ARMOR.Values.Cast<Item?>()))
        {
          foreach ((Item? ring1, int ring1Index) in ringsWithNull.Select((item, index) => (item, index)))
          {
            foreach (Item? ring2 in ringsWithNull.Take(ring1Index).Skip(1).Concat(ringsWithNull.Skip(ring1Index + 1)))
            {
              yield return new [] { weapon, armor, ring1, ring2 };
            }
          }
        }
      }
    }

    Func<long, long, long> minOrMax = (playerWins ? Math.Min : Math.Max);
    long minOrMaxTotalCost          = (playerWins ? long.MaxValue : long.MinValue);
    foreach (Item?[] items in getEquipmentCombinations())
    {
      long localPlayerHitPoints = playerHitPoints;
      long localBossHitPoints   = bossHitPoints;
      long playerDamage         = (items?.Sum(i => i?.damage ?? 0) ?? 0);
      long playerArmor          = (items?.Sum(i => i?.armor  ?? 0) ?? 0);

      while (localPlayerHitPoints > 0 && localBossHitPoints > 0)
      {
        localBossHitPoints -= (playerDamage - bossArmor);
        if (localBossHitPoints > 0)
        {
          localPlayerHitPoints -= (bossDamage - playerArmor);
        }
      }
      if ((localPlayerHitPoints > 0) == playerWins)
      {
        minOrMaxTotalCost = minOrMax(minOrMaxTotalCost, items!.Sum(i => i?.cost ?? 0));
      }
    }
    return minOrMaxTotalCost;
  }

  static long part1(long playerHitPoints, long bossHitPoints, long bossDamage, long bossArmor)
  {
    return getCost(playerHitPoints, bossHitPoints, bossDamage, bossArmor, true);
  }

  static long part2(long playerHitPoints, long bossHitPoints, long bossDamage, long bossArmor)
  {
    return getCost(playerHitPoints, bossHitPoints, bossDamage, bossArmor, false);
  }

  static void Main(string[] args)
  {
    IDictionary<string, long> values = File.ReadAllLines("2015/dec21.txt").Select(line => line.Split(':')).ToDictionary(tokens => tokens[0].Trim(),
                                                                                                                        tokens => long.Parse(tokens[1].Trim()));

    Console.WriteLine(part1(100, values["Hit Points"], values["Damage"], values["Armor"]));
    Console.WriteLine(part2(100, values["Hit Points"], values["Damage"], values["Armor"]));
  }
}
