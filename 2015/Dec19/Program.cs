﻿using System.Text.RegularExpressions;

long part1(IList<(string toReplace, string replacement)> replacements, string molecule)
{
  ISet<string> adjustedMolecules = new HashSet<string>();
  foreach ((string toReplace, string replacement) in replacements)
  {
    string[] surroundings = Regex.Split(molecule, toReplace);
    for (int i = 0; i < surroundings.Length - 1; i++)
    {
      adjustedMolecules.Add(string.Join(toReplace, surroundings.Take(i + 1)) + replacement + string.Join(toReplace, surroundings.Skip(i + 1)));
    }
  }

  return adjustedMolecules.Count;
}

long part2(IList<(string toReplace, string replacement)> replacements, string molecule)
{
  long count = 0;
  while (molecule != "e")
  {
    foreach ((string toReplace, string replacement) in replacements)
    {
      int pos = molecule.IndexOf(replacement);
      if (pos >= 0)
      {
        molecule = molecule.Substring(0, pos) + toReplace + molecule.Substring(pos + replacement.Length);
        count++;
      }
    }
  }
  return count;
}

IList<string>           lines        = File.ReadAllLines("2015/dec19.txt");
IList<(string, string)> replacements = lines.Select(str => Regex.Match(str, @"(\w+)\s+=>\s+(\w+)")).Where(m => m.Success).Select(m => (m.Groups[1].Value, m.Groups[2].Value)).ToList();

Console.WriteLine(part1(replacements, lines.Last()));
Console.WriteLine(part2(replacements, lines.Last()));
