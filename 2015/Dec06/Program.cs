﻿
using System.Text.RegularExpressions;

long part1(IList<(string, int, int, int, int)> instructions)
{
  IDictionary<string, Func<bool, bool>> INSTR_TO_FN = new Dictionary<string, Func<bool, bool>>()
  {
    { "turn on",  (v => true) },
    { "turn off", (v => false) },
    { "toggle",   (v => !v) },
  };

  bool[,] lights = new bool[1000,1000];
  foreach ((string instr, int x1, int y1, int x2, int y2) in instructions)
  {
    for (int x = x1; x <= x2; x++)
    {
      for (int y = y1; y <= y2; y++)
      {
        lights[y, x] = INSTR_TO_FN[instr](lights[y, x]);
      }
    }
  }

  return lights.Cast<bool>().Count(v => v);
}

long part2(IList<(string, int, int, int, int)> instructions)
{
  IDictionary<string, Func<int, int>> INSTR_TO_FN = new Dictionary<string, Func<int, int>>()
  {
    { "turn on",  (v => v + 1) },
    { "turn off", (v => v - Convert.ToInt32(v > 0)) },
    { "toggle",   (v => v + 2) },
  };

  int[,] lights = new int[1000,1000];
  foreach ((string instr, int x1, int y1, int x2, int y2) in instructions)
  {
    for (int x = x1; x <= x2; x++)
    {
      for (int y = y1; y <= y2; y++)
      {
        lights[y, x] = INSTR_TO_FN[instr](lights[y, x]);
      }
    }
  }

  return lights.Cast<int>().Sum(v => v);
}

IList<(string, int, int, int, int)> instructions = File.ReadAllLines("2015/dec06.txt").Select(str => Regex.Match(str, @"([A-Za-z ]+)\s+(\d+),(\d+)\s+through\s+(\d+),(\d+)"))
                                                                                      .Select(m   => (m.Groups[1].Value, int.Parse(m.Groups[2].Value), int.Parse(m.Groups[3].Value), int.Parse(m.Groups[4].Value), int.Parse(m.Groups[5].Value))).ToList();

Console.WriteLine(part1(instructions));
Console.WriteLine(part2(instructions));
