﻿using System.Text.RegularExpressions;

ushort calculateValue(IList<string> instructions, string input)
{
  IDictionary<string, Func<ushort, ushort, ushort>> INSTR_TO_FN = new Dictionary<string, Func<ushort, ushort, ushort>>()
  {
    { "AND",    ((l, r) => (ushort)(l & r)) },
    { "OR",     ((l, r) => (ushort)(l | r)) },
    { "LSHIFT", ((l, r) => (ushort)(l << r)) },
    { "RSHIFT", ((l, r) => (ushort)(l >> r)) },
  };

  IDictionary<string, ushort>                                         values           = new Dictionary<string, ushort>();
  IDictionary<string, (string, Func<ushort, ushort>)>                 one_dependency   = new Dictionary<string, (string, Func<ushort, ushort>)>();
  IDictionary<string, (string, string, Func<ushort, ushort, ushort>)> two_dependencies = new Dictionary<string, (string, string, Func<ushort, ushort, ushort>)>();

  ushort getValue(string v)
  {
    if (!values.ContainsKey(v))
    {
      if (one_dependency.ContainsKey(v))
      {
        (string other, Func<ushort, ushort> fn) = one_dependency[v];
        values[v] = fn(getValue(other));
      }
      else if (two_dependencies.ContainsKey(v))
      {
        (string l, string r, Func<ushort, ushort, ushort> fn) = two_dependencies[v];
        values[v] = fn(getValue(l), getValue(r));
      }
    }
    return values[v];
  }

  foreach (string instruction in instructions)
  {
    Match m;
    if ((m = Regex.Match(instruction, @"^(NOT)?\s*(\w+)\s*->\s*([A-Za-z]+)$")).Success)
    {
      ushort v;
      if (ushort.TryParse(m.Groups[2].Value, out v))
      {
        values[m.Groups[3].Value] = v;
      }
      else
      {
        one_dependency[m.Groups[3].Value] = (m.Groups[2].Value, (m.Groups[1].Success ? (v => (ushort)(~v)) : (v => v)));
      }
    }
    else if ((m = Regex.Match(instruction, @"^(\w+)\s*(\w+)\s*(\w+)\s*->\s*([A-Za-z]+)$")).Success)
    {
      ushort?[] v = new ushort?[2];
      foreach (int i in Enumerable.Range(0, 2))
      {
        ushort vv;
        if (ushort.TryParse(m.Groups[1 + 2 * i].Value, out vv))
        {
          v[i] = vv;
        }
      }
      var fn = INSTR_TO_FN[m.Groups[2].Value];
      if (v.All(vv => vv.HasValue))
      {
        values[m.Groups[4].Value] = fn(v[0]!.Value, v[1]!.Value);
      }
      else if (v[0].HasValue)
      {
        one_dependency[m.Groups[4].Value] = (m.Groups[3].Value, ((vv) => fn(v[0]!.Value, vv)));
      }
      else if (v[1].HasValue)
      {
        one_dependency[m.Groups[4].Value] = (m.Groups[1].Value, ((vv) => fn(vv, v[1]!.Value)));
      }
      else
      {
        two_dependencies[m.Groups[4].Value] = (m.Groups[1].Value, m.Groups[3].Value, fn);
      }
    }
  }
  return getValue(input);
}

long part1(IList<string> instructions)
{
  return calculateValue(instructions, "a");
}

long part2(IList<string> instructions)
{
  instructions.Add(String.Format("{0} -> b", calculateValue(instructions, "a")));
  return calculateValue(instructions, "a");
}

IList<string> instructions = File.ReadAllLines("2015/dec07.txt").ToList();

Console.WriteLine(part1(instructions));
Console.WriteLine(part2(instructions));
