﻿long getMinFirstGroupQE(IList<long> weights, long numGroups)
{
  IList<long[]> getPossibleFirstSets(IList<long> weightsSorted, long targetWeight)
  {
    IList<long>   workingSet   = new List<long>();
    IList<long[]> possibleSets = new List<long[]>();
    void inner(int currentIndex, long workingWeightSum)
    {
      for (int i = currentIndex; i < weightsSorted.Count; i++)
      {
        workingSet.Add(weights[i]);
        workingWeightSum += weights[i];
        if (workingWeightSum >= targetWeight)
        {
          if (workingWeightSum == targetWeight)
          {
            possibleSets.Add(workingSet.ToArray());
          }
          workingSet.RemoveAt(workingSet.Count - 1);
          return;
        }
        else
        {
          inner(i + 1, workingWeightSum);
          workingWeightSum -= weights[i];
          workingSet.RemoveAt(workingSet.Count - 1);
        }
      }
    }
    inner(0, 0);
    return possibleSets;
  }

  return getPossibleFirstSets(weights.OrderBy(v => v).ToList(),
                              weights.Sum() / numGroups).GroupBy(set => set.Length)
                                                        .Aggregate((working, group) => working.Key < group.Key ? working : group)
                                                        .Min(set => set.Aggregate((working, curr) => working * curr));
}

long part1(IList<long> weights)
{
  return getMinFirstGroupQE(weights, 3);
}

long part2(IList<long> weights)
{
  return getMinFirstGroupQE(weights, 4);
}

IList<long> weights = File.ReadAllLines("2015/dec24.txt").Select(line => long.Parse(line)).ToList();

Console.WriteLine(part1(weights));
Console.WriteLine(part2(weights));
