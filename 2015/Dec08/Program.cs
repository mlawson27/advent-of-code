﻿using System.Text.RegularExpressions;

long part1(IList<string> strings)
{
  return strings.Sum(str => Regex.Matches(str, @"""").Count +
                            Regex.Matches(str, @"\\\\").Count +
                            Regex.Matches(str, @"\\x[A-Fa-f0-9]{2}").Count * 3);
}

long part2(IList<string> strings)
{
  return strings.Sum(str => Regex.Matches(str, @"""").Count +
                            Regex.Matches(str, @"(?:^"")|(?:""$)").Count +
                            Regex.Matches(str, @"\\").Count);
}

IList<string> strings = File.ReadAllLines("2015/dec08.txt").ToList();

Console.WriteLine(part1(strings));
Console.WriteLine(part2(strings));
