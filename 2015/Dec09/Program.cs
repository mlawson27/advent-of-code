﻿using System.Text.RegularExpressions;

long findTotalDistance(IList<(string[] dests, long distance)> sortedDistances)
{
  IDictionary<string, int> endpointUsedCount = new Dictionary<string, int>();
  long                     totalDistance     = 0;

  while (sortedDistances.Count > 1)
  {
    (string[] dests, long distance) = sortedDistances[0];
    sortedDistances.RemoveAt(0);

    totalDistance += distance;
    foreach (string dest in dests)
    {
      if (!endpointUsedCount.ContainsKey(dest))
      {
        endpointUsedCount[dest] = 0;
      }
      endpointUsedCount[dest]++;
      if (endpointUsedCount[dest] > 1)
      {
        for (int i = sortedDistances.Count - 1; i >= 0; i--)
        {
          if (sortedDistances[i].dests.Contains(dest))
          {
            sortedDistances.RemoveAt(i);
          }
        }
      }
    }
  }

  return totalDistance;
}

long part1(IList<(string[] dests, long distance)> distances)
{
  return findTotalDistance(distances.OrderBy(set => set.distance).ToList());
}

long part2(IList<(string[] dests, long distance)> distances)
{
  return findTotalDistance(distances.OrderByDescending(set => set.distance).ToList());
}

IList<(string[], long)> distances = File.ReadAllLines("2015/dec09.txt").Select(str => Regex.Match(str, @"(\w+)\s+to\s+(\w+)\s*=\s*(\d+)")).Select(m => (new []{ m.Groups[1].Value, m.Groups[2].Value }, long.Parse(m.Groups[3].Value))).ToList();

Console.WriteLine(part1(distances));
Console.WriteLine(part2(distances));
