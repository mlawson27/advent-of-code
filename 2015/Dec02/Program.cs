﻿long part1(IList<long[]> dimensions)
{
  return dimensions.Sum(set => 3 * (set[0] * set[1]) + 2 * ((set[0] * set[2]) + (set[1] * set[2])));
}

long part2(IList<long[]> dimensions)
{
  return dimensions.Sum(set => 2 * (set[0] + set[1]) + set.Aggregate((running, v) => running * v));
}

IList<long[]> dimensions = File.ReadAllLines("2015/dec02.txt").Select(str => str.Split('x')).Select(tokens => tokens.Select(long.Parse).OrderBy(v => v).ToArray()).ToList();

Console.WriteLine(part1(dimensions));
Console.WriteLine(part2(dimensions));
