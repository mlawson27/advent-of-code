﻿long getLowestHouseNumber(long value, long presentsPerHouse, long? maxHousesPerElf)
{
  long getFactorSum(long value, IList<long> factors, long? maxFactorCount)
  {
    void inner(IList<long> factors, int startingIndex, ISet<long> used, long starting)
    {
      for (int i = startingIndex; i < factors.Count; i++)
      {
        used.Add(starting * factors[i]);
        inner(factors, i + 1, used, starting * factors[i]);
      }
    }

    ISet<long> used = new HashSet<long>();
    inner(factors, 0, used, 1);
    return used.Sum(v => v * Convert.ToInt64(!maxFactorCount.HasValue || (value / v <= 50)));
  }

  for (long houseNumber = 1; ; houseNumber++)
  {
    long        houseNumberTemp = houseNumber;
    IList<long> factors         = new List<long>();
    foreach (long valueToTest in new long[]{ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43 })
    {
      while (houseNumberTemp % valueToTest == 0)
      {
        factors.Add(valueToTest);
        houseNumberTemp = houseNumberTemp / valueToTest;
      }
    }
    long valueSum = ((getFactorSum(houseNumber, factors, maxHousesPerElf) +
                      Convert.ToInt64(!maxHousesPerElf.HasValue || (houseNumber <= maxHousesPerElf.Value!))) * presentsPerHouse);
    if (valueSum >= value)
    {
      return houseNumber;
    }
  }
}

long part1(long value)
{
  return getLowestHouseNumber(value, 10, null);
}

long part2(long value)
{
  return getLowestHouseNumber(value, 11, 50);
}

long value = long.Parse(File.ReadAllText("2015/dec20.txt"));

Console.WriteLine(part1(value));
Console.WriteLine(part2(value));
