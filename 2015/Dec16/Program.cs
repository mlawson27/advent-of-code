﻿using System.Text.RegularExpressions;

IDictionary<string, long> RECEIPT = new Dictionary<string, long>()
{
  { "children",    3 },
  { "cats",        7 },
  { "samoyeds",    2 },
  { "pomeranians", 3 },
  { "akitas",      0 },
  { "vizslas",     0 },
  { "goldfish",    5 },
  { "trees",       3 },
  { "cars",        2 },
  { "perfumes",    1 }
};

long part1(IList<IDictionary<string, long>> presents)
{
  return presents.Select((presents, i) => (presents, i))
                 .Where(p => p.presents.All(pp => RECEIPT.ContainsKey(pp.Key) && RECEIPT[pp.Key] == pp.Value))
                 .Select(p => p.i)
                 .First() + 1;
}

long part2(IList<IDictionary<string, long>> presents)
{
  Value ValueOrDefault<Key, Value>(IDictionary<Key, Value> dict, Key key, Value default_)
  {
    return dict.ContainsKey(key) ? dict[key] : default_;
  }

  IDictionary<string, Func<long, long, bool>> OPERATORS = new Dictionary<string, Func<long, long, bool>>()
  {
    { "cats",        ((expected, actual) => actual > expected) },
    { "trees",       ((expected, actual) => actual > expected) },
    { "pomeranians", ((expected, actual) => actual < expected) },
    { "goldfish",    ((expected, actual) => actual < expected) },
  };

  return presents.Select((presents, i) => (presents, i))
                 .Where(p => p.presents.All(pp => RECEIPT.ContainsKey(pp.Key) && ValueOrDefault(OPERATORS, pp.Key, (expected, actual) => expected == actual)(RECEIPT[pp.Key], pp.Value)))
                 .Select(p => p.i)
                 .First() + 1;
}

IList<IDictionary<string, long>> presents = File.ReadAllLines("2015/dec16.txt").Select(str => Regex.Matches(str, @"\s+(\w+):\s+(\d+),?"))
                                                                               .Select(ms => (IDictionary<string, long>)ms.ToDictionary(m => m.Groups[1].Value,
                                                                                                                                        m => long.Parse(m.Groups[2].Value))).ToList();

Console.WriteLine(part1(presents));
Console.WriteLine(part2(presents));
