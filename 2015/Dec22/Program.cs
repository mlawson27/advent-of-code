﻿static class Dec21
{
  struct Spell
  {
    public long   manaCost;
    public long   damage;
    public long   healAmount;
    public long   armorIncrease;
    public long   manaIncrease;
    public long   duration;
  }

  struct GameData
  {
    public long     playerHitPoints;
    public long     playerMana;
    public long     bossHitPoints;
    public Spell?[] activeSpells;
    public bool     playerTurn;
    public long     usedMana;

    public GameData(GameData other)
    {
      playerHitPoints = other.playerHitPoints;
      playerMana      = other.playerMana;
      bossHitPoints   = other.bossHitPoints;
      activeSpells    = other.activeSpells.ToArray();
      playerTurn      = other.playerTurn;
      usedMana        = other.usedMana;
    }
  }

  static readonly IList<Spell> SPELLS = new Spell[]
  {
    new Spell() { manaCost =  53, damage = 4, healAmount = 0, armorIncrease = 0, manaIncrease =   0, duration = 1 },
    new Spell() { manaCost =  73, damage = 2, healAmount = 2, armorIncrease = 0, manaIncrease =   0, duration = 1 },
    new Spell() { manaCost = 113, damage = 0, healAmount = 0, armorIncrease = 7, manaIncrease =   0, duration = 6 },
    new Spell() { manaCost = 173, damage = 3, healAmount = 0, armorIncrease = 0, manaIncrease =   0, duration = 6 },
    new Spell() { manaCost = 229, damage = 0, healAmount = 0, armorIncrease = 0, manaIncrease = 101, duration = 5 },
  };

  static long getMinMana(long playerHitPoints, long mana, long bossHitPoints, long bossDamage, long preTurnPlayerHitPointAdjustment)
  {
    void applySpell(ref GameData gameData, Spell activeSpell, ref long armorIncrease)
    {
      gameData.bossHitPoints   -= activeSpell.damage;
      gameData.playerHitPoints += activeSpell.healAmount;
      gameData.playerMana      += activeSpell.manaIncrease;
      armorIncrease            += activeSpell.armorIncrease;
    }

    IList<GameData> allGameData = new List<GameData>()
    {
      new GameData()
      {
        playerHitPoints = playerHitPoints,
        playerMana      = mana,
        bossHitPoints   = bossHitPoints,
        activeSpells    = SPELLS.Select(s => (Spell?)null).ToArray(),
        playerTurn      = true,
        usedMana        = 0
      }
    };

    while (allGameData.Any())
    {
      GameData gameData = allGameData.First();
      allGameData.RemoveAt(0);

      gameData.playerHitPoints += preTurnPlayerHitPointAdjustment;
      if (gameData.playerHitPoints <= 0)
      {
        continue;
      }

      long armorIncrease = 0;
      for (int spellIndex = 0; spellIndex < gameData.activeSpells.Length; spellIndex++)
      {
        if (gameData.activeSpells[spellIndex].HasValue)
        {
          Spell activeSpell = gameData.activeSpells[spellIndex]!.Value;
          applySpell(ref gameData, activeSpell, ref armorIncrease);
          activeSpell.duration--;
          gameData.activeSpells[spellIndex] = ((activeSpell.duration > 0) ? activeSpell : null);
        }
      }

      if (gameData.bossHitPoints <= 0)
      {
        return gameData.usedMana;
      }
      else if (!gameData.playerTurn)
      {
        gameData.playerHitPoints -= (bossDamage - armorIncrease);
        if (gameData.playerHitPoints > 0)
        {
          gameData.playerTurn = !gameData.playerTurn;
          allGameData.Add(gameData);
        }
      }
      else
      {
        gameData.playerTurn = !gameData.playerTurn;
        for (int spellIndex = 0; spellIndex < SPELLS.Count; spellIndex++)
        {
          Spell spell = SPELLS[spellIndex];
          if (gameData.activeSpells[spellIndex].HasValue || (gameData.playerMana < spell.manaCost))
          {
            continue;
          }

          GameData newGameData = new GameData(gameData);
          newGameData.activeSpells[spellIndex] = spell;
          newGameData.playerMana -= spell.manaCost;
          newGameData.usedMana   += spell.manaCost;
          allGameData.Add(newGameData);
        }
      }
    }

    return long.MaxValue;
  }

  static long part1(long playerHitPoints, long mana, long bossHitPoints, long bossDamage)
  {
    return getMinMana(playerHitPoints, mana, bossHitPoints, bossDamage, 0);
  }

  static long part2(long playerHitPoints, long mana, long bossHitPoints, long bossDamage)
  {
    return getMinMana(playerHitPoints, mana, bossHitPoints, bossDamage, -1);
  }

  static void Main(string[] args)
  {
    IDictionary<string, long> values = File.ReadAllLines("2015/dec22.txt").Select(line => line.Split(':')).ToDictionary(tokens => tokens[0].Trim(),
                                                                                                                        tokens => long.Parse(tokens[1].Trim()));

    Console.WriteLine(part1(50, 500, values["Hit Points"], values["Damage"]));
    Console.WriteLine(part2(50, 500, values["Hit Points"], values["Damage"]));
  }
}
