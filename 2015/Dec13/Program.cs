﻿using System.Text.RegularExpressions;

long calculateHappiness(IList<(string[] names, long happiness)> happinesses)
{
  IDictionary<string, IDictionary<string, long>> nameOtherNameHappiness = happinesses.SelectMany(p => p.names).Distinct().ToDictionary(name => name,
                                                                                                                                       name => (IDictionary<string, long>)new Dictionary<string, long>());

  foreach ((string[] names, long happiness) in happinesses)
  {
    nameOtherNameHappiness[names[0]][names[1]] = happiness;
  }

  IList<string[]> permutations;
  {
    void inner(IEnumerable<string> remaining, IList<string> used, IList<string[]> permutations)
    {
      foreach (string name in remaining)
      {
        used.Add(name);
        ISet<string> left = remaining.ToHashSet();
        left.Remove(name);
        if (left.Any())
        {
          inner(left, used, permutations);
        }
        else
        {
          permutations.Add(used.ToArray());
        }
        used.RemoveAt(used.Count - 1);
      }
    }

    IList<string>   used         = new List<string>();
                    permutations = new List<string[]>();
    inner(nameOtherNameHappiness.Keys, used, permutations);
  }

  long maxHappiness = 0;
  foreach (string[] permutation in permutations)
  {
    long happiness = 0;
    for (int i = 0; i < permutation.Length; i++)
    {
      int j = (i + 1) % permutation.Length;
      happiness += nameOtherNameHappiness[permutation[i]][permutation[j]];
      happiness += nameOtherNameHappiness[permutation[j]][permutation[i]];
    }
    maxHappiness = Math.Max(maxHappiness, happiness);
  }
  return maxHappiness;
}

long part1(IList<(string[] names, long happiness)> happinesses)
{
  return calculateHappiness(happinesses);
}

long part2(IList<(string[] names, long happiness)> happinesses)
{
  foreach (string name in happinesses.SelectMany(p => p.names).Distinct().ToArray())
  {
    happinesses.Add((new[] { "me", name }, 0));
    happinesses.Add((new[] { name, "me" }, 0));
  }
  return calculateHappiness(happinesses);
}

IList<(string[], long)> happinesses = File.ReadAllLines("2015/dec13.txt").Select(str => Regex.Match(str, @"(\w+)\s+would\s+(\w+)\s+(\d+)\s+happiness\s+units\s+by\s+sitting\s+next\s+to\s+(\w+)."))
                                                                         .Select(m => (new []{ m.Groups[1].Value, m.Groups[4].Value },
                                                                                       (Convert.ToInt32(m.Groups[2].Value == "gain") - Convert.ToInt32(m.Groups[2].Value == "lose")) * long.Parse(m.Groups[3].Value))).ToList();

Console.WriteLine(part1(happinesses));
Console.WriteLine(part2(happinesses));
