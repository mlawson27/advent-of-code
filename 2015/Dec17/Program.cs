﻿long part1(IList<long> containerSizes)
{
  long getCombinations(IList<long> containerSizesSortedWorking, long remaining, int starting)
  {
    long result = 0;
    for (int i = starting; remaining >= 0 && i < containerSizesSortedWorking.Count; i++)
    {
      result += getCombinations(containerSizesSortedWorking, remaining - containerSizesSortedWorking[i], i + 1);
    }
    return result + Convert.ToInt64(remaining == 0);
  }

  IList<long> containerSizesSorted = containerSizes.OrderBy(v => v).ToList();

  return getCombinations(containerSizesSorted, 150, 0);
}

long part2(IList<long> containerSizes)
{
  IDictionary<long, long> getCombinations(IList<long> containerSizesSortedWorking, long remaining, int starting, int containerCount)
  {
    IDictionary<long, long> containerCountToWaysCount = new Dictionary<long, long>();
    for (int i = starting; remaining >= 0 && i < containerSizesSortedWorking.Count; i++)
    {
      foreach ((long containerCountKey, long waysCount) in getCombinations(containerSizesSortedWorking, remaining - containerSizesSortedWorking[i], i + 1, containerCount + 1))
      {
        if (!containerCountToWaysCount.ContainsKey(containerCountKey))
        {
          containerCountToWaysCount[containerCountKey] = 0;
        }
        containerCountToWaysCount[containerCountKey] += waysCount;
      }
    }
    if (remaining == 0)
    {
      if (!containerCountToWaysCount.ContainsKey(containerCount))
      {
        containerCountToWaysCount[containerCount] = 0;
      }
      containerCountToWaysCount[containerCount]++;
    }
    return containerCountToWaysCount;
  }

  IList<long> containerSizesSorted = containerSizes.OrderBy(v => v).ToList();

  return getCombinations(containerSizesSorted, 150, 0, 1).OrderBy(pair => pair.Key).First().Value;
}

IList<long> containerSizes = File.ReadAllLines("2015/dec17.txt").Select(str =>long.Parse(str)).ToList();

Console.WriteLine(part1(containerSizes));
Console.WriteLine(part2(containerSizes));
