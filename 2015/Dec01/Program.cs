﻿long part1(string content)
{
  return content.Sum(c => Convert.ToInt64(c == '(') - Convert.ToInt64(c == ')'));
}

long part2(string content)
{
  long sum = 0;
  foreach ((char c, int index) in content.Select((c, index) => (c, index)))
  {
    sum += (Convert.ToInt64(c == '(') - Convert.ToInt64(c == ')'));
    if (sum < 0)
    {
      return index + 1;
    }
  }
  return -1;
}

string content = File.ReadAllText("2015/dec01.txt");

Console.WriteLine(part1(content));
Console.WriteLine(part2(content));
