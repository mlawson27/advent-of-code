﻿using System.Text.RegularExpressions;

long getSum(IDictionary<string, IDictionary<string, long>> ingredients, Func<IDictionary<string, long>, bool> criteria)
{
  long inner(IList<string> remaining, IDictionary<string, int> countsPerIngredient, int countRemaining)
  {
    if (remaining.Count == 1)
    {
      countsPerIngredient[remaining[0]] = countRemaining;

      IDictionary<string, long> propertiesSums = new Dictionary<string, long>();
      foreach (string ingredient in countsPerIngredient.Keys)
      {
        foreach ((string property, long value) in ingredients[ingredient])
        {
          if (!propertiesSums.ContainsKey(property))
          {
            propertiesSums[property] = 0;
          }
          propertiesSums[property] += (countsPerIngredient[ingredient] * value);
        }
      }

      countsPerIngredient.Remove(remaining[0]);

      return (criteria(propertiesSums) ? propertiesSums.Aggregate((long)1, (last, pair) => ((pair.Key == "calories") ? 1 : Math.Max(pair.Value, 0)) * last) : long.MinValue);
    }
    else
    {
      long maxResult = 0;
      for (int count = 0; count < countRemaining; count++)
      {
        string name = remaining[0];
        remaining.RemoveAt(0);
        countsPerIngredient[name] = count;
        maxResult = Math.Max(maxResult, inner(remaining, countsPerIngredient, countRemaining - count));
        remaining.Insert(0, name);
        countsPerIngredient.Remove(remaining[0]);
      }
      return maxResult;
    }
  }

  IDictionary<string, int> countsPerIngredient = new Dictionary<string, int>();
  return inner(ingredients.Keys.ToList(), countsPerIngredient, 100);
}

long part1(IDictionary<string, IDictionary<string, long>> ingredients)
{
  return getSum(ingredients, (prop => true));
}

long part2(IDictionary<string, IDictionary<string, long>> ingredients)
{
  return getSum(ingredients, (prop => prop["calories"] == 500));
}

IDictionary<string, IDictionary<string, long>> ingredients = File.ReadAllLines("2015/dec15.txt").Select(str => (Regex.Match(str, @"(\w+):"), Regex.Matches(str, @"(\w+)\s+(-?\d+)")))
                                                                                                .ToDictionary(p => p.Item1.Value,
                                                                                                              p => (IDictionary<string, long>)p.Item2.ToDictionary(m => m.Groups[1].Value,
                                                                                                                                                                   m => long.Parse(m.Groups[2].Value)));

Console.WriteLine(part1(ingredients));
Console.WriteLine(part2(ingredients));
