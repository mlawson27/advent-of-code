﻿using System.Security.Cryptography;

long part1(string content)
{
  for (long v = 0; ; v++)
  {
    string test = content + v;
    byte[] md5  = MD5.Create().ComputeHash(System.Text.Encoding.ASCII.GetBytes(test));
    if (md5.Take(3).Select((v, index) => (v, index)).All(pair => pair.index < 2 && pair.v == 0 || pair.index == 2 && pair.v < 0x10))
    {
      return v;
    }
  }
}

long part2(string content)
{
  for (long v = 0; ; v++)
  {
    string test = content + v;
    byte[] md5  = MD5.Create().ComputeHash(System.Text.Encoding.ASCII.GetBytes(test));
    if (md5.Take(3).All(v => v == 0))
    {
      return v;
    }
  }
}

string content = File.ReadAllText("2015/dec04.txt");

Console.WriteLine(part1(content));
Console.WriteLine(part2(content));
