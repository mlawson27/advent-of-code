﻿long getLineLength(string line, int numIterations)
{
  IList<(long count, char c)> counts = new List<(long count, char c)>();
  for (int iter = 0; iter < numIterations; iter++)
  {
    counts.Clear();
    char lastChar = line[0];
    long count    = 1;
    for (int i = 1; i < line.Length; i++)
    {
      if (line[i] == lastChar)
      {
        count++;
      }
      else
      {
        counts.Add((count, lastChar));
        lastChar  = line[i];
        count     = 1;
      }
    }
    counts.Add((count, lastChar));

    System.Text.StringBuilder b = new System.Text.StringBuilder((int)counts.Sum(p => Math.Ceiling(Math.Log10(p.count))));
    foreach ((long subCount, char subC) in counts)
    {
      b.Append(subCount).Append(subC);
    }
    line = b.ToString();
  }

  return line.Length;
}

long part1(string line)
{
  return getLineLength(line, 40);
}

long part2(string line)
{
  return getLineLength(line, 50);
}

string line = File.ReadAllText("2015/dec10.txt");

Console.WriteLine(part1(line));
Console.WriteLine(part2(line));
