﻿using System.Text.Json;

long getAllNumberCount(JsonDocument doc, string? propertyToSkip)
{
  string getPropertyString(JsonProperty prop)
  {
    if (prop.Value.ValueKind == JsonValueKind.String)
    {
      return prop.Value.GetString()!;
    }
    else
    {
      return String.Empty;
    }
  }

  long getValueForElement(JsonElement e)
  {
    switch (e.ValueKind)
    {
      case JsonValueKind.Array:
      {
        return e.EnumerateArray().Sum(subE => getValueForElement(subE));
      }
      case JsonValueKind.Object:
      {
        if (propertyToSkip != null && e.EnumerateObject().Any(property => getPropertyString(property).ToLower() == propertyToSkip!))
        {
          return 0;
        }
        else
        {
          return e.EnumerateObject().Sum(property => getValueForElement(property.Value));
        }
      }
      case JsonValueKind.Number:
      {
        return e.GetInt64();
      }
      default:
      {
        return 0;
      }
    }
  }
  return getValueForElement(doc.RootElement);
}

long part1(JsonDocument doc)
{
  return getAllNumberCount(doc, null);
}

long part2(JsonDocument doc)
{
  return getAllNumberCount(doc, "red");
}

JsonDocument doc;
using (FileStream stream = File.OpenRead("2015/dec12.txt"))
{
  doc = JsonDocument.Parse(stream);
}

Console.WriteLine(part1(doc));
Console.WriteLine(part2(doc));
