﻿long findOnLightCount(IList<string> board, bool cornersAlwaysOn)
{
  string EMPTY_LINE = new string('.', board[0].Length + 2);

  IList<(int iAdj, int jAdj)> ADJUSTS = Enumerable.Range(0, 9).Where(v => v != 4).Select(v => (v / 3 - 1, v % 3 - 1)).ToArray();
  ISet<(int, int)>            CORNERS = new HashSet<(int, int)>() { (1, 1), (1, board[0].Length), (board.Count, 1), (board.Count, board[0].Length) };

  IList<string> adjustedBoard = board.Select(str => "." + str + ".").ToList();
  adjustedBoard.Insert(0, EMPTY_LINE);
  adjustedBoard.Add(EMPTY_LINE);

  if (cornersAlwaysOn)
  {
    Func<string, string> setEdges = (str => ".#" + str.Substring(2, str.Length - 4) + "#.");

    foreach (int index in new []{ 1, adjustedBoard.Count - 2 })
    {
      adjustedBoard[index] = setEdges(adjustedBoard[index]);
    }
  }

  for (int iter = 0; iter < 100; iter++)
  {
    IList<string> newBoard = new List<string>();
    newBoard.Add(EMPTY_LINE);
    for (int i = 1; i < adjustedBoard.Count - 1; i++)
    {
      System.Text.StringBuilder lineBuilder = new System.Text.StringBuilder(adjustedBoard[i].Length);
      lineBuilder.Append('.');
      for (int j = 1; j < adjustedBoard[i].Length - 1; j++)
      {
        int onNeighbors = ADJUSTS.Count(p => adjustedBoard[i + p.iAdj][j + p.jAdj] == '#');
        if (cornersAlwaysOn && CORNERS.Contains((i, j)))
        {
          lineBuilder.Append('#');
        }
        else if (adjustedBoard[i][j] == '#')
        {
          lineBuilder.Append((onNeighbors == 2) || (onNeighbors == 3) ? '#' : '.');
        }
        else
        {
          lineBuilder.Append((onNeighbors == 3) ? '#' : '.');
        }
      }
      lineBuilder.Append('.');
      newBoard.Add(lineBuilder.ToString());
    }
    newBoard.Add(EMPTY_LINE);
    adjustedBoard = newBoard;
  }

  return adjustedBoard.Sum(line => line.Count(c => c == '#'));
}

long part1(IList<string> board)
{
  return findOnLightCount(board, false);
}

long part2(IList<string> board)
{
  return findOnLightCount(board, true);
}

IList<string> board = File.ReadAllLines("2015/dec18.txt");

Console.WriteLine(part1(board));
Console.WriteLine(part2(board));
