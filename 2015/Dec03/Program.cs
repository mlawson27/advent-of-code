﻿void updateCoordinates(string content, IDictionary<(long, long), long> coordinatesToPresentCount)
{
  long ns = 0;
  long ew = 0;
  foreach (char c in content)
  {
    switch (c)
    {
      case '^':
      case 'v':
      {
        ns += Convert.ToInt64(c == 'v') - Convert.ToInt64(c == '^');
        break;
      }
      case '<':
      case '>':
      {
        ew += Convert.ToInt64(c == '>') - Convert.ToInt64(c == '<');
        break;
      }
    }
    coordinatesToPresentCount[(ns, ew)] = (coordinatesToPresentCount.ContainsKey((ns, ew)) ? coordinatesToPresentCount[(ns, ew)] : 1);
  }
}

long part1(string content)
{
  IDictionary<(long, long), long> coordinatesToPresentCount = new Dictionary<(long, long), long>()
  {
    { (0, 0), 1 }
  };
  updateCoordinates(content, coordinatesToPresentCount);
  return coordinatesToPresentCount.Count;
}

long part2(string content)
{
  IDictionary<(long, long), long> coordinatesToPresentCount = new Dictionary<(long, long), long>()
  {
    { (0, 0), 1 }
  };
  foreach (int pos in Enumerable.Range(0, 2))
  {
    updateCoordinates(new String(content.Select((c, index) => (c, index)).Where((c, index) => index % 2 == pos).Select((pair, index) => pair.c).ToArray()), coordinatesToPresentCount);
  }
  return coordinatesToPresentCount.Count;
}

string content = File.ReadAllText("2015/dec03.txt");

Console.WriteLine(part1(content));
Console.WriteLine(part2(content));
