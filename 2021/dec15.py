from typing import List

def shortest_distance(board: List[List[int]]):
  distances_from_start = [[0 if x == 0 and y == 0 else None for x in range(len(board[y]))] for y in range(len(board))]
  possible_positions   = {(0, 1) : board[1][0], (1, 0) : board[0][1]}

  while distances_from_start[-1][-1] is None:
    position_tuple                     = min(possible_positions.items(), key=lambda tup: tup[1])
    ((min_x, min_y), min_distance)     = position_tuple
    distances_from_start[min_y][min_x] = min_distance
    possible_positions.pop((min_x, min_y))
    for (new_x, new_y) in [(min_x - 1, min_y), (min_x + 1, min_y), (min_x, min_y - 1), (min_x, min_y + 1)]:
      if new_x >= 0 and new_x < len(board[min_y]) and new_y >= 0 and new_y < len(board) and distances_from_start[new_y][new_x] is None:
        new_distance = distances_from_start[min_y][min_x] + board[new_y][new_x]
        possible_positions[(new_x, new_y)] = min(possible_positions.get((new_x, new_y), new_distance), new_distance)
  return distances_from_start[-1][-1]

def part1(board: List[List[int]]):
  return shortest_distance(board)

def part2(board: List[List[int]]):
  new_board = board
  for adjustment in range(1, 5):
    new_board = [new_board[y] + [(v + adjustment) % 10 + ((v + adjustment) >= 10) for v in board[y]] for y in range(len(board))]
  board_after_x_adjustments = new_board
  for adjustment in range(1, 5):
    new_board = new_board + [[(v + adjustment) % 10 + ((v + adjustment) >= 10) for v in line] for line in board_after_x_adjustments]
  return shortest_distance(new_board)

board: List[List[int]]
with open('2021/dec15.txt') as input_file:
  board = [[int(c) for c in line.rstrip()] for line in input_file]

print(part1(board))
print(part2(board))
