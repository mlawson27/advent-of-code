import functools
from typing import List, Optional, Tuple

CLOSING_CHAR = { '(' : ')',
                 '[' : ']',
                 '{' : '}',
                 '<' : '>' }

def test_line(line: str) -> Tuple[Optional[str], list[str]]:
  stack = []
  for c in line:
    if c in '([{<':
      stack.append(c)
    else:
      if c != CLOSING_CHAR[stack.pop()]:
        return (c, stack)
  return (None, stack)

def part1(lines: List[str]):
  SCORE_FOR_CHAR = { ')' : 3,
                     ']' : 57,
                     '}' : 1197,
                     '>' : 25137 }

  return sum(SCORE_FOR_CHAR[invalid_char] for (invalid_char, _) in (test_line(line) for line in lines) if invalid_char)

def part2(lines: List[str]):
  SCORE_FOR_CHAR = { ')' : 1,
                     ']' : 2,
                     '}' : 3,
                     '>' : 4 }

  local_scores = list(functools.reduce(lambda running, c: (running * 5) + SCORE_FOR_CHAR[CLOSING_CHAR[c]], reversed(stack), 0) for (invalid_char, stack) in (test_line(line) for line in lines) if not invalid_char)
  local_scores.sort()
  return local_scores[len(local_scores) // 2]

lines: List[str]
with open('2021/dec10.txt') as input_file:
  lines = [line.rstrip() for line in input_file]

print(part1(lines))
print(part2(lines))
