import functools
from typing import Callable, Dict, List, Tuple

def part1(direction_amounts : List[Tuple[str, int]]) -> int:
  translations : Dict[str, Callable[[Tuple[int,int],int],Tuple[int,int]]] = {
    'forward' : lambda depth, horiz, amount: (depth, horiz + amount),
    'down'    : lambda depth, horiz, amount: (depth + amount, horiz),
    'up'      : lambda depth, horiz, amount: (depth - amount, horiz),
  }
  return (lambda depth, horiz: depth * horiz)(*functools.reduce(lambda depth_horiz, direction_amount: translations[direction_amount[0]](*depth_horiz, direction_amount[1]), direction_amounts, (0,0)))

def part2(direction_amounts : List[Tuple[str, int]]) -> int:
  translations : Dict[str, Callable[[Tuple[int,int,int],int],Tuple[int,int,int]]] = {
    'forward' : lambda aim, depth, horiz, amount: (aim, depth + (aim * amount), horiz + amount),
    'down'    : lambda aim, depth, horiz, amount: (aim + amount, depth, horiz),
    'up'      : lambda aim, depth, horiz, amount: (aim - amount, depth, horiz),
  }
  return (lambda _, depth, horiz: depth * horiz)(*functools.reduce(lambda aim_depth_horiz, direction_amount: translations[direction_amount[0]](*aim_depth_horiz, direction_amount[1]), direction_amounts, (0,0,0)))

direction_amounts = []
with open('2021/dec02.txt') as input_file:
  direction_amounts = [(lambda tokens: (tokens[0], int(tokens[1])))(line.split()) for line in input_file]
print(part1(direction_amounts))
print(part2(direction_amounts))
