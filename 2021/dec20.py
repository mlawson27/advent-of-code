from typing import List

def get_on_pixel_count(key: str, input_image: List[str], count: int):
  def get_int(image_ints: List[List[int]], ii: int, jj: int, default: int):
    if ii >= 0 and ii < len(image_ints):
      if jj >= 0 and jj < len(image_ints[ii]):
        return image_ints[ii][jj]
    return default

  default          = 0
  input_image_ints = [[int(c == '#') for c in line] for line in input_image]
  for _ in range(count):
    new_image_ints = [([None] * (len(input_image_ints[0]) + 2)) for i in range(len(input_image_ints) + 2)]
    for i in range(-1, len(input_image_ints) + 1):
      vals = [default * 3] * 3
      for j in range(-1, len(input_image_ints[0]) + 1):
        vals                         = [(vals[i_adj + 1] << 1 | get_int(input_image_ints, i + i_adj, j + 1, default)) & ((1 << 3) - 1) for i_adj in range(-1, 2)]
        new_image_ints[i + 1][j + 1] = int(key[(vals[0] << 6 | vals[1] << 3 | vals[2])] == '#')
    default          = int(key[511 * default] == '#')
    input_image_ints = new_image_ints
  return sum(sum(v for v in line) for line in input_image_ints)

def part1(key: str, input_image: List[str]):
  return get_on_pixel_count(key, input_image, 2)

def part2(key: str, input_image: List[str]):
  return get_on_pixel_count(key, input_image, 50)

key: str
input_image: List[str]
with open('2021/dec20.txt') as input_file:
  key = input_file.readline().rstrip()
  input_file.readline()
  input_image = [line.rstrip() for line in input_file]

print(part1(key, input_image))
print(part2(key, input_image))
