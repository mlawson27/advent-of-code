import re
from typing import Dict, Tuple

def part1(player_starting_positions: Tuple[int, int]):
  count            = 0
  player_positions = list(player_starting_positions)
  scores           = [0] * len(player_starting_positions)
  current_player   = 0
  while all(score < 1000 for score in scores):
    i                                 = (((count + 3) * (count + 4)) // 2) - (((count * (count + 1))) // 2)
    count                            += 3
    player_positions[current_player] += i
    if player_positions[current_player] % 10 == 0:
      player_positions[current_player] = 10
    else:
      player_positions[current_player] = (player_positions[current_player] % 10)
    scores[current_player] += player_positions[current_player]
    current_player          = (current_player + 1) % len(player_positions)
  return min(scores) * count

def part2(player_starting_positions: Tuple[int, int]):
  def inner(scores: Tuple[Tuple[int, int], Tuple[int, int], int], cached_results: Dict[Tuple[Tuple[int, int], Tuple[int, int]], Tuple[int, int]]):
    POSSIBLE_OUTCOMES = { 3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1 }

    result = tuple([0] * (len(scores) - 1))
    for (dice_reading, num_outcomes) in POSSIBLE_OUTCOMES.items():
      position, score = scores[scores[-1]]
      new_position    = position + dice_reading
      if new_position % 10 == 0:
        new_position = 10
      else:
        new_position = (new_position % 10)
      new_score         = score + new_position
      new_entry: Tuple
      if scores[-1] == 0:
        new_entry = ((new_position, new_score), scores[1], 1)
      else:
        new_entry = (scores[0], (new_position, new_score), 0)
      if new_score >= 21:
        result = tuple(elem + (num_outcomes * (i == scores[-1])) for i, elem in enumerate(result))
      elif new_entry in cached_results:
        result = tuple(r1 + r2 * num_outcomes for (r1, r2) in zip(result, cached_results[new_entry]))
      else:
        result = tuple(r1 + r2 * num_outcomes for (r1, r2) in zip(result, inner(new_entry, cached_results)))
    cached_results[scores] = result
    return result

  starting       = tuple((pos, 0) for pos in player_starting_positions) + tuple([0])
  cached_results = {}

  return max(inner(starting, cached_results))

player_starting_positions: Tuple[int]
with open('2021/dec21.txt') as input_file:
  player_starting_positions = tuple(int(re.match(r"Player\s+\d+\s+starting\s+position:\s+(\d+)", line).group(1)) for line in input_file)

print(part1(player_starting_positions))
print(part2(player_starting_positions))
