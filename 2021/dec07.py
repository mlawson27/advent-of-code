from typing import List

def part1(positions: List[int]) -> int:
  positions = list(sorted(positions))
  return sum((abs(val - positions[len(positions) // 2]) for val in positions))

def part2(positions: List[int]) -> int:
  value_sum        = sum(positions)
  min_avg          = (value_sum // len(positions))
  possible_centers = range(min_avg, min_avg + 1 + bool(value_sum % len(positions)))
  return min((sum((sum(range(abs(val - center) + 1)) for val in positions)) for center in possible_centers))

positions: List[int]
with open('2021/dec07.txt') as input_file:
  positions = list(map(int, input_file.readline().split(',')))

print(part1(positions))
print(part2(positions))
