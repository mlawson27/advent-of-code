import functools
import itertools
import operator
from typing import Dict, List, Optional

def part1(heat_maps: List[str]):
  low_positions : List[int] = []
  for i, heat_map in enumerate(heat_maps):
    for j, c in enumerate(heat_map):
      if (i == 0 or heat_maps[i - 1][j] > c) and (i == len(heat_maps) - 1 or heat_maps[i + 1][j] > c) and \
         (j == 0 or heat_map [j - 1]    > c) and (j == len(heat_map)  - 1 or heat_map [j + 1]    > c):
         low_positions.append(int(c))

  return sum(low_position + 1 for low_position in low_positions)

def part2(heat_maps: List[str]):
  basin_to_count : Dict[int, int]            = {}
  working_basins : List[List[Optional[int]]] = [[None] * len(heat_map) for heat_map in heat_maps]
  next_basin     : int                       = 1

  def handle_pos(i : int, j : int):
    for ii, jj in itertools.chain(itertools.product(range(i - 1, i + 2, 2), [j]), itertools.product([i], range(j - 1, j + 2, 2))):
      if ii >= 0 and ii < len(heat_maps) and jj >= 0 and jj < len(heat_maps[i]) and heat_maps[ii][jj] != '9' and not working_basins[ii][jj]:
        working_basins[ii][jj] = working_basins[i][j]
        basin_to_count[working_basins[ii][jj]] += 1
        handle_pos(ii, jj)

  for i in range(len(heat_maps)):
    for j in range(len(heat_maps[i])):
      if heat_maps[i][j] != '9' and not working_basins[i][j]:
        working_basins[i][j]        = next_basin
        basin_to_count[next_basin]  = 1
        next_basin                 += 1
        handle_pos(i, j)

  return functools.reduce(operator.mul, list(sorted(basin_to_count.values()))[-3:])

heat_maps: List[str]
with open('2021/dec09.txt') as input_file:
  heat_maps = [line.rstrip() for line in input_file]

print(part1(heat_maps))
print(part2(heat_maps))
