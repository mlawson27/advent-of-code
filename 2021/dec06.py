from typing import List

def simulate_lanternfish(ages: List[int], num_days : int) -> int:
  number_per_age = [sum(age == value for age in ages) for value in range(9)]

  for _ in range(num_days):
    num_0 = number_per_age[0]
    number_per_age = number_per_age[1:7] + [number_per_age[7] + num_0] + number_per_age[8:] + [num_0]

  return sum(number_per_age)

def part1(ages: List[int]) -> int:
  return simulate_lanternfish(ages, 80)

def part2(ages: List[int]) -> int:
  return simulate_lanternfish(ages, 256)

ages: List[int]
with open('2021/dec06.txt') as input_file:
  ages = list(map(int, input_file.readline().split(',')))

print(part1(ages))
print(part2(ages))
