import math
import re
from typing import Callable, Tuple

def __inner_loop(range: Tuple[Tuple[int, int], Tuple[int, int]], pos: Tuple[int, int], vel: Tuple[int, int], working_vel: Tuple[int, int],
                 working_result: int, on_within_zone: Callable[[int, int], int]) -> int:
  (x_pos,         y_pos)         = pos
  (working_x_vel, working_y_vel) = working_vel
  while (x_pos <= range[0][1]) and (y_pos >= range[1][0]):
    x_pos += working_x_vel
    y_pos += working_y_vel
    if (x_pos >= x_range[0]) and (x_pos <= x_range[1]) and (y_pos >= y_range[0]) and (y_pos <= y_range[1]):
      return on_within_zone(vel[1], working_result)
    working_x_vel -= ((working_x_vel > 0) - (working_x_vel < 0))
    working_y_vel -= 1
  return working_result

def part1(x_range: Tuple[int, int], y_range: Tuple[int, int]):
  sequence_sum = lambda last_val, first_val = 0: ((last_val * (last_val + 1)) - (first_val * (first_val + 1))) // 2

  max_y = 0
  for x_vel in range(int(math.sqrt(x_range[0] * 2)), int(math.sqrt(x_range[1] * 2)) + 1):
    for y_vel in range(1, x_vel ** 2):
      working_x_vel  = max(x_vel - y_vel, 0)
      max_y = __inner_loop((x_range, y_range), (sequence_sum(x_vel, working_x_vel), sequence_sum(y_vel)), (x_vel, y_vel), (working_x_vel, 0), max_y,
                           lambda y_vel, max_y: max(max_y, sequence_sum(y_vel)))
  return max_y

def part2(x_range: Tuple[int, int], y_range: Tuple[int, int]):
  count = 0
  for x_vel in range(int(math.sqrt(x_range[0] * 2)), x_range[1] + 1):
    for y_vel in range(y_range[0], x_range[1] - x_vel):
      count = __inner_loop((x_range, y_range), (0, 0), (x_vel, y_vel), (x_vel, y_vel), count,
                           lambda _, count: count + 1)
  return count

with open('2021/dec17.txt') as input_file:
  (x_range, y_range) = (lambda m: ((int(m.group(1)), int(m.group(2))), (int(m.group(3)), int(m.group(4)))))(re.match(r'target\s+area:\s+x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)', input_file.read()))

print(part1(x_range, y_range))
print(part2(x_range, y_range))
