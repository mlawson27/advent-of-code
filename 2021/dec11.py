import itertools
from typing import List, Set, Tuple

def get_adjusted_board(board: List[str]) -> List[str]:
  ADJUSTMENTS = list(itertools.product(range(-1, 2), range(-1, 2)))
  ADJUSTMENTS.remove((0, 0))

  def flash(new_board_ints: List[List[int]], i: int, j: int, flashed_indices : Set[Tuple[int, int]]):
    for (iAdj, jAdj) in ADJUSTMENTS:
      (newI, newJ) = (i + iAdj, j + jAdj)
      if (newI >= 0 and newI < len(new_board_ints)) and \
         (newJ >= 0 and newJ < len(new_board_ints[newI])):
        new_board_ints[newI][newJ] += 1
        if new_board_ints[newI][newJ] >= 10 and not (newI, newJ) in flashed_indices:
          flashed_indices.add((newI, newJ))
          flash(new_board_ints, newI, newJ, flashed_indices)

  new_board_ints  = [[int(c) + 1 for c in line] for line in board]
  flashed_indices : Set[Tuple[int, int]] = set()
  for i in range(len(new_board_ints)):
    for j in range(len(new_board_ints[i])):
      if new_board_ints[i][j] >= 10 and not (i, j) in flashed_indices:
        flashed_indices.add((i, j))
        flash(new_board_ints, i, j, flashed_indices)

  return ["".join((str(v) if v < 10 else '0' for v in line)) for line in new_board_ints]

def part1(board: List[str]):
  flash_count = 0
  for _ in range(100):
    board = get_adjusted_board(board)
    flash_count += sum(sum(c == '0' for c in line) for line in board)
  return flash_count

def part2(board: List[str]):
  index = 1
  while True:
    board = get_adjusted_board(board)
    if all(all(c == '0' for c in line) for line in board):
      return index
    index += 1

board: List[str]
with open('2021/dec11.txt') as input_file:
  board = [line.rstrip() for line in input_file]

print(part1(board))
print(part2(board))
