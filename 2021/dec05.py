import itertools
import re
from typing import Dict, List, Tuple

def get_number_of_overlapping_points(line_coordinates: List[Tuple[Tuple[int, int], Tuple[int, int]]], allow_diagonals : bool) -> int:
  compare   = lambda value1, value2: (value1 > value2) - (value1 < value2)
  get_pairs = lambda range1, range2: zip(range1, range2) if len(range1) == len(range2) else itertools.product(range1, range2)

  board: Dict[Tuple[int, int], int] = {}
  for ((x1, y1), (x2, y2)) in line_coordinates:
    if not allow_diagonals and (x1 != x2) and (y1 != y2):
      continue
    (x_comp, y_comp) = (compare(x2, x1) or 1, (compare(y2, y1) or 1))
    for (x, y) in get_pairs(range(x1, x2 + x_comp, x_comp), range(y1, y2 + y_comp, y_comp)):
      board[(x, y)] = board[(x, y)] + 1 if (x, y) in board else 1

  return sum(value > 1 for value in board.values())

def part1(line_coordinates: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> int:
  return get_number_of_overlapping_points(line_coordinates, False)

def part2(line_coordinates: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> int:
  return get_number_of_overlapping_points(line_coordinates, True)

line_coordinates: List[Tuple[Tuple[int, int], Tuple[int, int]]]
with open('2021/dec05.txt') as input_file:
  line_coordinates = list(map(lambda match: ((int(match.group(1)), int(match.group(2))), (int(match.group(3)), int(match.group(4)))), (re.match(r'(\d+),(\d+)\s*->\s*(\d+),(\d+)', line) for line in input_file)))

print(part1(line_coordinates))
print(part2(line_coordinates))
