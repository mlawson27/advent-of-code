from typing import Any, Callable, Iterable, List, Tuple

def copy_pairs(pairs: List[Any]) -> List[Any]:
  result = []
  for e in pairs:
    if isinstance(e, int):
      result.append(e)
    else:
      result.append(copy_pairs(e))
  return result

def try_explode(parent_and_indices: List[Tuple[List[Any], int]]) -> bool:
  def update_left_or_right(value_to_add: int, parent_and_indices: List[Tuple[List[Any], int]], get_range: Callable[[int, int], Iterable], increment: bool, check_parents: bool) -> bool:
    (parent, index) = parent_and_indices[-1]
    for subindex in get_range(index, len(parent)):
      if isinstance(parent[subindex], int):
        parent[subindex] += value_to_add
        return True
      elif update_left_or_right(value_to_add, parent_and_indices + [(parent[subindex], -1 if increment else len(parent[subindex]))], get_range, increment, False):
        return True
    if len(parent_and_indices) > 1 and check_parents:
      return update_left_or_right(value_to_add, parent_and_indices[0:-1], get_range, increment, True)
    return False

  (parent, index) = parent_and_indices[-1]
  if all((isinstance(elem, int) for elem in parent[index])):
    ret = (len(parent_and_indices) >= 4)
    if ret:
      update_left_or_right(parent[index][0],  parent_and_indices, lambda i, _: range(i - 1, -1, -1),              False, True)
      update_left_or_right(parent[index][-1], parent_and_indices, lambda i, parent_len: range(i + 1, parent_len), True,  True)
      parent[index] = 0
    return ret
  else:
    return any(try_explode(parent_and_indices + [(parent[index], subindex)]) for subindex in range(len(parent[index])) if not isinstance(parent[index][subindex], int))

def try_split(pair: List[Any]) -> bool:
  for index in range(len(pair)):
    ret = False
    if isinstance(pair[index], int):
      ret = pair[index] >= 10
      if ret:
        pair[index] = [pair[index] // 2, (pair[index] + 1) // 2]
    else:
      ret = try_split(pair[index])
    if ret:
      return True
  return False

def get_magnitiude(value: Any) -> int:
  if isinstance(value, int):
    return value
  else:
    return 3 * get_magnitiude(value[0]) + 2 * get_magnitiude(value[1])

def part1(pairs: List[List[Any]]):
  pairs = copy_pairs(pairs)
  while(len(pairs) > 1):
    pair   = [pairs.pop(0), pairs.pop(0)]
    result = True
    while result:
      result = any(try_explode([(pair, i)]) for i in range(len(pair))) or try_split(pair)
    pairs.insert(0, pair)
  return get_magnitiude(pairs[0])

def part2(pairs: List[List[Any]]):
  max_magnitude = 0
  for i in range(len(pairs)):
    for j in range(i + 1, len(pairs)):
      for pair in [copy_pairs([pairs[i], pairs[j]]), copy_pairs([pairs[j], pairs[i]])]:
        result = True
        while result:
          result = any(try_explode([(pair, i)]) for i in range(len(pair))) or try_split(pair)
        max_magnitude = max(max_magnitude, get_magnitiude(pair))
  return max_magnitude

pairs: List[List[Any]] = []
with open('2021/dec18.txt') as input_file:
  def parse_line(line: str) -> Tuple[int, List[Any]]:
    vals : List[Any] = []
    i                = 0
    while i < len(line):
      if line[i].isdigit():
        vals.append(int(line[i]))
      elif line[i] == '[':
        (inner_i, val) = parse_line(line[i + 1:])
        vals.append(val)
        i += inner_i
      elif line[i] == ']':
        return (i + 1, vals)
      i += 1

  for line in input_file:
    pairs.append(parse_line(line.rstrip()[1:])[1])

print(part1(pairs))
print(part2(pairs))
