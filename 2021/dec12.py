from typing import Dict, List, Optional, Set

def helper(routes: Dict[str, List[str]], start: str, used_lower: Set[str], second_lower: Optional[str]) -> int:
  used_lower_local = used_lower.copy()
  num_routes       = 0
  if start.islower():
    used_lower_local.add(start)
  for next in routes[start]:
    if next == 'end':
      num_routes += 1
    elif not next.islower():
      num_routes += helper(routes, next, used_lower_local, second_lower)
    elif not next in used_lower_local or (second_lower is None and next != 'start'):
      num_routes += helper(routes, next, used_lower_local, next if next in used_lower_local else second_lower)
  return num_routes

def part1(routes: Dict[str, List[str]]):
  return helper(routes, 'start', set(), '')

def part2(routes: Dict[str, List[str]]):
  return helper(routes, 'start', set(), None)

routes: Dict[str, List[str]] = {}
with open('2021/dec12.txt') as input_file:
  for (a, b) in (line.rstrip().split('-') for line in input_file):
    for (x, y) in [(a, b), (b, a)]:
      if not x in routes:
        routes[x] = []
      routes[x].append(y)

print(part1(routes))
print(part2(routes))
