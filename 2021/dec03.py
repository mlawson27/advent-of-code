from typing import List
import math

def part1(values : List[int], num_bits : int) -> int:
  most_common = sum((sum(bool(value & (1 << bit)) for value in values) >= (len(values) / 2)) << bit for bit in range(num_bits))
  return most_common * (~most_common & ((1 << num_bits) - 1))

def part2(values : List[int], num_bits : int) -> int:
  def find_matching(use_most_common : bool) -> int:
    matching = values
    for bit in reversed(range(num_bits)):
      low_high = [[ value for value in matching if ((value >> bit) & 1) == test ] for test in range(2)]
      matching = low_high[use_most_common if len(low_high[0]) == len(low_high[1]) else (len(low_high[0]) < len(low_high[1])) == use_most_common]
      if len(matching) == 1:
        return matching[0]
  return find_matching(True) * find_matching(False)

num_bits : int
values   : List[int]
with open('2021/dec03.txt') as input_file:
  values   = [int(line, 2) for line in input_file]
  num_bits = max(math.ceil(math.log2(val)) for val in values)
print(part1(values, num_bits))
print(part2(values, num_bits))
