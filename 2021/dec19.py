import itertools
import re
from typing import Dict, List, Tuple, Set

def get_sensor_positions_and_translations(beacon_reports: List[Set[Tuple[int, int, int]]]):
  sensor_positions_and_translations = [((0, 0, 0), (lambda x: x)) if i == 0 else None for i in range(len(beacon_reports))]

  def rotate_x(coordinates: Tuple[int, int, int], count: int) -> Tuple[int, int, int]:
    working_coordinates = coordinates
    for _ in range(count):
      working_coordinates = (working_coordinates[0], -working_coordinates[2], working_coordinates[1])
    return working_coordinates
  def rotate_y(coordinates: Tuple[int, int, int], count: int) -> Tuple[int, int, int]:
    working_coordinates = coordinates
    for _ in range(count):
      working_coordinates = (working_coordinates[2], working_coordinates[1], -working_coordinates[0])
    return working_coordinates
  def rotate_z(coordinates: Tuple[int, int, int], count: int) -> Tuple[int, int, int]:
    working_coordinates = coordinates
    for _ in range(count):
      working_coordinates = (-working_coordinates[1], working_coordinates[0], working_coordinates[2])
    return working_coordinates

  distance_dicts = []
  for beacon_report in beacon_reports:
    distance_dict = {}
    for (x1, y1, z1), (x2, y2, z2) in itertools.combinations(beacon_report, 2):
      distance = ((x2 - x1) ** 2) + ((y2 - y1) ** 2) + ((z2 - z1) ** 2)
      assert(distance not in distance_dict)
      distance_dict[distance] = ((x1, y1, z1), (x2, y2, z2))
    distance_dicts.append(distance_dict)

  while not all(sensor_positions_and_translations):
    for i in range(len(beacon_reports)):
      if not sensor_positions_and_translations[i]:
        continue
      for j in (v for v in range(len(beacon_reports)) if i != v and not sensor_positions_and_translations[v]):
        common_distances = set(distance_dicts[i].keys()).intersection(distance_dicts[j].keys())
        if len(common_distances) >= (12 * 11) // 2:
          i_points_to_distance: Dict[Tuple[int, int, int], Set[int]]                    = {}
          j_points_to_distance: Dict[Tuple[int, int, int], Set[int]]                    = {}
          equivalent_points:    List[Tuple[Tuple[int, int, int], Tuple[int, int, int]]] = []
          for key in common_distances:
            for (points_to_distance, ps) in [(i_points_to_distance, distance_dicts[i][key]), (j_points_to_distance, distance_dicts[j][key])]:
              for p in ps:
                if not p in points_to_distance:
                  points_to_distance[p] = set()
                points_to_distance[p].add(key)
          while any(i_points_to_distance):
            for i_point in i_points_to_distance:
              found = False
              for j_point in j_points_to_distance:
                found = (i_points_to_distance[i_point] == j_points_to_distance[j_point])
                if found:
                  break
              if found:
                break
            if found:
              equivalent_points.append((i_point, j_point))
              j_points_to_distance.pop(j_point)
            i_points_to_distance.pop(i_point)

          for make_top in [lambda pair: pair,
                           lambda pair: rotate_x(pair, 1),
                           lambda pair: rotate_x(pair, 2),
                           lambda pair: rotate_x(pair, 3),
                           lambda pair: rotate_y(pair, 1),
                           lambda pair: rotate_y(pair, 3)]:
            same_count: int
            for rotate_z_count in range(4):
              equivalent_point_iter      = iter(equivalent_points)
              (x1, y1, z1), (x2, y2, z2) = next(equivalent_point_iter)
              (x2, y2, z2)               = rotate_z(make_top((x2, y2, z2)), rotate_z_count)
              (x_dist, y_dist, z_dist)   = ((x1 - x2), (y1 - y2), (z1 - z2))
              same_count                 = 0
              for (x1, y1, z1), (x2, y2, z2) in equivalent_point_iter:
                (x2, y2, z2) = tuple(val1 + val2 for (val1, val2) in zip(rotate_z(make_top((x2, y2, z2)), rotate_z_count), (x_dist, y_dist, z_dist)))
                same_count += (x2, y2, z2) == (x1, y1, z1)
              if same_count > len(equivalent_points) // 2:
                (ref_sensor_pos, ref_translation)    = sensor_positions_and_translations[i]
                translation                          = lambda p, i=i, make_top=make_top, rotate_z_count=rotate_z_count: sensor_positions_and_translations[i][1](rotate_z(make_top(p), rotate_z_count))
                sensor_positions_and_translations[j] = (tuple(new_dist + ref for (new_dist, ref) in zip(ref_translation((x_dist, y_dist, z_dist)), ref_sensor_pos)), translation)
                break
            if same_count > len(equivalent_points) // 2:
              break
  return sensor_positions_and_translations

def part1(beacon_reports: List[Set[Tuple[int, int, int]]]):
  sensor_positions_and_translations     = get_sensor_positions_and_translations(beacon_reports)
  all_points: Set[Tuple[int, int, int]] = set()
  for (report, (sensor_position, translation)) in zip(beacon_reports, sensor_positions_and_translations):
    for point in report:
      all_points.add(tuple(val1 + val2 for (val1, val2) in zip(translation(point), sensor_position)))
  return len(all_points)

def part2(beacon_reports: List[Set[Tuple[int, int, int]]]):
  sensor_positions_and_translations = get_sensor_positions_and_translations(beacon_reports)
  return max(sum(abs(v1 - v2) for v1, v2 in zip(sensor_positions_and_translations[i][0], sensor_positions_and_translations[j][0])) for (i, j) in itertools.combinations(range(len(sensor_positions_and_translations)), 2))

beacon_reports: List[Set[Tuple[int, int, int]]] = []
with open('2021/dec19.txt') as input_file:
  current_beacon_reports: Set[Tuple[int, int, int]] = None
  for line in input_file:
    line = line.rstrip()
    if re.match(r'---\s+scanner\s+\d+\s+---', line):
      if current_beacon_reports:
        beacon_reports.append(current_beacon_reports)
      current_beacon_reports = set()
    elif line:
      current_beacon_reports.add(tuple(int(token) for token in line.split(',')))
  beacon_reports.append(current_beacon_reports)

print(part1(beacon_reports))
print(part2(beacon_reports))
