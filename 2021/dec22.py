import functools
import itertools
import operator
import re
from typing import Callable, List, Tuple

def get_num_cubes_on(cuboid_positions: List[Tuple[bool, Tuple[int, int], Tuple[int, int], Tuple[int, int]]],
                     range_filter:     Callable[[Tuple[int, int], Tuple[int, int], Tuple[int, int]], bool]):
  ranges_and_on_off: List[Tuple[bool, Tuple[int, int], Tuple[int, int], Tuple[int, int]]] = []
  for (new_on, new_x_range, new_y_range, new_z_range) in cuboid_positions:
    if range_filter(new_x_range, new_y_range, new_z_range):
      new_active_ranges = ranges_and_on_off.copy()
      if new_on:
        new_active_ranges.append((new_on, new_x_range, new_y_range, new_z_range))
      for (on, x_range, y_range, z_range) in ranges_and_on_off:
        intersect_x = (max(new_x_range[0], x_range[0]), min(new_x_range[1], x_range[1]))
        intersect_y = (max(new_y_range[0], y_range[0]), min(new_y_range[1], y_range[1]))
        intersect_z = (max(new_z_range[0], z_range[0]), min(new_z_range[1], z_range[1]))
        if (intersect_x[0] <= intersect_x[1]) and (intersect_y[0] <= intersect_y[1]) and (intersect_z[0] <= intersect_z[1]):
          new_active_ranges.append((not on, intersect_x, intersect_y, intersect_z))
    ranges_and_on_off = new_active_ranges
  return sum((functools.reduce(operator.mul, (range[1] - range[0] + 1 for range in ranges[1:])) * (1 if ranges[0] else -1) for ranges in ranges_and_on_off))

def part1(cuboid_positions: List[Tuple[bool, Tuple[int, int], Tuple[int, int], Tuple[int, int]]]):
  return get_num_cubes_on(cuboid_positions, lambda x_range, y_range, z_range: all(min_v >= -50 and max_v <= 50 for (min_v, max_v) in [x_range, y_range, z_range]))

def part2(cuboid_positions: List[Tuple[bool, Tuple[int, int], Tuple[int, int], Tuple[int, int]]]):
  return get_num_cubes_on(cuboid_positions, lambda x_range, y_range, z_range: True)

cuboid_positions: List[Tuple[bool, Tuple[int, int], Tuple[int, int], Tuple[int, int]]]
with open('2021/dec22.txt') as input_file:
  cuboid_positions = list(map(lambda tokens: tuple(itertools.chain([tokens[0] == 'on'], (tuple(int(v) for v in re.match(r'\w=(-?\d+)..(-?\d+)', subtoken).groups()) for subtoken in tokens[1].split(',')))), (line.rstrip().split(' ') for line in input_file)))

print(part1(cuboid_positions))
print(part2(cuboid_positions))
