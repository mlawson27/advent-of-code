import operator
from typing import Callable, Iterable, List, Tuple

def find_value(instructions: List[Tuple[str, List[str]]], list_modifier_fn: Callable[[Iterable[int]], Iterable[int]]):
  INSTRUCTION_FNS = {
    'add': operator.add,
    'mul': operator.mul,
    'div': operator.ifloordiv,
    'mod': operator.imod,
    'eql': operator.eq,
  }

  working_stack: List[Tuple[int, int]]      = []
  rules:         List[Tuple[int, int, int]] = []

  current_input_index  = -1
  skip_next_stack_push = False
  next_y_add_is_push   = False
  for (instr_str, tokens) in instructions:
    if instr_str == 'inp':
      current_input_index  += 1
      skip_next_stack_push  = False
    elif instr_str == 'add':
      if tokens[0] == 'x' and tokens[1][0] == '-':
        (base_index, offset) = working_stack.pop()
        rules.append((base_index, offset + int(tokens[1]), current_input_index))
        skip_next_stack_push = True
      if tokens[0] == 'y':
        if next_y_add_is_push and not skip_next_stack_push:
          working_stack.append((current_input_index, int(tokens[1])))
        next_y_add_is_push = tokens[1] == 'w'

  result_arr = [0] * (len(rules) * 2)
  for (base_index, offset, other_index) in rules:
    for v in list_modifier_fn(range(1, 10)):
      possible_other_v = v + offset
      if possible_other_v >= 1 and possible_other_v <= 9:
        result_arr[base_index]  = v
        result_arr[other_index] = possible_other_v
        break

  value_arr_iter = iter(result_arr)
  variables      = { 'w': 0, 'x': 0, 'y': 0, 'z': 0 }
  for (instr_str, tokens) in instructions:
    if instr_str == 'inp':
      variables[tokens[0]] = next(value_arr_iter)
    else:
      variables[tokens[0]] = int(INSTRUCTION_FNS[instr_str](*[variables[token] if token in variables else int(token) for token in tokens]))
  assert(variables['z'] == 0)
  return sum(v * (10 ** i) for (i, v) in enumerate(reversed(result_arr)))

def part1(instructions: List[Tuple[str, List[str]]]):
  return find_value(instructions, reversed)

def part2(instructions: List[Tuple[str, List[str]]]):
  return find_value(instructions, lambda x: x)

instructions: List[Tuple[str, List[str]]]
with open('2021/dec24.txt') as input_file:
  instructions = [(line[0:3], line[4:].rstrip().split(' ')) for line in input_file]

print(part1(instructions))
print(part2(instructions))
