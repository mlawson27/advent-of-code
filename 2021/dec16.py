import functools
import operator
from typing import Callable, Iterable, List, Tuple

def parse_packet(bin_str: str) -> Tuple[int, int, int]:
  FUNCTIONS: List[Callable[[Iterable[int]], int]] = [
    sum,
    lambda values: functools.reduce(operator.mul, values, 1),
    min,
    max,
    None,
    lambda values: int(operator.gt(*values)),
    lambda values: int(operator.lt(*values)),
    lambda values: int(operator.eq(*values)),
  ]

  version = int(bin_str[0:3], 2)
  typ     = int(bin_str[3:6], 2)
  if typ == 4:
    value      = 0
    range_iter = iter(range(6, len(bin_str), 5))
    for i in range_iter:
      value = ((value << 4) | int(bin_str[i+1:i+5], 2))
      if bin_str[i] == '0':
        break
    return (next(range_iter), version, value)
  else:
    length_len   = (11 if bin_str[6] == '1' else 15)
    length_val   = int(bin_str[7:7+length_len], 2)
    packet_start = 0
    packet_count = 0
    version_sum  = version
    inner_values = []
    while [packet_start, packet_count][length_len == 11] < length_val:
      (inner_packet_len, inner_version, value) = parse_packet(bin_str[7 + length_len + packet_start:])
      packet_start += inner_packet_len
      version_sum  += inner_version
      packet_count += 1
      inner_values.append(value)
    return (7 + length_len + packet_start, version_sum, FUNCTIONS[typ](inner_values))

def part1(hex_str: str):
  return parse_packet(''.join('{:04b}'.format(int(c, 16)) for c in hex_str))[1]

def part2(hex_str: str):
  return parse_packet(''.join('{:04b}'.format(int(c, 16)) for c in hex_str))[2]

hex_str : str
with open('2021/dec16.txt') as input_file:
  hex_str = input_file.read()

print(part1(hex_str))
print(part2(hex_str))
