import concurrent.futures
from typing import List

state: List[List[int]]
with open('2021/dec25.txt') as input_file:
  state = [[((c == 'v') << 1) + (c == '>') for c in line.rstrip()] for line in input_file]

interation_count = 0
with concurrent.futures.ThreadPoolExecutor() as executor:
  new_state: List[List[int]]
  def run_horizontal(i: int):
    any_change = False
    j          = 0
    while j < len(state[i]):
      if state[i][j] == 1 and state[i][(j + 1) % len(state[i])] == 0:
        new_state[i][j]                        = 0
        new_state[i][(j + 1) % len(state[i])]  = 1
        j                                     += 1
        any_change                             = True
      j += 1
    return any_change
  def run_vertical(j: int):
    any_change = False
    i          = 0
    while i < len(state):
      if state[i][j] == 2 and ((state[(i + 1) % len(state)][j] == 0 and state[(i + 1) % len(state)][(j - 1) % len(state[i])] != 1) or \
                               (state[(i + 1) % len(state)][j] == 1 and state[(i + 1) % len(state)][(j + 1) % len(state[i])] == 0)):
        new_state[i][j]                         = 0
        new_state[(i + 1) % len(new_state)][j]  = 2
        i                                      += 1
        any_change                              = True
      i += 1
    return any_change

  while True:
    new_state         = [line.copy() for line in state]
    interation_count += 1
    if (sum(executor.map(run_horizontal, range(len(state)))) + sum(executor.map(run_vertical, range(len(state[0]))))) == 0:
      break
    state = new_state
print(interation_count)
