import re
from typing import List, Set, Tuple

def perform_fold(dot_positions: Set[Tuple[int, int]], fold_axis: str, fold_line: int):
  new_dot_positions: Set[Tuple[int, int]] = set()
  for (x, y) in dot_positions:
    if fold_axis == 'x':
      new_dot_positions.add((2 * fold_line - x if x > fold_line else x, y))
    else:
      new_dot_positions.add((x, 2 * fold_line - y if y > fold_line else y))
  return new_dot_positions

def part1(dot_positions: Set[Tuple[int, int]], folds: List[Tuple[str, int]]):
  return len(perform_fold(dot_positions, *(folds[0])))

def part2(dot_positions: Set[Tuple[int, int]], folds: List[Tuple[str, int]]):
  for (fold_axis, fold_line) in folds:
    dot_positions = perform_fold(dot_positions, fold_axis, fold_line)
  (max_x, max_y) = (max(dot_positions, key=lambda pair: pair[axis])[axis] for axis in range(2))
  return '\n'.join(''.join('#' if (x, y) in dot_positions else '.' for x in range(max_x + 1)) for y in range(max_y + 1))

dot_positions: Set[Tuple[int, int]]  = set()
folds:         List[Tuple[str, int]] = []
with open('2021/dec13.txt') as input_file:
  line_iter = iter(input_file)
  for line in (line.rstrip() for line in line_iter):
    if not line:
      break
    tokens = line.split(',')
    dot_positions.add((int(tokens[0]), int(tokens[1])))
  for line in (line.rstrip() for line in line_iter):
    match = re.match(r'fold\s+along\s+([xy])=(\d+)', line)
    folds.append((match.group(1), int(match.group(2))))

print(part1(dot_positions, folds))
print(part2(dot_positions, folds))
