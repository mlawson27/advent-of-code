from typing import List, Optional, Tuple

def get_winning_number_and_board(numbers: List[int], boards: List[List[List[Optional[int]]]]) -> Tuple[int, List[List[Optional[int]]]]:
  won = lambda board, row, col: all(board[row][col_iter] is None for col_iter in range(len(board[row]))) or \
                                all(board[row_iter][col] is None for row_iter in range(len(board)))

  for number in numbers:
    for board_number, board in enumerate(boards):
      exit_board = False
      for row, line in enumerate(board):
        for column, value in enumerate(line):
          exit_board = (number == value)
          if exit_board:
            line[column] = None
            if won(board, row, column):
              boards.pop(board_number)
              return number, board
            break
        if exit_board:
          break

def part1(numbers: List[int], boards: List[List[List[Optional[int]]]]) -> int:
  local_boards = [[[value for value in line] for line in board] for board in boards]
  winning_number, winning_board = get_winning_number_and_board(numbers, local_boards)
  return sum(sum(value or 0 for value in line) for line in winning_board) * winning_number

def part2(numbers: List[int], boards: List[List[List[Optional[int]]]]) -> int:
  local_boards = [[[value for value in line] for line in board] for board in boards]
  for _ in range(len(boards)):
    winning_number, winning_board = get_winning_number_and_board(numbers, local_boards)
  return sum(sum(value or 0 for value in line) for line in winning_board) * winning_number

numbers: List[int]
boards:  List[List[Optional[int]]] = []
with open('2021/dec04.txt') as input_file:
  numbers = [int(token) for token in input_file.readline().rstrip().split(',')]
  input_file.readline()
  board: List[List[Optional[int]]] = []
  for line in input_file:
    if line.rstrip():
      board.append([int(token) for token in line.split()])
    else:
      boards.append(board)
      board = []
  boards.append(board)

print(part1(numbers, boards))
print(part2(numbers, boards))
