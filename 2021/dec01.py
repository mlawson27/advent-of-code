from typing import List

def part1(values : List[int]) -> int:
  return sum(values[i + 1] > values[i] for i in range(len(values) - 1))

def part2(values : List[int]) -> int:
  return sum(sum(values[i+1:i+4]) > sum(values[i:i+3]) for i in range(len(values) - 3))

values : List[int]
with open('2021/dec01.txt') as input_file:
  values = [int(line) for line in input_file]
print(part1(values))
print(part2(values))
