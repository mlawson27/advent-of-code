from typing import List, Pattern, Tuple

def part1(patterns_and_digits: List[Tuple[List[str], List[str]]]):
  return sum((sum(len(digit_str) in {2, 3, 4, 7} for digit_str in digits)) for _, digits in patterns_and_digits)

def part2(patterns_and_digits: List[Tuple[List[str], List[str]]]):
  value_sum = 0
  for (patterns, digits) in patterns_and_digits:
    patterns_by_len = sorted(patterns, key=len)
    pattern_to_val  = { patterns_by_len[0] : '1', patterns_by_len[1] : '7', patterns_by_len[2] : '4', patterns_by_len[-1] : '8' }
    pattern_sets    = set(patterns_by_len).difference(pattern_to_val.keys())

    right_pair                 = set(patterns_by_len[0])
    top                        = set(patterns_by_len[1]).difference(right_pair).pop()
    nine_chars_without_bottom  = set(patterns_by_len[2]).union((top))
    nine_str                   = next(pattern for pattern in pattern_sets if len(nine_chars_without_bottom.symmetric_difference(pattern)) == 1)
    pattern_sets.remove(nine_str)
    bottom                     = nine_chars_without_bottom.symmetric_difference(nine_str).pop()
    three_chars_without_middle = right_pair.union({top, bottom})
    three_str                  = next(pattern for pattern in pattern_sets if len(three_chars_without_middle.symmetric_difference(pattern)) == 1)
    pattern_sets.remove(three_str)
    middle                     = three_chars_without_middle.symmetric_difference(three_str).pop()
    zero_str                   = next(pattern for pattern in pattern_sets if len(set(pattern).symmetric_difference(set(patterns_by_len[-1]).difference((middle)))) == 0)
    pattern_sets.remove(zero_str)
    six_str                    = next(pattern for pattern in pattern_sets if len(pattern) == 6)
    pattern_sets.remove(six_str)
    upper_right                = set(patterns_by_len[-1]).difference(set(six_str)).pop()
    bottom_left                = set(patterns_by_len[-1]).difference(set(nine_str)).pop()
    five_str                   = next(pattern for pattern in pattern_sets if len(set(pattern).symmetric_difference(set(patterns_by_len[-1]).difference((upper_right, bottom_left)))) == 0)
    pattern_sets.remove(five_str)

    pattern_to_val.update({ zero_str : '0', three_str : '3', six_str : '6', nine_str : '9', five_str : '5', pattern_sets.pop() : '2' })
    value_sum  += int(''.join(pattern_to_val[next(expected_str for expected_str in patterns_by_len if len(expected_str) == len(digit_str) and len(set(digit_str).symmetric_difference(expected_str)) == 0)] for digit_str in digits))

  return value_sum

patterns_and_digits: List[Tuple[List[str], List[str]]]
with open('2021/dec08.txt') as input_file:
  patterns_and_digits = list(map(lambda pair: tuple(item.split(' ') for item in pair), (line.rstrip().split(' | ') for line in input_file)))

print(part1(patterns_and_digits))
print(part2(patterns_and_digits))
