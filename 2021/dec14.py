from typing import Callable, Dict, Iterable, Tuple

def perform_replacements_and_get_count(polymer: str, rules: Dict[str, str], iterations: int):
  def run_iteration(get_rule_strs_and_counts: Callable[[], Iterable[Tuple[str, int]]]):
    local_str_counts: Dict[str, int] = {}
    for (rule_str, count) in get_rule_strs_and_counts():
      replacement_rule_str = rules[rule_str]
      for replacement_str in [rule_str[0] + replacement_rule_str, replacement_rule_str + rule_str[1]]:
        if replacement_str not in local_str_counts:
          local_str_counts[replacement_str] = 0
        local_str_counts[replacement_str] += count
    return local_str_counts

  str_counts = run_iteration(lambda : ((polymer[i:i+2], 1) for i in range(len(polymer) - 1)))
  for _ in range(1, iterations):
    str_counts = run_iteration(str_counts.items)

  char_counts: Dict[str, int] = {}
  for (rule_str, count) in str_counts.items():
    for c in rule_str:
      if not c in char_counts:
        char_counts[c] = 0
      char_counts[c] += count

  return ((max(char_counts.values()) + 1) // 2) - ((min(char_counts.values()) + 1) // 2)

def part1(polymer: str, rules: Dict[str, str]):
  return perform_replacements_and_get_count(polymer, rules, 10)

def part2(polymer: str, rules: Dict[str, str]):
  return perform_replacements_and_get_count(polymer, rules, 40)

polymer: str
rules:   Dict[str, str]
with open('2021/dec14.txt') as input_file:
  line_iter = iter(input_file)
  polymer   = next(line_iter).rstrip()
  next(line_iter)
  rules     = {tokens[0] : tokens[1] for tokens in (line.rstrip().split(' -> ') for line in line_iter) }

print(part1(polymer, rules))
print(part2(polymer, rules))
