use std::io::BufRead;

fn part1(lines: &std::vec::Vec<String>) {
    let game_regex  = regex::Regex::new(r"\d+").unwrap();
    let working_sum = lines.iter().enumerate()
                                  .map(|(cur_y, line)| game_regex.find_iter(line).map(move |num_match| [cur_y.checked_sub(1), cur_y.checked_add(1), Some(cur_y)].iter().filter_map(|y_or_none| y_or_none.map(|y| (num_match.start().saturating_sub(1)..num_match.end().saturating_add(1)).map(move |x| (x, y))))
                                                                                                                                                                       .flatten()
                                                                                                                                                                       .filter(|&(x, y)| lines.get(y).is_some_and(|o_line| o_line.as_bytes().get(x).is_some_and(|b: &u8| *b != ('.' as u8) && b.is_ascii_punctuation())))
                                                                                                                                                                       .next()
                                                                                                                                                                       .and_then(|_| num_match.as_str().parse::<usize>().ok())
                                                                                                                                                                       .unwrap_or_default()))
                                  .flatten()
                                  .sum::<usize>();
    println!("{}", working_sum);
}

fn part2(lines: &std::vec::Vec<String>) {
    let mut gear_product_sum = 0;
    for (cur_y, line) in lines.iter().enumerate() {
        for (cur_x, c) in line.bytes().enumerate() {
            if c == ('*' as u8) {
                let mut touching_numbers_count = 0usize;
                let mut gear_product           = 1usize;
                for y_adj in (cur_y.wrapping_sub(1)..(cur_y + 2)).filter(|&v| v < lines.len()) {
                    let mut last_was_digit = false;
                    for x_adj in (cur_x.wrapping_sub(1)..(cur_x + 2)).filter(|&v| v < line.len()) {
                        let is_digit = lines[y_adj].as_bytes()[x_adj].is_ascii_digit();
                        if is_digit && !last_was_digit {
                            let start_of_int        = lines[y_adj][0..x_adj].as_bytes().iter().enumerate().rev().take_while(|(_, c)| c.is_ascii_digit()).last().map_or(x_adj, |(pos, _)| pos);
                            touching_numbers_count += 1;
                            gear_product           *= lines[y_adj][start_of_int..].as_bytes().iter().take_while(|&c| c.is_ascii_digit())
                                                                                                    .fold(0usize,
                                                                                                          |working, c| working * 10 + ((*c as char).to_digit(10).unwrap() as usize));
                        }
                        last_was_digit = is_digit;
                    }
                }
                if touching_numbers_count == 2 {
                    gear_product_sum += gear_product;
                }
            }
        }
    }
    println!("{}", gear_product_sum);
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec03.txt").unwrap());
    let lines = reader.lines().map(|line| line.unwrap()).collect::<Vec<String>>();

    let current = std::time::Instant::now();
    part1(&lines);
    let post_p1 = std::time::Instant::now();
    part2(&lines);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}