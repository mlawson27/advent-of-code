use std::io::BufRead;

#[derive(Clone, Copy)]
enum Direction
{
  North,
  West,
  South,
  East
}

fn tilt_and_get_new_round_rock_positions(round_rock_positions: &Vec<[usize; 2]>, cube_rock_positions: &Vec<[usize; 2]>, map_bounds: [usize; 2], direction: Direction) -> Vec<[usize; 2]> {
    let compare_index = match direction {
        Direction::North | Direction::South => 1usize,
        Direction::East  | Direction::West  => 0usize
    };

    let (base_pos, compare_fn, operation) = match direction {
        Direction::North | Direction::West => (&[0usize, 0usize], usize::cmp        as for<'a> fn(&'a usize, &'a usize) -> std::cmp::Ordering, usize::wrapping_add as fn(usize, usize) -> usize),
        Direction::South | Direction::East => (&map_bounds,       (|a, b| b.cmp(a)) as for<'a> fn(&'a usize, &'a usize) -> std::cmp::Ordering, usize::wrapping_sub as fn(usize, usize) -> usize)
    };

    let mut round_by_index = vec![Vec::<&[usize; 2]>::new(); map_bounds[compare_index] + 1];
    let mut cube_by_index  = round_by_index.clone();
    for (positions, by_index) in [(round_rock_positions, &mut round_by_index), (cube_rock_positions, &mut cube_by_index)] {
        for pos in positions {
            by_index[pos[1 - compare_index]].push(pos);
        }
        for index in by_index {
            index.sort_by(|a, b| compare_fn(&b[compare_index], &a[compare_index]));
        }
    }

    round_by_index.iter().enumerate().flat_map(|(i, by_index)| {
        let mut cubes_at_index = cube_by_index[i].iter();
        let mut next_cube      = cubes_at_index.by_ref().next();
        by_index.iter().enumerate().map(move |(i, round_pos)| {
            while next_cube.is_some() && compare_fn(&round_pos[compare_index], &next_cube.unwrap()[compare_index]) == std::cmp::Ordering::Less {
                next_cube = cubes_at_index.by_ref().next();
            }
            let mut new_pos = **round_pos;
            new_pos[compare_index] = operation(next_cube.unwrap_or(&base_pos)[compare_index],
                                               by_index[(i+1)..].iter().take_while(|c| next_cube.is_none() || compare_fn(&next_cube.unwrap()[compare_index], &c[compare_index]) == std::cmp::Ordering::Less).count() + (next_cube.is_some() as usize));
            new_pos
        })
    }).collect::<Vec<_>>()
}

fn part1(round_rock_positions: &Vec<[usize; 2]>, cube_rock_positions: &Vec<[usize; 2]>, map_bounds: [usize; 2]) {
    println!("{}", tilt_and_get_new_round_rock_positions(&round_rock_positions, &cube_rock_positions, map_bounds, Direction::North).iter().map(|pos|map_bounds[1] - pos[1] + 1).sum::<usize>());
}

fn part2(round_rock_positions: &Vec<[usize; 2]>, cube_rock_positions: &Vec<[usize; 2]>, map_bounds: [usize; 2]) {
    const NUM_ITERATIONS: usize = 1000000000;

    let mut working_round_rock_positions = round_rock_positions.clone();
    let mut state_to_iteration           = std::collections::HashMap::<Vec<[usize; 2]>, usize>::new();
    let mut i                            = 0;
    let mut found_loop                   = false;
    while i < NUM_ITERATIONS {
        let new_round_rock_positions = [Direction::North, Direction::West, Direction::South, Direction::East].iter().fold(working_round_rock_positions.clone(),
                                                                                                                          |working, direction| tilt_and_get_new_round_rock_positions(&working, cube_rock_positions, map_bounds, *direction));
        if !found_loop {
            let found_state = state_to_iteration.get(&new_round_rock_positions);
            if found_state.is_some() {
                let loop_len = i - found_state.unwrap();
                i += ((NUM_ITERATIONS - i) / loop_len) * loop_len;
                found_loop = true;
            }
            else {
                state_to_iteration.insert(new_round_rock_positions.clone(), i);
            }
        }
        working_round_rock_positions = new_round_rock_positions;
        i += 1;
    }
    println!("{}", working_round_rock_positions.iter().map(|pos|map_bounds[1] - pos[1] + 1).sum::<usize>())
}

pub fn run() {
    let reader   = std::io::BufReader::new(std::fs::File::open("input//dec14.txt").unwrap());
    let (map_bounds, round_rock_positions, cube_rock_positions) = {
        let map = reader.lines().map(|line| line.unwrap()).collect::<Vec<_>>();
        assert!(map[1..].iter().all(|line| line.len() == map[0].len()));
        let get_positions = |char_to_find| {
            map.iter().enumerate().flat_map(|(y, line)| line.char_indices().filter_map(move |(x, c)| (c == char_to_find).then_some([x, y]))).collect::<Vec<_>>()
        };
        ([map[0].len() - 1, map.len() - 1], get_positions('O'), get_positions('#'))
    };
    let current = std::time::Instant::now();
    part1(&round_rock_positions, &cube_rock_positions, map_bounds);
    let post_p1 = std::time::Instant::now();
    part2(&round_rock_positions, &cube_rock_positions, map_bounds);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}