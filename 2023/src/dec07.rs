use std::io::BufRead;

fn total_winnings<const J_SCORE: char>(hands_and_ranks: &Vec<([char; 5], usize)>) -> usize {
    let compare_hand = |hand_a: &[char; 5], hand_b: &[char; 5]| -> std::cmp::Ordering {
        let card_score = |c: char| -> char {
            match c {
                'A' => ('9' as u8 + 5) as char,
                'K' => ('9' as u8 + 4) as char,
                'Q' => ('9' as u8 + 3) as char,
                'J' => J_SCORE,
                'T' => ('9' as u8 + 1) as char,
                _   =>  c
            }
        };

        let hand_type_score = |hand: &[char; 5]| -> u8 {
            let mut hand_card_scores = hand.clone().map(card_score);
            hand_card_scores.sort_by(|a, b| b.cmp(a));

            let mut cur_count       = 1u8;
            let mut last_card_score = hand_card_scores[0];
            let mut max_count       = 0u8;
            let mut two_count       = 0u8;
            let mut j_count         = 0u8;

            for &card_score in hand_card_scores.iter().skip(1).chain([char::MAX].iter()) {
                if J_SCORE == '1' && card_score == J_SCORE {
                    j_count += 1;
                }
                else if card_score != last_card_score {
                    two_count      += (cur_count >= 2) as u8;
                    max_count       = max_count.max(cur_count);
                    cur_count       = 1;
                    last_card_score = card_score;
                }
                else {
                    cur_count += 1;
                }
            }
            two_count += (j_count > 0 && max_count < 2) as u8;
            max_count += j_count;

            max_count + two_count + 2 * ((max_count >= 4) as u8) - ((max_count == 2) as u8)
        };

        match hand_type_score(hand_a).cmp(&hand_type_score(hand_b)) {
            std::cmp::Ordering::Equal => {},
            cmp                       => return cmp
        }
        for (&a_char, &b_char) in hand_a.iter().zip(hand_b.iter()) {
            match card_score(a_char).cmp(&card_score(b_char)) {
                std::cmp::Ordering::Equal => continue,
                cmp                       => return cmp
            }
        }
        return std::cmp::Ordering::Equal;
    };

    let mut sorted_hands_hand_ranks = hands_and_ranks.iter().collect::<Vec<_>>();
    sorted_hands_hand_ranks.sort_by(|&(hand_a, _), (hand_b, _)| compare_hand(hand_a, hand_b));
    sorted_hands_hand_ranks.iter()
                           .enumerate()
                           .map(|(i, (_, bid))| (i + 1) * bid)
                           .sum::<usize>()
}

fn part1(hands_and_ranks: &Vec<([char; 5], usize)>) {
    println!("{}", total_winnings::<{('9' as u8 + 1) as char}>(hands_and_ranks));
}

fn part2(hands_and_ranks: &Vec<([char; 5], usize)>) {
    println!("{}", total_winnings::<'1'>(hands_and_ranks));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec07.txt").unwrap());
    let hands_and_ranks = reader.lines().map(|line| {
        line.unwrap().split_whitespace().enumerate()
                                        .fold((['\0'; 5], 0usize),
                                              |working, (i, token)| match i {
                                                0 => (TryInto::<&[u8; 5]>::try_into(token.as_bytes()).unwrap().map(char::from), working.1),
                                                1 => (working.0, token.parse::<usize>().unwrap()),
                                                _ => panic!("Wrong number of tokens")
                                              })
    }).collect::<Vec<_>>();

    let current = std::time::Instant::now();
    part1(&hands_and_ranks);
    let post_p1 = std::time::Instant::now();
    part2(&hands_and_ranks);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}