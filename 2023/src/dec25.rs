use std::io::BufRead;

pub fn run() {
    fn populate_first_group(connection_map: &std::collections::HashMap<String, std::collections::HashSet<String>>) -> std::collections::HashSet<&str> {
        fn inner<'a>(connection_map: &'a std::collections::HashMap<String, std::collections::HashSet<String>>,
                     starting:       &'a str,
                     group:          &mut std::collections::HashSet<&'a str>) {
            group.insert(starting);
            for connection in connection_map[starting].iter() {
                if !group.contains(connection.as_str()) {
                    inner(connection_map, connection, group);
                }
            }
        }
        let mut group = std::collections::HashSet::new();
        inner(connection_map, connection_map.keys().next().unwrap(), &mut group);
        group
    }

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec25.txt").unwrap());

    let current = std::time::Instant::now();

    let mut connection_map = reader.lines().filter_map(Result::ok)
                                           .fold(std::collections::HashMap::new(), |mut ret, line| {
        let mut split = line.split(": ");
        let     first = split.next().unwrap();
        for token in split.next().unwrap().split_whitespace() {
            let values = [first, token];
            for i in 0..values.len() {
                ret.entry(values[i].to_string()).or_insert(std::collections::HashSet::new()).insert(values[1 - i].to_string());
            }
        }
        ret
    });

    println!("{}", (||{
        let mut route_seen_count = std::collections::HashMap::new();
        for (start, start_map) in connection_map.iter() {
            let mut seen        = std::collections::HashMap::new();
            let mut state_queue = std::collections::VecDeque::new();
            state_queue.push_back((start_map, vec![start.as_str()]));
            seen.insert(start.as_str(), 0usize);
            while !state_queue.is_empty() {
                let (current_connections, path_so_far) = state_queue.pop_front().unwrap();
                for connection in current_connections.iter() {
                    let moves_to_connection = seen.get_mut(connection.as_str());
                    if moves_to_connection.is_none() {
                        seen.insert(connection, path_so_far.len());
                        let mut working_path = path_so_far.clone();
                        working_path.push(connection);
                        state_queue.push_back((&connection_map[connection], working_path))
                    }
                    else if *moves_to_connection.as_deref().unwrap() >= path_so_far.len() {
                        *moves_to_connection.unwrap() = path_so_far.len();
                        for &[left, right] in path_so_far.windows(2).map(|arr| TryInto::<&[&str; 2]>::try_into(arr).unwrap()) {
                            *route_seen_count.entry((left < right).then(|| [left, right]).unwrap_or_else(|| [right, left])).or_insert(0usize) += 1;
                        }
                        *route_seen_count.entry((*path_so_far.last().unwrap() < connection).then(|| [*path_so_far.last().unwrap(), connection]).unwrap_or_else(|| [connection, *path_so_far.last().unwrap()])).or_insert(0usize) += 1;
                    }
                }
            }
        }
        let mut routes = route_seen_count.drain().collect::<Vec<_>>();
        routes.sort_by_key(|(_, count)| *count);

        let disabled_connections = routes.drain(..).take(3).map(|(pair, _)| pair.map(str::to_string)).collect::<Vec<_>>();
        for pair in disabled_connections {
            for i in 0..pair.len() {
                connection_map.get_mut(&pair[i]).unwrap().remove(&pair[1 - i]);
            }
        }
        let group = populate_first_group(&connection_map);
        (connection_map.len() - group.len()) * group.len()
    })());

    let post = std::time::Instant::now();
    println!("{} us", (post - current).as_micros());
}