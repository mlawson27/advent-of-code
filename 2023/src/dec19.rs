use std::io::BufRead;

struct Condition {
    index_to_test: usize,
    compare_is_gt: bool,
    value:         usize
}

struct Rule {
    condition:       Option<Condition>,
    output_workflow: String
}

fn part1(workflows: &std::collections::HashMap<String, Vec<Rule>>, ratings: &Vec<[usize; 4]>) {
    const IS_GT_TO_CMP_FN: [for <'a, 'b> fn (&'a usize, &'b usize) -> bool; 2] = [
        usize::lt,
        usize::gt
    ];

    println!("{:?}", ratings.iter().filter(|rating_set| {
        let mut current_workflow_name = "in";
        loop {
            current_workflow_name = workflows[current_workflow_name].iter().filter(|rule| rule.condition.as_ref().map_or(true, |condition| IS_GT_TO_CMP_FN[condition.compare_is_gt as usize](&rating_set[condition.index_to_test], &condition.value))).next().unwrap().output_workflow.as_str();
            if current_workflow_name == "R" {
                return false;
            }
            if current_workflow_name == "A" {
                return true;
            }
        }
    }).flatten().sum::<usize>());
}

fn part2(workflows: &std::collections::HashMap<String, Vec<Rule>>, _ratings: &Vec<[usize; 4]>) {
    fn inner (outer_allowed: [[usize; 2]; 4], current_workflow: &str, workflows: &std::collections::HashMap<String, Vec<Rule>>) -> usize {
        const IS_GT_TO_MIN_MAX_FN: [fn (usize, usize) -> usize; 2] = [
            usize::max,
            usize::min
        ];

        workflows[current_workflow].iter().fold((&mut outer_allowed.clone(), 0usize), |(working_rule, sum), rule| {
            let orig_working_and_cond = rule.condition.as_ref().map(|cond| {
                    let is_gt_index                               = !cond.compare_is_gt as usize;
                    let orig_working                              = working_rule[cond.index_to_test][is_gt_index];
                    working_rule[cond.index_to_test][is_gt_index] = IS_GT_TO_MIN_MAX_FN[is_gt_index](working_rule[cond.index_to_test][is_gt_index], cond.value + 1 - 2 * is_gt_index);
                    (orig_working, cond)
                });
            let this_result = match rule.output_workflow.as_str() {
                    "A"  => working_rule.map(|[min, max]| (max - min + 1)).iter().product::<usize>(),
                    "R"  => 0,
                    name => inner(*working_rule, name, workflows),
                };
            if let Some(&(orig_working, cond)) = orig_working_and_cond.as_ref() {
                let is_gt_index                                   = cond.compare_is_gt as usize;
                working_rule[cond.index_to_test][1 - is_gt_index] = orig_working;
                working_rule[cond.index_to_test][is_gt_index]     = IS_GT_TO_MIN_MAX_FN[is_gt_index](working_rule[cond.index_to_test][is_gt_index], cond.value);
            }
            (working_rule, sum + this_result)
        }).1
    }
    println!("{}", inner([[1usize, 4000usize]; 4], "in", workflows));
}

pub fn run() {
    fn char_to_array_index(c: char) -> usize {
        match c {
            'a' => 0,
            'm' => 1,
            's' => 2,
            'x' => 3,
            _   => panic!("Bad char")
        }
    }

    let rule_regex     = regex::Regex::new(r"(?:([amsx])([<>])(\d+):)?(\w+)[,}]").unwrap();
    let workflow_regex = regex::Regex::new(r"^(\w+)\{((?:[^,}]+[,}])+)$").unwrap();

    let     reader    = std::io::BufReader::new(std::fs::File::open("input//dec19.txt").unwrap());
    let mut lines     = reader.lines();
    let     workflows = lines.by_ref().take_while(|l| l.as_deref().is_ok_and(|l| !l.is_empty())).map(|line| {
        let line           = line.unwrap();
        let outer_captures = workflow_regex.captures(line.as_str()).unwrap();
        (String::from(outer_captures.get(1).unwrap().as_str()),
         rule_regex.captures_iter(outer_captures.get(2).unwrap().as_str()).map(|c| {
            Rule { condition: c.get(1).map(|m| m.as_str().chars().next().unwrap()).zip(c.get(2).map(|m| m.as_str()))
                                                                                  .zip(c.get(3).map(|m| m.as_str().parse::<usize>().unwrap()))
                                                                                  .map(|((c, cmp), value)| Condition { index_to_test: char_to_array_index(c),
                                                                                                                       compare_is_gt: cmp == ">",
                                                                                                                       value: value}),
                   output_workflow: String::from(c.get(4).unwrap().as_str()) }
        }).collect::<Vec<_>>())
    }).collect::<std::collections::HashMap<_, _>>();
    let     ratings = lines.by_ref().map(|line| {
        let line = line.unwrap();
        line[1..line.len()- 1].split(',').map(|assign_str| {
                                                let mut split = assign_str.split('=');
                                                (char_to_array_index(split.next().unwrap().chars().next().unwrap()),
                                                 split.next().unwrap().parse::<usize>().unwrap())
                                              }).fold([0usize; 4],
                                                      |mut working, (index, value)|{
                                                        working[index] = value;
                                                        working
                                                      })
    }).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&workflows, &ratings);
    let post_p1 = std::time::Instant::now();
    part2(&workflows, &ratings);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}