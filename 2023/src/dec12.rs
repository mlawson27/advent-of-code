use std::io::BufRead;

fn get_counts<'a, 'b>(line: &'a str, counts: &'b [usize], broken_len: usize, results: &mut std::collections::HashMap<(&'a str, &'b [usize], usize), usize>) -> usize {
    let cache_key     = (line, counts, broken_len);
    let cached_result = results.get(&cache_key);
    if cached_result.is_some() {
        return *cached_result.unwrap();
    }

    let mut working_counts     = counts;
    let mut working_broken_len = broken_len;
    let mut first_question_pos = Option::<usize>::None;
    for (i, c) in line.char_indices().chain(std::iter::once_with(|| (line.len(), '.'))) {
        match c {
            '?' => { first_question_pos = Some(i + 1); break; },
            '.' => {
                if working_broken_len > 0 {
                    if working_broken_len != *working_counts.first().unwrap_or(&0) {
                        results.insert(cache_key, 0);
                        return 0;
                    }
                    else {
                        working_counts = &working_counts[1..];
                    }
                    working_broken_len = 0;
                }
            },
            '#' => {
                working_broken_len += 1;
            }
            _   => panic!("Bad character found")
        }
    }

    let result = first_question_pos.is_none().then(|| working_counts.is_empty() as usize)
                                             .unwrap_or_else(|| (working_broken_len == 0 || working_counts.first().map_or(false, |len| working_broken_len == *len)).then(|| get_counts(&line[first_question_pos.unwrap()..], &working_counts[((working_broken_len > 0) as usize)..], 0, results)).unwrap_or(0) +
                                                                get_counts(&line[first_question_pos.unwrap()..], working_counts, working_broken_len + 1, results));
    results.insert(cache_key, result);
    result
}

fn part1(conditions: &Vec<(String, Vec<usize>)>) {
    println!("{}", conditions.iter().map(|(line, counts)| get_counts(&line[..], &counts[..], 0, &mut std::collections::HashMap::<_, _>::new())).sum::<usize>());
}

fn part2(conditions: &Vec<(String, Vec<usize>)>) {
    let new_conditions = conditions.iter().map(|(s, l)| ([s.as_str(); 5].join("?"), [l; 5].iter().flat_map(|v| *v).map(|v| *v).collect::<Vec<_>>())).collect::<Vec<_>>();
    println!("{}", new_conditions.iter().map(|(line, counts)| get_counts(&line[..], &counts[..], 0, &mut std::collections::HashMap::<_, _>::new())).sum::<usize>());
}

pub fn run() {
    let reader     = std::io::BufReader::new(std::fs::File::open("input//dec12.txt").unwrap());
    let conditions = reader.lines().map(|l| {
                        let     binding = l.unwrap();
                        let mut tokens  = binding.split_whitespace();
                        (String::from(tokens.next().unwrap()),
                         tokens.next().unwrap().split(',').map(|token| token.parse::<usize>().unwrap()).collect::<Vec<_>>())
                     }).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&conditions);
    let post_p1 = std::time::Instant::now();
    part2(&conditions);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}