pub mod dec01;
pub mod dec02;
pub mod dec03;
pub mod dec04;
pub mod dec05;
pub mod dec06;
pub mod dec07;
pub mod dec08;
pub mod dec09;
pub mod dec10;
pub mod dec11;
pub mod dec12;
pub mod dec13;
pub mod dec14;
pub mod dec15;
pub mod dec16;
pub mod dec17;
pub mod dec18;
pub mod dec19;
pub mod dec20;
pub mod dec21;
pub mod dec22;
pub mod dec23;
pub mod dec24;
pub mod dec25;

const DAY_TO_RUN_FN : &[fn()] = &[
    dec01::run,
    dec02::run,
    dec03::run,
    dec04::run,
    dec05::run,
    dec06::run,
    dec07::run,
    dec08::run,
    dec09::run,
    dec10::run,
    dec11::run,
    dec12::run,
    dec13::run,
    dec14::run,
    dec15::run,
    dec16::run,
    dec17::run,
    dec18::run,
    dec19::run,
    dec20::run,
    dec21::run,
    dec22::run,
    dec23::run,
    dec24::run,
    dec25::run
];

fn main() {
    let arg = std::env::args().skip(1).next();
    if arg.is_none() {
        println!("Specify a day of the month to run");
    }
    else {
        let day = arg.as_ref().unwrap().parse::<usize>();
        if day.is_err() || *day.as_ref().unwrap() > DAY_TO_RUN_FN.len() || *day.as_ref().unwrap() == 0 {
            println!("Invalid day: {}", arg.unwrap());
        }
        else {
            DAY_TO_RUN_FN[day.unwrap() - 1]();
        }
    }
}
