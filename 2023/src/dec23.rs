use std::io::BufRead;

fn max_distance<const ALLOW_GOING_UP_SLOPES: bool>(map: &Vec<String>) -> usize {
    fn max_distance_from(start_pos: [usize; 2], end_pos: [usize; 2], moves_so_far: usize, distance_to_forks: &std::collections::HashMap<[usize; 2], std::collections::HashMap<[usize; 2], usize>>, seen: &mut std::collections::HashSet<[usize; 2]>) -> Option::<usize> {
        if start_pos == end_pos {
            return Some(moves_so_far)
        }
        distance_to_forks.get(&start_pos).and_then(|distance_to_fork| {
            seen.insert(start_pos);
            let mut result = None::<usize>;
            for (next_intersection_start, distance_to_intersection) in distance_to_fork.iter() {
                if !seen.contains(next_intersection_start) {
                    let inner_result = max_distance_from(*next_intersection_start, end_pos, moves_so_far + distance_to_intersection, distance_to_forks, seen);
                    result           = result.map(|a| inner_result.map_or(a, |b| a.max(b))).or(inner_result);
                }
            }
            seen.remove(&start_pos);
            result
        })
    }

    let mut map                  = map.clone();
    let     [start_pos, end_pos] = [(map.first(), 1), (map.last(), map.len() - 2)].map(|(l, y)| [l.unwrap().char_indices().filter_map(|(i, c)| (c == '.').then_some(i)).next().unwrap(), y]);
    [(start_pos[1] - 1) as usize, (end_pos[1] + 1) as usize].iter().for_each(|y| map[*y] = map[*y].replace(".", "#"));
    let distance_to_forks = {
        let mut distance_to_forks = std::collections::HashMap::new();
        let mut pos_queue         = std::collections::VecDeque::new();
        pos_queue.push_back((start_pos, start_pos, start_pos, 0usize));

        while !pos_queue.is_empty() {
            let (working_pos, last_pos, last_start_pos, distance_since_start) = pos_queue.pop_front().unwrap();
            if working_pos == end_pos {
                distance_to_forks.entry(last_start_pos).or_insert(std::collections::HashMap::new()).insert(end_pos, distance_since_start);
                continue;
            }

            let mut next_positions      = [Option::<[usize; 2]>::None; 4];
            let mut num_valid_positions = 0usize;
            for (next_pos, dir_char) in [([working_pos[0].wrapping_sub(1), working_pos[1]],                 b'<'),
                                         ([working_pos[0].wrapping_add(1), working_pos[1]],                 b'>'),
                                         ([working_pos[0],                 working_pos[1].wrapping_sub(1)], b'^'),
                                         ([working_pos[0],                 working_pos[1].wrapping_add(1)], b'v')] {
                if map[next_pos[1] as usize].as_bytes()[next_pos[0] as usize] != b'#' &&
                   next_pos != last_pos &&
                   (ALLOW_GOING_UP_SLOPES ||
                    map[working_pos[1] as usize].as_bytes()[working_pos[0] as usize] == b'.' ||
                    map[working_pos[1] as usize].as_bytes()[working_pos[0] as usize] == dir_char) {
                    next_positions[num_valid_positions] = Some(next_pos);
                    num_valid_positions += 1;
                }
            }
            match num_valid_positions {
                0 => {},
                1 => {
                    pos_queue.push_front((next_positions[0].unwrap(), working_pos, last_start_pos, distance_since_start + 1));
                }
                v => {
                    let distance_to_fork = distance_to_forks.entry(last_start_pos).or_insert(std::collections::HashMap::new());
                    if !distance_to_fork.contains_key(&working_pos) {
                        distance_to_fork.insert(working_pos, distance_since_start + 1);
                        for next_pos in &next_positions[0..v] {
                            pos_queue.push_front((next_pos.unwrap(), working_pos, working_pos, 0));
                        }
                    }
                }
            }
        }
        distance_to_forks
    };
    max_distance_from(start_pos, end_pos, 0, &distance_to_forks, &mut std::collections::HashSet::new()).unwrap() + 2
}

fn part1(map: &Vec<String>) {
    println!("{}", max_distance::<false>(map));
}

fn part2(map: &Vec<String>) {
    println!("{}", max_distance::<true>(map));
}

pub fn run() {
    let reader  = std::io::BufReader::new(std::fs::File::open("input//dec23.txt").unwrap());
    let map     = reader.lines().map(|line| line.unwrap()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&map);
    let post_p1 = std::time::Instant::now();
    part2(&map);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}