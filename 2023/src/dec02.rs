use std::io::BufRead;

enum CardColor
{
  Red,
  Blue,
  Green,
  _COUNT
}

type Subgame = [usize; CardColor::_COUNT as usize];
type Game    = (usize, std::vec::Vec<Subgame>);

trait ArrayZippable<'a, T, const N: usize> {
    fn zip<'b, U>(self: &'a Self, other: &'b[U; N]) -> [(&'a T, &'b U); N];
}

impl<'a, T, const N: usize> ArrayZippable<'a, T, N> for [T; N] {
    fn zip<'b, U> (self: &'a[T; N], other: &'b[U; N]) -> [(&'a T, &'b U); N] {
        std::array::from_fn::<_, N, _>(|i| (&self[i], &other[i]))
    }
}

fn part1(games: &std::vec::Vec<Game>) {
    const GAME_CONSTRAINTS: Subgame = [12, 14, 13];

    fn score_if_game_is_possible(game: &Game) -> Option<usize> {
        game.1.iter().all(|subgame| subgame.iter().zip(GAME_CONSTRAINTS).all(|(&this_v, constrain_v)| this_v <= constrain_v)).then_some(game.0)
    }

    println!("{}", games.iter().filter_map(score_if_game_is_possible).sum::<usize>());
}

fn part2(games: &std::vec::Vec<Game>) {
    println!("{}", games.iter().map(|game| game.1.iter().fold([0, 0, 0], |working: Subgame, subgame: &Subgame| working.zip(subgame).map(|(l, r)| *l.max(r))).iter().product::<usize>()).sum::<usize>());
}

pub fn run() {
    let game_regex = regex::Regex::new(r"Game\s+(\d+):\s+(.+)").unwrap();
    let card_regex = regex::Regex::new(r"(\d+)\s+([a-z]+)").unwrap();

    let parse_game = |line: &str| -> Result<Game, &'static str> {
        match game_regex.captures(line) {
            Some(captures) => {
                Ok((captures.get(1).unwrap().as_str().parse::<usize>().unwrap(),
                    captures.get(2).unwrap().as_str().split(';').map(|sub_game| *sub_game.split(',').fold(&mut [0, 0, 0], |working, token| {
                        let captures = card_regex.captures(token).unwrap();
                        working[match captures.get(2).unwrap().as_str() {
                            "blue"  => CardColor::Blue,
                            "red"   => CardColor::Red,
                            "green" => CardColor::Green,
                            s       => panic!("Invalid color: {}", s)
                        } as usize] += captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
                        working
                    })).collect::<std::vec::Vec<Subgame>>()))
            }
            None => Err("Bad parse of outer value")
        }
    };

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec02.txt").unwrap());
    let games = reader.lines().map(|line| parse_game(line.unwrap().as_str()).unwrap()).collect::<Vec<Game>>();

    let current = std::time::Instant::now();
    part1(&games);
    let post_p1 = std::time::Instant::now();
    part2(&games);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}