use std::io::BufRead;

type CardData = (usize, std::collections::HashSet<usize>, std::collections::HashSet<usize>);

fn part1(card_data: &std::vec::Vec<CardData>) {
    println!("{}", card_data.iter().filter_map(|(_, left, right)| left.intersection(&right).count().checked_sub(1usize).map(|v| 1usize << v))
                                   .sum::<usize>());
}

fn part2(card_data: &std::vec::Vec<CardData>) {
    println!("{}", card_data.iter().filter_map(|(c, left, right)| { let num_matches = left.intersection(&right).count(); (num_matches > 0).then_some((c, num_matches))})
                                   .fold(card_data.iter().map(|(c, _, _)| (c, 1usize)).collect::<std::collections::HashMap<_, _>>(),
                                         |mut cards, (card_number, num_matches)| {
                                            let num_new_cards = *cards.get(&card_number).unwrap();
                                            for new_card_number in card_number + 1 .. card_number + 1 + num_matches {
                                                *cards.get_mut(&new_card_number).unwrap() += num_new_cards;
                                            }
                                            cards
                                         })
                                   .values()
                                   .sum::<usize>());
}

pub fn run() {
    let line_regex = regex::Regex::new(r"Card\s+(\d+):\s+([\d\s]+)\s+\|\s+([\d\s]+)").unwrap();
    let ws_regex   = regex::Regex::new(r"\s+").unwrap();

    let parse_line = |line: &str| -> CardData {
        let to_set = |piece: &str| {
            ws_regex.split(piece).map(|token| token.parse::<usize>().unwrap()).collect::<std::collections::HashSet<usize>>()
        };

        let captures = line_regex.captures(line).unwrap();
        (captures.get(1).unwrap().as_str().parse::<usize>().unwrap(), to_set(captures.get(2).unwrap().as_str()), to_set(captures.get(3).unwrap().as_str()))
    };

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec04.txt").unwrap());
    let card_data = reader.lines().map(|line| parse_line(line.unwrap().as_str()))
                                           .collect::<Vec<CardData>>();

    let current = std::time::Instant::now();
    part1(&card_data);
    let post_p1 = std::time::Instant::now();
    part2(&card_data);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}