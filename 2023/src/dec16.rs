use std::io::BufRead;

#[derive(Clone, Copy)]
enum Direction {
    Left,
    Right,
    Up,
    Down
}

fn get_num_energized_tiles(map: &Vec<String>, starting_position: [usize; 2], starting_direction: Direction) -> usize {
    assert!(map[1..].iter().all(|line| line.len() == map[0].len()));
    let mut visited     = vec![vec![0u8; map[0].len()]; map.len()];
    let mut state_queue = vec![(starting_position, starting_direction)];
    while !state_queue.is_empty() {
        let (current_position, current_direction) = state_queue.pop().unwrap();
        let next_position = match current_direction {
            Direction::Left  => [current_position[0].wrapping_sub(1), current_position[1]],
            Direction::Right => [current_position[0].wrapping_add(1), current_position[1]],
            Direction::Up    => [current_position[0],                 current_position[1].wrapping_sub(1)],
            Direction::Down  => [current_position[0],                 current_position[1].wrapping_add(1)],
        };
        if next_position[1] < map.len() && next_position[0] < map[next_position[1]].len() {
            let curr_dir_as_arr = &[current_direction];
            let next_directions: &[Direction] = match (current_direction, map[next_position[1]].as_bytes()[next_position[0]]) {
                (_,                b'.')  => curr_dir_as_arr,
                (Direction::Left,  b'-')  => curr_dir_as_arr,
                (Direction::Right, b'-')  => curr_dir_as_arr,
                (Direction::Up,    b'|')  => curr_dir_as_arr,
                (Direction::Down,  b'|')  => curr_dir_as_arr,
                (Direction::Left,  b'/')  => &[Direction::Down],
                (Direction::Right, b'/')  => &[Direction::Up],
                (Direction::Up,    b'/')  => &[Direction::Right],
                (Direction::Down,  b'/')  => &[Direction::Left],
                (Direction::Left,  b'\\') => &[Direction::Up],
                (Direction::Right, b'\\') => &[Direction::Down],
                (Direction::Up,    b'\\') => &[Direction::Left],
                (Direction::Down,  b'\\') => &[Direction::Right],
                (Direction::Left,  b'|')  => &[Direction::Up, Direction::Down],
                (Direction::Right, b'|')  => &[Direction::Up, Direction::Down],
                (Direction::Up,    b'-')  => &[Direction::Left, Direction::Right],
                (Direction::Down,  b'-')  => &[Direction::Left, Direction::Right],
                _                         => panic!("Invalid character and direction")
            };
            for next_direction in next_directions {
                if visited[next_position[1]][next_position[0]] & (1u8 << (*next_direction as usize)) == 0 {
                    visited[next_position[1]][next_position[0]] |= 1u8 << (*next_direction as usize);
                    state_queue.push((next_position, *next_direction));
                }
            }
        }
    }
    visited.iter().map(|line| line.iter().filter(|v| **v != 0).count()).sum::<usize>()
}

fn part1(map: &Vec<String>) {
    println!("{}", get_num_energized_tiles(map, [usize::MAX, 0], Direction::Right));
}

fn part2(map: &Vec<String>) {
    println!("{}", (0..map.len())             .map(|y| ([usize::MAX,   y],            Direction::Right)).chain(
                   (0..map.len())             .map(|y| ([map[y].len(), y],            Direction::Left))).chain(
                   (0..map[0].len())          .map(|x| ([x,            usize::MAX],   Direction::Down))).chain(
                   (0..map[map.len()-1].len()).map(|x| ([x,            map[x].len()], Direction::Up))).map(|(position, direction)| {
                       get_num_energized_tiles(map, position, direction)
                   }).max().unwrap());
}

pub fn run() {
    let reader  = std::io::BufReader::new(std::fs::File::open("input//dec16.txt").unwrap());
    let map     = reader.lines().map(|line| line.unwrap()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&map);
    let post_p1 = std::time::Instant::now();
    part2(&map);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}