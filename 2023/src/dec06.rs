use std::io::BufRead;

fn num_ways_to_exceed_distance(time: usize, distance: usize) -> usize {
    fn inner<B>(range: B, time: usize, distance: usize) -> impl Iterator<Item = usize>
    where B: Iterator<Item = usize> {
        range.map(move |hold_time| hold_time * (time - hold_time))
             .take_while(move |&this_distance| this_distance > distance)
    }

    inner((0..(time/2)).rev(), time, distance).chain(inner((time/2)..(time+1), time, distance)).count()
}

fn part1(times_and_distances: &Vec<(usize, usize)>) {
    println!("{}", times_and_distances.iter()
                                      .map(|&(time, distance)| num_ways_to_exceed_distance(time, distance))
                                      .product::<usize>());
}

fn part2(times_and_distances: &Vec<(usize, usize)>) {
    let (time, distance) = times_and_distances.iter().fold((0usize, 0usize),
                                                            |(working_time, working_distance),
                                                                &(new_time, new_distance)| {
                                                                    (working_time * 10usize.pow(new_time.ilog10() + 1) + new_time,
                                                                     working_distance * 10usize.pow(new_distance.ilog10() + 1) + new_distance)
                                                                });
    println!("{}", num_ways_to_exceed_distance(time, distance));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec06.txt").unwrap());
    let times_and_distances = {
        let mut lines = reader.lines();
        lines.by_ref().next().unwrap().unwrap().split_whitespace().skip(1).map(|token| token.parse::<usize>().unwrap()).zip(lines.by_ref().next().unwrap().unwrap().split_whitespace().skip(1).map(|token| token.parse::<usize>().unwrap())).collect::<Vec<(usize, usize)>>()
    };

    let current = std::time::Instant::now();
    part1(&times_and_distances);
    let post_p1 = std::time::Instant::now();
    part2(&times_and_distances);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}