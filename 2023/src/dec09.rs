use std::io::BufRead;

fn next_value(subvalues: &Vec<i64>, value_fn: fn(&Vec<i64>, i64)->i64) -> i64 {
    let diffs = subvalues.windows(2).map(|window| window[1] - window[0]).collect::<Vec<_>>();
    value_fn(subvalues, if diffs.iter().all(|&v| v == 0) { 0 } else { next_value(&diffs, value_fn) })
}

fn part1(values: &Vec<Vec<i64>>) {
    println!("{}", values.iter().map(|v| next_value(v, |v1, i| v1.last().unwrap() + i)).sum::<i64>());
}

fn part2(values: &Vec<Vec<i64>>) {
    println!("{}", values.iter().map(|v| next_value(v, |v1, i| v1.first().unwrap() - i)).sum::<i64>());
}

pub fn run() {
    let reader  = std::io::BufReader::new(std::fs::File::open("input//dec09.txt").unwrap());
    let values  = reader.lines().map(|l| l.unwrap().split_whitespace().map(|token| token.parse::<i64>().unwrap()).collect::<Vec<_>>()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&values);
    let post_p1 = std::time::Instant::now();
    part2(&values);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}