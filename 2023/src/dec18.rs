use std::io::BufRead;

fn get_num_filled_cubes(dig_plan: &Vec<(char, usize, usize)>, direction_and_amount_for_plan_entry: fn(&(char, usize, usize)) -> (char, usize)) -> usize {
    let mut current_position = [usize::MAX / 2, usize::MAX / 2];
    let mut vertices         = Vec::with_capacity(dig_plan.len());
    let mut edge_count       = 0usize;
    for plan_entry in dig_plan {
        let (direction, amount) = direction_and_amount_for_plan_entry(plan_entry);
        let (adjustment_index, adjustment_function) = match direction {
            'U' => (1usize, usize::wrapping_sub as fn(usize, usize) -> usize),
            'D' => (1usize, usize::wrapping_add as fn(usize, usize) -> usize),
            'L' => (0usize, usize::wrapping_sub as fn(usize, usize) -> usize),
            'R' => (0usize, usize::wrapping_add as fn(usize, usize) -> usize),
            _   => panic!("Bad direction character")
        };
        current_position[adjustment_index] = adjustment_function(current_position[adjustment_index], amount);
        vertices.push(current_position);
        edge_count += amount;
    }

    (vertices.iter().zip(vertices.iter().cycle().skip(1)).fold([0usize, 0usize],
                                                               |[working_l, working_r], (a, b)| [working_l + (a[0] - usize::MAX / 2) * (b[1] - usize::MAX / 2),
                                                                                                 working_r + (a[1] - usize::MAX / 2) * (b[0] - usize::MAX / 2)])
                                                         .iter().fold(0usize, |working, v| working.abs_diff(*v)) + edge_count) / 2 + 1
}

fn part1(dig_plan: &Vec<(char, usize, usize)>) {
    println!("{}", get_num_filled_cubes(dig_plan, |(direction, amount, _)| (*direction, *amount)));
}

fn part2(dig_plan: &Vec<(char, usize, usize)>) {
    println!("{}", get_num_filled_cubes(dig_plan, |(_, _, hex_value)| (['R', 'D', 'L', 'U'][hex_value & 0xf], *hex_value >> 4)));
}

pub fn run() {
    let line_regex = regex::Regex::new(r"([URLD])\s+(\d+)\s+\(#([A-Fa-f0-9]+)\)").unwrap();

    let reader   = std::io::BufReader::new(std::fs::File::open("input//dec18.txt").unwrap());
    let dig_plan = reader.lines().map(|line| {
        let line = line.unwrap();
        let captures = line_regex.captures(line.as_str()).unwrap();
        (captures.get(1).unwrap().as_str().chars().next().unwrap(),
         captures.get(2).unwrap().as_str().parse::<usize>().unwrap(),
         usize::from_str_radix(captures.get(3).unwrap().as_str(), 16).unwrap())
    }).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&dig_plan);
    let post_p1 = std::time::Instant::now();
    part2(&dig_plan);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}