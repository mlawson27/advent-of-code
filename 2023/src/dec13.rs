use std::io::BufRead;

fn get_fold_pos_value<const DIFF_COUNT: usize>(map: &Vec<Vec<u8>>) -> Option<usize> {
       (1usize..map.first().unwrap().len()).filter(|i| map.iter().map(|line| (&line[0..*i]).iter().rev().take(line.len() - i)
                                                                                                        .zip((&line[*i.. ]).iter().take(*i))
                                                                                                        .filter(|(b1, b2)| **b1 != **b2).count()).sum::<usize>() == DIFF_COUNT).next().or_else(
    || (1usize..map.len())                 .filter(|i| (&map [0..*i]).iter().rev().take(map.len() - i)
                                                                                  .zip((&map [*i.. ]).iter().take(*i))
                                                                                  .map(|(l1, l2)| l1.iter().zip(l2.iter())
                                                                                                           .filter(|(b1, b2)| *b1 != *b2).count()).sum::<usize>() == DIFF_COUNT).next().map(|v| v * 100))
}

fn part1(maps: &Vec<Vec<Vec<u8>>>) {
    println!("{}", maps.iter().map(|map| get_fold_pos_value::<0>(map).unwrap()).sum::<usize>());
}

fn part2(maps: &Vec<Vec<Vec<u8>>>) {
    println!("{}", maps.iter().map(|map| get_fold_pos_value::<1>(map).unwrap()).sum::<usize>());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec13.txt").unwrap());
    let maps   = {
        let mut lines = reader.lines();
        let mut maps  = Vec::<Vec<Vec<u8>>>::new();
        loop {
            let sublines = lines.by_ref().map(|l| l.unwrap())
                                         .take_while(|line| !line.is_empty())
                                         .map(|line| line.as_bytes().to_owned())
                                         .collect::<Vec<_>>();
            if sublines.is_empty() {
                break;
            }
            assert!(sublines[1..].iter().all(|line| line.len() == sublines[0].len()));
            maps.push(sublines);
        }
        maps
    };
    let current = std::time::Instant::now();
    part1(&maps);
    let post_p1 = std::time::Instant::now();
    part2(&maps);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}