use std::io::BufRead;

fn part1(coordinates: &Vec<[[i64; 3]; 2]>) {
    println!("{}", coordinates.iter().enumerate().map(|(i, [coord_i, vel_i])| {
        let slope_i   = (vel_i[0] != 0).then_some(vel_i[1] as f64 / vel_i[0] as f64);
        let y_inter_i = slope_i.map(|s| coord_i[1] as f64 - s * coord_i[0] as f64);
        coordinates[i+1..].iter().filter(|[coord_j, vel_j]| {
            let slope_j   = (vel_j[0] != 0).then_some(vel_j[1] as f64 / vel_j[0] as f64);
            let y_inter_j = slope_j.map(|s| coord_j[1] as f64 - s * coord_j[0] as f64);
            if slope_i == slope_j && y_inter_i != y_inter_j {
                return false;
            }
            let inter_x  = (y_inter_j.unwrap() - y_inter_i.unwrap()) / (slope_i.unwrap() - slope_j.unwrap());
            let inter_y  = slope_i.unwrap() * inter_x + y_inter_i.unwrap();
            let inter_ts = [(coord_i, vel_i), (coord_j, vel_j)].map(|(coord, vel)| (inter_x - coord[0] as f64) / vel[0] as f64);
            [inter_x, inter_y].iter().all(|v| *v >= 200000000000000.0 && *v <= 400000000000000.0) && inter_ts.iter().all(|t| *t >= 0.0)
        }).count()
    }).sum::<usize>());
}

fn part2(coordinates: &Vec<[[i64; 3]; 2]>) {
    fn cross_matrix<T>(vec: &nalgebra::Vector3<T>) -> nalgebra::Matrix3<T>
    where T: std::ops::Neg<Output = T> + std::default::Default + Copy {
        nalgebra::Matrix3::new(Default::default(), vec[2],             -vec[1],
                               -vec[2],            Default::default(), vec[0],
                               vec[1],             -vec[0],            Default::default())
    }

    let nalg_vecs = coordinates.iter().map(|pair| pair.map(|vec| nalgebra::Vector3::from(vec.map(|v| v as f64)))).collect::<Vec<_>>();
    let m         = {
        let quarters = [cross_matrix(&nalg_vecs[0][1]) - cross_matrix(&nalg_vecs[1][1]),
                        cross_matrix(&nalg_vecs[0][1]) - cross_matrix(&nalg_vecs[2][1]),
                        cross_matrix(&nalg_vecs[1][0]) - cross_matrix(&nalg_vecs[0][0]),
                        cross_matrix(&nalg_vecs[2][0]) - cross_matrix(&nalg_vecs[0][0])];
        nalgebra::Matrix6::from_fn(|col, row| quarters[row / 3 * 2 + col / 3][(col % 3) * 3 + row % 3])
    };
    let rhs       = {
        let halves = [-nalg_vecs[0][0].cross(&nalg_vecs[0][1]) + nalg_vecs[1][0].cross(&nalg_vecs[1][1]),
                      -nalg_vecs[0][0].cross(&nalg_vecs[0][1]) + nalg_vecs[2][0].cross(&nalg_vecs[2][1])];
        nalgebra::Vector6::from_fn(|i, _| halves[i / 3][i % 3])
    };
    println!("{}", (m.try_inverse().unwrap() * rhs).iter().take(3).map(|v| v.round() as i64).sum::<i64>());
}

pub fn run() {
    fn as_array<const N: usize, T, E>(iter: T) -> Result<[E; N], &'static str>
    where T: Iterator<Item = E>, E: std::marker::Copy, E: std::default::Default {
        let mut ret = [E::default(); N];
        let     len = (0..N).zip(iter).fold(0usize, |working_size, (i, v)| {
            ret[i] = v;
            working_size + 1
        });

        (len == N).then_some(ret).ok_or("Incorrect length")
    }

    let at_sep_regex    = regex::Regex::new(r"\s*@\s*").unwrap();
    let comma_sep_regex = regex::Regex::new(r"\s*,\s*").unwrap();

    let reader      = std::io::BufReader::new(std::fs::File::open("input//dec24.txt").unwrap());
    let coordinates = reader.lines().map(|line| line.unwrap())
                                    .map(|line| as_array::<2, _, _>(at_sep_regex.split(line.as_str())).unwrap().map(|half_str| as_array::<3, _, _>(comma_sep_regex.split(half_str)).unwrap().map(|val_str| val_str.parse::<i64>().unwrap()))).collect::<Vec<_>>();
    let current     = std::time::Instant::now();
    part1(&coordinates);
    let post_p1 = std::time::Instant::now();
    part2(&coordinates);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}