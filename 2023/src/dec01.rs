use std::io::BufRead;

fn part1(lines: &std::vec::Vec<String>) {
    fn first_digit(chars: impl std::iter::Iterator<Item = char>) -> u32 { chars.filter_map(|c| c.to_digit(10)).next().unwrap() }
    println!("{}", lines.iter().map(|line| first_digit(line.chars()) * 10 + first_digit(line.chars().rev())).sum::<u32>());
}

fn part2(lines: &std::vec::Vec<String>) {
    const TEXT : &[&str] = &[
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ];

    fn line_result(line: &str) -> u32 {
        fn inner<'a, B>(line: &'a str, adjust_fn: impl FnOnce(std::str::CharIndices<'a>) -> B) -> u32
        where B: std::iter::Iterator<Item = (usize, char)> {
            adjust_fn(line.char_indices()).filter_map(|(i, c)| c.to_digit(10).or_else(|| TEXT.iter().enumerate().filter_map(|(value, &text_digit)| line.get(i..i+text_digit.len())
                                                                                                                                                       .and_then(|subline| (subline == text_digit).then_some(value as u32)))
                                                                             .next()))
                                          .next()
                                          .unwrap()
        }
        inner(line, std::convert::identity) * 10 + inner(line, |iter| iter.rev())
    }
    println!("{}", lines.iter().map(String::as_str).map(line_result).sum::<u32>());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec01.txt").unwrap());
    let lines = reader.lines().map(Result::unwrap).collect::<Vec<String>>();

    let current = std::time::Instant::now();
    part1(&lines);
    let post_p1 = std::time::Instant::now();
    part2(&lines);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}