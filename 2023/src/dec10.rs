use std::io::BufRead;

trait ArrayZippable<'a, T, const N: usize> {
    fn zip<'b, U>(self: &'a Self, other: &'b[U; N]) -> [(&'a T, &'b U); N];
}

impl<'a, T, const N: usize> ArrayZippable<'a, T, N> for [T; N] {
    fn zip<'b, U> (self: &'a[T; N], other: &'b[U; N]) -> [(&'a T, &'b U); N] {
        std::array::from_fn::<_, N, _>(|i| (&self[i], &other[i]))
    }
}

fn do_walk<T, F>(map: &Vec<String>, state: T, fold_fn: F) -> (T, Vec<Vec<char>>)
where F: Fn(T, ([usize; 2], usize)) -> T {
    let starting_position = map.iter().enumerate()
                                      .filter_map(|(y, line)| line.as_bytes().iter().enumerate()
                                                                                    .filter_map(|(x, c)| (*c == 'S' as u8).then_some(x))
                                                                                    .next().map(|x| [x, y])).next().unwrap();
    let mut visited     = map.iter().map(|line| vec!['\0'; line.len()]).collect::<Vec<_>>();
    let mut state_queue = std::collections::VecDeque::<([usize; 2], usize)>::new();
    visited[starting_position[1]][starting_position[0]] = {
        let has_t = match map.get(starting_position[1].wrapping_sub(1)).map(|l| l.as_bytes()[starting_position[0]] as char).unwrap_or('.') {
            '|' | '7' | 'F' => true,
            _               => false
        };
        let has_b = match map.get(starting_position[1].saturating_add(1)).map(|l| l.as_bytes()[starting_position[0]] as char).unwrap_or('.') {
            '|' | 'L' | 'J' => true,
            _               => false
        };
        let has_l = match map[starting_position[1]].as_bytes().get(starting_position[0].wrapping_sub(1)).map(|b| *b as char).unwrap_or('.') {
            '-' | 'L' | 'F' => true,
            _               => false
        };
        let has_r = match map[starting_position[1]].as_bytes().get(starting_position[0].saturating_add(1)).map(|b| *b as char).unwrap_or('.') {
            '-' | 'J' | '7' => true,
            _               => false
        };
        match (has_t, has_b, has_l, has_r) {
            (true,  true,  false, false) => '|',
            (false, false, true,  true)  => '-',
            (true,  false, true,  false) => 'L',
            (true,  false, false, true)  => 'J',
            (false, true,  true,  false) => '7',
            (false, true,  false, true)  => 'F',
            _                            => panic!("Should be able to see this combo")
        }
    };
    state_queue.push_back((starting_position, 0usize));
    let mut working_state = state;
    while !state_queue.is_empty() {
        let (current_position, moves_so_far) = state_queue.pop_front().unwrap();
        working_state = fold_fn(working_state, (current_position, moves_so_far));
        for adjustment in [[-1isize, 0], [1, 0], [0, -1], [0, 1]] {
            let new_position = current_position.zip(&adjustment).map(|(pos, adj)| pos.wrapping_add_signed(*adj));
            if (new_position[1] < map.len()) &&
               (new_position[0] < map[new_position[1]].len()) &&
               (visited[new_position[1]][new_position[0]] == '\0') {
                let new_char = map[new_position[1]].as_bytes()[new_position[0]] as char;
                if match (map[current_position[1]].as_bytes()[current_position[0]] as char,
                          new_char,
                          adjustment) {
                    ('S', '|', [ 0, -1]) => true,
                    ('S', '7', [ 0, -1]) => true,
                    ('S', 'F', [ 0, -1]) => true,
                    ('S', '|', [ 0,  1]) => true,
                    ('S', 'L', [ 0,  1]) => true,
                    ('S', 'J', [ 0,  1]) => true,
                    ('S', '-', [-1,  0]) => true,
                    ('S', 'L', [-1,  0]) => true,
                    ('S', 'F', [-1,  0]) => true,
                    ('S', '-', [ 1,  0]) => true,
                    ('S', 'J', [ 1,  0]) => true,
                    ('S', '7', [ 1,  0]) => true,
                    ('|', '|', [ 0, -1]) => true,
                    ('|', '7', [ 0, -1]) => true,
                    ('|', 'F', [ 0, -1]) => true,
                    ('|', '|', [ 0,  1]) => true,
                    ('|', 'L', [ 0,  1]) => true,
                    ('|', 'J', [ 0,  1]) => true,
                    ('-', '-', [-1,  0]) => true,
                    ('-', 'L', [-1,  0]) => true,
                    ('-', 'F', [-1,  0]) => true,
                    ('-', '-', [ 1,  0]) => true,
                    ('-', 'J', [ 1,  0]) => true,
                    ('-', '7', [ 1,  0]) => true,
                    ('L', '|', [ 0, -1]) => true,
                    ('L', 'F', [ 0, -1]) => true,
                    ('L', '7', [ 0, -1]) => true,
                    ('L', '-', [ 1,  0]) => true,
                    ('L', '7', [ 1,  0]) => true,
                    ('L', 'J', [ 1,  0]) => true,
                    ('J', '|', [ 0, -1]) => true,
                    ('J', 'F', [ 0, -1]) => true,
                    ('J', '7', [ 0, -1]) => true,
                    ('J', '-', [-1,  0]) => true,
                    ('J', 'L', [-1,  0]) => true,
                    ('J', 'F', [-1,  0]) => true,
                    ('F', '|', [ 0,  1]) => true,
                    ('F', 'J', [ 0,  1]) => true,
                    ('F', 'L', [ 0,  1]) => true,
                    ('F', '-', [ 1,  0]) => true,
                    ('F', 'J', [ 1,  0]) => true,
                    ('F', '7', [ 1,  0]) => true,
                    ('7', '|', [ 0,  1]) => true,
                    ('7', 'J', [ 0,  1]) => true,
                    ('7', 'L', [ 0,  1]) => true,
                    ('7', '-', [-1,  0]) => true,
                    ('7', 'L', [-1,  0]) => true,
                    ('7', 'F', [-1,  0]) => true,
                    _                    => false
                } {
                    visited[new_position[1]][new_position[0]] = new_char;
                    state_queue.push_back((new_position, moves_so_far + 1));
                }
            }
        }
    }
    (working_state, visited)
}

fn part1(map: &Vec<String>) {
    println!("{}", do_walk(map, 0usize, |working, state| working.max(state.1)).0);
}

fn part2(map: &Vec<String>) {
    let     visited        = do_walk(map, 0, |_, _| 0).1;
    let mut current_inside = false;
    let mut inside_count   = 0usize;
    let mut coming_from    = '\0';
    for line in visited {
        for tile in line {
            match tile {
                '|' => current_inside = !current_inside,
                'L' => coming_from = 'U',
                'F' => coming_from = 'D',
                'J' => if coming_from == 'D' { current_inside = !current_inside },
                '7' => if coming_from == 'U' { current_inside = !current_inside },
                _  => {},
            }
            inside_count += (current_inside && (tile == '\0')) as usize;
        }
    }
    println!("{}", inside_count);
}

pub fn run() {
    let reader  = std::io::BufReader::new(std::fs::File::open("input//dec10.txt").unwrap());
    let map     = reader.lines().map(|l| l.unwrap()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&map);
    let post_p1 = std::time::Instant::now();
    part2(&map);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}