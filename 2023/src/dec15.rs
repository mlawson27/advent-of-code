use std::io::BufRead;

fn hash(step: &str) -> usize {
    step.chars().fold(0usize, |working, c| ((working + (c as usize)) * 17) % 256)
}

fn part1(steps: &Vec<String>) {
    println!("{}", steps.iter().map(|step| hash(step.as_str())).sum::<usize>());
}

fn part2(steps: &Vec<String>) {
    println!("{}", {
        steps.iter().fold(&mut std::array::from_fn::<_, 256, _>(|_| Vec::<(&str, usize)>::new()), |boxes, step| {
            if step.ends_with('-') {
                let name   = &step[..(step.len() - 1)];
                let box_id = hash(name);
                if let Some(matching_pos) = boxes[box_id].iter().enumerate().filter_map(|(i, (other_name, _))| (other_name == &name).then_some(i)).next() {
                    boxes[box_id].remove(matching_pos);
                }
            }
            else {
                let mut vals   = step.split('=');
                let     name   = vals.by_ref().next().unwrap();
                let     len    = vals.by_ref().next().unwrap().parse::<usize>().unwrap();
                let     box_id = hash(name);
                if let Some((_, other_len)) = boxes[box_id].iter_mut().filter(|(other_name, _)| other_name == &name).next() {
                    *other_len = len;
                }
                else {
                    boxes[box_id].push((name, len));
                }
            }
            boxes
        }).iter().enumerate().map(|(box_id, b)| b.iter().enumerate().map(move |(box_index, (_, val))| (box_id + 1) * (box_index + 1) * *val)).flatten().sum::<usize>()
    });
}

pub fn run() {
    let reader   = std::io::BufReader::new(std::fs::File::open("input//dec15.txt").unwrap());
    let steps = reader.lines().next().unwrap().unwrap().split(',').map(|token| token.to_owned()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&steps);
    let post_p1 = std::time::Instant::now();
    part2(&steps);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}