use std::io::BufRead;

#[derive(PartialEq)]
enum ModuleType
{
    Default,
    FlipFlop,
    Conjunction
}

struct Module {
    module_type:  ModuleType,
    downstream_i: Vec<usize>,
    input_mask:   u128
}

enum ModuleTypeState {
    Default(),
    FlipFlop(bool),
    Conjunction(u128)
}

fn run_state_machine<Fn>(modules: &Vec<Module>, broadcaster_i: usize, num_presses: usize, mut exit_condition: Fn) -> usize
where Fn: core::ops::FnMut(usize, bool, usize) -> bool {
    let mut module_queue = std::collections::VecDeque::new();
    let mut module_state = modules.iter().map(|m| match m.module_type {
                                                  ModuleType::Default  => ModuleTypeState::Default(),
                                                  ModuleType::FlipFlop => ModuleTypeState::FlipFlop(false),
                                                  ModuleType::Conjunction => ModuleTypeState::Conjunction(0u128)
                                              }).collect::<Vec<_>>();

    for i in 1..=num_presses {
        module_queue.push_back((broadcaster_i, false, 0usize));
        while !module_queue.is_empty() {
            let (module_i, last_signal, signal_source_i) = module_queue.pop_front().unwrap();
            let working_module                           = &modules[module_i];
            if exit_condition(module_i, last_signal, signal_source_i) {
                return i;
            }
            let signal_to_send = match working_module.module_type {
                    ModuleType::Default  => Some(last_signal),
                    ModuleType::FlipFlop => match &mut module_state[module_i] {
                            ModuleTypeState::FlipFlop(is_on) => {
                                if !last_signal {
                                    *is_on = !*is_on;
                                }
                                (!last_signal).then_some(*is_on)
                            }
                            _ => panic!("Bad type/state match")
                        },
                    ModuleType::Conjunction => match &mut module_state[module_i] {
                            ModuleTypeState::Conjunction(input_mask) => {
                                *input_mask = *input_mask & !(1u128 << signal_source_i) | ((last_signal as u128) << signal_source_i);
                                Some(*input_mask != working_module.input_mask)
                            },
                            _ => panic!("Bad type/state match")
                        }
                };
            if signal_to_send.is_some() {
                for i in working_module.downstream_i.iter() {
                    module_queue.push_back((*i, signal_to_send.unwrap(), module_i));
                }
            }
        }
    }
    num_presses
}

fn part1(modules: &Vec<Module>, broadcaster_i: usize, _rx_i: usize) {
    let mut num_signals = [0usize; 2];
    run_state_machine(modules, broadcaster_i, 1000, |_, last_signal, _| { num_signals[last_signal as usize] += 1; false });
    println!("{}", num_signals.iter().product::<usize>());
}

fn part2(modules: &Vec<Module>, broadcaster_i: usize, rx_i: usize) {
    assert!(modules[rx_i].input_mask.is_power_of_two());
    let rx_source_module_i = modules[rx_i].input_mask.ilog2() as usize;
    assert!(modules[rx_source_module_i].module_type == ModuleType::Conjunction);
    println!("{}", (0..modules.len()).filter(|source_source_i| modules[rx_source_module_i].input_mask & (1u128 << source_source_i) != 0)
                                     .map(|source_source_i| run_state_machine(modules, broadcaster_i, usize::MAX, |signal_i, last_signal, signal_source_i| signal_i == rx_source_module_i && signal_source_i == source_source_i && last_signal))
                                     .product::<usize>());
}

pub fn run() {
    let line_regex = regex::Regex::new(r"([%&])?(\w+)\s+->\s+(\w+(?:,\s+\w+)*)").unwrap();

    let reader                           = std::io::BufReader::new(std::fs::File::open("input//dec20.txt").unwrap());
    let (modules, [broadcaster_i, rx_i]) = {
        let (mut modules, mut downstream_str_combines): (Vec<_>, Vec<_>) = reader.lines().map(|line| line.unwrap()).enumerate().map(|(i, line)| {
            let captures = line_regex.captures(line.as_str()).unwrap();
            (Module{ module_type: match captures.get(1).map(|m| m.as_str()) {
                            None      => ModuleType::Default,
                            Some("%") => ModuleType::FlipFlop,
                            Some("&") => ModuleType::Conjunction,
                            _         => panic!("Invalid module type")
                        },
                     downstream_i: Vec::new(),
                     input_mask: 0 },
             ((String::from(captures.get(2).unwrap().as_str()), i),
              String::from(captures.get(3).unwrap().as_str())))
        }).unzip();
        let (mut name_to_i, downstream_strs): (std::collections::HashMap<_, _>, Vec<_>) = downstream_str_combines.drain(..).unzip();
        for (i, downstream_str) in downstream_strs.iter().enumerate() {
            for downstream_mod_name in downstream_str.split(", ") {
                let find_result = name_to_i.get(downstream_mod_name).map(|i| *i);
                let downstream_i = find_result.unwrap_or_else(|| {
                    modules.push(Module { module_type: ModuleType::Default, downstream_i: Vec::new(), input_mask: 0u128 });
                    name_to_i.insert(String::from(downstream_mod_name), modules.len() - 1);
                    modules.len() - 1
                });
                modules[i].downstream_i.push(downstream_i);
                modules[downstream_i].input_mask |= 1u128 << i;
            }
        }
        (modules, ["broadcaster", "rx"].map(|to_find| *name_to_i.iter().filter_map(|(name, i)| (name == to_find).then_some(i)).next().unwrap()))
    };
    let current = std::time::Instant::now();
    part1(&modules, broadcaster_i, rx_i);
    let post_p1 = std::time::Instant::now();
    part2(&modules, broadcaster_i, rx_i);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}