use itertools::Itertools;
use std::io::BufRead;

fn distance_between_all_galaxies<const EXPANSION_CONSTANT: usize>(map: &Vec<String>) -> usize {
    assert!(map[1..].iter().all(|line| line.len() == map[0].len()));
    let mut rows_all_empty   = vec![true; map.len()];
    let mut cols_all_empty   = vec![true; map[0].len()];
    let mut galaxy_positions = Vec::<[usize; 2]>::new();
    for (y, line) in map.iter().enumerate() {
        for x in line.char_indices().filter_map(|(x, c)| (c == '#').then_some(x)) {
            rows_all_empty[y] = false;
            cols_all_empty[x] = false;
            galaxy_positions.push([x, y]);
        }
    }
    for (dim, all_empty) in [(0usize, &cols_all_empty), (1usize, &rows_all_empty)] {
        for i in (0..all_empty.len()).rev().filter(|i: &usize| all_empty[*i]) {
            for v in galaxy_positions.iter_mut().filter(|v| v[dim] >= i) {
                v[dim] += EXPANSION_CONSTANT - 1;
            }
        }
    }
    galaxy_positions.iter().tuple_combinations().map(|(l, r)| l.iter().zip(r.iter()).map(|(a, b)| a.abs_diff(*b)).sum::<usize>()).sum()
}

fn part1(map: &Vec<String>) {
    println!("{}", distance_between_all_galaxies::<2>(map));
}

fn part2(map: &Vec<String>) {
    println!("{}", distance_between_all_galaxies::<1000000>(map));
}

pub fn run() {
    let reader  = std::io::BufReader::new(std::fs::File::open("input//dec11.txt").unwrap());
    let map     = reader.lines().map(|l| l.unwrap()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&map);
    let post_p1 = std::time::Instant::now();
    part2(&map);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}