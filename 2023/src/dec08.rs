use std::io::BufRead;

fn num_steps_until(starting_i: usize, exit_condition: impl Fn(&str)->bool, nodes: &Vec<(String, [usize; 2])>, directions: &Vec<usize>) -> usize {
    directions.iter().cycle().scan(starting_i, |working, &c| {
        *working = nodes[*working].1[c];
        (!exit_condition(nodes[*working].0.as_str())).then_some(c)
    }).count() + 1
}

fn part1(nodes: &Vec<(String, [usize; 2])>, directions: &Vec<usize>) {
    println!("{}", num_steps_until(nodes.iter().enumerate().filter_map(|(i, (name, _))|  (name == "AAA").then_some(i)).next().unwrap(), |working| working == "ZZZ", nodes, directions));
}

fn part2(nodes: &Vec<(String, [usize; 2])>, directions: &Vec<usize>) {
    fn lcm(a: usize, b: usize) -> usize{
        fn gcd(n: usize, m: usize) -> usize {
            let mut n_working = n;
            let mut m_working = m;
            while m_working != 0 {
                if m_working < n_working {
                    std::mem::swap(&mut m_working, &mut n_working);
                }
                m_working %= n_working;
            }
            n_working
        }
        return a * (b / gcd(a, b));
    }

    println!("{}", nodes.iter().enumerate()
                               .filter_map(|(i, (name, _))| name.ends_with('A').then_some(i))
                               .map(|working| num_steps_until(working, |working| working.ends_with('Z'), nodes, directions))
                               .reduce(lcm)
                               .unwrap());
}

pub fn run() {
    let node_regex = regex::Regex::new(r"([\w]{3})\s+=\s+\(([\w]{3}),\s+([\w]{3})\)").unwrap();

    let     reader     = std::io::BufReader::new(std::fs::File::open("input//dec08.txt").unwrap());
    let mut lines      = reader.lines().map(Result::unwrap);
    let     directions = lines.by_ref().next().unwrap().chars()
                                                       .map(|c| match c { 'L' => 0usize, 'R' => 1usize, _ => panic!("Incorrect direction") })
                                                       .collect::<Vec<_>>();
    let nodes =  {
        let mut nodes_working = lines.by_ref().skip(1).map(|line| {
                                                        let captures = node_regex.captures(line.as_str()).unwrap();
                                                        (String::from(captures.get(1).unwrap().as_str()),
                                                         [String::from(captures.get(2).unwrap().as_str()), String::from(captures.get(3).unwrap().as_str())])
                                                    }).collect::<Vec<_>>();
        let     name_to_i     = nodes_working.iter()
                                             .enumerate()
                                             .map(|(i, (name, _))| (name.to_owned(), i))
                                             .collect::<std::collections::HashMap<_, _>>();
        nodes_working.drain(..)
                     .map(|(name, dirs)| (name.to_owned(), dirs.map(|subname| name_to_i[subname.as_str()])))
                     .collect::<Vec<_>>()
    };

    let current = std::time::Instant::now();
    part1(&nodes, &directions);
    let post_p1 = std::time::Instant::now();
    part2(&nodes, &directions);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}