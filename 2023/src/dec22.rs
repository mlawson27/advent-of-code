use std::io::BufRead;

fn get_block_support_is_info(block_ranges: &Vec<[[usize; 3]; 2]>) -> [Vec<std::collections::HashSet<usize>>; 2] {
    fn all_coordinates<const BOTTOM_Z_ONLY: bool, const Z_OFFSET: usize>(range: &[[usize; 3]; 2]) -> impl Iterator<Item = [usize; 3]> + '_ {
        (range[0][0]..(range[1][0] + 1)).flat_map(move |x| (range[0][1]..(range[1][1] + 1)).flat_map(move |y| (range[0][2]..(range[!BOTTOM_Z_ONLY as usize][2] + 1)).map(move |z| [x, y, z - Z_OFFSET])))
    }

    let     make_ret_vec           = || (0..block_ranges.len()).map(|_| std::collections::HashSet::<usize>::new()).collect::<Vec<_>>();
    let mut block_to_i             = std::collections::HashMap::new();
    let mut block_supports_is_info = [make_ret_vec(), make_ret_vec()];
    let mut working_ranges         = block_ranges.clone();
    working_ranges.sort_by_key(|v| v[0][2]);

    for (i, range) in working_ranges.iter_mut().enumerate() {
        let mut new_range = *range;
        let mut supported = false;
        loop {
            if new_range[0][2] == 1 {
                break;
            }
            for coord in all_coordinates::<true, 1>(&new_range) {
                if let Some(coord_owner) = block_to_i.get(&coord) {
                    block_supports_is_info[0][*coord_owner as usize].insert(i);
                    block_supports_is_info[1][i].insert(*coord_owner);
                    supported = true;
                }
            }
            if supported {
                break;
            }
            new_range.iter_mut().for_each(|r| r[2] -= 1);
        }
        block_to_i.extend(all_coordinates::<false, 0>(&new_range).map(|coord| (coord, i)));
        *range = new_range;
    }
    block_supports_is_info
}

fn part1(block_ranges: &Vec<[[usize; 3]; 2]>) {
    let [block_supports_is, block_supported_by_is] = get_block_support_is_info(block_ranges);
    println!("{}", block_supports_is.iter().filter(|l| l.iter().all(|j| block_supported_by_is[*j].len() != 1)).count());
}

fn part2(block_ranges: &Vec<[[usize; 3]; 2]>) {
    fn get_explosion_count(i: usize, block_supports_is: &Vec<std::collections::HashSet<usize>>, block_supported_by_is: &mut Vec<std::collections::HashSet<usize>>, removed_pairs: &mut Vec<(usize, usize)>) -> usize {
        let mut ret = 0usize;
        for j in block_supports_is[i].iter() {
            block_supported_by_is[*j].remove(&i);
            removed_pairs.push((*j, i));
            if block_supported_by_is[*j].is_empty() {
                ret += 1 + get_explosion_count(*j, block_supports_is, block_supported_by_is, removed_pairs);
            }
        }
        ret
    }

    let [block_supports_is, mut block_supported_by_is] = get_block_support_is_info(block_ranges);
    let get_explosion_count_wrapper = |i: usize| -> usize {
        let mut removed_pairs = Vec::new();
        let     ret           = get_explosion_count(i, &block_supports_is, &mut block_supported_by_is, &mut removed_pairs);
        for (j, i) in removed_pairs {
            block_supported_by_is[j].insert(i);
        }
        ret
    };

    println!("{}", (0..block_ranges.len()).map(get_explosion_count_wrapper).sum::<usize>());
}

pub fn run() {
    fn as_array<const N: usize, T, E>(iter: T) -> Result<[E; N], &'static str>
    where T: Iterator<Item = E>, E: std::marker::Copy, E: std::default::Default {
        let mut ret = [E::default(); N];
        let     len = (0..N).zip(iter).fold(0usize, |working_size, (i, v)| {
            ret[i] = v;
            working_size + 1
        });

        (len == N).then_some(ret).ok_or("Incorrect length")
    }

    let reader       = std::io::BufReader::new(std::fs::File::open("input//dec22.txt").unwrap());
    let block_ranges = reader.lines().map(|line| line.unwrap())
                                     .map(|line| {
                                        let ret = as_array::<2, _, _>(line.split('~').map(|token| as_array::<3, _, _>(token.split(',')).unwrap().map(|t| t.parse::<usize>().unwrap()))).unwrap();
                                        assert!(ret[0] <= ret[1]);
                                        ret
                                     }).collect::<Vec<_>>();
    let current      = std::time::Instant::now();
    part1(&block_ranges);
    let post_p1 = std::time::Instant::now();
    part2(&block_ranges);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}