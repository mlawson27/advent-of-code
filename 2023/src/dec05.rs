extern crate rayon;
use std::{io::BufRead, cmp::Ordering};
use rayon::iter::ParallelIterator;
use rayon::iter::IntoParallelIterator;

fn get_map_val(v: usize, map: &Vec<[usize; 3]>) -> usize {
    let map_entry_pos = map.binary_search_by(|elem| match elem[1].cmp(&v) {
        Ordering::Equal => Ordering::Less,
        ord => ord
    }).unwrap_err();
    if map_entry_pos == 0 {
        return v;
    }
    let map_entry = map[map_entry_pos - 1];
    if v > (map_entry[1] + map_entry[2]) { v } else { map_entry[0] + (v - map_entry[1]) }
}

fn part1(starting_values: &Vec<usize>, ordered_maps: &Vec<&Vec<[usize; 3]>>) {
    println!("{}", starting_values.iter().map(|&v| ordered_maps.iter().cloned().fold(v, get_map_val)).min().unwrap());
}

fn part2(starting_values: &Vec<usize>, ordered_maps: &Vec<&Vec<[usize; 3]>>) {
    println!("{}", starting_values.chunks_exact(2)
                                  .filter_map(|arr| TryInto::<&[usize; 2]>::try_into(arr).ok())
                                  .map(|&[starting, len]| (starting..(starting + len)).into_par_iter()
                                                                                      .map(|v| ordered_maps.iter().cloned().fold(v, get_map_val))
                                                                                      .min().unwrap())
                                  .min().unwrap());
}

pub fn run() {
    let starting_regex = regex::Regex::new(r"(\w+)s:\s+(\d+(?:\s+\d+)*)").unwrap();
    let map_title_regex = regex::Regex::new(r"(\w+)-to-(\w+)\s+map:").unwrap();

    let     reader = std::io::BufReader::new(std::fs::File::open("input//dec05.txt").unwrap());
    let mut lines = reader.lines().map(|line| line.unwrap());

    let (starting_name, starting_values) = lines.next().map(|line| {
        let c = starting_regex.captures(line.as_str()).unwrap();
        (String::from(c.get(1).unwrap().as_str()), c.get(2).unwrap().as_str().split_whitespace().map(|token| token.parse::<usize>().unwrap()).collect::<Vec<_>>())
    }).unwrap();
    lines.next().unwrap();

    let maps = {
        let mut working = std::collections::HashMap::<String, (String, Vec<[usize; 3]>)>::new();
        loop {
            let maybe_next_line = lines.by_ref().next();
            if maybe_next_line.is_none() {
                break;
            }
            let next_line = maybe_next_line.unwrap();
            let title_captures = map_title_regex.captures(next_line.as_str()).unwrap();
            let mut map_entries = lines.by_ref().take_while(|line| !line.is_empty())
                                    .map(|line| line.split_whitespace()
                                                            .enumerate()
                                                            .fold([0usize; 3],
                                                               |mut working, (i, token)| {
                                                                working[i] = token.parse::<usize>().unwrap();
                                                                working
                                                            }))
                                    .collect::<Vec<_>>();
            map_entries.sort_by(|a, b| a[1].cmp(&b[1]));
            working.insert(String::from(title_captures.get(1).unwrap().as_str()),
                           (String::from(title_captures.get(2).unwrap().as_str()),
                               map_entries));

        }
        working
    };

    // Pre-compute ordering of maps for speed
    let ordered_maps = {
        let mut result = Vec::<&Vec<[usize; 3]>>::with_capacity(maps.len());
        let mut working_name = &starting_name;
        loop {
            let map_iter = maps.get(working_name);
            if map_iter.is_none() {
                break;
            }
            result.push(&map_iter.unwrap().1);
            working_name = &map_iter.unwrap().0;
        }
        result
    };

    let current = std::time::Instant::now();
    part1(&starting_values, &ordered_maps);
    let post_p1 = std::time::Instant::now();
    part2(&starting_values, &ordered_maps);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}