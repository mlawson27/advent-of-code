use std::io::BufRead;

fn get_min_heat_loss<const MIN_DIRECTION_COUNT: usize, const MAX_DIRECTION_COUNT: usize>(map: &Vec<Vec<usize>>) -> Option<usize> {
    let mut seen_state  = std::collections::HashMap::new();
    let mut state_queue = std::collections::BinaryHeap::new();
    state_queue.extend([2u8, 3u8].iter().map(|dir| std::cmp::Reverse((0usize, [0usize, 0usize], *dir, 0u8))));
    while !state_queue.is_empty() {
        let (current_distance, [current_x, current_y], last_dir, last_dir_count) = state_queue.pop().unwrap().0;
        if current_y == map.len() - 1 && current_x == map[current_y].len() - 1 && last_dir_count as usize >= MIN_DIRECTION_COUNT {
            return Some(current_distance);
        }
        for (i, &[next_x, next_y]) in [[current_x.wrapping_sub(1), current_y],
                                       [current_x,                 current_y.wrapping_sub(1)],
                                       [current_x.wrapping_add(1), current_y],
                                       [current_x,                 current_y.wrapping_add(1)]].iter().enumerate() {
            if i == (last_dir as usize + 2) % 4 || ((last_dir_count as usize) < MIN_DIRECTION_COUNT && i != last_dir as usize) || next_y >= map.len() || next_x >= map[next_y].len() {
                continue;
            }
            let direction_count = last_dir_count * ((last_dir == i as u8) as u8) + 1;
            let new_distance    = current_distance + map[next_y][next_x];
            if (direction_count as usize) > MAX_DIRECTION_COUNT {
                continue
            }
            let distance_to_position = seen_state.entry(([next_x, next_y], i as u8, direction_count)).or_insert(usize::MAX);
            if new_distance < *distance_to_position {
                *distance_to_position = new_distance;
                state_queue.push(std::cmp::Reverse((new_distance, [next_x, next_y], i as u8, direction_count)));
            }
        }
    }
    None
}

fn part1(map: &Vec<Vec<usize>>) {
    println!("{}", get_min_heat_loss::<1, 3>(map).unwrap());
}

fn part2(map: &Vec<Vec<usize>>) {
    println!("{}", get_min_heat_loss::<4, 10>(map).unwrap());
}

pub fn run() {
    let reader  = std::io::BufReader::new(std::fs::File::open("input//dec17.txt").unwrap());
    let map     = reader.lines().map(|line| line.unwrap().chars().map(|c| c.to_digit(10).unwrap() as usize).collect::<Vec<_>>()).collect::<Vec<_>>();
    let current = std::time::Instant::now();
    part1(&map);
    let post_p1 = std::time::Instant::now();
    part2(&map);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}