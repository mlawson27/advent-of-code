use std::io::BufRead;

fn get_num_plots_after_steps<const WRAPPING: bool, const NUM_POINTS: usize>(map: &Vec<String>, s_position: [usize; 2], step_values: [usize; NUM_POINTS]) -> [(usize, usize); NUM_POINTS] {
    let mut seen        = std::collections::HashSet::new();
    let mut state_queue = std::collections::VecDeque::new();
    let mut ret         = [(0usize, 0usize); NUM_POINTS];
    let mut value_i     = 0usize;
    state_queue.push_back((s_position, 0usize));
    while !state_queue.is_empty() {
        let (current_position, steps_so_far) = state_queue.pop_front().unwrap();
        value_i += (steps_so_far >  step_values[value_i]) as usize;
        if steps_so_far == step_values[value_i] {
            ret[value_i].1 += 1;
            if value_i == ret.len() - 1 {
                continue;
            }
        }
        for next_pos in [[current_position[0].wrapping_sub(1), current_position[1]],
                         [current_position[0].wrapping_add(1), current_position[1]],
                         [current_position[0],                 current_position[1].wrapping_sub(1)],
                         [current_position[0],                 current_position[1].wrapping_add(1)]] {
            let next_pos_to_use = if WRAPPING {
                let next_y = next_pos[1].wrapping_add(map.len() * 100) % map.len();
                Some([next_pos[0].wrapping_add(map[next_y].len() * 100) % map[next_y].len(), next_y])
            }
            else if next_pos[1] < map.len() &&
                    next_pos[0] < map[next_pos[1]].len() {
                Some(next_pos)
            }
            else {
                None
            };
            if next_pos_to_use.is_some() &&
               !seen.contains(&(next_pos, (steps_so_far + 1))) &&
               map[next_pos_to_use.unwrap()[1]].as_bytes()[next_pos_to_use.unwrap()[0]] != b'#' {
                seen.insert((next_pos, (steps_so_far + 1)));
                state_queue.push_back((next_pos, steps_so_far + 1));
            }
        }
    }
    ret
}

fn part1(map: &Vec<String>, s_position: [usize; 2]) {
    println!("{}", get_num_plots_after_steps::<false, 1>(map, s_position, [64])[0].1);
}

fn part2(map: &Vec<String>, s_position: [usize; 2]) {
    let points = get_num_plots_after_steps::<true, 3>(map, s_position, [65usize, 65usize + 131usize, 65usize + 2 * 131usize]).map(|(x, y)| (x / 131, y));
    let c      = points[0].1;
    let a      = (points[2].1 - 2 * points[1].1 + c) / 2;
    let b      = points[1].1 - a - c;
    println!("{}", a * (26501365usize / 131).pow(2) + b * (26501365usize / 131) + c);
}

pub fn run() {
    let reader     = std::io::BufReader::new(std::fs::File::open("input//dec21.txt").unwrap());
    let map        = reader.lines().map(|line| line.unwrap()).collect::<Vec<_>>();
    let s_position = map.iter().enumerate().filter_map(|(y, line)| line.char_indices().filter_map(|(x, c)| (c == 'S').then_some([x, y])).next()).next().unwrap();
    let current    = std::time::Instant::now();
    part1(&map, s_position);
    let post_p1 = std::time::Instant::now();
    part2(&map, s_position);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}