use std::io::BufRead;

enum Instruction {
    Addx {amount: i32},
    Noop
}

impl Instruction {
    fn execute(&self, x : &mut i32, cycles_this_instr: &mut u32) -> bool {
        match self {
            Instruction::Addx { amount } => {
                *cycles_this_instr += 1;
                if *cycles_this_instr == 2 {
                    *x += amount;
                    return true;
                }
                false
            },
            Instruction::Noop => true
        }
    }
}

fn part1(instructions: &Vec<Instruction>) {
    let cycles_to_check       = [20u32, 60, 100, 140, 180, 220];
    let mut cycle_to_check_it = cycles_to_check.iter();
    let mut cycle_to_check    = cycle_to_check_it.next().unwrap();

    let mut result = 0i32;

    let mut x = 1;

    let mut current_cycle    = 0u32;
    let mut instruction_iter = instructions.iter();
    let mut instruction      = instruction_iter.next().unwrap();
    let mut this_instr_cycle = 0u32;
    loop {
        current_cycle += 1;
        if *cycle_to_check == current_cycle {
            result += x * (*cycle_to_check as i32);
            let next = cycle_to_check_it.next();
            if next.is_none() {
                break;
            }
            cycle_to_check = next.unwrap();
        }
        if instruction.execute(&mut x, &mut this_instr_cycle) {
            instruction      = instruction_iter.next().unwrap();
            this_instr_cycle = 0u32;
        }
    }

    println!("{}", result);
}

fn part2(instructions: &Vec<Instruction>) {
    let mut pixels_on = [[false; 40usize]; 6];

    let mut x = 1;

    let mut current_cycle    = 0usize;
    let mut instruction_iter = instructions.iter();
    let mut instruction      = instruction_iter.next().unwrap();
    let mut this_instr_cycle = 0u32;
    loop {
        pixels_on[current_cycle / 40][current_cycle % 40] = [x - 1, x, x + 1].contains(&((current_cycle % 40) as i32));
        current_cycle += 1;
        if current_cycle >= pixels_on.iter().map(|x| x.len()).sum() {
            break;
        }
        if instruction.execute(&mut x, &mut this_instr_cycle) {
            instruction      = instruction_iter.next().unwrap();
            this_instr_cycle = 0u32;
        }
    }

    pixels_on.iter()
             .for_each(|pixels_on| {
                println!("{}", String::from_iter(pixels_on.map(|pixel_on| ['.', '#'][pixel_on as usize])));
             });
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec10.txt").unwrap());

    let instructions = reader.lines().map(|line_or_err| line_or_err.unwrap())
                                     .map(|line| match &line[0..4] {
                                       "addx" => Ok(Instruction::Addx { amount: line[5..].parse::<i32>().unwrap() }),
                                       "noop" => Ok(Instruction::Noop),
                                       _ => Err("Bad")
                                     }.unwrap())
                                     .collect::<Vec<Instruction>>();

    let current = std::time::Instant::now();
    part1(&instructions);
    let post_p1 = std::time::Instant::now();
    part2(&instructions);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}