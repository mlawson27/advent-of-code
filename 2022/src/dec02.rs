use std::io::BufRead;

fn part1(choices: &std::vec::Vec<(i32, i32)>) {
    fn points(other_choice: i32, my_choice: i32) -> i32 {
        // Both choices: 0 = Rock, 1 = Paper, 2 = Scissors
        return ((my_choice - other_choice + 1).rem_euclid(3) * 3) + my_choice + 1;
    }

    println!("{}", choices.iter().map(|choice_pair| points(choice_pair.0, choice_pair.1)).sum::<i32>());
}

fn part2(choices: &std::vec::Vec<(i32, i32)>) {
    fn points(other_choice: i32, my_choice: i32) -> i32 {
        // Other choice: 0 = Rock, 1 = Paper, 2 = Scissors
        // My choice: 0 = Lose, 1 = Draw, 2 = Win
        return ((other_choice + my_choice - 1).rem_euclid(3) + 1) + (my_choice * 3);
    }

    println!("{}", choices.iter().map(|choice_pair| points(choice_pair.0, choice_pair.1)).sum::<i32>());
}

pub fn run() {
  let reader = std::io::BufReader::new(std::fs::File::open("input//dec02.txt").unwrap());

  let choices = reader.lines().map(|line_or_err| line_or_err.map(|line| (((line.as_bytes()[0] - ('A' as u8)) as i32), ((line.as_bytes()[2] - ('X' as u8)) as i32))).unwrap()).collect();

  let current = std::time::Instant::now();
  part1(&choices);
  let post_p1 = std::time::Instant::now();
  part2(&choices);
  let post_p2 = std::time::Instant::now();
  println!("p1: {} us", (post_p1 - current).as_micros());
  println!("p2: {} us", (post_p2 - post_p1).as_micros());
}