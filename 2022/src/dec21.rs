use std::io::BufRead;

#[derive(Clone)]
enum Op {
    Add,
    Sub,
    Mul,
    Div
}

#[derive(Clone)]
enum Job {
    Number(i64),
    Operation(String, String, Op)
}

impl Job {
    pub fn parse(line: &str) -> Job {
        let job_int = line.parse::<i64>();
        if job_int.is_ok() {
            Job::Number(job_int.unwrap())
        }
        else {
            Job::Operation(line[0..4].to_string(), line[7..].to_string(), match &line[5..6] {
                "+" => Ok(Op::Add),
                "-" => Ok(Op::Sub),
                "*" => Ok(Op::Mul),
                "/" => Ok(Op::Div),
                _   => Err("Unknown op")
            }.unwrap())
        }
    }
}

fn maybe_add_values<'a>(values: &mut std::collections::HashMap::<&'a str, i64>, monkey: &'a str, job: &Job, number_condition: fn(&str) -> bool) -> bool {
    if !values.contains_key(monkey) {
        match job {
            Job::Number(v) => {
                if number_condition(monkey) {
                    values.insert(monkey, *v);
                    return true;
                }
            }
            Job::Operation(l, r, op) => {
                let op_fn: fn(i64, i64) -> i64 = match op {
                    Op::Add => std::ops::Add::add,
                    Op::Sub => std::ops::Sub::sub,
                    Op::Mul => std::ops::Mul::mul,
                    Op::Div => std::ops::Div::div
                };
                if values.contains_key(l.as_str()) && values.contains_key(r.as_str()) {
                    values.insert(monkey, op_fn(*values.get(l.as_str()).unwrap(), *values.get(r.as_str()).unwrap()));
                    return true;
                }
            }
        }
    }
    false
}

fn part1(jobs: &std::collections::HashMap<String, Job>) {
    let mut values = std::collections::HashMap::<&str, i64>::new();

    while !values.contains_key("root") {
        jobs.iter().for_each(|(monkey, job)| { maybe_add_values(&mut values, monkey, job, |_| true); });
    }

    println!("{}", values.get("root").unwrap());
}

fn part2(jobs: &std::collections::HashMap<String, Job>) {
    let root_job = jobs.get("root").unwrap().clone();

    let root_left:  String;
    let root_right: String;

    match root_job {
        Job::Operation(l, r, _op) => {
            root_left  = l;
            root_right = r;
        },
        Job::Number(_v) => panic!("root job should be an operation")
    }

    let mut values = std::collections::HashMap::<&str, i64>::new();
    loop {
        if !jobs.iter().fold(false, |ret, (monkey, job)| maybe_add_values(&mut values, monkey, job, |monkey| monkey != "humn") || ret) {
            break;
        }
    }

    assert!(values.get(root_left.as_str()).is_none() != values.get(root_right.as_str()).is_none());

    let (mut changing_name, mut unchanging_value) = {
        let [cn, uv] = values.get(root_left.as_str()).map_or_else(|| [&root_left, &root_right], |_| [&root_right, &root_left]);
        (cn.as_str(), *values.get(uv.as_str()).unwrap())
    };

    while changing_name != "humn" {
        match jobs.get(changing_name).unwrap() {
            Job::Operation(l, r, op) => {
                assert!(values.get(l.as_str()).is_none() != values.get(r.as_str()).is_none());
                if values.get(l.as_str()).is_some() {
                    let other        = *values.get(l.as_str()).unwrap();
                    changing_name    = r.as_str();
                    unchanging_value = match op {
                        Op::Add => unchanging_value - other,
                        Op::Sub => other - unchanging_value,
                        Op::Mul => unchanging_value / other,
                        Op::Div => other / unchanging_value,
                    };
                }
                else {
                    let other        = *values.get(r.as_str()).unwrap();
                    changing_name    = l.as_str();
                    unchanging_value = match op {
                        Op::Add => unchanging_value - other,
                        Op::Sub => unchanging_value + other,
                        Op::Mul => unchanging_value / other,
                        Op::Div => unchanging_value * other,
                    };
                }
            }
            Job::Number(_v) => {
                panic!("Job should not be a number");
            }
        }
    }
    println!("{}", unchanging_value);
}

pub fn run() {
    fn parse_line(line: &str) -> (String, Job) {
        let (name, job_str) = line.split_once(": ").unwrap();

        (String::from(name), Job::parse(job_str))
    }

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec21.txt").unwrap());

    let jobs = reader.lines().filter_map(Result::ok)
                             .map(|line| parse_line(line.as_str()))
                             .collect::<std::collections::HashMap<String, Job>>();

    let current = std::time::Instant::now();
    part1(&jobs);
    let post_p1 = std::time::Instant::now();
    part2(&jobs);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}