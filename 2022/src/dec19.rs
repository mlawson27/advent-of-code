use std::io::BufRead;

#[repr(i32)]
#[derive(Clone, Copy, PartialEq)]
enum Resource {
    Ore,
    Clay,
    Obsidian,
    Geode
}

type Robot = (Resource, Vec<(Resource, u16)>);

fn get_quality_level(blueprint: &Vec<Robot>, time: u8) -> usize {
    let mut current_states = std::collections::VecDeque::<(u8, [(u16, u16); 4])>::new();
    current_states.push_back((0, [(1u16, 0u16), (0u16, 0u16), (0u16, 0u16), (0u16, 0u16)]));

    let mut max_geodes = 0;

    let mut max_requirements = [0u16; 4];
    for (_, robot) in blueprint.iter() {
        for (robot_type, count) in robot {
            max_requirements[*robot_type as usize] = max_requirements[*robot_type as usize].max(*count);
        }
    }

    while !current_states.is_empty() {
        let (time_so_far, num_robots_and_resources) = current_states.pop_front().unwrap();

        if time_so_far == time {
            max_geodes = max_geodes.max(num_robots_and_resources[Resource::Geode as usize].1);
            continue;
        }

        let mut new_num_robots_and_resources = num_robots_and_resources.clone();

        for (num_robots, resource_count) in new_num_robots_and_resources.iter_mut() {
            *resource_count += *num_robots;
        }

        if blueprint.last().unwrap().1.iter().all(|&(resource, count)| num_robots_and_resources[resource as usize].1 >= count) {
            let mut local_new_num_robots_and_resources = new_num_robots_and_resources.clone();
            local_new_num_robots_and_resources[Resource::Geode as usize].0 += 1;
            for (resource, count) in blueprint.last().unwrap().1.iter() {
                local_new_num_robots_and_resources[*resource as usize].1 -= count;
            }
            current_states.push_back((time_so_far + 1, local_new_num_robots_and_resources));
            continue;
        }

        for (robot_type, robot_requirements) in blueprint.iter().rev().skip(1) {
            if robot_requirements.iter().all(|&(resource, count)| num_robots_and_resources[resource as usize].1 >= count) &&
                num_robots_and_resources[*robot_type as usize].0 < max_requirements[*robot_type as usize] {
                let mut local_new_num_robots_and_resources = new_num_robots_and_resources.clone();
                local_new_num_robots_and_resources[*robot_type as usize].0 += 1;
                for (resource, count) in robot_requirements {
                    local_new_num_robots_and_resources[*resource as usize].1 -= count;
                }
                current_states.push_back((time_so_far + 1, local_new_num_robots_and_resources));
            }
        }

        if num_robots_and_resources[0..3].iter().zip(max_requirements[0..3].iter()).all(|((_num_robots, count), max_count)| *count < (*max_count * 3 + 1) / 2) {
            current_states.push_back((time_so_far + 1, new_num_robots_and_resources));
        }
    }
    max_geodes as usize
}

fn part1(blueprints: &Vec<Vec<Robot>>) {
    println!("{}", blueprints.iter().enumerate()
                                    .map(|(i, blueprint)| (i + 1) * get_quality_level(blueprint, 24))
                                    .sum::<usize>());
}

fn part2(blueprints: &Vec<Vec<Robot>>) {
    println!("{}", blueprints[0..3].iter().map(|blueprint| get_quality_level(blueprint, 32)).product::<usize>());
}

pub fn run() {
    fn robot_from_captures(captures: &regex::Captures) -> Robot {
        fn name_to_resource(name: &str) -> Resource {
            match name {
                "ore"      => Ok(Resource::Ore),
                "clay"     => Ok(Resource::Clay),
                "obsidian" => Ok(Resource::Obsidian),
                "geode"    => Ok(Resource::Geode),
                _          => Err("No matching resource")
            }.unwrap()
        }

        let mut required_resources = Vec::<(Resource, u16)>::new();
        for i in (2..captures.len()).step_by(2) {
            if captures.get(i).is_some() && captures.get(i + 1).is_some() {
                required_resources.push((name_to_resource(captures.get(i + 1).unwrap().as_str()),
                                         captures.get(i).unwrap().as_str().parse::<u16>().unwrap()))
            }
        }
        (name_to_resource(captures.get(1).unwrap().as_str()), required_resources)
    }

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec19.txt").unwrap());


    let robot_regex = regex::Regex::new(r"Each\s+(\w+)\s+robot\s+costs\s+(\d+)\s+(\w+)(?:\s+and\s+(\d+)\s+(\w+))?.").unwrap();

    let mut blueprints = reader.lines().filter_map(Result::ok)
                                       .map(|line| robot_regex.captures_iter(line.as_str()).map(|c| robot_from_captures(&c)).collect::<Vec<Robot>>())
                                       .collect::<Vec<Vec<Robot>>>();

    for robot_list in blueprints.iter_mut() {
        robot_list.sort_by(|r1, r2| (r1.0 as i32).cmp(&(r2.0 as i32)));
    }

    let current = std::time::Instant::now();
    part1(&blueprints);
    let post_p1 = std::time::Instant::now();
    part2(&blueprints);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}