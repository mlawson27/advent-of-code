use std::io::BufRead;

fn part1(stacks: &Vec<Vec<char>>, moves: &Vec<(usize, usize, usize)>) {
    let mut stacks_mut = stacks.clone();
    for &(num, from_plus_1, to_plus_1) in moves {
        for _i in 0..num {
            let top_item = stacks_mut[from_plus_1 - 1].pop().unwrap();
            stacks_mut[to_plus_1 - 1].push(top_item);
        }
    }

    println!("{}", String::from_iter(stacks_mut.iter().map(|stack| stack.last().unwrap())));
}

fn part2(stacks: &Vec<Vec<char>>, moves: &Vec<(usize, usize, usize)>) {
    let mut stacks_mut = stacks.clone();
    for &(num, from_plus_1, to_plus_1) in moves {
        let from_len  = stacks_mut[from_plus_1 - 1].len();
        let top_items = stacks_mut[from_plus_1 - 1].split_off(from_len - num);
        stacks_mut[to_plus_1 - 1].extend(top_items);
    }

    println!("{}", String::from_iter(stacks_mut.iter().map(|stack| stack.last().unwrap())));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec05.txt").unwrap());

    let stacks : Vec<Vec<char>>;
    let moves: Vec<(usize, usize, usize)>;
    {
        let mut line_iter = reader.lines().map(|l| l.unwrap());

        let mut stacks_lines     = line_iter.by_ref().take_while(|line| !line.is_empty()).collect::<Vec<String>>();
        let     longest_line_len = stacks_lines.iter().map(|line| line.len()).max().unwrap();

        stacks_lines.pop();

        stacks = (0..(longest_line_len + 1) / 4).map(|v|stacks_lines.iter().rev()
                                                                           .map(|line| line.chars().nth((v * 4) + 1))
                                                                           .filter_map(|c_opt| c_opt.and_then(|c| if c.is_whitespace() { None } else { Some(c) }))
                                                                           .collect::<Vec<char>>())
                                                .collect();

        let move_regex = regex::Regex::new(r"move\s+(\d+)\s+from\s+(\d+)\s+to\s+(\d+)").unwrap();
        moves = line_iter.by_ref().map(|line| match move_regex.captures(line.as_str()).unwrap().iter()
                                                                                               .skip(1)
                                                                                               .map(|c| c.unwrap().as_str().parse::<usize>().unwrap())
                                                                                               .collect::<Vec<usize>>()[..] {
                                                                    				         [num, from, to] => Ok((num, from, to)),
                                                                    					   _ => Err("Bad")
                                                                                               }.unwrap()).collect();
    }
    let current = std::time::Instant::now();
    part1(&stacks, &moves);
    let post_p1 = std::time::Instant::now();
    part2(&stacks, &moves);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}