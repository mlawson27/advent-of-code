use std::io::BufRead;

fn simulate_rope<const NUM_POSITIONS: usize>(moves: &Vec<(char, u32)>) -> usize {
    let mut x_poses = [[0i32; 2]; NUM_POSITIONS];

    let mut unique_positions = std::collections::HashSet::<[i32; 2]>::new();
    unique_positions.insert(*x_poses.last().unwrap());

    fn do_move_single(existing_pos: [i32; 2], dir_char: char) -> [i32; 2] {
        match dir_char {
            'R' => Ok([existing_pos[0] + 1, existing_pos[1]]),
            'L' => Ok([existing_pos[0] - 1, existing_pos[1]]),
            'U' => Ok([existing_pos[0], existing_pos[1] - 1]),
            'D' => Ok([existing_pos[0], existing_pos[1] + 1]),
            _ => Err("Bad")
        }.unwrap()
    }

    for &(dir_char, amount) in moves {
        for _ in 0..amount {
            x_poses[0] = do_move_single(x_poses[0], dir_char);
            for i in 1..NUM_POSITIONS {
                if x_poses[i - 1].iter().zip(x_poses[i].iter()).any(|(&pref_coord, &curr_coord)| pref_coord.abs_diff(curr_coord) > 1) {
                    let last_x_pos = x_poses[i - 1];
                    let curr_x_pos = x_poses[i].as_mut();
                    last_x_pos.iter().zip(curr_x_pos.iter_mut()).for_each(|(&prev_coord, curr_coord)| {
                        *curr_coord += prev_coord.cmp(&curr_coord) as i32;
                    });
                    if i == NUM_POSITIONS - 1 {
                        unique_positions.insert(x_poses[NUM_POSITIONS - 1]);
                    }
                }
                else {
                    break;
                }
            }
        }
    }
    unique_positions.len()
}

fn part1(moves: &Vec<(char, u32)>) {
    println!("{}", simulate_rope::<2>(moves));
}

fn part2(moves: &Vec<(char, u32)>) {
    println!("{}", simulate_rope::<10>(moves));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec09.txt").unwrap());

    let moves = reader.lines().map(|line_or_err| line_or_err.unwrap())
                                                .map(|line| (line.chars().next().unwrap(), line[2..].parse::<u32>().unwrap()))
                                                .collect::<Vec<(char, u32)>>();

    let current = std::time::Instant::now();
    part1(&moves);
    let post_p1 = std::time::Instant::now();
    part2(&moves);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}