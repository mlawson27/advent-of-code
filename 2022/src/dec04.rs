use std::io::BufRead;

fn part1(sets: &std::vec::Vec<(std::collections::HashSet<i32>, std::collections::HashSet<i32>)>) {
    println!("{}", sets.iter().map(|(l, r)| (l.is_superset(&r) || r.is_superset(&l)) as i32).sum::<i32>())
}

fn part2(sets: &std::vec::Vec<(std::collections::HashSet<i32>, std::collections::HashSet<i32>)>) {
    println!("{}", sets.iter().map(|(l, r)| (!l.intersection(&r).next().is_none()) as i32).sum::<i32>())
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec04.txt").unwrap());

    let sets =
        reader.lines().map(|line_or_err| match line_or_err.unwrap().splitn(4, |c| c == ',' || c == '-')
            .map(|s| s.parse::<i32>().unwrap())
            .collect::<Vec<i32>>()[..] {
                [a, b, c, d] => Ok((std::collections::HashSet::from_iter(a..b+1), std::collections::HashSet::from_iter(c..d+1))),
                _ => Err("Bad")
            }.unwrap()).collect::<Vec<(std::collections::HashSet<i32>, std::collections::HashSet<i32>)>>();

    let current = std::time::Instant::now();
    part1(&sets);
    let post_p1 = std::time::Instant::now();
    part2(&sets);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}