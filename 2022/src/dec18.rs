use std::io::BufRead;

fn faces_for_coordinate(coord: &(i32, i32, i32)) -> [(i32, i32, i32); 6] {
    [(coord.0 - 1, coord.1,     coord.2),
     (coord.0 + 1, coord.1,     coord.2),
     (coord.0,     coord.1 - 1, coord.2),
     (coord.0,     coord.1 + 1, coord.2),
     (coord.0,     coord.1,     coord.2 - 1),
     (coord.0,     coord.1,     coord.2 + 1)]
}

fn part1(coordinates: &Vec<(i32, i32, i32)>) {
    let coordinate_set = std::collections::HashSet::<&(i32, i32, i32)>::from_iter(coordinates.iter());

    println!("{}", coordinates.iter().flat_map(faces_for_coordinate)
                                     .filter(|face| !coordinate_set.contains(face))
                                     .count());
}

fn part2(coordinates: &Vec<(i32, i32, i32)>) {
    let coordinate_set = std::collections::HashSet::<&(i32, i32, i32)>::from_iter(coordinates.iter());

    let (min_x, min_y, min_z) = coordinates.iter().fold((i32::MAX, i32::MAX, i32::MAX), |working, coord| (working.0.min(coord.0), working.1.min(coord.1), working.2.min(coord.2)));
    let (max_x, max_y, max_z) = coordinates.iter().fold((i32::MIN, i32::MIN, i32::MIN), |working, coord| (working.0.max(coord.0), working.1.max(coord.1), working.2.max(coord.2)));

    let exposed_faces = coordinates.iter().flat_map(faces_for_coordinate)
                                          .filter(|face| !coordinate_set.contains(face))
                                          .collect::<Vec<(i32, i32, i32)>>();

    let mut exposed_face_count        = 0;
    let mut known_blocked_faces_set   = std::collections::HashSet::<(i32, i32, i32)>::new();
    let mut known_unblocked_faces_set = std::collections::HashSet::<(i32, i32, i32)>::new();
    for face in exposed_faces {
        let mut pos_queue            = std::collections::VecDeque::<(i32, i32, i32)>::new();
        let mut local_seen_faces_set = std::collections::HashSet::<(i32, i32, i32)>::new();
        pos_queue.push_back(face);

        let mut found = false;
        while !pos_queue.is_empty() {
            let working_position = pos_queue.pop_front().unwrap();

            if known_unblocked_faces_set.contains(&working_position) ||
               working_position.0 < min_x || working_position.1 < min_y || working_position.2 < min_z ||
               working_position.0 > max_x || working_position.1 > max_y || working_position.2 > max_z {
                found = true;
                break;
            }

            if known_blocked_faces_set.contains(&working_position) {
                break;
            }

            for next in faces_for_coordinate(&working_position) {
                if !local_seen_faces_set.contains(&next) && !coordinates.contains(&next) {
                    pos_queue.push_back(next);
                    local_seen_faces_set.insert(next);
                }
            }
        }

        if found {
            exposed_face_count += 1;
            known_unblocked_faces_set.extend(local_seen_faces_set);
        }
        else {
            known_blocked_faces_set.extend(local_seen_faces_set);
        }
    }

    println!("{}", exposed_face_count);
}

pub fn run() {
    fn to_coordinates<T: Iterator<Item = i32>>(mut it: T) -> (i32, i32, i32) {
        (it.next().unwrap(), it.next().unwrap(), it.next().unwrap())
    }

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec18.txt").unwrap());

    let coordinates = reader.lines().filter_map(Result::ok)
                                    .map(|line| to_coordinates(line.split(",").map(|token| token.parse::<i32>().unwrap())))
                                    .collect::<Vec<(i32, i32, i32)>>();

    let current = std::time::Instant::now();
    part1(&coordinates);
    let post_p1 = std::time::Instant::now();
    part2(&coordinates);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}