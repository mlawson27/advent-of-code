use std::io::BufRead;

enum Operand {
    Value(u32),
    Existing
}

struct Monkey {
    items: Vec<u32>,
    operation: fn(u64, u64) -> u64,
    right_operand: Operand,
    test: u32,
    give_if_true: u32,
    give_if_false: u32
}

impl Clone for Operand {
    fn clone(&self) -> Self {
        match self {
            Operand::Existing => Operand::Existing,
            Operand::Value(v) => Operand::Value(*v)
        }
    }
}

impl Clone for Monkey {
    fn clone(&self) -> Self {
        Monkey { items:         self.items.clone(),
                 operation:     self.operation,
                 right_operand: self.right_operand.clone(),
                 test:          self.test,
                 give_if_true:  self.give_if_true,
                 give_if_false: self.give_if_false }
    }
}

fn perform_rounds(monkeys: &Vec<Monkey>, num_rounds: u32, worry_level: Option<u32>) -> Vec<usize> {
    let mut monkeys = monkeys.clone();

    let mut inspected_counts = (0..monkeys.len()).map(|_| 0usize).collect::<Vec<usize>>();

    let ops_mult = monkeys.iter().map(|m| m.test as u64).product::<u64>();

    for _round in 0..num_rounds {
        for i in 0..monkeys.len() {
            let source_items = std::mem::replace(&mut monkeys[i].items, Vec::<u32>::new());
            inspected_counts[i] += source_items.len();
            for item in source_items {
                let mut item    = item;
                let mut item_64 = item as u64;
                item_64 = (monkeys[i].operation)(item_64, match monkeys[i].right_operand {
                    Operand::Existing => item_64,
                    Operand::Value(v) => v as u64
                });
                if worry_level.is_some() {
                    item = (item_64 as u32) / worry_level.unwrap()
                }
                else {
                    item = (item_64 % ops_mult) as u32;
                }
                let to_give_index = if (item % monkeys[i].test) == 0 { monkeys[i].give_if_true } else { monkeys[i].give_if_false } as usize;
                monkeys[to_give_index].items.push(item);
            }
        }
    }
    inspected_counts
}

fn part1(monkeys: &Vec<Monkey>) {
    let mut inspected_counts = perform_rounds(monkeys, 20, Some(3));
    inspected_counts.sort_by(|a, b| b.cmp(a));
    println!("{}", inspected_counts[0..2].iter().product::<usize>());
}

fn part2(monkeys: &Vec<Monkey>) {
    let mut inspected_counts = perform_rounds(monkeys, 10000, None);
    inspected_counts.sort_by(|a, b| b.cmp(a));
    println!("{}", inspected_counts[0..2].iter().product::<usize>());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec11.txt").unwrap());

    let mut monkeys = Vec::<Monkey>::new();
    let mut file_it = reader.lines();

    let items_regex     = regex::Regex::new(r"\d+").unwrap();
    let operation_regex = regex::Regex::new(r"Operation:\s+new\s+=\s+old\s+([*+])\s+((?:\d+)|(?:old))").unwrap();
    let test_regex      = regex::Regex::new(r"Test:\s+divisible\s+by\s+(\d+)").unwrap();
    let throw_to_regex  = regex::Regex::new(r"If\s+(?:(?:true)|(?:false)):\s+throw\s+to\s+monkey\s+(\d+)").unwrap();

    loop {
        let monkey_content = file_it.by_ref().skip(1)
                                             .map_while(|line_or_err| line_or_err.map_or(None::<String>, |line| if line.is_empty() { None::<String> } else { Some(line) }))
                                             .collect::<Vec<String>>();
        if monkey_content.is_empty() {
            break;
        }
        assert!(monkey_content.len() == 5);

        let operation_match = operation_regex.captures(monkey_content[1].as_str()).unwrap();

        monkeys.push(Monkey{ items:         items_regex.captures_iter(monkey_content[0].as_str())
                                                       .map(|c|c.get(0).unwrap().as_str().parse::<u32>().unwrap())
                                                       .collect::<Vec<u32>>(),
                             operation:     if operation_match.get(1).unwrap().as_str() == "+" { std::ops::Add::add } else { std::ops::Mul::mul },
                             right_operand: (|operand| match operand {
                                                 "old" => Operand::Existing,
                                                 _     => Operand::Value(operand.parse::<u32>().unwrap())
                                             })(operation_match.get(2).unwrap().as_str()),
                             test:          test_regex.captures(monkey_content[2].as_str()).unwrap().get(1).unwrap().as_str().parse::<u32>().unwrap(),
                             give_if_true:  throw_to_regex.captures(monkey_content[3].as_str()).unwrap().get(1).unwrap().as_str().parse::<u32>().unwrap(),
                             give_if_false: throw_to_regex.captures(monkey_content[4].as_str()).unwrap().get(1).unwrap().as_str().parse::<u32>().unwrap() });
    }

    let current = std::time::Instant::now();
    part1(&monkeys);
    let post_p1 = std::time::Instant::now();
    part2(&monkeys);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}