use std::io::BufRead;

fn find_min_num_moves(map: &Vec<Vec<char>>, starting_position: &(i32, i32), ending_position: &(i32, i32)) -> Option<usize> {
    fn to_elevation_char(c: char) -> char {
        match c {
            'E' => 'z',
            'S' => 'a',
            _ => c
        }
    }

    let mut seen_positions = std::collections::HashSet::<(i32, i32)>::new();

    let mut positions_queue = std::collections::VecDeque::<((i32, i32), usize)>::new();
    positions_queue.push_back((*starting_position, 0usize));

    while !positions_queue.is_empty() {
        let (current_position, moves_so_far) = positions_queue.pop_front().unwrap();

        if current_position == *ending_position {
            return Some(moves_so_far);
        }

        let current_elevation_char = to_elevation_char(map[current_position.1 as usize][current_position.0 as usize]);
        for adjustment in [(1i32, 0i32), (0, 1), (-1, 0), (0, -1)] {
            if (current_position.0 != 0 || adjustment.0 >= 0)                  &&
               (((current_position.0 + adjustment.0) as usize) < map[0].len()) &&
               (current_position.1 != 0 || adjustment.1 >= 0)                  &&
               (((current_position.1 + adjustment.1) as usize) < map.len()) {

                let new_position = (current_position.0 + adjustment.0, current_position.1 + adjustment.1);
                let new_elevation_char = to_elevation_char(map[new_position.1 as usize][new_position.0 as usize]);
                if new_elevation_char as u32 <= (current_elevation_char as u32) + 1 &&
                   !seen_positions.contains(&new_position) {
                    positions_queue.push_back((new_position, moves_so_far + 1));
                    seen_positions.insert(new_position);
                }
            }
        }
    }
    None
}

fn part1(map: &Vec<Vec<char>>, starting_position: &(i32, i32), ending_position: &(i32, i32)) {
    println!("{}", find_min_num_moves(map, starting_position, ending_position).unwrap());
}

fn part2(map: &Vec<Vec<char>>, ending_position: &(i32, i32)) {
    println!("{}", map.iter().enumerate()
                             .flat_map(|(row, line)| line.iter().enumerate().map(move |(col, &c)| ((col as i32, row as i32), c)))
                             .filter(|(_, c)| *c == 'a')
                             .filter_map(|(pos, _)| find_min_num_moves(map, &pos, ending_position))
                             .min().unwrap());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec12.txt").unwrap());

    let map = reader.lines().filter_map(Result::ok)
                                            .map(|line| line.chars().collect::<Vec<char>>())
                                            .collect::<Vec<Vec<char>>>();
    let starting_position: (i32, i32);
    let ending_position:   (i32, i32);
    {
        let mut positions_iter = map.iter().enumerate()
                                           .flat_map(|(row, line)| line.iter().enumerate()
                                                                              .map(move |(col, &c)| ((col as i32, row as i32), c)))
                                           .filter(|(_p, c)| c.is_uppercase());
        let mut positions = [positions_iter.next().unwrap(), positions_iter.next().unwrap()];
        positions.sort_by(|a, b| a.1.cmp(&b.1));

        let _ec: char;
        let _sc: char;
        [(ending_position, _ec), (starting_position, _sc)] = positions;
    }

    let current = std::time::Instant::now();
    part1(&map, &starting_position, &ending_position);
    let post_p1 = std::time::Instant::now();
    part2(&map, &ending_position);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}