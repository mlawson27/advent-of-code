use std::io::BufRead;

enum Packet {
    Value(i32),
    SubPacket(Vec<Packet>)
}

impl Packet {
    fn cmp_vec<'a, L: ExactSizeIterator<Item = &'a Packet>, R: ExactSizeIterator<Item = &'a Packet>>(l: L, r: R) -> std::cmp::Ordering {
        let     default = l.len().cmp(&r.len());
        let mut l_mod   = l;
        let mut r_mod   = r;
        for (l_inner, r_inner) in l_mod.by_ref().zip(r_mod.by_ref()) {
            let inner_cmp = l_inner.cmp(r_inner);
            if inner_cmp != std::cmp::Ordering::Equal {
                return inner_cmp
            }
        }
        default
    }

    fn cmp(&self, other: &Packet) -> std::cmp::Ordering {
        match self {
            Packet::SubPacket(self_packets) => match other {
                Packet::SubPacket(other_packets) => Self::cmp_vec(self_packets.iter(), other_packets.iter()),
                Packet::Value(_)                 => Self::cmp_vec(self_packets.iter(), std::iter::once(other))
            }
            Packet::Value(self_value) => match other {
                Packet::SubPacket(other_packets) => Self::cmp_vec(std::iter::once(self), other_packets.iter()),
                Packet::Value(other_value)       => self_value.cmp(other_value)
            }
        }
    }
}

impl PartialEq for Packet {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == std::cmp::Ordering::Equal
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

fn part1(packets: &Vec<(Packet, Packet)>) {
    println!("{}", packets.iter().enumerate()
                                 .filter(|(_, (p1, p2))| p1 <= p2)
                                 .map(|(i, _)| i + 1)
                                 .sum::<usize>());
}

fn part2(packets: &Vec<(Packet, Packet)>) {
    let new_packets = [Packet::SubPacket(vec![Packet::SubPacket(vec![Packet::Value(2)])]),
                       Packet::SubPacket(vec![Packet::SubPacket(vec![Packet::Value(6)])])];

    let mut packets = packets.iter().flat_map(|(p1, p2)| std::iter::once(p1).chain(std::iter::once(p2)))
                                    .chain(new_packets.iter())
                                    .collect::<Vec<&Packet>>();

    packets.sort_by(|p1, p2| (*p1).cmp(*p2));

    println!("{}", packets.iter().enumerate().filter(|(_, p)| new_packets.contains(p)).map(|(i, _)| i + 1).product::<usize>())
}

pub fn run() {
    fn parse_packet_line(line: &str) -> Packet {
        fn helper(line: &str, i: &mut usize) -> Result<Packet, &'static str> {
            let mut int_len     = 0;
            let mut this_result = Vec::<Packet>::new();
            while *i < line.len() {
                let c = line.as_bytes()[*i] as char;
                match c {
                    '[' => {
                        *i += 1;
                        this_result.push(helper(line, i).unwrap());
                    },
                    '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9' => int_len += 1,
                    ']'|',' => {
                        if int_len > 0 {
                            this_result.push(Packet::Value(line[*i - int_len..*i].parse::<i32>().unwrap()));
                            int_len = 0;
                        }
                        if c == ']' {
                            return Ok(Packet::SubPacket(this_result))
                        }
                    },
                    _ => continue
                }
                *i += 1
            }
            this_result.pop().map_or(Err("No data"), |p| Ok(p))
        }

        let mut i = 0;
        helper(line, &mut i).unwrap()
    }

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec13.txt").unwrap());

    let mut lines_it = reader.lines();
    let mut packets: Vec<(Packet, Packet)> = Vec::new();
    loop {
        let mut vals = lines_it.by_ref().map_while(|line_or_err| line_or_err.map_or(None::<String>, |line| if line.is_empty() { None } else { Some(line) }))
                                        .map(|line| parse_packet_line(line.as_str()));

        let first = vals.next();
        if first.is_none() {
            break;
        }

        packets.push((first.unwrap(), vals.next().unwrap()));
        lines_it.next();
    }

    let current = std::time::Instant::now();
    part1(&packets);
    let post_p1 = std::time::Instant::now();
    part2(&packets);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}