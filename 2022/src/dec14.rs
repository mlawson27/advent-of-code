use std::io::BufRead;

fn get_min_num_moves(board: &mut Vec<Vec<char>>, drop_x: i32) -> usize {
    let mut moves_so_far = 0usize;

    'outer: loop {
        let mut sand_point = (drop_x, 0i32);

        loop {
            let     new_y     = sand_point.1 + 1;
            let mut any_point = false;

            if new_y as usize >= board.len() {
                break 'outer;
            }

            for x_adjustment in [0, -1, 1] {
                let test_point = (sand_point.0 + x_adjustment, new_y);
                if test_point.0 < 0 || test_point.0 as usize > board[test_point.1 as usize].len() {
                    break 'outer;
                }

                if board[test_point.1 as usize][test_point.0 as usize] == '.' {
                    sand_point = test_point;
                    any_point  = true;
                    break;
                }
            }
            if !any_point {
                if sand_point == (drop_x, 0i32) {
                    moves_so_far += 1;
                    break 'outer;
                }
                board[sand_point.1 as usize][sand_point.0 as usize] = 'o';
                break;
            }
        }

        moves_so_far += 1;
    }
    moves_so_far
}

fn part1(board: &Vec<Vec<char>>, drop_x: i32) {
    let mut board = board.clone();
    println!("{}", get_min_num_moves(&mut board, drop_x));
}

fn part2(board: &Vec<Vec<char>>, drop_x: i32) {
    let     data_to_insert = vec!['.'; board.len()];
    let mut board          = board.iter().map(|row| data_to_insert.iter().chain(row.iter()).chain(data_to_insert.iter()).map(|&c| c).collect()).collect::<Vec<Vec<char>>>();
    let     drop_x         = drop_x + (board.len() as i32);
    let     row_len        = board[0].len();

    board.extend(['.', '#'].iter().map(|c| vec![*c; row_len]));

    println!("{}", get_min_num_moves(&mut board, drop_x));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec14.txt").unwrap());

    let line_coordinates = reader.lines().filter_map(|line_or_err| line_or_err.ok())
                                         .map(|line| line.split(" -> ").map(|token| token.split(',').map(|sub_token| sub_token.parse::<i32>().unwrap()))
                                                                       .map(|mut parts| (parts.next().unwrap(), parts.next().unwrap()))
                                                                       .collect::<Vec<(i32, i32)>>())
                                         .collect::<Vec<Vec<(i32, i32)>>>();

    let x_min = line_coordinates.iter().flat_map(|cs| cs.iter().map(|p| p.0))
                                       .min().unwrap();

    let (x_max, y_max) = line_coordinates.iter().flat_map(|cs| cs.iter())
                                                .fold((0, 0), |accum_c, working_c| (accum_c.0.max(working_c.0), accum_c.1.max(working_c.1)));

    let mut board = (0..y_max+1).map(|_| (x_min..x_max+1).map(|_| '.').collect::<Vec<char>>()).collect::<Vec<Vec<char>>>();

    line_coordinates.iter().for_each(|line| line.windows(2).for_each(|pair| (pair[0].0.min(pair[1].0)..pair[0].0.max(pair[1].0)+1).for_each(|x| (pair[0].1.min(pair[1].1)..pair[0].1.max(pair[1].1)+1).for_each(|y| board[y as usize][(x - x_min) as usize] = '#'))));

    let current = std::time::Instant::now();
    part1(&board, 500 - x_min);
    let post_p1 = std::time::Instant::now();
    part2(&board, 500 - x_min);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}