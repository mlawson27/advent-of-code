use std::io::Read;

const ROCK_0: ([u8; 1], usize) = ([0b1111], 4);

const ROCK_1: ([u8; 3], usize) = ([0b010,
                                   0b111,
                                   0b010], 3);

const ROCK_2: ([u8; 3], usize) = ([0b001,
                                   0b001,
                                   0b111], 3);

const ROCK_3: ([u8; 4], usize) = ([1,
                                   1,
                                   1,
                                   1], 1);

const ROCK_4: ([u8; 2], usize) = ([0b11,
                                   0b11], 2);

fn get_tower_size(jet_stream: &str, num_rocks: usize) -> usize {
    let mut room = std::collections::VecDeque::<u8>::new();

    let     stream_vec = jet_stream.chars().collect::<Vec<char>>();
    let mut stream_i   = 0usize;

    let mut heights       = Vec::<usize>::new();
    let mut height_deltas = Vec::<u8>::new();

    for rock_i in 0..num_rocks {
        let (rock, rock_len) = match rock_i % 5 {
            0 => Ok((Vec::from(ROCK_0.0), ROCK_0.1)),
            1 => Ok((Vec::from(ROCK_1.0), ROCK_1.1)),
            2 => Ok((Vec::from(ROCK_2.0), ROCK_2.1)),
            3 => Ok((Vec::from(ROCK_3.0), ROCK_3.1)),
            4 => Ok((Vec::from(ROCK_4.0), ROCK_4.1)),
            _ => Err("Bad")
        }.unwrap();

        let (right, top) = (|| {
            let mut right = 5 - rock_len;
            for fall_amt in (0..=room.len() + 3).rev() {
                let shift = if stream_vec[stream_i] == '<' { -1 } else { 1 };
                stream_i  = (stream_i + 1) % stream_vec.len();

                if (right > 0 && shift > 0) || (right < 7 - rock_len && shift < 0) {
                    let     new_right = (right as i32 - shift) as usize;
                    let mut blocked   = false;
                    if room.len() > 0 && fall_amt < room.len() {
                        for (rock_i, room_i) in (0..rock.len()).rev().zip((0..room.len() - fall_amt).rev()) {
                            if room[room_i] & (rock[rock_i] << new_right) != 0 {
                                blocked = true;
                                break;
                            }
                        }
                    }
                    if !blocked {
                        right = new_right;
                    }
                }

                if fall_amt > 0 && fall_amt <= room.len() {
                    for (rock_i, room_i) in (0..rock.len()).rev().zip((0..=room.len() - fall_amt).rev()) {
                        if room[room_i] & (rock[rock_i] << right) != 0 {
                            return (right, if room.len() > fall_amt { Some(room.len() - fall_amt - 1) } else { None });
                        }
                    }
                }
            }
            return (right, if room.len() > 0 { Some(0) } else { None });
        })();

        let curr_height = room.len();

        let mut rock_it = rock.iter().rev();
        if top.is_some() {
            for (room_line, &rock_line) in room.iter_mut().take(top.unwrap() + 1).rev().zip(rock_it.by_ref()) {
                *room_line |= rock_line << right;
            }
        }

        for &line in rock_it {
            room.push_front(line << right);
        }

        heights.push(room.len());
        height_deltas.push((room.len() - curr_height) as u8);

        if height_deltas.len() >= 5000 {
            let mut diffs      = [0usize; 3];
            let mut diff_count = 0;
            for i in 0..height_deltas.len() - 1000 {
                if height_deltas[i..i+1000].iter().eq(height_deltas[height_deltas.len() - 1000..height_deltas.len()].iter()) {
                    diffs[diff_count] = i;
                    diff_count += 1;
                    if diff_count == diffs.len() {
                        if (0..diffs.len()-1).map(|i| heights[diffs[i + 1]] - heights[diffs[i]]).any(|diff| diff != heights[diffs[1]] - heights[diffs[0]]) {
                            break;
                        }
                        let i_diff = diffs[1] - diffs[0];
                        return heights[(num_rocks - 1) % i_diff] + (heights[diffs[1]] - heights[diffs[0]]) * (num_rocks / i_diff);
                    }
                }
            }
        }
    }

    room.len()
}

fn part1(jet_stream: &str, num_rocks: usize) {
    println!("{}", get_tower_size(jet_stream, num_rocks));
}

fn part2(jet_stream: &str, num_rocks: usize) {
    println!("{}", get_tower_size(jet_stream, num_rocks));
}

pub fn run() {
    let mut file = std::fs::File::open("input//dec17.txt").unwrap();

    let mut jet_stream: String = String::new();
    file.read_to_string(&mut jet_stream).unwrap();

    let current = std::time::Instant::now();
    part1(&jet_stream.as_str(), 2022);
    let post_p1 = std::time::Instant::now();
    part2(&jet_stream.as_str(), 1000000000000);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}