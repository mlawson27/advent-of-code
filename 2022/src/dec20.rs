use std::io::BufRead;

fn do_decrypt(numbers: &Vec<i64>, num_iterations: usize) -> i64 {
    let mut numbers_new = numbers.iter().enumerate().map(|(i, v)| (i, *v)).collect::<Vec::<(usize, i64)>>();

    for _i in 0..num_iterations {
        for (orig_i, value) in numbers.iter().enumerate() {
            let i     = numbers_new.iter().position(|&v| v == (orig_i, *value)).unwrap();
            let new_i = ((i as i64) + *value).rem_euclid(numbers_new.len() as i64 - 1) as usize;
            if i != new_i {
                numbers_new.remove(i);
                numbers_new.insert(new_i as usize, (orig_i, *value));
            }
        }
    }

    let pos_of_0 = numbers_new.iter().position(|&v| v.1 == 0).unwrap();

    [1000, 2000, 3000].iter().map(|v| numbers_new[(pos_of_0 + v) % numbers_new.len()].1).sum::<i64>()
}

fn part1(numbers: &Vec<i64>) {
    println!("{}", do_decrypt(numbers, 1));
}

fn part2(numbers: &Vec<i64>, decryption_key: i64) {
    let numbers = numbers.iter().map(|v| *v * decryption_key).collect::<Vec<i64>>();
    println!("{}", do_decrypt(&numbers, 10));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec20.txt").unwrap());

    let numbers = reader.lines().filter_map(Result::ok)
                                .map(|line| line.parse::<i64>().unwrap())
                                .collect::<Vec<i64>>();

    let current = std::time::Instant::now();
    part1(&numbers);
    let post_p1 = std::time::Instant::now();
    part2(&numbers, 811589153);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}