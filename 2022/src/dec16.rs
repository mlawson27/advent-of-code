use std::{io::BufRead, collections::HashMap};

fn part1(valve_info: &HashMap<String, (i32, Vec<String>)>, time: i32) {
    let mut current_states = std::collections::VecDeque::<(&str, i32, i32, i32, String)>::new();

    current_states.push_back(("AA", 1, 0, 0, String::from("")));

    let mut max_flow = 0;

    let mut seen_states = std::collections::HashSet::<(&str, String)>::new();

    while !current_states.is_empty() {
        let (current_valve, time_so_far, total_flow, flow_per_t, open_valves) = current_states.pop_front().unwrap();

        if time_so_far == time {
            max_flow = max_flow.max(total_flow + flow_per_t);
            continue;
        }

        if !open_valves.contains(current_valve) && valve_info[current_valve].0 != 0 {
            let mut new_open_valves = open_valves.clone();
            if !new_open_valves.is_empty() {
                new_open_valves.push_str(",")
            }
            new_open_valves.push_str(current_valve);
            current_states.push_back((current_valve, time_so_far + 1, total_flow + flow_per_t, flow_per_t + valve_info[current_valve].0, new_open_valves));
        }
        for other_valve in valve_info[current_valve].1.iter() {
            if seen_states.contains(&(other_valve.as_str(), open_valves.clone())) {
                continue
            }

            seen_states.insert((other_valve, open_valves.clone()));
            current_states.push_back((other_valve, time_so_far + 1, total_flow + flow_per_t, flow_per_t, open_valves.clone()));
        }
    }

    println!("{}", max_flow);
}

fn part2(valve_info: &HashMap<String, (i32, Vec<String>)>, time: i32) {
    let time = time - 4;

    let mut current_states = std::collections::VecDeque::<(&str, &str, i32, i32, i32, String)>::new();

    current_states.push_back(("AA", "AA", 1, 0, 0, String::from("")));

    let mut max_flow = 0;

    let mut seen_states = std::collections::HashSet::<(&str, &str, String)>::new();

    let top_rates = {
        let mut working = valve_info.values().map(|v| v.0).collect::<Vec<i32>>();
        working.sort();
        working.drain(0..working.len() - 3);
        working[1] += working[0];
        working[2] += working[1];
        working
    };

    let num_non_zero_flow_values = valve_info.values().filter(|(v, _)| *v > 0).count();

    let intervals = [time / 4, 3 * time / 8, time / 2];

    'main_loop: while !current_states.is_empty() {
        let (current_valve, elephant_valve, time_so_far, total_flow, flow_per_t, open_valves) = current_states.pop_front().unwrap();

        if time_so_far == time {
            max_flow = max_flow.max(total_flow + flow_per_t);
            continue;
        }

        if open_valves.len() == num_non_zero_flow_values * 3 - 1 {
            max_flow = max_flow.max(total_flow + (time + 1 - time_so_far) * flow_per_t);
        }

        for (&interval, &rate) in intervals.iter().zip(top_rates.iter()) {
            if interval > time_so_far {
                break;
            }
            if flow_per_t < rate {
                continue 'main_loop;
            }
        }

        for my_other_valve in std::iter::once(current_valve).chain(valve_info[current_valve].1.iter().map(String::as_str)) {
            'inner: for elephant_other_valve in std::iter::once(elephant_valve).chain(valve_info[elephant_valve].1.iter().map(String::as_str)) {
                if my_other_valve == elephant_other_valve {
                    continue;
                }

                let mut new_open_valves = open_valves.clone();
                let mut new_flow_per_t     = flow_per_t;

                for (other_valve, this_valve) in [(my_other_valve, current_valve), (elephant_other_valve, elephant_valve)] {
                    if other_valve == this_valve {
                        if !new_open_valves.contains(other_valve) && valve_info[other_valve].0 != 0 {
                            if !new_open_valves.is_empty() {
                                new_open_valves.push_str(",")
                            }
                            new_open_valves.push_str(other_valve);
                            new_flow_per_t += valve_info[other_valve].0;
                        }
                        else {
                            continue 'inner;
                        }
                    }
                }

                if seen_states.contains(&(my_other_valve, elephant_other_valve, new_open_valves.clone())) ||
                   seen_states.contains(&(elephant_other_valve, my_other_valve, new_open_valves.clone())) {
                    continue
                }

                seen_states.insert((my_other_valve, elephant_other_valve, new_open_valves.clone()));
                current_states.push_back((my_other_valve, elephant_other_valve, time_so_far + 1, total_flow + flow_per_t, new_flow_per_t, new_open_valves));
            }
        }
    }

    println!("{}", max_flow);
}

pub fn run() {
    fn data_from_captures(captures: &regex::Captures) -> (String, (i32, Vec<String>)) {
        (captures.get(1).unwrap().as_str().to_string(),
         (captures.get(2).unwrap().as_str().parse::<i32>().unwrap(),
          captures.get(3).unwrap().as_str().split(", ").map(|n|n.to_string()).collect::<Vec<String>>()))
    }

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec16.txt").unwrap());


    let line_regex = regex::Regex::new(r"Valve\s+([A-Z]{2,4})\s+has\s+flow\s+rate=(\d+);\s+tunnel(?:s)?\s+lead(?:s)?\s+to\s+valve(?:s)?\s+((?:[A-Z]{2,4})(?:(?:\s*,\s*)(?:[A-Z]{2,4}))*)").unwrap();

    let valve_info = std::collections::HashMap::from_iter(reader.lines().filter_map(Result::ok).map(|line|data_from_captures(&line_regex.captures(line.as_str()).unwrap())));

    let current = std::time::Instant::now();
    part1(&valve_info, 30);
    let post_p1 = std::time::Instant::now();
    part2(&valve_info, 30);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}