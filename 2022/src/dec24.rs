use std::io::BufRead;

type BlizzardsMap = std::collections::HashMap<(i32, i32), u8>;

fn traverse_map(blizzards: &mut BlizzardsMap, starting_pos: &(i32, i32), ending_pos: &(i32, i32), max_x: usize, max_y: usize) -> usize {
    let mut states = std::collections::VecDeque::<(i32, i32, usize)>::new();
    states.push_back((starting_pos.0, starting_pos.1, 0));

    let mut seen_states       = std::collections::HashSet::<(i32, i32, usize)>::new();
    let mut last_moves_so_far = 1usize;
    loop {
        let (current_x, current_y, moves_so_far) = states.pop_front().unwrap();

        if (current_x, current_y) == *ending_pos {
            return moves_so_far
        }

        if moves_so_far != last_moves_so_far {
            let mut new_blizzards = BlizzardsMap::new();
            for ((blizzard_x, blizzard_y), mask) in blizzards.drain() {
                for (i, &(adj_x, adj_y)) in [(1, 0), (-1, 0), (0, 1), (0, -1)].iter().enumerate() {
                    if mask & (0b1 << i) != 0 {
                        *new_blizzards.entry(((blizzard_x + adj_x).rem_euclid(max_x as i32), (blizzard_y + adj_y).rem_euclid(max_y as i32))).or_insert(0b0000) |= 0b1 << i;
                    }
                }
            }
            *blizzards = new_blizzards;
        }

        for adjustment in [(0, 1), (1, 0), (0, -1), (-1, 0), (0, 0)] {
            let new_pos = (current_x + adjustment.0, current_y + adjustment.1);
            if (new_pos == *starting_pos ||
                new_pos == *ending_pos ||
                (new_pos.0 >= 0 && new_pos.0 < max_x as i32 && new_pos.1 >= 0 && new_pos.1 < max_y as i32)) &&
               !blizzards.contains_key(&new_pos) &&
               !seen_states.contains(&(new_pos.0, new_pos.1, moves_so_far + 1)) {
                states.push_back((new_pos.0, new_pos.1, moves_so_far + 1));
                seen_states.insert((new_pos.0, new_pos.1, moves_so_far + 1));
            }
        }
        last_moves_so_far = moves_so_far;
    }
}

fn part1(map: &Vec<String>, starting_pos: &(i32, i32), ending_pos: &(i32, i32)) {
    let mut blizzards = map.iter().enumerate().flat_map(|(y, line)| line.char_indices().filter_map(move |(x, c)| {
        let v = (((c == '^') as u8) << 3) | (((c == 'v') as u8) << 2) | (((c == '<') as u8) << 1) | (c == '>') as u8;
        if v != 0 {
            Some(((x as i32, y as i32), v))
        }
        else {
            None
        }})).collect::<BlizzardsMap>();
    println!("{}", traverse_map(&mut blizzards, starting_pos, ending_pos, map.first().unwrap().len(), map.len()));
}

fn part2(map: &Vec<String>, starting_pos: &(i32, i32), ending_pos: &(i32, i32)) {
    let mut blizzards = map.iter().enumerate().flat_map(|(y, line)| line.char_indices().filter_map(move |(x, c)| {
        let v = (((c == '^') as u8) << 3) | (((c == 'v') as u8) << 2) | (((c == '<') as u8) << 1) | (c == '>') as u8;
        (v != 0).then_some(((x as i32, y as i32), v))
    })).collect::<BlizzardsMap>();

    println!("{}", [(starting_pos, ending_pos), (ending_pos, starting_pos), (starting_pos, ending_pos)].iter().map(|(start, end)| traverse_map(&mut blizzards, start, end, map.first().unwrap().len(), map.len())).sum::<usize>() + 2);
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec24.txt").unwrap());

    let mut map = reader.lines().filter_map(Result::ok)
                                .map(|line| line[1..line.len() - 1].to_string())
                                .collect::<Vec<String>>();

    let starting_pos = (map.first().unwrap().find('.').unwrap() as i32, -1);
    let ending_pos   = (map.get(map.len() - 1).unwrap().find('.').unwrap() as i32, map.len() as i32 - 2);

    map.remove(0);
    map.pop();

    let current = std::time::Instant::now();
    part1(&map, &starting_pos, &ending_pos);
    let post_p1 = std::time::Instant::now();
    part2(&map, &starting_pos, &ending_pos);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}