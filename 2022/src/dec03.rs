use std::io::BufRead;

fn points_for_char(c: char) -> i32 {
    return if c.is_uppercase() {(c as i32) - ('A' as i32) + 26} else {(c as i32) - ('a' as i32)} + 1;
}

fn part1(lines: &std::vec::Vec<String>) {
    println!("{}", lines.iter().map(|line| (&line[0..line.len()/2], &line[line.len()/2..line.len()]))
                               .map(|(first_half, second_half)| first_half.chars().find(|c| second_half.contains(*c)).unwrap())
                               .map(points_for_char)
                               .sum::<i32>());
}

fn part2(lines: &std::vec::Vec<String>) {
    println!("{}", lines.chunks(3).map(|v| v[0].chars().find(|c| v[1].contains(*c) && v[2].contains(*c)).unwrap())
                                  .map(points_for_char)
                                  .sum::<i32>());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec03.txt").unwrap());

    let lines = reader.lines().map(&Result::unwrap).collect();

    let current = std::time::Instant::now();
    part1(&lines);
    let post_p1 = std::time::Instant::now();
    part2(&lines);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}