use std::io::BufRead;

fn get_position_of_first_unique_n_chars<const N: usize>(line: &str) -> Result<usize, &str> {
    let mut char_counts: [u16; 26] = Default::default();
    let mut recent_chars: [u8; N] = [0; N];
    let mut diff_count: usize = 0;

    for (i, &c) in line.as_bytes().iter().enumerate() {
        if i >= N {
            let old_char = recent_chars[i % N] as usize;
            char_counts[old_char] -= 1;
            diff_count -= (char_counts[old_char] == 0) as usize
        }

        let c_index = c - ('a' as u8);
        diff_count += (char_counts[c_index as usize] == 0) as usize;
        if diff_count == N {
            return Ok(i + 1);
        }
        char_counts[c_index as usize] += 1;
        recent_chars[i % N] = c_index;
    }
    return Err("Bad")
}

fn part1(lines: &Vec<String>) {
    for line in lines {
        println!("{}", get_position_of_first_unique_n_chars::<4>(line.as_str()).unwrap());
    }
}

fn part2(lines: &Vec<String>) {
    for line in lines {
        println!("{}", get_position_of_first_unique_n_chars::<14>(line.as_str()).unwrap());
    }
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec06.txt").unwrap());

    let lines = reader.lines().map(|line_or_err| line_or_err.unwrap()).collect::<Vec<String>>();

    let current = std::time::Instant::now();
    part1(&lines);
    let post_p1 = std::time::Instant::now();
    part2(&lines);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}