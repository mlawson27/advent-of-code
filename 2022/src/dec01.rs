use std::io::BufRead;

fn part1(per_elf_calorie_sums: &std::vec::Vec<i32>) {
  println!("{}", per_elf_calorie_sums.first().unwrap());
}

fn part2(per_elf_calorie_sums: &std::vec::Vec<i32>) {
  println!("{}", per_elf_calorie_sums[0..3].iter().sum::<i32>());
}

pub fn run() {
  let reader = std::io::BufReader::new(std::fs::File::open("input//dec01.txt").unwrap());
  let mut calories_or_errs = reader.lines().map(|line| line.unwrap().parse::<i32>());
  let calories_or_errs = calories_or_errs.by_ref();

  let mut per_elf_calorie_sums = std::vec::Vec::<i32>::new();

  loop {
    let current_elf_calorie_sum = calories_or_errs.take_while(| calories_or_err | calories_or_err.is_ok())
                                                       .map(| calories | calories.unwrap())
                                                       .sum::<i32>();

    if current_elf_calorie_sum == 0 {
      break;
    }

    per_elf_calorie_sums.push(current_elf_calorie_sum);
  }
  per_elf_calorie_sums.sort_unstable_by(|a, b| b.cmp(a));

  let current = std::time::Instant::now();
  part1(&per_elf_calorie_sums);
  let post_p1 = std::time::Instant::now();
  part2(&per_elf_calorie_sums);
  let post_p2 = std::time::Instant::now();
  println!("p1: {} us", (post_p1 - current).as_micros());
  println!("p2: {} us", (post_p2 - post_p1).as_micros());
}