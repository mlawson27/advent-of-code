use std::io::BufRead;

struct RangeSet {
    ranges: Vec<(i32, i32)>
}

impl RangeSet {
    fn new() -> RangeSet {
        RangeSet { ranges: Vec::new() }
    }

    fn add_range(&mut self, start: i32, end: i32) {
        if start > end {
            return;
        }

        let mut position_to_insert = 0usize;
        loop {
            if position_to_insert >= self.ranges.len() {
                if self.ranges.last().map_or(false, |r| end < r.1) {
                    return;
                }
                self.ranges.push((start, end));
                break
            }
            if start < self.ranges[position_to_insert].0 || (start == self.ranges[position_to_insert].0 && end < self.ranges[position_to_insert].1) {
                self.ranges.insert(position_to_insert, (start, end));
                break;
            }
            position_to_insert += 1;
        }

        for i in (0..self.ranges.len() - 1).rev() {
            while self.ranges[i].1 + 1 >= self.ranges[i + 1].0 {
                self.ranges[i].1 = self.ranges[i].1.max(self.ranges[i + 1].1);
                self.ranges.remove(i + 1);
                if i + 1 == self.ranges.len() {
                    break;
                }
            }
        }
    }

    fn count(&self) -> usize {
        self.ranges.iter().map(|(lower, upper)| (upper - lower + 1) as usize)
                          .sum()
    }

    fn first_gap(&self) -> Option<i32> {
        self.ranges.get(1).map(|r| r.0 - 1)
    }
}

fn part1<const Y: i32>(sensors_and_beacons: &Vec<((i32, i32), (i32, i32))>) {
    let mut safe_x_ranges = RangeSet::new();

    let mut beacons_in_row = Vec::<i32>::new();

    for (sensor_pos, beacon_pos) in sensors_and_beacons.iter() {
        let distance_to_nearest_beacon = sensor_pos.0.abs_diff(beacon_pos.0) + sensor_pos.1.abs_diff(beacon_pos.1);
        let distance_to_y              = sensor_pos.1.abs_diff(Y);
        let remaining                  = (distance_to_nearest_beacon as i32) - (distance_to_y as i32);

        if beacon_pos.1 == Y && !beacons_in_row.contains(&beacon_pos.0) {
            beacons_in_row.push(beacon_pos.0);
        }

        safe_x_ranges.add_range(sensor_pos.0 - remaining, sensor_pos.0 + remaining);
    }
    println!("{}", safe_x_ranges.count() - beacons_in_row.len());
}

fn part2<const MAX_X_AND_Y: i32>(sensors_and_beacons: &Vec<((i32, i32), (i32, i32))>) {
    let mut safe_x_ranges = RangeSet::new();
    let mut first_disjoint_point = None::<(i32, i32)>;
    for y in 0..=MAX_X_AND_Y {
        safe_x_ranges.ranges.clear();
        for (sensor_pos, beacon_pos) in sensors_and_beacons.iter() {
            let distance_to_nearest_beacon = sensor_pos.0.abs_diff(beacon_pos.0) + sensor_pos.1.abs_diff(beacon_pos.1);
            let distance_to_y              = sensor_pos.1.abs_diff(y);
            let remaining                  = (distance_to_nearest_beacon as i32) - (distance_to_y as i32);

            safe_x_ranges.add_range(0.max(sensor_pos.0 - remaining), MAX_X_AND_Y.min(sensor_pos.0 + remaining));
        }
        let first_gap = safe_x_ranges.first_gap();
        if first_gap.is_some() {
            first_disjoint_point = Some((first_gap.unwrap(), y));
            break;
        }
    }

    println!("{}", (first_disjoint_point.unwrap().0 as i64) * 4000000 + first_disjoint_point.unwrap().1 as i64);
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec15.txt").unwrap());

    let line_regex = regex::Regex::new(r"Sensor\s+at\s+x=(-?\d+),\s+y=(-?\d+):\s+closest\s+beacon\s+is\s+at\s+x=(-?\d+),\s+y=(-?\d+)").unwrap();

    let sensors_and_beacons = reader.lines().filter_map(Result::ok)
                                            .map(|line|line_regex.captures(line.as_str()).unwrap().iter().skip(1).map(|group| group.unwrap().as_str().parse::<i32>().unwrap()).collect::<Vec<i32>>())
                                            .map(|match_it|((match_it[0], match_it[1]), (match_it[2], match_it[3])))
                                            .collect::<Vec<((i32, i32), (i32, i32))>>();

    let current = std::time::Instant::now();
    part1::<2000000>(&sensors_and_beacons);
    let post_p1 = std::time::Instant::now();
    part2::<4000000>(&sensors_and_beacons);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}