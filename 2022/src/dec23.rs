use std::io::BufRead;

fn simulate(board: &Vec<Vec<char>>, num_rounds: Option<usize>) -> usize {
    const MOVES : [[(i32, i32); 3]; 4] = [[(-1, -1), (0, -1), (1, -1)], [(-1, 1), (0, 1), (1, 1)], [(-1, -1), (-1, 0), (-1, 1)], [(1, -1), (1, 0), (1, 1)]];

    let mut positions = board.iter().enumerate()
                                    .flat_map(|(y, line)| line.iter().enumerate().map(move |(x, c)| (x, y, c)))
                                    .filter_map(|(x, y, c)| if *c == '#' { Some((x as i32, y as i32)) } else { None })
                                    .collect::<std::collections::HashSet<(i32, i32)>>();

    for round in 0..num_rounds.unwrap_or(usize::MAX) {
        let mut new_positions = std::collections::HashMap::<(i32, i32), Option<(i32, i32)>>::new();

        for &(x, y) in positions.iter() {
            let mut any_other_within_position = false;
            for (adj_x, adj_y) in [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)] {
                any_other_within_position = positions.contains(&(x + adj_x, y + adj_y));
                if any_other_within_position {
                    break;
                }
            }
            if !any_other_within_position {
                continue;
            }

            for to_check_arr in MOVES.iter().cycle().skip(round).take(MOVES.len()) {
                let mut any_other_within_position = false;
                for &(adj_x, adj_y) in to_check_arr {
                    any_other_within_position = positions.contains(&(x + adj_x, y + adj_y));
                    if any_other_within_position {
                        break;
                    }
                }
                if !any_other_within_position {
                    let new_position = (x + to_check_arr[1].0, y + to_check_arr[1].1);
                    let existing = new_positions.get_mut(&new_position);
                    if existing.is_some() {
                        *existing.unwrap() = None;
                    }
                    else {
                        new_positions.insert(new_position, Some((x, y)));
                    }
                    break;
                }
            }
        }

        if new_positions.is_empty() {
            return round + 1;
        }

        for (new_position, original_position) in new_positions {
            if original_position.is_some() {
                assert!(positions.remove(&original_position.unwrap()));
                positions.insert(new_position);
            }
        }
    }

    positions.iter().fold([(i32::MAX, i32::MIN), (i32::MAX, i32::MIN)],
                          |working, val| [(working[0].0.min(val.0), working[0].1.max(val.0)),
                                          (working[1].0.min(val.1), working[1].1.max(val.1))])
                    .iter().map(|(min, max)| max - min + 1)
                           .product::<i32>() as usize - positions.len()
}

fn part1(board: &Vec<Vec<char>>) {
    println!("{}", simulate(board, Some(10)));
}

fn part2(board: &Vec<Vec<char>>) {
    println!("{}", simulate(board, None));
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec23.txt").unwrap());


    let board = reader.lines().filter_map(Result::ok)
                              .map(|line| Vec::from_iter(line.chars()))
                              .collect::<Vec<Vec<char>>>();

    let current = std::time::Instant::now();
    part1(&board);
    let post_p1 = std::time::Instant::now();
    part2(&board);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}