use std::io::BufRead;

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec25.txt").unwrap());

    let current = std::time::Instant::now();

    let mut ret = reader.lines().filter_map(Result::ok)
                                .map(|line| line.chars().fold(0i64, |ret, c| ret * 5 + match c {
                                    '2' => 2,
                                    '1' => 1,
                                    '0' => 0,
                                    '-' => -1,
                                    '=' => -2,
                                    _   => panic!("Bad char")
                                })).sum::<i64>();

    let mut ret_chars      = ['\0'; 64];
    let mut current_char_i = ret_chars.len();
    while ret > 0 {
        current_char_i            -= 1;
        ret_chars[current_char_i]  = ['0', '1', '2', '=', '-'][ret.rem_euclid(5) as usize];
        ret                        = (ret + 2) / 5;
    }
    ret_chars[current_char_i..].iter().for_each(|c| print!("{}", c));
    println!();

    let post = std::time::Instant::now();
    println!("{} us", (post - current).as_micros());
}