use std::io::BufRead;

fn part1(grid: &Vec<Vec<u8>>) {
    let mut visible_count = 0usize;

    let mut already_handled: Vec<Vec<bool>> = grid.iter().map(|row|row.iter().map(|_| false).collect()).collect();

    for i in 0..grid.len() {
        let mut last_max = [None::<u8>; 4];
        for j in 0..grid.len() {
            for (i, &(row, col)) in [(i, j), (j, i), (i, grid.len() - j - 1), (grid.len() - j - 1, i)].iter().enumerate() {
                if last_max[i].map_or(true, |last| last < grid[row][col]) {
                    last_max[i] = Some(grid[row][col]);
                    if !already_handled[row][col] {
                        visible_count += 1;
                        already_handled[row][col] = true;
                    }
                }
            }
        }
    }
    println!("{}", visible_count);
}

fn part2(grid: &Vec<Vec<u8>>) {
    let row_fn = |sub_row: usize, row: usize, col: usize| grid[sub_row][col] < grid[row][col];
    let col_fn = |sub_col: usize, row: usize, col: usize| grid[row][sub_col] < grid[row][col];

    let elem_fn = |row, col| {
        [((0..row).rev().take_while(|&sub_row| row_fn(sub_row, row, col)).count(), row),
         ((0..col).rev().take_while(|&sub_col| col_fn(sub_col, row, col)).count(), col),
         (((row + 1)..grid.len()).take_while(|&sub_row| row_fn(sub_row, row, col)).count(), grid.len() - 1 - row),
         (((col + 1)..grid.len()).take_while(|&sub_col| col_fn(sub_col, row, col)).count(), grid.len() - 1 - col)].iter()
                                                                                                                         .map(|&(count, max_count)| count + (count != max_count) as usize)
                                                                                                                         .product::<usize>()
    };
    println!("{}", (0..grid.len()).map(|row| (0..grid.len()).map(|col| elem_fn(row, col)).max().unwrap()).max().unwrap());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec08.txt").unwrap());

    let grid = reader.lines().map(|line_or_err| line_or_err.unwrap()
                                                                                                .as_bytes()
                                                                                                .iter()
                                                                                                .map(|byte| byte - ('0' as u8))
                                                                                                .collect::<Vec<u8>>()).collect::<Vec<Vec<u8>>>();

    let current = std::time::Instant::now();
    part1(&grid);
    let post_p1 = std::time::Instant::now();
    part2(&grid);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}