use std::io::BufRead;

enum AmountOrDirection {
    Amount(i32),
    Direction(char)
}

fn part1(map: &Vec<String>, directions: &Vec<AmountOrDirection>) {
    const ADJUSTMENT_FOR_DIRECTION: [(i32, i32); 4] = [(1, 0), (0, 1), (-1, 0), (0, -1)];

    let max_line_len = map.iter().map(|l| l.len()).max().unwrap();

    let mut current_pos   = (map[0].char_indices().find(|&(_x, c)| c == '.').unwrap().0, 0usize);
    let mut current_dir_i = 0;
    for amount_or_direction in directions {
        match amount_or_direction {
            AmountOrDirection::Amount(amount) => {
                let adjustment = &ADJUSTMENT_FOR_DIRECTION[current_dir_i];
                'adjustment: for _ in 0..*amount {
                    let mut next_pos = (current_pos.0 as i32, current_pos.1 as i32);
                    loop {
                        next_pos   = (next_pos.0 as i32 + adjustment.0, next_pos.1 as i32 + adjustment.1);
                        next_pos.1 = next_pos.1.rem_euclid(map.len() as i32);
                        next_pos.0 = next_pos.0.rem_euclid(max_line_len as i32);
                        match map[next_pos.1 as usize].chars().nth(next_pos.0 as usize) {
                            Some('#') => break 'adjustment,
                            Some('.') => break,
                            _   => continue
                        }
                    }
                    current_pos = (next_pos.0 as usize, next_pos.1 as usize);
                }
            },
            AmountOrDirection::Direction(turn) => {
                current_dir_i = ((current_dir_i as i32) + (if *turn == 'L' { -1 } else { 1 })).rem_euclid(ADJUSTMENT_FOR_DIRECTION.len() as i32) as usize;
            }
        }
    }

    println!("{}", (1000 * (current_pos.1 + 1)) + (4 * (current_pos.0 + 1)) + current_dir_i);
}

fn part2(map: &Vec<String>, directions: &Vec<AmountOrDirection>) {
    const ADJUSTMENT_FOR_DIRECTION: [(i32, i32); 4] = [(1, 0), (0, 1), (-1, 0), (0, -1)];

    let mut current_pos   = (map[0].char_indices().find(|&(_x, c)| c == '.').unwrap().0, 0usize);
    let mut current_dir_i = 0;
    for amount_or_direction in directions {
        match amount_or_direction {
            AmountOrDirection::Amount(amount) => {
                'adjustment: for _ in 0..*amount {
                    let     adjustment = &ADJUSTMENT_FOR_DIRECTION[current_dir_i];
                    let mut next_pos   = (current_pos.0 as i32, current_pos.1 as i32);
                    let mut next_dir_i = current_dir_i;
                    loop {
                        next_pos = (next_pos.0 as i32 + adjustment.0, next_pos.1 as i32 + adjustment.1);

                        // Top left
                        if next_pos.0 < 50 && next_pos.1 < 50 && current_dir_i == 2 {
                            next_pos.0 = 0;
                            next_pos.1 = 100 + 50 - next_pos.1 - 1;
                            next_dir_i = 0;
                        }
                        else if next_pos.0 < 100 && next_pos.1 < 0 && current_dir_i == 3 {
                            next_pos.1 = 100 + next_pos.0;
                            next_pos.0 = 0;
                            next_dir_i = 0;
                        }

                        // Top right
                        else if next_pos.0 >= 150 && next_pos.1 < 50 && current_dir_i == 0 {
                            next_pos.0 = 99;
                            next_pos.1 = 150 - next_pos.1 - 1;
                            next_dir_i = 2;
                        }
                        else if next_pos.0 >= 100 && next_pos.1 >= 50 && current_dir_i == 1 {
                            next_pos.1 = next_pos.0 - 50;
                            next_pos.0 = 99;
                            next_dir_i = 2;
                        }
                        else if next_pos.0 < 150 && next_pos.1 < 0 && current_dir_i == 3 {
                            next_pos.0 = next_pos.0 - 100;
                            next_pos.1 = 199;
                            next_dir_i = 3;
                        }

                        // Upper middle
                        else if next_pos.0 < 50 && next_pos.1 < 100 && current_dir_i == 2 {
                            next_pos.0 = next_pos.1 - 50;
                            next_pos.1 = 100;
                            next_dir_i = 1;
                        }
                        else if next_pos.0 >= 100 && next_pos.1 >= 50 && next_pos.1 < 100 && current_dir_i == 0 {
                            next_pos.0 = next_pos.1 + 50;
                            next_pos.1 = 49;
                            next_dir_i = 3;
                        }

                        // Lower middle right
                        else if next_pos.0 >= 100 && next_pos.1 >= 100 && current_dir_i == 0 {
                            next_pos.0 = 149;
                            next_pos.1 = 150 - next_pos.1 - 1;
                            next_dir_i = 2;
                        }
                        else if next_pos.0 >= 50 && next_pos.1 >= 150 && current_dir_i == 1 {
                            next_pos.1 = next_pos.0 + 100;
                            next_pos.0 = 49;
                            next_dir_i = 2;
                        }

                        // Lower middle left
                        else if next_pos.0 < 0 && next_pos.1 < 150 && current_dir_i == 2 {
                            next_pos.0 = 50;
                            next_pos.1 = 150 - next_pos.1 - 1;
                            next_dir_i = 0;
                        }
                        else if next_pos.0 < 50 && next_pos.1 < 100 && current_dir_i == 3 {
                            next_pos.1 = next_pos.0 + 50;
                            next_pos.0 = 50;
                            next_dir_i = 0;
                        }

                        // Bottom
                        else if next_pos.0 >= 50 && next_pos.1 >= 150 && current_dir_i == 0 {
                            next_pos.0 = next_pos.1 - 100;
                            next_pos.1 = 149;
                            next_dir_i = 3;
                        }
                        else if next_pos.0 < 50 && next_pos.1 >= 200 && current_dir_i == 1 {
                            next_pos.0 = next_pos.0 + 100;
                            next_pos.1 = 0;
                            next_dir_i = 1;
                        }
                        else if next_pos.0 < 0 && next_pos.1 >= 150 && current_dir_i == 2 {
                            next_pos.0 = 50 + (next_pos.1 - 150);
                            next_pos.1 = 0;
                            next_dir_i = 1;
                        }

                        match map[next_pos.1 as usize].chars().nth(next_pos.0 as usize) {
                            Some('#') => break 'adjustment,
                            Some('.') => break,
                            _   => continue
                        }
                    }
                    current_pos   = (next_pos.0 as usize, next_pos.1 as usize);
                    current_dir_i = next_dir_i;
                }
            },
            AmountOrDirection::Direction(turn) => {
                current_dir_i = ((current_dir_i as i32) + (if *turn == 'L' { -1 } else { 1 })).rem_euclid(ADJUSTMENT_FOR_DIRECTION.len() as i32) as usize;
            }
        }
    }

    println!("{}", (1000 * (current_pos.1 + 1)) + (4 * (current_pos.0 + 1)) + current_dir_i);
}

pub fn run() {
    let directions_regex = regex::Regex::new(r"(\d+)|([LR])");

    let reader = std::io::BufReader::new(std::fs::File::open("input//dec22.txt").unwrap());

    let mut lines_it = reader.lines();

    let map = lines_it.by_ref().filter_map(Result::ok)
                               .take_while(|l| !l.is_empty())
                               .collect::<Vec<String>>();

    let directions = directions_regex.unwrap().captures_iter(lines_it.by_ref().next().unwrap().unwrap().as_str()).map(|c| if c.get(1).is_some() { AmountOrDirection::Amount(c.get(1).unwrap().as_str().parse::<i32>().unwrap()) } else { AmountOrDirection::Direction(c.get(2).unwrap().as_str().chars().next().unwrap()) })
                                                                                                                                         .collect::<Vec<AmountOrDirection>>();

    let current = std::time::Instant::now();
    part1(&map, &directions);
    let post_p1 = std::time::Instant::now();
    part2(&map, &directions);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}