use std::io::BufRead;

trait Sized {
    fn size(&self) -> usize;
}

enum FileOrDirectoryNode {
    File(FileNode),
    Directory(DirectoryNode)
}

struct FileNode {
    pub _name: String,
    pub size: usize
}

struct DirectoryNode {
    _name: String,
    size: usize,
    children: Vec<FileOrDirectoryNode>
}

impl DirectoryNode {
    fn add_child(&mut self, c: FileOrDirectoryNode) {
        self.size += c.size();
        self.children.push(c);
    }

    fn children_iter(&self) -> impl Iterator<Item = &FileOrDirectoryNode> {
        self.children.iter()
    }
}

impl Sized for FileNode {
    fn size(&self) -> usize {
        self.size
    }
}

impl Sized for DirectoryNode {
    fn size(&self) -> usize {
        self.size
    }
}

impl Sized for FileOrDirectoryNode {
    fn size(&self) -> usize {
        match self {
            FileOrDirectoryNode::Directory(d) => return d.size(),
            FileOrDirectoryNode::File(f) => return f.size()
        };
    }
}

fn part1(top_level_directory: &DirectoryNode) {
    fn get_folders_with_max_size_sum(max_size: usize, dir: &DirectoryNode) -> usize {
        return dir.children_iter()
                  .filter_map(|c| match c { FileOrDirectoryNode::Directory(d) => Some(get_folders_with_max_size_sum(max_size, d)),
                                            _ => None  })
                  .sum::<usize>() + if dir.size() <= max_size { dir.size() } else { 0usize } ;
    }

    println!("{}", get_folders_with_max_size_sum(100000, &top_level_directory));
}

fn part2(top_level_directory: &DirectoryNode) {
    fn get_min_folder_size_above_threshold(min_size_threshold: usize, dir: &DirectoryNode) -> Option<usize> {
        return dir.children_iter()
                  .filter_map(|c| match c {
                    FileOrDirectoryNode::Directory(d) => Some(d),
                    _ => None
                  })
                  .filter_map(|d| get_min_folder_size_above_threshold(min_size_threshold, d))
                  .min().map(|v| v).or_else(|| { let this_size = dir.size(); if this_size >= min_size_threshold { Some(this_size) } else {None} });
    }

    println!("{}", get_min_folder_size_above_threshold(30000000 - (70000000 - (top_level_directory.size())), &top_level_directory).unwrap());
}

pub fn run() {
    let reader = std::io::BufReader::new(std::fs::File::open("input//dec07.txt").unwrap());

    let lines = reader.lines().map(|line_or_err| line_or_err.unwrap()).collect::<Vec<String>>();

    fn parse_directory<'a, I>(line_iter: &mut I) -> DirectoryNode
    where I: Iterator<Item = &'a str> {
        let cd_regex = regex::Regex::new(r"^\$\s+cd\s+(.+)$").unwrap();
        let f_regex = regex::Regex::new(r"(\d+)\s+(.+)$").unwrap();

        let mut dir_stack = std::collections::VecDeque::<DirectoryNode>::new();

        for line in line_iter.by_ref() {
            let m = cd_regex.captures(line);
            if m.is_some() {
                let name = m.unwrap().get(1).unwrap().as_str();
                match name {
                    ".." => {
                        let left_dir = dir_stack.pop_back().unwrap();
                        dir_stack.back_mut().unwrap().add_child(FileOrDirectoryNode::Directory(left_dir));
                    }
                    _ => { dir_stack.push_back(DirectoryNode{ _name: String::from(name), size: 0, children: std::vec::Vec::new() }); }
                }
            }
            else {
                let m = f_regex.captures(line);
                if m.is_some() {
                    dir_stack.back_mut().unwrap().add_child(FileOrDirectoryNode::File(FileNode{_name: String::from(m.as_ref().unwrap().get(2).unwrap().as_str()),
                                                                                               size: m.as_ref().unwrap().get(1).unwrap().as_str().parse().unwrap() }));
                }
            }
        }
        while dir_stack.len() > 1 {
            let left_dir = dir_stack.pop_back().unwrap();
            dir_stack.back_mut().unwrap().add_child(FileOrDirectoryNode::Directory(left_dir));
        }
        return dir_stack.pop_back().unwrap();
    }

    let top_level_directory = parse_directory(lines.iter().map(|line| line.as_str()).by_ref());

    let current = std::time::Instant::now();
    part1(&top_level_directory);
    let post_p1 = std::time::Instant::now();
    part2(&top_level_directory);
    let post_p2 = std::time::Instant::now();
    println!("p1: {} us", (post_p1 - current).as_micros());
    println!("p2: {} us", (post_p2 - post_p1).as_micros());
}