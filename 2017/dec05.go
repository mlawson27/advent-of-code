package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func part1(offsets []int) int {
	offsetsAdj := make([]int, len(offsets))
	copy(offsetsAdj, offsets)
	index := 0
	instructionCount := 0
	for index >= 0 && index < len(offsetsAdj) {
		newJump := offsetsAdj[index]
		offsetsAdj[index]++
		index += newJump
		instructionCount++
	}
	return instructionCount
}

func part2(offsets []int) int {
	offsetsAdj := make([]int, len(offsets))
	copy(offsetsAdj, offsets)
	index := 0
	instructionCount := 0
	for index >= 0 && index < len(offsetsAdj) {
		newJump := offsetsAdj[index]
		if newJump >= 3 {
			offsetsAdj[index]--
		} else {
			offsetsAdj[index]++
		}
		index += newJump
		instructionCount++
	}
	return instructionCount
}

func main() {
	contentBytes, err := os.ReadFile("dec05.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	offsets := make([]int, 0, len(lines))
	for _, line := range lines {
		rowVal, _ := strconv.Atoi(line)
		offsets = append(offsets, rowVal)
	}

	fmt.Println(part1(offsets))
	fmt.Println(part2(offsets))
}
