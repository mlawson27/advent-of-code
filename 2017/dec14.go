package main

import (
	"fmt"
	"math/bits"
	"strconv"
)

func colToByteIBitI(col int) (int, int) {
	return (col / 8), (8 - (col % 8) - 1)
}

func markFound(grid *[128][16]uint8, i int, j int) {
	byteI, bitI := colToByteIBitI(j)

	if (grid[i][byteI]>>bitI)&0b1 == 0b1 {
		grid[i][byteI] &= ^byte(1 << bitI)
		if i > 0 {
			markFound(grid, i-1, j)
		}
		if i < len(grid)-1 {
			markFound(grid, i+1, j)
		}
		if j > 0 {
			markFound(grid, i, j-1)
		}
		if j < len(grid[i])*8-1 {
			markFound(grid, i, j+1)
		}
	}
}

func knotHash(content string) [16]uint8 {
	lengths := make([]int, len(content)+5)
	for i, c := range content {
		lengths[i] = int(c)
	}
	copy(lengths[len(content):], []int{17, 31, 73, 47, 23})

	var values [256]int
	for i := 1; i < len(values); i++ {
		values[i] = i
	}

	currentPos := 0
	skipSize := 0

	for r := 0; r < 64; r++ {
		for _, length := range lengths {
			for i := 0; i < length/2; i++ {
				posA := (currentPos + i) % len(values)
				posB := (currentPos + length - 1 - i) % len(values)

				orig := values[posA]
				values[posA] = values[posB]
				values[posB] = orig
			}
			currentPos = (currentPos + length + skipSize) % len(values)
			skipSize++
		}
	}

	var ret [16]uint8
	for i := 0; i < len(values); i += 16 {
		v := values[i]
		for j := 1; j < 16; j++ {
			v = v ^ values[i+j]
		}
		ret[i/16] = uint8(v)
	}
	return ret
}

func main() {
	const inputBase = "vbqugkhl"

	numBitsSet := 0
	var grid [128][16]uint8
	for i := 0; i < len(grid); i++ {
		grid[i] = knotHash(inputBase + "-" + strconv.Itoa(i))
		for _, v := range grid[i] {
			numBitsSet += bits.OnesCount8(v)
		}
	}
	fmt.Println(numBitsSet)

	numGroups := 0
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i])*8; j++ {
			byteI, bitI := colToByteIBitI(j)
			if ((grid[i][byteI] >> bitI) & 0b1) == 0b1 {
				numGroups++
				markFound(&grid, i, j)
			}
		}
	}
	fmt.Println(numGroups)
}
