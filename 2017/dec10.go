package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func doRounds(numRounds int, lengths []int) [256]int {
	var values [256]int
	for i := 1; i < len(values); i++ {
		values[i] = i
	}

	currentPos := 0
	skipSize := 0

	for r := 0; r < numRounds; r++ {
		for _, length := range lengths {
			for i := 0; i < length/2; i++ {
				posA := (currentPos + i) % len(values)
				posB := (currentPos + length - 1 - i) % len(values)

				orig := values[posA]
				values[posA] = values[posB]
				values[posB] = orig
			}
			currentPos = (currentPos + length + skipSize) % len(values)
			skipSize++
		}
	}

	return values
}

func main() {
	splitRe := regexp.MustCompile(`,\s*`)

	contentBytes, err := os.ReadFile("dec10.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	var lengths []int
	if content == "" {
		lengths = []int{}
	} else {
		tokens := splitRe.Split(content, -1)
		lengths = make([]int, 0, len(tokens))
		for _, vStr := range tokens {
			v, _ := strconv.Atoi(vStr)
			lengths = append(lengths, v)
		}
	}

	r1 := doRounds(1, lengths)
	fmt.Println(r1[0] * r1[1])

	lengthsP2 := make([]int, len(content)+5)
	for i, c := range content {
		lengthsP2[i] = int(c)
	}
	copy(lengthsP2[len(content):], []int{17, 31, 73, 47, 23})

	r64 := doRounds(64, lengthsP2)
	for i := 0; i < len(r64); i += 16 {
		v := r64[i]
		for j := 1; j < 16; j++ {
			v = v ^ r64[i+j]
		}
		fmt.Printf("%0.2x", v)
	}
	fmt.Println()
}
