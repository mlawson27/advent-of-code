package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	contentBytes, err := os.ReadFile("dec06.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	tokens := strings.Split(content, "\t")
	blocks := make([]int, 0, len(tokens))
	for _, line := range tokens {
		block, _ := strconv.Atoi(line)
		blocks = append(blocks, block)
	}

	numIterations := 0
	seenBlocks := map[string]int{}
	for true {
		tokenStr := fmt.Sprint(blocks[0])
		for i := 1; i < len(blocks); i++ {
			tokenStr += "," + fmt.Sprint(blocks[i])
		}
		_, found := seenBlocks[tokenStr]
		if found {
			fmt.Println(numIterations)
			fmt.Println(numIterations - seenBlocks[tokenStr])
			break
		}
		seenBlocks[tokenStr] = numIterations
		numIterations++

		maxI := 0
		maxV := blocks[maxI]
		for i := 1; i < len(blocks); i++ {
			if blocks[i] > maxV {
				maxI = i
				maxV = blocks[maxI]
			}
		}

		blocks[maxI] = 0
		for ; maxV > 0; maxV-- {
			maxI = (maxI + 1) % len(blocks)
			blocks[maxI]++
		}
	}
}
