package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Component struct {
	ports   [2]int
	portSum int
}

func findStrongestChain(components []Component, used []bool, currentEnd *int) int {
	maxStrength := 0
	for i := 0; i < len(components); i++ {
		if !used[i] {
			for usedEndpoint := 0; usedEndpoint < len(components[i].ports); usedEndpoint++ {
				if (currentEnd == nil && components[i].ports[usedEndpoint] == 0) || (currentEnd != nil && components[i].ports[usedEndpoint] == *currentEnd) {
					used[i] = true
					workingLen := components[i].portSum + findStrongestChain(components, used, &components[i].ports[1-usedEndpoint])
					if workingLen > maxStrength {
						maxStrength = workingLen
					}
					used[i] = false
				}
			}
		}
	}
	return maxStrength
}

func findLongestChain(components []Component, used []bool, currentEnd *int) (int, int) {
	maxLen := 0
	strengthAtMaxLen := 0
	for i := 0; i < len(components); i++ {
		if !used[i] {
			for usedEndpoint := 0; usedEndpoint < len(components[i].ports); usedEndpoint++ {
				if (currentEnd == nil && components[i].ports[usedEndpoint] == 0) || (currentEnd != nil && components[i].ports[usedEndpoint] == *currentEnd) {
					used[i] = true
					workingLen, workingStrength := findLongestChain(components, used, &components[i].ports[1-usedEndpoint])
					workingLen++
					workingStrength += components[i].portSum
					if workingLen > maxLen || (workingLen == maxLen && workingStrength > strengthAtMaxLen) {
						maxLen = workingLen
						strengthAtMaxLen = workingStrength
					}
					used[i] = false
				}
			}
		}
	}
	return maxLen, strengthAtMaxLen
}

func main() {
	contentBytes, err := os.ReadFile("dec24.txt")
	if err != nil {
		panic(err)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	components := make([]Component, 0, len(lines))
	for _, l := range lines {
		endpointStrs := strings.Split(l, "/")
		if len(endpointStrs) != 2 {
			panic(errors.New("More than 2 endpoints on line " + l))
		}
		var component Component
		portSum := 0
		for i := 0; i < len(component.ports); i++ {
			val, err := strconv.Atoi(endpointStrs[i])
			if err != nil {
				panic(errors.New("Value not an integer " + endpointStrs[i]))
			}
			component.ports[i] = val
			portSum += val
		}
		component.portSum = portSum
		components = append(components, component)
	}

	used := make([]bool, len(components))
	fmt.Println(findStrongestChain(components, used, nil))
	_, strength := findLongestChain(components, used, nil)
	fmt.Println(strength)
}
