package main

import (
	"fmt"
	"os"
	"strings"
)

func abs(v int) int {
	if v < 0 {
		return -v
	}
	return v
}

func stepsAway(north int, east int) int {
	return (abs(north) + abs(east)) / 2
}

func main() {
	contentBytes, err := os.ReadFile("dec11.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	tokens := strings.Split(content, ",")

	north := 0
	east := 0
	maxStepsAway := 0
	for _, token := range tokens {
		switch token {
		case "n":
			north += 2
		case "ne":
			north++
			east++
		case "se":
			north--
			east++
		case "s":
			north -= 2
		case "sw":
			north--
			east--
		case "nw":
			north++
			east--
		}
		maxStepsAway = max(maxStepsAway, stepsAway(north, east))
	}

	fmt.Println(stepsAway(north, east))
	fmt.Println(maxStepsAway)
}
