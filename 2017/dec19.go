package main

import (
	"fmt"
	"os"
	"strings"
	"unicode"
)

type Direction int

const (
	Up Direction = iota
	Down
	Left
	Right
)

func directionToAdj(dir Direction) (int, int) {
	switch dir {
	case Up:
		return 0, -1
	case Down:
		return 0, 1
	case Left:
		return -1, 0
	case Right:
		return 1, 0
	}
	return 0, 0
}

func main() {
	contentBytes, err := os.ReadFile("dec19.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")

	posX := 0
	posY := 0
	dir := Down
	for ; posX < len(lines[0]) && lines[0][posX] != '|'; posX++ {
	}

	udTurns := [2]Direction{Left, Right}
	lrTurns := [2]Direction{Up, Down}

	seenChars := make([]rune, 0, 26)
	numSteps := 0

	adjustPosIfValid := func(adjX int, adjY int) bool {
		if posY+adjY >= 0 && posY+adjY < len(lines) &&
			posX+adjX >= 0 && posX+adjX < len(lines[posY+adjY]) &&
			lines[posY+adjY][posX+adjX] != ' ' {
			posX = posX + adjX
			posY = posY + adjY
			return true
		}
		return false
	}
	for true {
		numSteps++
		if unicode.IsLetter(rune(lines[posY][posX])) {
			seenChars = append(seenChars, rune(lines[posY][posX]))
		}
		if !adjustPosIfValid(directionToAdj(dir)) {
			if lines[posY][posX] == '+' {
				var turns *[2]Direction
				if dir == Up || dir == Down {
					turns = &udTurns
				} else {
					turns = &lrTurns
				}
				for _, newDir := range turns {
					if adjustPosIfValid(directionToAdj(newDir)) {
						dir = newDir
						break
					}
				}
			} else {
				break
			}
		}
	}
	fmt.Println(string(seenChars[:]))
	fmt.Println(numSteps)
}
