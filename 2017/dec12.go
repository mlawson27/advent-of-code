package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func numConnectionsFrom(connections [][]int, index int, working []bool) int {
	countThisLoop := 1
	working[index] = true

	for _, connection := range connections[index] {
		if !working[connection] {
			working[connection] = true
			countThisLoop += numConnectionsFrom(connections, connection, working)
		}
	}

	return countThisLoop
}

func main() {
	contentBytes, err := os.ReadFile("dec12.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")

	connections := make([][]int, len(lines))

	for _, line := range lines {
		sidesOfConnection := strings.Split(line, " <-> ")
		thisIndex, _ := strconv.Atoi(sidesOfConnection[0])
		thisConnectionStrs := strings.Split(sidesOfConnection[1], ", ")
		thisConnections := make([]int, 0, len(thisConnectionStrs))
		for _, thisConnectionStr := range thisConnectionStrs {
			thisConnection, _ := strconv.Atoi(thisConnectionStr)
			thisConnections = append(thisConnections, thisConnection)
		}
		connections[thisIndex] = thisConnections
	}

	fmt.Println(numConnectionsFrom(connections, 0, make([]bool, len(connections))))

	workingPt2 := make([]bool, len(connections))
	numGroups := 0
	for i := 0; i < len(connections); i++ {
		if !workingPt2[i] {
			numConnectionsFrom(connections, i, workingPt2)
			numGroups++
		}
	}
	fmt.Println(numGroups)
}
