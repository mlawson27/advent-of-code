package main

import (
	"fmt"
	"math"
)

type Direction int

const (
	Right Direction = 0
	Up    Direction = 1
	Left  Direction = 2
	Down  Direction = 3
	Count Direction = 4
)

func next(dir Direction, x int, y int) (int, int) {
	switch dir {
	case Right:
		return x + 1, y
	case Down:
		return x, y + 1
	case Left:
		return x - 1, y
	case Up:
		return x, y - 1
	}
	return 0, 0
}

func part1(input int, gridSize int) int {
	grid := make([][]int, 0, gridSize)
	for i := 0; i < gridSize; i++ {
		grid = append(grid, make([]int, gridSize))
	}

	posX := gridSize / 2
	posY := gridSize/2 - 1
	val := 1
	dir := Left
	for val <= input {
		newDir := (dir + 1) % Count
		newPosX, newPosY := next(newDir, posX, posY)
		if grid[newPosY][newPosX] == 0 {
			dir = newDir
			posX = newPosX
			posY = newPosY
		} else {
			posX, posY = next(dir, posX, posY)
		}
		grid[posY][posX] = val
		val++
	}

	return int(math.Abs(float64((gridSize/2)-posX)) + math.Abs(float64((gridSize/2)-posY)))
}

func part2(input int, gridSize int) int {
	grid := make([][]int, 0, gridSize)
	for i := 0; i < gridSize; i++ {
		grid = append(grid, make([]int, gridSize))
	}

	posX := gridSize / 2
	posY := gridSize / 2
	grid[posY][posX] = 1
	val := 2
	dir := Down
	for val <= input {
		newDir := (dir + 1) % Count
		newPosX, newPosY := next(newDir, posX, posY)
		if grid[newPosY][newPosX] == 0 {
			dir = newDir
			posX = newPosX
			posY = newPosY
		} else {
			posX, posY = next(dir, posX, posY)
		}

		sum := 0
		for yAdj := -1; yAdj < 2; yAdj++ {
			for xAdj := -1; xAdj < 2; xAdj++ {
				if (posX != 0 || posY != 0) && posX+xAdj >= 0 && posX+xAdj < gridSize && posY+yAdj >= 0 && posY+yAdj < gridSize {
					sum += grid[posY+yAdj][posX+xAdj]
				}
			}
		}
		if sum > input {
			return sum
		}
		grid[posY][posX] = sum
	}

	return 0
}

func main() {
	const input = 289326
	gridSize := int(math.Ceil(math.Sqrt(input)))
	gridSize += 1 - (gridSize % 2)

	fmt.Println(part1(input, gridSize))
	fmt.Println(part2(input, gridSize))
}
