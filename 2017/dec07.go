package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Tower struct {
	name      string
	weight    int
	subTowers []*Tower
}

func subTowerWeight(tower *Tower) int {
	ret := tower.weight
	if tower.subTowers != nil {
		for _, subTower := range tower.subTowers {
			ret += subTowerWeight(subTower)
		}
	}
	return ret
}

func findUnbalancedDiscAdjustment(tower *Tower) int {
	for _, subTower := range tower.subTowers {
		subTowerAdjustment := findUnbalancedDiscAdjustment(subTower)
		if subTowerAdjustment != 0 {
			return subTowerAdjustment
		}
	}

	towerWeightToSubtowers := map[int][]*Tower{}
	towerWeights := make([]int, 0, len(tower.subTowers))
	for _, subTower := range tower.subTowers {
		subTowerWeight := subTowerWeight(subTower)
		_, found := towerWeightToSubtowers[subTowerWeight]
		if !found {
			towerWeightToSubtowers[subTowerWeight] = make([]*Tower, 0, len(tower.subTowers))
			towerWeights = append(towerWeights, subTowerWeight)
		}
		towerWeightToSubtowers[subTowerWeight] = append(towerWeightToSubtowers[subTowerWeight], subTower)
	}
	if len(towerWeights) > 2 {
		panic(errors.New("More than 2 distinct subtower weights found"))
	}

	if len(towerWeights) == 1 {
		return 0
	}

	for i, weight := range towerWeights {
		if len(towerWeightToSubtowers[weight]) == 1 {
			return towerWeightToSubtowers[weight][0].weight + (towerWeights[1-i] - weight)
		}
	}
	return 0
}

func main() {
	lineRe := regexp.MustCompile(`([a-z]+)\s+\((\d+)\)(?:\s+->\s+((?:[a-z]+(?:,\s+)?)*))?`)
	splitRe := regexp.MustCompile(`,\s+`)

	contentBytes, err := os.ReadFile("dec07.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")

	nameToTower := map[string]*Tower{}
	towerToSubTower := map[string][]string{}
	for _, line := range lines {
		match := lineRe.FindStringSubmatch(line)
		weight, _ := strconv.Atoi(match[2])
		tower := Tower{name: match[1], weight: weight, subTowers: nil}
		nameToTower[tower.name] = &tower
		if match[3] != "" {
			nestedTowerNames := splitRe.Split(match[3], -1)
			towerToSubTower[tower.name] = nestedTowerNames
		}
	}
	var topLevelTower *Tower = nil
	for len(towerToSubTower) > 0 {
		toRemove := make([]string, 0, len(towerToSubTower))
		for towerName, subTowerNames := range towerToSubTower {
			allLowestLevel := true
			for _, subTowerName := range subTowerNames {
				_, found := towerToSubTower[subTowerName]
				if found {
					allLowestLevel = false
					break
				}
			}
			if allLowestLevel {
				nameToTower[towerName].subTowers = make([]*Tower, 0, len(subTowerNames))
				for _, subTowerName := range subTowerNames {
					nameToTower[towerName].subTowers = append(nameToTower[towerName].subTowers, nameToTower[subTowerName])
				}
				toRemove = append(toRemove, towerName)
			}
		}
		for _, towerName := range toRemove {
			if len(towerToSubTower) == 1 {
				topLevelTower = nameToTower[towerName]
			}
			delete(towerToSubTower, towerName)
		}
	}

	fmt.Println(topLevelTower.name)
	fmt.Println(findUnbalancedDiscAdjustment(topLevelTower))
}
