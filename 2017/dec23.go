package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type InstructionName int

type Argument struct {
	isReg bool
	value int
}

type Instruction struct {
	name InstructionName
	args [2]Argument
}

type RegSet ['h' - 'a' + 1]int64

const (
	set InstructionName = iota
	sub
	mul
	jnz
)

func main() {
	instructionNameToVal := map[string]InstructionName{
		"set": set,
		"sub": sub,
		"mul": mul,
		"jnz": jnz,
	}

	contentBytes, err := os.ReadFile("dec23.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	instructions := make([]Instruction, 0, len(lines))
	for _, line := range lines {
		tokens := strings.Split(line, " ")
		instrVal, found := instructionNameToVal[tokens[0]]
		if !found {
			panic(errors.New("Instruction name not found: " + tokens[0]))
		}
		var args [2]Argument
		for i, token := range tokens[1:] {
			args[i].isReg = len(token) == 1 && unicode.IsLower(rune(token[0]))
			if args[i].isReg {
				args[i].value = int(token[0])
			} else {
				args[i].value, err = strconv.Atoi(token)
				if err != nil {
					panic(err)
				}
			}
		}
		instructions = append(instructions, Instruction{name: instrVal, args: args})
	}

	var pc int64
	var regs RegSet
	var numMults int

	pc = 0
	regs = RegSet{}
	numMults = 0
	for pc >= 0 && pc < int64(len(instructions)) {
		instr := &instructions[pc]
		var argV int64
		if instr.args[1].isReg {
			argV = regs[instr.args[1].value-'a']
		} else {
			argV = int64(instr.args[1].value)
		}
		switch instr.name {
		case set:
			regs[instr.args[0].value-'a'] = argV
		case sub:
			regs[instr.args[0].value-'a'] -= argV
		case mul:
			regs[instr.args[0].value-'a'] *= argV
			numMults++
		case jnz:
			{
				var jVal int64
				if instr.args[0].isReg {
					jVal = regs[instr.args[0].value-'a']
				} else {
					jVal = int64(instr.args[0].value)
				}
				if jVal != 0 {
					pc += argV
				} else {
					pc++
				}
			}
		}
		if instr.name != jnz {
			pc++
		}
	}
	fmt.Println(numMults)

	pc = 0
	regs = RegSet{}
	numMults = 0
	regs['a'-'a'] = 1
	regs['b'-'a'] = 84
	regs['b'-'a'] *= 100
	regs['b'-'a'] -= -100000
	regs['c'-'a'] = regs['b'-'a']
	regs['c'-'a'] -= -17000
	for true {
		regs['f'-'a'] = 1
		regs['d'-'a'] = 2
		regs['e'-'a'] = 2
		for ; regs['d'-'a'] < regs['b'-'a']; regs['d'-'a']++ {
			if regs['b'-'a']%regs['d'-'a'] == 0 {
				regs['f'-'a'] = 0
				break
			}
		}
		if regs['f'-'a'] == 0 {
			regs['h'-'a']++
		}
		if regs['b'-'a'] == regs['c'-'a'] {
			break
		}
		regs['b'-'a'] -= -17
	}
	fmt.Println(regs['h'-'a'])
}
