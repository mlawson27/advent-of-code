package main

import (
	"errors"
	"fmt"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Vec3D [3]int

type Particle struct {
	position Vec3D
	velocity Vec3D
	accel    Vec3D
}

func main() {
	lineRe := regexp.MustCompile(`p=<(-?\d+,-?\d+,-?\d+)>,\s+v=<(-?\d+,-?\d+,-?\d+)>,\s+a=<(-?\d+,-?\d+,-?\d+)>`)

	contentBytes, err := os.ReadFile("dec20.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	particles := make([]Particle, 0, len(lines))
	for _, line := range lines {
		tokens := lineRe.FindStringSubmatch(line)
		if tokens == nil {
			panic(errors.New("Failed to parse line: " + line))
		}
		particle := Particle{}
		for i, vec := range []*Vec3D{&particle.position, &particle.velocity, &particle.accel} {
			subtokens := strings.Split(tokens[i+1], ",")
			if len(subtokens) != len(vec) {
				panic(errors.New("Incorrect number of tokens: " + line))
			}
			for j, subtoken := range subtokens {
				val, err := strconv.Atoi(subtoken)
				if err != nil {
					panic(err)
				}
				vec[j] = val
			}
		}
		particles = append(particles, particle)
	}

	var minDists Vec3D = Vec3D{math.MaxInt, math.MaxInt, math.MaxInt}
	minAccelI := 0
	for i, particle := range particles {
		var dists Vec3D
		for i, vec := range []*Vec3D{&particle.accel, &particle.velocity, &particle.position} {
			for _, v := range vec {
				if v < 0 {
					v = -v
				}
				dists[i] += v
			}
		}
		if dists[0] < minDists[0] ||
			dists[0] == minDists[0] && (dists[1] < minDists[1] ||
				dists[1] == minDists[1] && dists[2] == minDists[2]) {
			minDists = dists
			minAccelI = i
		}
	}
	fmt.Println(minAccelI)

	collisionIs := make([]int, 0, len(particles))
	for t := 0; t < 200; t++ {
		for i := 0; i < len(particles)-1; i++ {
			for j := i + 1; j < len(particles); j++ {
				if particles[i].position == particles[j].position {
					for _, c := range []int{i, j} {
						found := false
						for _, v := range collisionIs {
							found = v == c
							if found {
								break
							}
						}
						if !found {
							collisionIs = append(collisionIs, c)
						}
					}
				}
			}
		}
		sort.Ints(collisionIs)
		for i := len(collisionIs) - 1; i >= 0; i-- {
			particles = append(particles[0:collisionIs[i]], particles[collisionIs[i]+1:]...)
		}
		collisionIs = collisionIs[len(collisionIs):]
		for i := 0; i < len(particles); i++ {
			for c := 0; c < len(particles[i].velocity); c++ {
				particles[i].velocity[c] += particles[i].accel[c]
			}
			for c := 0; c < len(particles[i].position); c++ {
				particles[i].position[c] += particles[i].velocity[c]
			}
		}
	}
	fmt.Println(len(particles))
}
