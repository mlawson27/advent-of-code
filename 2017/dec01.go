package main

import (
	"fmt"
	"os"
)

func part1(content string) int {
	sum := 0
	for i_minus_1 := range content[1:] {
		i := i_minus_1 + 1
		if content[i] == content[i-1] {
			sum += int(content[i] - '0')
		}
	}
	if content[len(content)-1] == content[0] {
		sum += int(content[0] - '0')
	}
	return sum
}

func part2(content string) int {
	sum := 0
	for i := range content[0 : len(content)/2] {
		if content[i] == content[i+len(content)/2] {
			sum += int(content[i] - '0')
		}
	}
	return sum * 2
}

func main() {
	contentBytes, err := os.ReadFile("dec01.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)

	fmt.Println(part1(content))
	fmt.Println(part2(content))
}
