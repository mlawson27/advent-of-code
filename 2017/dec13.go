package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Layer struct {
	length     int
	currentPos int
	direction  int
}

type PeriodAndIndex struct {
	index  int
	period int
}

func main() {
	contentBytes, err := os.ReadFile("dec13.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")

	layers := map[int]*Layer{}
	maxLayer := 0
	for _, line := range lines {
		sidesOfLayerStr := strings.Split(line, ": ")
		layerNum, _ := strconv.Atoi(sidesOfLayerStr[0])
		layerLen, _ := strconv.Atoi(sidesOfLayerStr[1])
		layers[layerNum] = &Layer{length: layerLen, currentPos: 0, direction: 1}
		maxLayer = max(maxLayer, layerNum)
	}

	severity := 0
	for i := 0; i <= maxLayer; i++ {
		layer, found := layers[i]
		if found && layer.currentPos == 0 {
			severity += i * layer.length
		}
		for _, l := range layers {
			if l.length > 1 {
				l.currentPos += l.direction
				if l.currentPos == 0 || l.currentPos == l.length-1 {
					l.direction = -l.direction
				}
			}
		}
	}
	fmt.Println(severity)

	layerArr := make([]PeriodAndIndex, 0, len(layers))
	for i, l := range layers {
		layerArr = append(layerArr, PeriodAndIndex{index: i, period: (l.length - 1) * 2})
	}
	for delay := 0; true; delay++ {
		ok := true
		for _, l := range layerArr {
			ok = ((delay + l.index) % l.period) != 0
			if !ok {
				break
			}
		}
		if ok {
			fmt.Println(delay)
			break
		}
	}
}
