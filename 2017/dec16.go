package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Instruction struct {
	typeChar rune
	arg1     int
	arg2     int
}

const topChar = 'p'

func doRound(instrs []Instruction, orderToName *[topChar - 'a' + 1]rune, nameToOrder *[topChar - 'a' + 1]int) {
	for _, instr := range instrs {
		switch instr.typeChar {
		case 's':
			{
				for it := 0; it < instr.arg1; it++ {
					charToInsert := orderToName[len(orderToName)-1]
					for i := len(orderToName) - 1; i >= 1; i-- {
						orderToName[i] = orderToName[i-1]
						nameToOrder[orderToName[i]-'a'] = i
					}
					orderToName[0] = charToInsert
					nameToOrder[charToInsert-'a'] = 0
				}
			}

		case 'x':
			{
				pos1 := instr.arg1
				pos2 := instr.arg2
				temp := orderToName[pos1]
				orderToName[pos1] = orderToName[pos2]
				orderToName[pos2] = temp
				nameToOrder[orderToName[pos1]-'a'] = pos1
				nameToOrder[orderToName[pos2]-'a'] = pos2
			}

		case 'p':
			{
				char1 := rune(instr.arg1)
				char2 := rune(instr.arg2)
				temp := nameToOrder[char1-'a']
				nameToOrder[char1-'a'] = nameToOrder[char2-'a']
				nameToOrder[char2-'a'] = temp
				orderToName[nameToOrder[char1-'a']] = char1
				orderToName[nameToOrder[char2-'a']] = char2
			}
		}
	}
}

func main() {
	var swapRe = regexp.MustCompile(`s(\d+)`)
	var exchangeRe = regexp.MustCompile(`x(\d+)/(\d+)`)
	var parnerRe = regexp.MustCompile(`p([a-z])/([a-z])`)

	contentBytes, err := os.ReadFile("dec16.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	instrStrings := strings.Split(content, ",")
	instrs := make([]Instruction, 0, len(instrStrings))
	for _, instrString := range instrStrings {
		typeChar := rune(instrString[0])
		match := swapRe.FindStringSubmatch(instrString)
		if match != nil {
			numToMove, _ := strconv.Atoi(match[1])
			instrs = append(instrs, Instruction{typeChar: typeChar, arg1: numToMove})
			continue
		}

		match = exchangeRe.FindStringSubmatch(instrString)
		if match != nil {
			pos1, _ := strconv.Atoi(match[1])
			pos2, _ := strconv.Atoi(match[2])
			instrs = append(instrs, Instruction{typeChar: typeChar, arg1: pos1, arg2: pos2})
			continue
		}

		match = parnerRe.FindStringSubmatch(instrString)
		if match != nil {
			char1 := rune(match[1][0])
			char2 := rune(match[2][0])
			instrs = append(instrs, Instruction{typeChar: typeChar, arg1: int(char1), arg2: int(char2)})
			continue
		}

		panic(errors.New("Instruction not matching one of the templates"))
	}

	var orderToName [topChar - 'a' + 1]rune
	var nameToOrder [topChar - 'a' + 1]int

	for i := 0; i < len(orderToName); i++ {
		orderToName[i] = rune(int('a') + i)
		nameToOrder[i] = i
	}

	doRound(instrs, &orderToName, &nameToOrder)
	fmt.Println(string(orderToName[:]))

	firstRoundState := orderToName
	for i := 1; i < 1_000_000_000; i++ {
		doRound(instrs, &orderToName, &nameToOrder)
		if orderToName == firstRoundState {
			i = (1_000_000_000 / i) * i
		}
	}
	fmt.Println(string(orderToName[:]))
}
