package main

import (
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"
)

func sortWord(word string) string {
	charArray := []rune(word)
	sort.Slice(charArray, func(i int, j int) bool {
		return charArray[i] < charArray[j]
	})
	return string(charArray)
}

func part1(words [][]string) int {
	validPhrases := 0
	for _, line := range words {
		wordMap := map[string]bool{}
		anyFound := false
		for _, word := range line {
			_, found := wordMap[word]
			if found {
				anyFound = true
				break
			}
			wordMap[word] = true
		}
		if !anyFound {
			validPhrases++
		}
	}
	return validPhrases
}

func part2(words [][]string) int {

	validPhrases := 0
	for _, line := range words {
		wordMap := map[string]bool{}
		anyFound := false
		for _, word := range line {
			sortedWord := sortWord(word)
			_, found := wordMap[sortedWord]
			if found {
				anyFound = true
				break
			}
			wordMap[sortedWord] = true
		}
		if !anyFound {
			validPhrases++
		}
	}
	return validPhrases
}

func main() {
	wsRe := regexp.MustCompile("\\s")

	contentBytes, err := os.ReadFile("dec04.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	words := make([][]string, 0, len(lines))
	for _, line := range lines {
		lineWords := wsRe.Split(line, -1)
		words = append(words, lineWords)
	}

	fmt.Println(part1(words))
	fmt.Println(part2(words))
}
