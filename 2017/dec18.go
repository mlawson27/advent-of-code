package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type InstructionName int

type Instruction struct {
	name     InstructionName
	reg      rune
	arg      int
	argIsReg bool
}

const (
	snd InstructionName = iota
	set
	add
	mul
	mod
	rcv
	jgz
)

type Program struct {
	pc       int
	regs     [26]int
	rcvQueue []int
	numSent  int
}

func step(program *Program, instructions []Instruction, onSnd func(int, bool) bool, onRcv func(rune) bool) {
	instr := &instructions[program.pc]
	var argV int
	if instr.argIsReg {
		argV = program.regs[instr.arg-'a']
	} else {
		argV = instr.arg
	}
	switch instr.name {
	case snd:
		if !onSnd(instr.arg, instr.argIsReg) {
			return
		}
	case set:
		program.regs[instr.reg-'a'] = argV
	case add:
		program.regs[instr.reg-'a'] += argV
	case mul:
		program.regs[instr.reg-'a'] *= argV
	case mod:
		program.regs[instr.reg-'a'] %= argV
	case rcv:
		if !onRcv(rune(instr.arg) - 'a') {
			return
		}
	case jgz:
		break
	}
	if instr.name != jgz {
		program.pc++
	} else {
		regIsNum := unicode.IsDigit(instr.reg)
		var jVal int
		if regIsNum {
			jVal = int(instr.reg - '0')
		} else {
			jVal = program.regs[instr.reg-'a']
		}
		if jVal > 0 {
			program.pc += argV
		} else {
			program.pc++
		}
	}
}

func main() {
	instructionNameToVal := map[string]InstructionName{
		"snd": snd,
		"set": set,
		"add": add,
		"mul": mul,
		"mod": mod,
		"rcv": rcv,
		"jgz": jgz,
	}

	contentBytes, err := os.ReadFile("dec18.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	instructions := make([]Instruction, 0, len(lines))
	for _, line := range lines {
		tokens := strings.Split(line, " ")
		instrVal, found := instructionNameToVal[tokens[0]]
		if !found {
			panic(errors.New("Instruction name not found: " + tokens[0]))
		}
		var arg int
		var argIsReg bool
		if len(tokens) == 2 {
			tokens = append(tokens[0:2], tokens[1:]...)
			tokens[1] = "a"
		}
		argIsReg = len(tokens[2]) == 1 && unicode.IsLower(rune(tokens[2][0]))
		if argIsReg {
			arg = int(tokens[2][0])
		} else {
			arg, err = strconv.Atoi(tokens[2])
			if err != nil {
				panic(err)
			}
		}
		instructions = append(instructions, Instruction{name: instrVal, reg: rune(tokens[1][0]), arg: arg, argIsReg: argIsReg})
	}

	lastPlayedSoundFreq := 0
	lastRecoveredSoundFreq := 0
	programP1 := Program{}
	for programP1.pc >= 0 && programP1.pc < len(instructions) && lastRecoveredSoundFreq == 0 {
		step(&programP1, instructions,
			func(arg int, argIsReg bool) bool {
				if argIsReg {
					arg = programP1.regs[arg-'a']
				}
				lastPlayedSoundFreq = arg
				return true
			},
			func(reg rune) bool {
				if programP1.regs[reg] != 0 {
					lastRecoveredSoundFreq = lastPlayedSoundFreq
				}
				return true
			})
	}
	fmt.Println(lastRecoveredSoundFreq)

	var programsP2 [2]Program
	for i := 0; i < len(programsP2); i++ {
		programsP2[i].regs['p'-'a'] = i
		programsP2[i].rcvQueue = make([]int, 0, 500)
	}
	for true {
		allWaiting := true
		for i := 0; i < len(programsP2); i++ {
			if programsP2[i].pc >= 0 && programsP2[i].pc < len(instructions) {
				prePc := programsP2[i].pc
				step(&programsP2[i], instructions,
					func(arg int, argIsReg bool) bool {
						if argIsReg {
							arg = programsP2[i].regs[arg-'a']
						}
						programsP2[1-i].rcvQueue = append(programsP2[1-i].rcvQueue, arg)
						programsP2[i].numSent++
						return true
					},
					func(reg rune) bool {
						if len(programsP2[i].rcvQueue) > 0 {
							programsP2[i].regs[reg] = programsP2[i].rcvQueue[0]
							programsP2[i].rcvQueue = programsP2[i].rcvQueue[1:]
							return true
						}
						return false
					})
				allWaiting = allWaiting && (programsP2[i].pc == prePc)
			}
		}
		if allWaiting {
			break
		}
	}
	fmt.Println(programsP2[1].numSent)
}
