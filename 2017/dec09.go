package main

import (
	"fmt"
	"os"
)

func main() {
	contentBytes, err := os.ReadFile("dec09.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)

	groupCount := 0
	inJunk := false
	nestingLevel := 0
	junkCount := 0
	for i := 0; i < len(content); i++ {
		switch content[i] {
		case '!':
			i++
		case '<':
			if inJunk {
				junkCount++
			}
			inJunk = true
		case '>':
			inJunk = false
		case '{':
			if !inJunk {
				nestingLevel++
				groupCount += nestingLevel
			} else {
				junkCount++
			}
		case '}':
			if !inJunk {
				nestingLevel--
			} else {
				junkCount++
			}
		default:
			if inJunk {
				junkCount++
			}
		}
	}

	fmt.Println(groupCount)
	fmt.Println(junkCount)
}
