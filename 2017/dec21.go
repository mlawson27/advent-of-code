package main

import (
	"fmt"
	"os"
	"strings"
)

func flipOverX(input string) string {
	workingArr := []rune(input)
	numParts := strings.Count(input, "/") + 1
	partLen := (len(input) - numParts + 1) / numParts
	for partI := 0; partI < numParts; partI++ {
		partOffset := partI*partLen + partI
		for subPartI := 0; subPartI < partLen; subPartI++ {
			workingArr[partOffset+subPartI] = rune(input[partOffset+partLen-subPartI-1])
		}
	}
	return string(workingArr)
}

func rotateRight(input string) string {
	workingArr := []rune(input)
	numParts := strings.Count(input, "/") + 1
	partLen := (len(input) - numParts + 1) / numParts
	for partI := 0; partI < numParts; partI++ {
		for subPartI := 0; subPartI < partLen; subPartI++ {
			workingArr[partI*(partLen+1)+subPartI] = rune(input[(numParts-subPartI-1)*(partLen+1)+partI])
		}
	}
	return string(workingArr)
}

const startPattern = ".#./..#/###"

func main() {
	contentBytes, err := os.ReadFile("dec21.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	ruleMap := map[string]string{}
	for _, line := range lines {
		lineParts := strings.Split(line, " => ")
		workingRule := lineParts[0]
		ruleMap[workingRule] = lineParts[1]
		ruleMap[flipOverX(workingRule)] = lineParts[1]
		for i := 0; i < 3; i++ {
			workingRule = rotateRight(workingRule)
			ruleMap[workingRule] = lineParts[1]
			ruleMap[flipOverX(workingRule)] = lineParts[1]
		}
	}

	workingBoardArr := strings.Split(startPattern, "/")
	doIter := func() {
		sideLen := len(workingBoardArr)
		var squareSideLen int
		if sideLen%2 == 0 {
			squareSideLen = 2
		} else {
			squareSideLen = 3
		}
		squaresPerSide := sideLen / squareSideLen
		newWorkingBoardArr := make([]string, squaresPerSide*(squareSideLen+1))
		for y := 0; y < squaresPerSide; y++ {
			for x := 0; x < sideLen; x += squareSideLen {
				workingSquare := ""
				for squarePart := 0; squarePart < squareSideLen; squarePart++ {
					if workingSquare != "" {
						workingSquare += "/"
					}
					workingSquare += workingBoardArr[y*squareSideLen+squarePart][x : x+squareSideLen]
				}
				workingSquare = ruleMap[workingSquare]
				for squarePart := 0; squarePart < squareSideLen+1; squarePart++ {
					newWorkingBoardArr[y*(squareSideLen+1)+squarePart] += workingSquare[squarePart*(squareSideLen+2) : (squarePart+1)*(squareSideLen+2)-1]
				}
			}
		}
		workingBoardArr = newWorkingBoardArr
	}
	iter := 0
	for _, iterCount := range [2]int{5, 18} {
		for ; iter < iterCount; iter++ {
			doIter()
		}
		setCount := 0
		for _, line := range workingBoardArr {
			setCount += strings.Count(line, "#")
		}
		fmt.Println(setCount)
	}
}
