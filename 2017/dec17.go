package main

import (
	"fmt"
)

const numSpins = 304

func doIter(i int, nodeArr *[50000001]int) {
	currentNode := i - 1
	for j := 0; j < numSpins; j++ {
		currentNode = nodeArr[currentNode]
	}
	nodeArr[i] = nodeArr[currentNode]
	nodeArr[currentNode] = i
}

func main() {
	var nodeArr [50000001]int

	i := 1
	for ; i <= 2017; i++ {
		doIter(i, &nodeArr)
	}
	fmt.Println(nodeArr[i-1])

	for ; i < len(nodeArr); i++ {
		doIter(i, &nodeArr)
	}
	fmt.Println(nodeArr[0])
}
