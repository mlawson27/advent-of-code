package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func part1(rows [][]int) int {
	sum := 0
	for _, row := range rows {
		min := math.MaxInt
		max := math.MinInt
		for _, val := range row {
			if min > val {
				min = val
			}
			if max < val {
				max = val
			}
		}
		sum += (max - min)
	}
	return sum
}

func part2(rows [][]int) int {
	sum := 0
	for _, row := range rows {
		found := false
		for i, v1 := range row {
			for _, v2 := range row[i+1:] {
				if v1 >= v2 && v1%v2 == 0 {
					found = true
					sum += (v1 / v2)
					break
				} else if v2 >= v1 && v2%v1 == 0 {
					found = true
					sum += (v2 / v1)
					break
				}
			}
			if found {
				break
			}
		}
	}
	return sum
}

func main() {
	wsRe := regexp.MustCompile("\\s")

	contentBytes, err := os.ReadFile("dec02.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	rows := make([][]int, 0, len(lines))
	for _, line := range lines {
		rowStrings := wsRe.Split(line, -1)
		row := make([]int, 0, len(rowStrings))
		for _, token := range rowStrings {
			rowVal, _ := strconv.Atoi(token)
			row = append(row, rowVal)
		}
		rows = append(rows, row)
	}

	fmt.Println(part1(rows))
	fmt.Println(part2(rows))
}
