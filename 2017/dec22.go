package main

import (
	"fmt"
	"os"
	"strings"
)

type Direction int

const (
	Up Direction = iota
	Right
	Down
	Left
)

type State int

const (
	Weakened State = iota
	Infected
	Flagged
)

func directionToAdj(dir Direction) (int, int) {
	switch dir {
	case Up:
		return 0, -1
	case Down:
		return 0, 1
	case Left:
		return -1, 0
	case Right:
		return 1, 0
	}
	return 0, 0
}

func main() {
	contentBytes, err := os.ReadFile("dec22.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	setPositionsP1 := map[[2]int]bool{}
	setPositionsP2 := map[[2]int]State{}
	for y, line := range lines {
		for x, c := range line {
			pos := [2]int{x - len(line)/2, y - len(lines)/2}
			if c == '#' {
				setPositionsP1[pos] = true
				setPositionsP2[pos] = Infected
			}
		}
	}

	currentDir := Up
	workingPos := [2]int{0, 0}
	causedBursts := 0
	for iteration := 0; iteration < 10000; iteration++ {
		_, isSet := setPositionsP1[workingPos]
		if isSet {
			currentDir = (currentDir + 1) % 4
			delete(setPositionsP1, workingPos)
		} else {
			currentDir = (currentDir + 3) % 4
			setPositionsP1[workingPos] = true
			causedBursts++
		}
		adjX, adjY := directionToAdj(currentDir)
		workingPos[0] += adjX
		workingPos[1] += adjY
	}
	fmt.Println(causedBursts)

	currentDir = Up
	workingPos = [2]int{0, 0}
	causedBursts = 0
	for iteration := 0; iteration < 10000000; iteration++ {
		state, isNotClean := setPositionsP2[workingPos]
		if isNotClean {
			switch state {
			case Weakened:
				setPositionsP2[workingPos] = Infected
				causedBursts++
			case Infected:
				setPositionsP2[workingPos] = Flagged
				currentDir = (currentDir + 1) % 4
			case Flagged:
				delete(setPositionsP2, workingPos)
				currentDir = (currentDir + 2) % 4
			}
		} else {
			currentDir = (currentDir + 3) % 4
			setPositionsP2[workingPos] = Weakened
		}
		adjX, adjY := directionToAdj(currentDir)
		workingPos[0] += adjX
		workingPos[1] += adjY
	}
	fmt.Println(causedBursts)
}
