package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type ValueCondition struct {
	valueToWrite int
	xDir         int
	nextState    rune
}

type StateInfo struct {
	valueConditions [2]ValueCondition
}

func main() {
	beginRe := regexp.MustCompile(`Begin\s+in\s+state\s+([A-Z]).`)
	checksumAfterRe := regexp.MustCompile(`Perform\s+a\s+diagnostic\s+checksum\s+after\s+(\d+)\s+steps.`)
	inStateRe := regexp.MustCompile(`In\s+state\s+([A-Z]):`)
	ifValueIsRe := regexp.MustCompile(`\s+If\s+the\s+current\s+value\s+is\s+([01]):`)
	writeValueRe := regexp.MustCompile(`\s+-\s+Write\s+the\s+value\s+([01]).`)
	moveSlotRe := regexp.MustCompile(`\s+-\s+Move\s+one\s+slot\s+to\s+the\s+((?:left)|(?:right)).`)
	continueWithRe := regexp.MustCompile(`\s+-\s+Continue\s+with\s+state\s+([A-Z]).`)

	contentBytes, err := os.ReadFile("dec25.txt")
	if err != nil {
		panic(err)
	}
	content := string(contentBytes)
	sections := strings.Split(content, "\n\n")
	headerLines := strings.Split(sections[0], "\n")
	sections = sections[1:]
	var startState rune
	var stepsToChecksum int
	statesInfo := make([]StateInfo, len(sections))
	if len(headerLines) != 2 {
		panic(errors.New("First section not 2 lines"))
	}
	beginMatch := beginRe.FindStringSubmatch(headerLines[0])
	if beginMatch == nil {
		panic(errors.New("Couldn't match begin line" + headerLines[0]))
	}
	startState = rune(beginMatch[1][0])
	checksumAfterMatch := checksumAfterRe.FindStringSubmatch(headerLines[1])
	if checksumAfterMatch == nil {
		panic(errors.New("Couldn't match checksum after line" + headerLines[1]))
	}
	stepsToChecksum, err = strconv.Atoi(checksumAfterMatch[1])
	if err != nil {
		panic(errors.New("Steps to checksum not an integer " + checksumAfterMatch[1]))
	}
	for _, section := range sections {
		sectionLines := strings.Split(section, "\n")
		var workingStateInfo *StateInfo = nil
		var workingCondition *ValueCondition = nil
		for _, sectionLine := range sectionLines {
			match := inStateRe.FindStringSubmatch(sectionLine)
			if match != nil {
				workingStateInfo = &statesInfo[rune(match[1][0])-'A']
				continue
			}
			match = ifValueIsRe.FindStringSubmatch(sectionLine)
			if match != nil {
				val, _ := strconv.Atoi(match[1])
				workingCondition = &workingStateInfo.valueConditions[val]
				continue
			}
			match = writeValueRe.FindStringSubmatch(sectionLine)
			if match != nil {
				val, _ := strconv.Atoi(match[1])
				workingCondition.valueToWrite = val
				continue
			}
			match = moveSlotRe.FindStringSubmatch(sectionLine)
			if match != nil {
				if match[1] == "left" {
					workingCondition.xDir = -1
				} else {
					workingCondition.xDir = 1
				}
				continue
			}
			match = continueWithRe.FindStringSubmatch(sectionLine)
			if match != nil {
				workingCondition.nextState = rune(match[1][0])
				continue
			}
			panic(errors.New("Line not understood " + sectionLine))
		}
	}

	currentSetBits := map[int]bool{}
	currentState := startState
	workingXPos := 0
	for i := 0; i < stepsToChecksum; i++ {
		workingStateInfo := &statesInfo[currentState-'A']
		_, valueIs1 := currentSetBits[workingXPos]
		var workingCondition *ValueCondition = nil
		if valueIs1 {
			workingCondition = &workingStateInfo.valueConditions[1]
		} else {
			workingCondition = &workingStateInfo.valueConditions[0]
		}
		if workingCondition.valueToWrite == 1 {
			currentSetBits[workingXPos] = true
		} else if valueIs1 {
			delete(currentSetBits, workingXPos)
		}
		workingXPos += workingCondition.xDir
		currentState = workingCondition.nextState
	}
	fmt.Println(len(currentSetBits))
}
