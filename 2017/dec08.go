package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Instruction struct {
	reg         string
	adjustment  int
	regCond     string
	compareFunc func(int, int) bool
	valCond     int
}

func eq(a int, b int) bool {
	return a == b
}

func ne(a int, b int) bool {
	return a != b
}

func lt(a int, b int) bool {
	return a < b
}

func le(a int, b int) bool {
	return a <= b
}

func gt(a int, b int) bool {
	return a > b
}

func ge(a int, b int) bool {
	return a >= b
}

func main() {
	lineRe := regexp.MustCompile(`([a-z]+)\s+((?:inc)|(?:dec))\s+(-?\d+)\s+if\s+([a-z]+)\s+([!<>=]{1,2})\s+(-?\d+)`)

	operatorToFn := map[string]func(int, int) bool{"==": eq, "!=": ne, "<": lt, "<=": le, ">": gt, ">=": ge}

	contentBytes, err := os.ReadFile("dec08.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")

	instructions := make([]Instruction, 0, len(lines))
	registers := map[string]int{}
	for _, line := range lines {
		match := lineRe.FindStringSubmatch(line)
		adjustment, _ := strconv.Atoi(match[3])
		valCond, _ := strconv.Atoi(match[6])
		compareFunc := operatorToFn[match[5]]
		if strings.ToLower(match[2]) == "dec" {
			adjustment = -adjustment
		}

		instructions = append(instructions, Instruction{reg: match[1], adjustment: adjustment, regCond: match[4], compareFunc: compareFunc, valCond: valCond})

		registers[match[1]] = 0
		registers[match[6]] = 0
	}

	maxEverVal := math.MinInt
	for _, instruction := range instructions {
		if instruction.compareFunc(registers[instruction.regCond], instruction.valCond) {
			registers[instruction.reg] += instruction.adjustment
			maxEverVal = max(maxEverVal, registers[instruction.reg])
		}
	}

	maxReg := math.MinInt
	for _, val := range registers {
		maxReg = max(maxReg, val)
	}

	fmt.Println(maxReg)
	fmt.Println(maxEverVal)
}
