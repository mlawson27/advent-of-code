package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const (
	A = 0
	B = 1
)

func nextVal(curVal int, factor int) int {
	return int(int64(curVal) * int64(factor) % 2147483647)
}

func main() {
	contentBytes, err := os.ReadFile("dec15.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	content := string(contentBytes)
	lines := strings.Split(content, "\n")
	if len(lines) != 2 {
		panic(errors.New("More than 2 lines in input file"))
	}

	var vals [2]int
	for i, line := range lines {
		split := strings.Split(line, " ")
		v, _ := strconv.Atoi(split[len(split)-1])
		vals[i] = v
	}

	factors := [2]int{16807, 48271}

	valsPt1 := vals
	matchingCount := 0
	for i := 0; i < 40000000; i++ {
		for v := 0; v < len(factors); v++ {
			valsPt1[v] = nextVal(valsPt1[v], factors[v])
		}
		if (valsPt1[0] & 0xFFFF) == (valsPt1[1])&0xFFFF {
			matchingCount++
		}
	}
	fmt.Println(matchingCount)

	valsPt2 := vals
	matchingCount = 0
	for i := 0; i < 5000000; i++ {
		for v := 0; v < len(factors); v++ {
			for true {
				valsPt2[v] = nextVal(valsPt2[v], factors[v])
				if valsPt2[v]%(4<<v) == 0 {
					break
				}
			}
		}
		if (valsPt2[0] & 0xFFFF) == (valsPt2[1])&0xFFFF {
			matchingCount++
		}
	}
	fmt.Println(matchingCount)
}
