#include <fstream>
#include <iostream>
#include <string>

int main(int, char**)
{
  int64_t result;
  int64_t card_public_key, door_public_key;
  int64_t card_loop_size,  door_loop_size;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec25.txt");
    infile >> card_public_key >> door_public_key;
  }
  {
    for (auto& [public_key, loop_size] : { std::tie(card_public_key, card_loop_size),
                                           std::tie(door_public_key, door_loop_size) }) {
      loop_size = 0;
      int64_t val = 1;
      for (;;) {
        loop_size++;
        val = ((val * 7) % 20201227);
        if (val == public_key) {
          break;
        }
      }
    }
  }
  {
    result = 1;
    for (int64_t i = 0; i < door_loop_size; i++) {
      result = ((result * card_public_key) % 20201227);
    }
  }

  std::cout << "encryption key: " << result << std::endl;

  return 0;
}
