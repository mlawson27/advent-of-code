#include <fstream>
#include <cassert>
#include <iostream>
#include <map>
#include <regex>
#include <string>
#include <unordered_set>
#include <vector>

int main(int, char**)
{
  int64_t result = 0;
  std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>> ingredient_allergy_pairs;
  std::unordered_set<std::string> no_possible_allergies;
  std::map<std::string, std::string> ingredients_by_allergy;
  {
    static const std::regex CONTAINS_REGEX("contains\\s+");

    std::ifstream infile;
    std::string line;
    infile.open("2020/dec21.txt");
    while (infile && std::getline(infile, line)) {
      std::smatch contains_match;
      if (std::regex_search(line, contains_match, CONTAINS_REGEX)) {
        auto& new_pair = ingredient_allergy_pairs.emplace_back();
        for (auto&& [str, container] : { std::tie(contains_match.prefix(), new_pair.first),
                                         std::tie(contains_match.suffix(), new_pair.second) }) {
          std::smatch word_match;
          auto working = str.str();
          while(std::regex_search(working, word_match, std::regex("\\w+"))) {
            container.insert(word_match.str());
            working = word_match.suffix();
          }
        }
      }
    }
  }

  {
    auto working_pairs = ingredient_allergy_pairs;
    std::unordered_set<std::string> allergens_to_skip;
    for (;;) {
      bool any_alleries_left = false;
      std::string allergy_to_test;
      std::unordered_set<std::string> possible_allergen_ingredients;

      for (auto&& [ingredients, allergies] : working_pairs) {
        any_alleries_left = !allergies.empty();
        if (any_alleries_left && (allergens_to_skip.find(*allergies.begin()) == allergens_to_skip.end())) {
          allergy_to_test = *allergies.begin();
          possible_allergen_ingredients = ingredients;
          break;
        }
      }
      if (!any_alleries_left) {
        break;
      }
      for (auto&& [ingredients, allergies] : working_pairs) {
        if (allergies.find(allergy_to_test) != allergies.end()) {
          decltype(possible_allergen_ingredients) working;
          for (auto const& possible_ingredient : possible_allergen_ingredients) {
            if (ingredients.find(possible_ingredient) != ingredients.end()) {
              working.insert(possible_ingredient);
            }
          }
          std::swap(possible_allergen_ingredients, working);
        }
      }
      if (possible_allergen_ingredients.size() > 1) {
        allergens_to_skip.insert(allergy_to_test);
      }
      else {
        auto it = ingredients_by_allergy.insert({ allergy_to_test, *possible_allergen_ingredients.begin() }).first;
        for (auto&& [ingredients, allergies] : working_pairs) {
          if (ingredients.find(it->second) != ingredients.end()) {
            ingredients.erase(it->second);
          }
          if (allergies.find(it->first) != allergies.end()) {
            allergies.erase(it->first);
          }
        }
        allergens_to_skip.clear();

        for (auto&& [ingredients, allergies] : working_pairs) {
          for (auto const& possible_allergen_ingredient : possible_allergen_ingredients) {
            if (ingredients.find(possible_allergen_ingredient) != ingredients.end()) {
              ingredients.erase(possible_allergen_ingredient);
            }
          }
          if (allergies.find(allergy_to_test) != allergies.end()) {
            allergies.erase(allergy_to_test);
          }
        }
      }
    }
    for (auto&& [ingredients, allergies] : working_pairs) {
      for (auto const& ingredient : ingredients) {
        if (no_possible_allergies.find(ingredient) == no_possible_allergies.end()) {
          no_possible_allergies.insert(ingredient);
        }
      }
    }
  }

  result = 0;
  for (auto const& ingredient : no_possible_allergies) {
    for (auto&& [ingredients, allergies] : ingredient_allergy_pairs) {
      result += (ingredients.find(ingredient) != ingredients.end());
    }
  }

  std::string dangerous_list;
  for (auto&& [allergy, ingredient] : ingredients_by_allergy) {
    if (!dangerous_list.empty()) {
      dangerous_list += ",";
    }
    dangerous_list += ingredient;
  }

  std::cout << "result: " << result << std::endl;
  std::cout << "dangerous list: " << dangerous_list << std::endl;

  return 0;
}
