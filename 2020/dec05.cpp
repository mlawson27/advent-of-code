#include <array>
#include <bitset>
#include <fstream>
#include <iostream>
#include <string>

int main(int, char**)
{
  int result = 0;
  std::array<std::array<bool, 8>, 128> total;
  for (auto& inner : total) {
    inner.fill(false);
  }

  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec05.txt");
    while (infile && std::getline(infile, line)) {
      std::bitset<7> fb;
      std::bitset<3> lr;

      for (std::size_t i = 0; i < fb.size(); i++) {
        fb.set(fb.size() - i - 1, line[i] == 'B');
      }
      for (std::size_t i = 0; i < lr.size(); i++) {
        lr.set(lr.size() - i - 1, line[fb.size() + i] == 'R');
      }

      total[fb.to_ulong()][lr.to_ulong()] = true;

      result = std::max(result,
                        static_cast<int>((fb.to_ulong() * 8) + lr.to_ulong()));
    }
  }

  std::cout << "max seat ID: " << result << std::endl;

  bool found = false;
  for (std::size_t fb = 0; !found && fb < total.size(); fb++) {
    for (std::size_t lr = 0; !found && lr < total[fb].size(); lr++) {
      if (!total[fb][lr]) {
        total[fb][lr] = true;
      }
      else {
        found = true;
      }
    }
  }

  found = false;
  for (std::size_t fb = 0; !found && fb < total.size(); fb++) {
    for (std::size_t lr = 0; !found && lr < total[fb].size(); lr++) {
      if (!total[fb][lr]) {
        std::cout << "open ID: " << ((fb * 8) + lr) << std::endl;
        found = true;
      }
    }
  }

  return 0;
}
