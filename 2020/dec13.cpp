#include <fstream>
#include <iostream>
#include <numeric>
#include <regex>
#include <string>
#include <vector>

int main(int, char**)
{
  std::vector<std::pair<int, int>> bus_ids;
  int depart_time;

  int64_t result;
  {
    static const std::regex NUMBER_REGEX("(?:x|(\\d+))");

    std::ifstream infile;
    infile.open("2020/dec13.txt");

    std::string line;
    std::smatch match;
    int index = 0;
    infile >> depart_time >> line;
    while (std::regex_search(line, match, NUMBER_REGEX)) {
      if (match[1].matched) {
        bus_ids.push_back({ std::stoi(match[1]), index });
      }
      index++;
      line = match.suffix();
    }
  }

  // result = std::numeric_limits<decltype(result)>::max();
  // int min_id;
  // for (int id : bus_ids) {
  //   auto minutes_since_last = (depart_time % id);
  //   if ((id - minutes_since_last) < result) {
  //     result = (id - minutes_since_last);
  //     min_id = id;
  //   }
  // }
  // result *= min_id;
  // std::cout << "id * minutes to wait: " << result << std::endl;

  result = bus_ids[0].first + ((bus_ids[0].first - bus_ids[0].second + bus_ids[0].first) % bus_ids[0].first);
  decltype(result) to_add = bus_ids[0].first;
  for (int i = 1; i < bus_ids.size(); i++) {
    auto [bus_id, offset] = bus_ids[i];
    offset = ((((bus_id - offset) % bus_id) + bus_id) % bus_id);
    while ((result % bus_id) != offset) {
      result += to_add;
    }
    to_add = std::lcm(to_add, bus_id);
  }
  std::cout << "earilest time: " << result << std::endl;

  return 0;
}
