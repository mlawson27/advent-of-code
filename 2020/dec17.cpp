#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

template<typename T, typename... x>
struct make_vector_for_dim;

template<typename T>
struct make_vector_for_dim<T>
{
  using type = T;
};
template<typename T, typename X, typename... Others>
struct make_vector_for_dim<T, X, Others...>
{
  using type = std::vector<typename make_vector_for_dim<T, Others...>::type>;
};

template<typename element_type, typename... Args>
void allocate_vector_helper(typename make_vector_for_dim<element_type, std::size_t, Args...>::type& vec, std::size_t N, Args&&... args)
{
  for (std::size_t i = 0; i < N; i++) {
    vec.emplace_back();
    if constexpr (sizeof...(Args) > 0) {
      allocate_vector_helper<element_type>(vec.back(), std::forward<Args>(args)...);
    }
  }
}
template<typename element_type, typename... Args>
auto allocate_vector_helper(std::size_t N, Args&&... args)
  -> typename make_vector_for_dim<element_type, std::size_t, Args...>::type
{
  typename make_vector_for_dim<element_type, std::size_t, Args...>::type ret;
  allocate_vector_helper<element_type>(ret, N, std::forward<Args>(args)...);
  return ret;
}

template<typename T>
void copy_table(T const& orig, T& new_) {
  for (std::size_t dim = 0; dim < orig.size(); dim++) {
    if constexpr(std::is_same_v<T, std::vector<bool>>) {
      new_[dim + 1] = orig[dim];
    }
    else {
      copy_table(orig[dim], new_[dim + 1]);
    }
  }
}

template<typename T, typename... Args>
auto get_count(T const& vec, bool any_non_zero, std::size_t dim, Args... args) {
  static constexpr auto dimension_passes = [](auto dim, auto inc, auto max_len) {
    return (((inc < 0) && (dim > 0)) ||
            (inc  == 0) ||
            ((inc > 0) && (dim < (max_len - 1))));
  };

  std::size_t result = 0;
  for (int inc = -1; inc <= 1; inc++) {
    if (dimension_passes(dim, inc, vec.size())) {
      if constexpr(!std::is_same_v<T, std::vector<bool>>) {
        result += get_count(vec[dim + inc], any_non_zero || (inc != 0), std::forward<Args>(args)...);
      }
      else if (any_non_zero || (inc != 0)) {
        result += vec[dim + inc];
      }
    }
  }
  return result;
}

template<typename T, typename U, typename... Args>
void update_table(T const& orig, U const& orig_elem, U& new_, Args... dims) {
  for (std::size_t dim = 0; dim < new_.size(); dim++) {
    if constexpr(!std::is_same_v<U, std::vector<bool>>) {
      update_table(orig, orig_elem[dim], new_[dim], dims..., dim);
    }
    else {
      auto count = get_count(orig, false, dims..., dim);
      if (orig_elem[dim]) {
        new_[dim] = ((count == 2) || (count == 3));
      }
      else {
        new_[dim] = (count == 3);
      }
    }
  }
}

auto perform_step(typename make_vector_for_dim<bool, std::size_t, std::size_t, std::size_t, std::size_t>::type const& table)
{
  auto new_dims       = std::array{ table.size() + 2, table[0].size() + 2, table[0][0].size() + 2, table[0][0][0].size() + 2 };
  auto new_orig_table = allocate_vector_helper<bool>(new_dims[0], new_dims[1], new_dims[2], new_dims[3]);
  auto new_table      = allocate_vector_helper<bool>(new_dims[0], new_dims[1], new_dims[2], new_dims[3]);

  copy_table(table, new_orig_table);
  update_table(new_orig_table, new_orig_table, new_table);

  return new_table;
}

template <typename T>
static constexpr std::size_t sum(T const& v)
{
  std::size_t result = 0;
  for (auto const& elem : v) {
    if constexpr(std::is_same_v<T, std::vector<bool>>) {
      result += elem;
    }
    else {
      result += sum(elem);
    }
  }
  return result;
}

int main(int, char**)
{
  decltype(allocate_vector_helper<bool>(0, 0, 0, 0)) table;
  std::array<std::size_t, 4> reverse_dims;
  int64_t result;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec17.txt");

    std::vector<std::string> current_lines;
    while (infile && std::getline(infile, line)) {
      if (!current_lines.empty()) {
        assert(line.size() == current_lines.front().length());
      }
      current_lines.push_back(line);
    }
    assert(current_lines.size() == current_lines.front().size());
    reverse_dims = std::array{ std::size_t(1), std::size_t(1), current_lines.front().size(), current_lines.size() };
    table = allocate_vector_helper<bool>(reverse_dims[0], reverse_dims[1], reverse_dims[2], reverse_dims[3]);
    auto& in_table = table[0][0];
    for (std::size_t y = 0; y < in_table.size(); y++) {
      for (std::size_t x = 0; x < in_table[0].size(); x++) {
        in_table[y][x] = (current_lines[y][x] == '#');
      }
    }
  }

  for (int i = 0; i < 6; i++) {
    std::swap(table, perform_step(table));
    result = sum(table);
  }

  result = sum(table);

  std::cout << "# active cubes: " << result << std::endl;

  return 0;
}
