#include <fstream>
#include <iostream>
#include <string>
#include <regex>
#include <vector>

enum class AsmInst {
  acc,
  jmp,
  nop
};

int main(int, char**)
{
  std::vector<std::pair<AsmInst, int>> instructions;
  {
    static const std::regex ASM_REGEX("((?:jmp)|(?:nop)|(?:acc))\\s+(\\+|-)(\\d+)");

    std::ifstream infile;
    std::string line;
    infile.open("2020/dec08.txt");

    while (infile && std::getline(infile, line)) {
      std::smatch match;
      if (std::regex_match(line, match, ASM_REGEX)) {
        AsmInst inst;
        int arg;
        if (match[1] == "acc") {
          inst = AsmInst::acc;
        }
        else if (match[1] == "jmp") {
          inst = AsmInst::jmp;
        }
        else {
          inst = AsmInst::nop;
        }

        arg = std::stoi(match[3]);
        if (match[2] == "-") {
          arg = -arg;
        }

        instructions.emplace_back(inst, arg);
      }
    }
  }

  std::vector<bool> hit(instructions.size());
  int accum;
  for (int i = 0; i < instructions.size(); i++) {
    {
      auto& [inst, arg] = instructions[i];
      switch (inst) {
        case AsmInst::acc:
          break;
        case AsmInst::nop:
          inst = AsmInst::jmp;
          break;
        case AsmInst::jmp:
          inst = AsmInst::nop;
          break;
      }
    }

    accum = 0;
    int current = 0;
    std::fill(hit.begin(), hit.end(), false);
    while(current < instructions.size() && !hit[current]) {
      hit[current] = true;

      auto&& [inst, arg] = instructions[current];

      switch (inst) {
        case AsmInst::acc:
          accum += arg;
          [[fallthrough]];
        case AsmInst::nop:
          current++;
          break;
        case AsmInst::jmp:
          current += arg;
          break;
      }
    }

    if (current < instructions.size())
    {
      auto& [inst, arg] = instructions[i];
      switch (inst) {
        case AsmInst::acc:
          break;
        case AsmInst::nop:
          inst = AsmInst::jmp;
          break;
        case AsmInst::jmp:
          inst = AsmInst::nop;
          break;
      }
    }
    else {
      break;
    }
  }

  std::cout << "accum: " << accum << std::endl;
  // std::cout << "num bags needed: " << count_map(DESIRED_CONTAINER, containers) << std::endl;

  return 0;
}
