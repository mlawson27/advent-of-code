#include <fstream>
#include <iostream>
#include <regex>
#include <string>

int main(int, char**)
{
  int valid_count_p1 = 0;
  int valid_count_p2 = 0;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec02.txt");
    while (infile && std::getline(infile, line)) {
      std::regex line_regex("(\\d+)-(\\d+)\\s*(\\w):\\s+(\\w+)");
      std::smatch	match_results;

      if (std::regex_match(line, match_results, line_regex)) {
        int         min_count = std::stoi(match_results[1]);
        int         max_count = std::stoi(match_results[2]);
        char        char_test = match_results[3].str()[0];
        std::string pw        = match_results[4].str();

        auto char_found_count = std::count(pw.cbegin(),
                                           pw.cend(),
                                           char_test);

        valid_count_p1 += ((char_found_count >= min_count) &&
                           (char_found_count <= max_count));

        valid_count_p2 += ((pw[min_count - 1] == char_test) !=
                           (pw[max_count - 1] == char_test));
      }
    }
  }

  std::cout << "valid passwords: " << valid_count_p1 << ", " << valid_count_p2 << std::endl;

  return 0;
}
