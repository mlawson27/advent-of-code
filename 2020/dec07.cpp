#include <fstream>
#include <iostream>
#include <string>
#include <regex>
#include <unordered_map>
#include <unordered_set>

bool test_map(std::string const& test,
              std::string const& subtest,
              std::unordered_map<std::string, std::unordered_map<std::string, int>> const& map)
{
  auto found = map.find(subtest);
  if (found == map.cend()) {
    return false;
  }

  for (auto&& [substr, count] : found->second)
  {
    if ((substr == test) ||
        test_map(test, substr, map)) {
      return true;
    }
  }

  return false;
}

int count_map(std::string const& test,
              std::unordered_map<std::string, std::unordered_map<std::string, int>> const& map)
{
  auto found = map.find(test);
  if (found == map.cend()) {
    return 0;
  }

  int result = 0;
  for (auto&& [substr, count] : found->second)
  {
    result += count * (count_map(substr, map) + 1);
  }

  return result;
}

int main(int, char**)
{
  static const std::string DESIRED_CONTAINER = "shiny gold";

  std::unordered_map<std::string, std::unordered_map<std::string, int>> containers;
  std::size_t result = 0;
  {
    static const std::regex RULE_REGEX("(\\d+)?\\s*(\\w\\w+\\s+\\w+)\\s+bags?");

    std::ifstream infile;
    std::string line;
    infile.open("2020/dec07.txt");

    while (infile && std::getline(infile, line)) {
      std::smatch match;
      std::string parent;
      while (std::regex_search(line, match, RULE_REGEX)) {
        std::string result = match[2].str();
        if (parent.empty()) {
          parent = result;
          containers[parent] = {};
        }
        else if (result != "no other") {
          containers[parent].insert(std::pair{ result, std::stoi(match[1]) });
        }
        line = match.suffix();
      }
    }
  }

  for (auto&& [key, pair] : containers) {
    result += test_map(DESIRED_CONTAINER, key, containers);
  }

  std::cout << "num bags containing: " << result << std::endl;
  std::cout << "num bags needed: " << count_map(DESIRED_CONTAINER, containers) << std::endl;

  return 0;
}
