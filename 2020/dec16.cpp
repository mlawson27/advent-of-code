#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using results_type = std::unordered_map<std::string, std::unordered_set<std::size_t>>;

void remove_singles(results_type&                       possible_results,
                    typename results_type::mapped_type& possible_cols,
                    std::size_t                         i)
{
  possible_cols.erase(i);
  if (possible_cols.size() == 1) {
    auto only_left = *possible_cols.begin();
    for (auto&& [sub_rule_name, sub_possible_cols] : possible_results) {
      if ((&possible_cols != &sub_possible_cols) &&
          (sub_possible_cols.find(only_left) != sub_possible_cols.end())) {
        remove_singles(possible_results, sub_possible_cols, only_left);
      }
    }
  }
}

int main(int, char**)
{
  std::unordered_map<std::string, std::array<std::pair<int64_t, int64_t>, 2>> rules;
  std::vector<int64_t> your_ticket;
  std::vector<std::vector<int64_t>> nearby_tickets;

  int64_t result;
  {
    static const std::regex RULE_REGEX("([^:]+):\\s+(?:(\\d+)-(\\d+))\\s+or\\s+(?:(\\d+)-(\\d+))");
    static const std::regex NUM_REGEX ("\\d+");

    std::ifstream infile;
    infile.open("2020/dec16.txt");

    std::string line;
    std::smatch match;

    while (infile && std::getline(infile, line) && std::regex_match(line, match, RULE_REGEX)) {
      rules.insert(std::pair{ match[1].str(),
                              std::array { std::pair { std::stoll(match[2]), std::stoll(match[3]) },
                                           std::pair { std::stoll(match[4]), std::stoll(match[5]) }} });
    }
    infile >> std::ws;
    std::getline(infile, line);
    std::getline(infile, line);
    while (std::regex_search(line, match, NUM_REGEX)) {
      your_ticket.push_back(std::stoll(match.str()));
      line = match.suffix();
    }

    infile >> std::ws;
    std::getline(infile, line);
    while (infile && std::getline(infile, line)) {
      auto& nearby_tickets_line = nearby_tickets.emplace_back();
      while (std::regex_search(line, match, NUM_REGEX)) {
        nearby_tickets_line.push_back(std::stoll(match.str()));
        line = match.suffix();
      }
    }
  }

  result = 0;
  {
    decltype(nearby_tickets) valid_tickets;
    for (auto const& nearby_ticket_line : nearby_tickets) {
      bool inside_all = true;
      for (auto const& nearby_ticket_num : nearby_ticket_line) {
        bool inside_any = false;
        for (auto&& [rule_name, rule_ranges] : rules) {
          for (auto const& rule_range : rule_ranges) {
            inside_any = (inside_any || ((nearby_ticket_num >= rule_range.first) &&
                                         (nearby_ticket_num <= rule_range.second)));
          }
        }
        inside_all = (inside_all && inside_any);
      }
      if (inside_all) {
        valid_tickets.push_back(nearby_ticket_line);
      }
    }
    std::swap(nearby_tickets, valid_tickets);
  }
  {
    results_type possible_results;
    for (auto&& [rule_name, rule_ranges] : rules) {
      auto rule_pair_it = possible_results.insert(decltype(possible_results)::value_type { rule_name, {} }).first;
      for(std::size_t i = 0; i < your_ticket.size(); i++) {
        rule_pair_it->second.insert(i);
      }
    }

    for (auto&& [rule_name, possible_cols] : possible_results) {
      auto const& rule_ranges = rules[rule_name];
      for (std::size_t i = 0; i < your_ticket.size(); i++) {
        for (auto const& nearby_ticket_line : nearby_tickets) {
          bool inside_any = false;
          for (auto const& rule_range : rule_ranges) {
            inside_any = (inside_any || ((nearby_ticket_line[i] >= rule_range.first) &&
                                         (nearby_ticket_line[i] <= rule_range.second)));
          }
          if ((possible_cols.find(i) != possible_cols.end()) &&
              !inside_any) {
            remove_singles(possible_results, possible_cols, i);
          }
        }
      }
    }
    result = 1;
    for (auto&& [rule_name, possible_cols] : possible_results) {
      assert(possible_cols.size() == 1);
      std::smatch m;
      if (std::regex_search(rule_name, m, std::regex("departure"))) {
        result *= your_ticket[*possible_cols.begin()];
      }
    }
  }

  std::cout << "error rate: " << result << std::endl;

  return 0;
}
