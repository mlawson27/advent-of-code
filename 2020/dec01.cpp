#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>

int main(int, char**)
{
  std::set<int> nums;
  std::ifstream infile;

  {
    infile.open("2020/dec01.txt");
    while (infile) {
      int num;
      infile >> num;
      nums.insert(num);
    }
  }

  for (auto i = nums.cbegin(); i != nums.cend(); i = std::next(i)) {
    for (auto j = std::next(i); (*i + *j + *nums.cbegin()) < 2020 && j != nums.cend(); j = std::next(j)) {
      if (std::binary_search(nums.cbegin(),
                             nums.cend(),
                             2020 - *i - *j)) {
        std::cout << "Product: " << *i * *j * (2020 - *i - *j) << std::endl;
        return 0;
      }
    }
  }
  return -1;
}
