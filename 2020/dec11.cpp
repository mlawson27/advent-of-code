#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

int perform_translations(std::vector<std::string>& lines)
{
  int changes = 0;
  std::vector<std::string> new_lines = lines;

  static constexpr auto count_adjacent = [](std::size_t row, std::size_t col, char to_test, decltype(lines) lines) {
    static constexpr auto nearest_seat_matches = [](int row, int col, int row_inc, int col_inc, char to_test, decltype(lines) lines) {
      while ((row + row_inc) >= 0 &&
              static_cast<std::size_t>((row + row_inc)) < lines.size() &&
              (col + col_inc) >= 0 &&
              static_cast<std::size_t>((col + col_inc)) < lines[row].size()) {
        row += row_inc;
        col += col_inc;
        if ((lines[row][col] == '#') || (lines[row][col] == 'L')) {
          return (lines[row][col] == to_test);
        }
      }
      return false;
    };

    int sum = 0;
    for (int row_inc = -1; row_inc <= 1; row_inc++) {
      for (int col_inc = -1; col_inc <= 1; col_inc++) {
        if (col_inc || row_inc) {
          sum += nearest_seat_matches(static_cast<int>(row),
                                      static_cast<int>(col),
                                      row_inc,
                                      col_inc,
                                      to_test,
                                      lines);
        }
      }
    }
    return sum;
  };

  for (std::size_t row = 0; row < lines.size(); row++) {
    for (std::size_t col = 0; col < lines[row].size(); col++) {
      auto& new_line_char = new_lines[row][col];
      switch (lines[row][col]) {
        case 'L': {
          if (count_adjacent(row, col, '#', lines) == 0) {
            new_line_char = '#';
            changes++;
          }
          break;
        }
        case '#': {
          if (count_adjacent(row, col, '#', lines) >= 5) {
            new_line_char = 'L';
            changes++;
          }
          break;
        }
        default: break;
      }
    }
  }

  std::swap(lines, new_lines);

  return changes;
}

int main(int, char**)
{
  std::vector<std::string> lines;

  int64_t result;
  {
    std::ifstream infile;
    infile.open("2020/dec11.txt");

    std::string line;
    while (infile && std::getline(infile, line)) {
      lines.push_back(line);
    }
  }

  while(perform_translations(lines))
  {
    // for (auto const& line: lines) {
    //   std:: cout << line << std::endl;
    // }
    // std::cout << std::endl;
  }
  result = std::accumulate(lines.begin(),
                           lines.end(),
                           0,
                           [](int current, std::string& str) { return current + static_cast<int>(std::count(str.begin(), str.end(), '#')); });


  // result = get_count(nums.cbegin(), nums.cend());

  std::cout << "# occupied seats: " << result << std::endl;
  // std::cout << "num bags needed: " << count_map(DESIRED_CONTAINER, containers) << std::endl;

  return 0;
}
