#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

using direction = std::pair<char, int>;
using position  = std::tuple<direction, direction>;

void process_movement(position& wp_current, position& sh_current, direction const& movement)
{
  static constexpr auto move_cardinal = [](direction& wp_current, direction movement, char dir1, char dir2) {
    auto&  [wp_pos_char,   wp_pos_int]   = wp_current;
    auto&& [movement_char, movement_int] = movement;

    if (wp_pos_char != movement_char) {
      movement_int *= -1;
    }
    wp_pos_int += movement_int;
    if (wp_pos_int < 0) {
      wp_pos_int = abs(wp_pos_int);
      wp_pos_char = (wp_pos_char == dir1
                     ? dir2
                     : dir1);
    }
  };

  static constexpr auto rotate_quarter_turn = [](position& wp_current, bool left) {
    auto& [wp_ns_pos,      wp_ew_pos]     = wp_current;
    auto& [wp_ns_pos_char, wp_ns_pos_int] = wp_ns_pos;
    auto& [wp_ew_pos_char, wp_ew_pos_int] = wp_ew_pos;

    for (auto* facing_char_ptr : { &wp_ns_pos_char, &wp_ew_pos_char }) {
      auto& facing_char = *facing_char_ptr;
      switch (facing_char) {
        case 'N':
          facing_char = (left ? 'W' : 'E');
          break;
        case 'E':
          facing_char = (left ? 'N' : 'S');
          break;
        case 'S':
          facing_char = (left ? 'E' : 'W');
          break;
        case 'W':
          facing_char = (left ? 'S' : 'N');
          break;
        default: break;
      }
    }
    std::swap(wp_ns_pos, wp_ew_pos);
  };

  auto& [wp_ns_pos,      wp_ew_pos]     = wp_current;
  auto& [wp_ns_pos_char, wp_ns_pos_int] = wp_ns_pos;
  auto& [wp_ew_pos_char, wp_ew_pos_int] = wp_ew_pos;

  auto& [sh_ns_pos,      sh_ew_pos]     = sh_current;
  auto& [sh_ns_pos_char, sh_ns_pos_int] = sh_ns_pos;
  auto& [sh_ew_pos_char, sh_ew_pos_int] = sh_ew_pos;

  auto [movement_char, movement_int] = movement;

  switch (movement_char)
  {
    case 'F': {
      move_cardinal(sh_ns_pos, { wp_ns_pos_char, wp_ns_pos_int * movement_int }, 'N', 'S');
      move_cardinal(sh_ew_pos, { wp_ew_pos_char, wp_ew_pos_int * movement_int }, 'E', 'W');
      break;
    }
    case 'E':
    case 'W': {
      move_cardinal(wp_ew_pos, movement, 'E', 'W');
      break;
    }
    case 'N':
    case 'S': {
      move_cardinal(wp_ns_pos, movement, 'N', 'S');
      break;
    }
    case 'L':
    case 'R': {
      int quarter_turns = (movement_int / 90);
      for (int i = 0; i < quarter_turns; i++) {
        rotate_quarter_turn(wp_current, (movement_char == 'L'));
      }
      break;
    }
    default: break;
  }
}

int main(int, char**)
{
  position wp_current = { { 'N', 1 }, { 'E', 10 } };
  position sh_current = { { 'N', 0 }, { 'E',  0 } };

  std::vector<direction> movements;
  int64_t result;
  {
    std::ifstream infile;
    infile.open("2020/dec12.txt");

    std::string line;
    while (infile) {
      char dir;
      int movement;
      infile >> dir >> movement >> std::ws;

      movements.emplace_back(dir, movement);
    }
  }

  for (auto& movement : movements)
  {
    process_movement(wp_current, sh_current, movement);
  }
  auto& [ns_pos, ew_pos] = sh_current;

  result = ns_pos.second + ew_pos.second;

  std::cout << "current position: " << result << std::endl;

  return 0;
}
