#include <array>
#include <fstream>
#include <iostream>
#include <list>
#include <string>
#include <unordered_set>

std::string join(std::list<std::size_t> const& hand) {
  std::string ret = "";
  for (auto it = hand.begin(); it != hand.end(); it = std::next(it)) {
    if (it != hand.begin()) {
      ret += ",";
    }
    ret += std::to_string(*it);
  }
  return ret;
}

bool play_game(std::array<std::list<std::size_t>, 2>& player_hands)
{
  std::unordered_set<std::string> prev_hand_strs;

  while (!player_hands[0].empty() && !player_hands[1].empty()) {
    auto hands_str = join(player_hands[0]) + ";" + join(player_hands[1]);
    if (prev_hand_strs.find(hands_str) != prev_hand_strs.end()) {
      return false;
    }
    prev_hand_strs.insert(hands_str);

    auto p1 = player_hands[0].front();
    auto p2 = player_hands[1].front();
    player_hands[0].pop_front();
    player_hands[1].pop_front();

    bool winner;
    if ((p1 <= player_hands[0].size()) &&
        (p2 <= player_hands[1].size())) {
      std::array<std::list<std::size_t>, 2> sub_game_hands;
      for (auto&& [player_num, num_to_take] : { std::pair{ false, p1 },
                                                std::pair{ true,  p2 } }) {
        std::size_t count = 0;
        for (auto it = player_hands[player_num].begin(); count < num_to_take; it = std::next(it), count++) {
          sub_game_hands[player_num].push_back(*it);
        }
      }
      winner = play_game(sub_game_hands);
    }
    else {
      winner = (p2 > p1);
    }
    if (winner) {
      player_hands[1].push_back(p2);
      player_hands[1].push_back(p1);
    }
    else {
      player_hands[0].push_back(p1);
      player_hands[0].push_back(p2);
    }
  }
  return player_hands[0].empty();
}

int main(int, char**)
{
  std::array<std::list<std::size_t>, 2> player_hands;
  int64_t result = 0;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec22.txt");
    for (int i = 0; i < 2; i++) {
      std::getline(infile, line);
      while (infile && std::getline(infile, line) && !line.empty()) {
        player_hands[i].push_back(std::stoul(line));
      }
    }
  }

  auto& winner = player_hands[play_game(player_hands)];
  result = 0;
  int count = 1;
  for (auto rev = winner.rbegin(); rev != winner.rend(); rev++)
  {
    result += (*rev * count);
    count++;
  }

  std::cout << "winning score: " << result << std::endl;

  return 0;
}
