#include <array>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <optional>
#include <regex>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

enum direction_t {
  LEFT,
  TOP,
  RIGHT,
  BOTTOM,
  DIR_COUNT
};

enum modification_flags_t {
  NONE        = 0,
  ROTATED_90  = 1,
  ROTATED_180 = 2,
  ROTATED_270 = 3,
  FLIPPED_X   = 4,
  FLIPPED_Y   = 8,
  FLIPPED     = (FLIPPED_X | FLIPPED_Y)
};

union tile_extent_to_id_val_t {
  struct {
    int64_t id      : 60;
    int64_t dir     :  3;
    int64_t flipped :  1;
  } set;
  int64_t val;
};

using tile_extents_by_id_t = std::unordered_map<int64_t, std::array<std::array<std::optional<uint64_t>, DIR_COUNT>, 2>>;
using tile_extents_to_id_t = std::unordered_map<uint64_t, std::unordered_set<int64_t>>;
using position_vector_t    = std::vector<std::vector<int64_t>>;

static constexpr auto OPPOSITE_DIR = std::array{ RIGHT, BOTTOM, LEFT, TOP };

uint64_t flip(uint64_t extent, std::size_t len)
{
  decltype(extent) result = 0;
  for (std::size_t i = 0; i < len; i++) {
    result = (result | (((extent >> (len - i - 1)) & 0b1) << i));
  }
  return result;
}

void reorient_text(std::vector<std::string>& in, int translation)
{
  bool flip_x = ((translation & FLIPPED_X) != 0);
  bool flip_y = ((translation & FLIPPED_Y) != 0);

  switch (translation & 0b11) {
    case NONE: break;
    case ROTATED_90: {
      std::vector<std::string> working;
      for (std::size_t inner = 0; inner < in[0].size(); inner++) {
        working.emplace_back();
        for (std::size_t outer = 0; outer < in.size(); outer++) {
          working[inner] += in[in[0].size() - 1 - outer][inner];
        }
      }
      std::swap(in, working);
      break;
    }
    case ROTATED_180: {
      flip_x = !flip_x;
      flip_y = !flip_y;
      break;
    }
    case ROTATED_270: {
      std::vector<std::string> working;
      for (std::size_t inner = 0; inner < in[0].size(); inner++) {
        working.emplace_back();
        for (std::size_t outer = 0; outer < in.size(); outer++) {
          working[inner] += in[outer][in[0].size() - 1 - inner];
        }
      }
      std::swap(in, working);
      break;
    }
    default: throw;
  }

  if (flip_x) {
    for (auto& str : in) {
      std::reverse(str.begin(), str.end());
    }
  }
  if (flip_y) {
    std::reverse(in.begin(), in.end());
  }
}

void reorient_id(int64_t                                                id,
                 int                                                    translation,
                 tile_extents_by_id_t&                                  tile_extents_by_id,
                 tile_extents_to_id_t&                                  tile_extents_to_id,
                 std::unordered_map<int64_t, std::vector<std::string>>& images_strs)
{
  static constexpr auto swap_extents_to_id_on_dir = [](int64_t id, int dir1, int dir2, int flipped_int,
                                                       tile_extents_by_id_t& tile_extents_by_id,
                                                       tile_extents_to_id_t& tile_extents_to_id) {
    for (auto dir : { dir1, dir2 }) {
      auto& containers = tile_extents_by_id[id][flipped_int];
      if (containers[dir].has_value()) {
        auto& set = tile_extents_to_id[*containers[dir]];
        tile_extent_to_id_val_t val_un;
        val_un.set.id      = id;
        val_un.set.dir     = dir;
        val_un.set.flipped = flipped_int;
        if (set.find(val_un.val) != set.end()) {
          set.erase(val_un.val);
          val_un.set.dir = ((dir == dir1) ? dir2 : dir1);
          set.insert(val_un.val);
        }
      }
    }
  };

  static constexpr auto swap_extents_to_id_on_flipped = [](int64_t id, int dir,
                                                           tile_extents_by_id_t& tile_extents_by_id,
                                                           tile_extents_to_id_t& tile_extents_to_id) {
    for (auto flipped : { false, true }) {
      auto& containers = tile_extents_by_id[id][flipped];
      if (containers[dir].has_value()) {
        auto& set = tile_extents_to_id[*containers[dir]];
        tile_extent_to_id_val_t val_un;
        val_un.set.id      = id;
        val_un.set.dir     = dir;
        val_un.set.flipped = flipped;
        if (set.find(val_un.val) != set.end()) {
          set.erase(val_un.val);
          val_un.set.flipped = !flipped;
          set.insert(val_un.val);
        }
      }
    }
  };

  bool flip_x = ((translation & FLIPPED_X) != 0);
  bool flip_y = ((translation & FLIPPED_Y) != 0);

  switch (translation & 0b11) {
    case NONE: break;
    case ROTATED_90: {
      for (int flipped_int = 0; flipped_int < 2; flipped_int++) {
        auto& containers = tile_extents_by_id[id][flipped_int];
        for (int i = 1; i < containers.size(); i++) {
          swap_extents_to_id_on_dir(id, i, 0, flipped_int, tile_extents_by_id, tile_extents_to_id);
          std::swap(containers[0], containers[i]);
        }
      }
      for (auto dir : { TOP, BOTTOM }) {
        swap_extents_to_id_on_flipped(id, dir, tile_extents_by_id, tile_extents_to_id);
        std::swap(tile_extents_by_id[id][false][dir], tile_extents_by_id[id][true][dir]);
      }
      break;
    }
    case ROTATED_180: {
      flip_x = !flip_x;
      flip_y = !flip_y;
      break;
    }
    case ROTATED_270: {
      for (int flipped_int = 0; flipped_int < 2; flipped_int++) {
        auto& containers = tile_extents_by_id[id][flipped_int];
        for (int i = 0; i < containers.size() - 1; i++) {
          swap_extents_to_id_on_dir(id, 4 - i - 1, 0, flipped_int, tile_extents_by_id, tile_extents_to_id);
          std::swap(containers[0], containers[4 - i - 1]);
        }
      }
      for (auto dir : { LEFT, RIGHT }) {
        swap_extents_to_id_on_flipped(id, dir, tile_extents_by_id, tile_extents_to_id);
        std::swap(tile_extents_by_id[id][false][dir], tile_extents_by_id[id][true][dir]);
      }
      break;
    }
    default: throw;
  }

  if (flip_x) {
    for (int flipped_int = 0; flipped_int < 2; flipped_int++) {
      auto& containers = tile_extents_by_id[id][flipped_int];
      swap_extents_to_id_on_dir(id, LEFT, RIGHT, flipped_int, tile_extents_by_id, tile_extents_to_id);
      std::swap(containers[LEFT], containers[RIGHT]);
    }
    for (auto dir : { TOP, BOTTOM }) {
      swap_extents_to_id_on_flipped(id, dir, tile_extents_by_id, tile_extents_to_id);
      std::swap(tile_extents_by_id[id][false][dir], tile_extents_by_id[id][true][dir]);
    }
  }
  if (flip_y) {
    for (int flipped_int = 0; flipped_int < 2; flipped_int++) {
      auto& containers = tile_extents_by_id[id][flipped_int];
      for (auto dir : { TOP, BOTTOM }) {
        swap_extents_to_id_on_dir(id, TOP, BOTTOM, flipped_int, tile_extents_by_id, tile_extents_to_id);
      }
      std::swap(containers[TOP], containers[BOTTOM]);
    }
    for (auto dir : { LEFT, RIGHT }) {
      swap_extents_to_id_on_flipped(id, dir, tile_extents_by_id, tile_extents_to_id);
      std::swap(tile_extents_by_id[id][false][dir], tile_extents_by_id[id][true][dir]);
    }
  }

  reorient_text(images_strs[id], translation);
};


void align_image(tile_extents_by_id_t&                                  tile_extents_by_id,
                 tile_extents_to_id_t&                                  tile_extents_to_id,
                 position_vector_t&                                     position,
                 std::unordered_map<int64_t, std::vector<std::string>>& images_strs)
{
  const auto length = static_cast<std::size_t>(std::lround(std::sqrt(tile_extents_by_id.size())));

  static const auto try_find_id_for_extent = [&tile_extents_to_id](int64_t id, uint64_t extent, int64_t* found_id, int* found_direction, int* found_flipped_int) {
    std::size_t count = 0;
    auto it = tile_extents_to_id.find(extent);
    if (it != tile_extents_to_id.end()) {
      for (auto const& val : it->second) {
        tile_extent_to_id_val_t val_un;
        val_un.val = val;
        if (val_un.set.id != id) {
          if (found_id != nullptr) {
            *found_id = val_un.set.id;
          }
          if (found_direction != nullptr) {
            *found_direction = static_cast<int>(val_un.set.dir);
          }
          if (found_flipped_int != nullptr) {
            *found_flipped_int = static_cast<int>(val_un.set.flipped);
          }
          count++;
        }
      }
    }
    return count;
  };

  std::unordered_map<int64_t, std::array<bool, DIR_COUNT>> non_matches;
  for (auto&& [id, extents] : tile_extents_by_id) {
    for (int direction = 0; direction < DIR_COUNT; direction++) {
      auto result = try_find_id_for_extent(id, *extents[false][direction], nullptr, nullptr, nullptr);
      assert(result < 2);
      if (result == 0) {
        auto it = non_matches.find(id);
        if (it == non_matches.end()) {
          it = non_matches.insert({ id, {} }).first;
          it->second.fill(false);
        }
        it->second[direction] = true;
      }
    }
  }

  std::unordered_set<int64_t> corners;
  for (auto&& [id, dirs] : non_matches) {
    if (std::count(dirs.cbegin(),
                    dirs.cend(),
                    true) == 2) {
      corners.insert(id);
    }
  }
  assert(corners.size() == 4);

  position.resize(length);
  for (auto& elem : position)
  {
    elem.resize(length);
  }

  static const auto remove_id_dir = [&tile_extents_by_id, &tile_extents_to_id](int64_t id, int dir) {
    auto iter = tile_extents_by_id.find(id);
    if (iter != tile_extents_by_id.end()) {
      for (int flipped_int = 0; flipped_int < 2; flipped_int++) {
        auto const& extent = iter->second[flipped_int][dir];
        if (extent.has_value()) {
          auto to_id_iter = tile_extents_to_id.find(*extent);
          if (to_id_iter != tile_extents_to_id.end()) {
            tile_extent_to_id_val_t val_un;
            val_un.set.id      = id;
            val_un.set.dir     = dir;
            val_un.set.flipped = flipped_int;
            if (to_id_iter->second.find(val_un.val) != to_id_iter->second.end()) {
              to_id_iter->second.erase(val_un.val);
              if (to_id_iter->second.empty()) {
                tile_extents_to_id.erase(*extent);
              }
            }
          }
          iter->second[flipped_int][dir].reset();
        }
      }
      if (!std::any_of(iter->second[0].cbegin(),
                       iter->second[0].cend(),
                       std::mem_fn(&std::optional<uint64_t>::has_value))) {
        tile_extents_by_id.erase(id);
      }
    }
  };

  static constexpr auto to_translation = [](int dest, int from, bool flipped)
    -> modification_flags_t {
    switch (dest) {
      case LEFT: {
        switch (from) {
          case LEFT:   return static_cast<modification_flags_t>(NONE        | (flipped  * FLIPPED_Y));
          case RIGHT:  return static_cast<modification_flags_t>(ROTATED_180 | (!flipped * FLIPPED_Y));
          case TOP:    return static_cast<modification_flags_t>(ROTATED_270 | (!flipped * FLIPPED_Y));
          case BOTTOM: return static_cast<modification_flags_t>(ROTATED_90  | (flipped  * FLIPPED_Y));
          default: throw;
        }
      }
      case RIGHT: {
        switch (from) {
          case LEFT:   return static_cast<modification_flags_t>(ROTATED_180 | (!flipped * FLIPPED_Y));
          case RIGHT:  return static_cast<modification_flags_t>(NONE        | (flipped  * FLIPPED_Y));
          case TOP:    return static_cast<modification_flags_t>(ROTATED_90  | (flipped  * FLIPPED_Y));
          case BOTTOM: return static_cast<modification_flags_t>(ROTATED_270 | (!flipped * FLIPPED_Y));
          default: throw;
        }
      }
      case TOP: {
        switch (from) {
          case LEFT:   return static_cast<modification_flags_t>(ROTATED_90  | (!flipped * FLIPPED_X));
          case RIGHT:  return static_cast<modification_flags_t>(ROTATED_270 | (flipped  * FLIPPED_X));
          case TOP:    return static_cast<modification_flags_t>(NONE        | (flipped  * FLIPPED_X));
          case BOTTOM: return static_cast<modification_flags_t>(ROTATED_180 | (!flipped * FLIPPED_X));
          default: throw;
        }
      }
      case BOTTOM: {
        switch (from) {
          case LEFT:   return static_cast<modification_flags_t>(ROTATED_270 | (flipped  * FLIPPED_X));
          case RIGHT:  return static_cast<modification_flags_t>(ROTATED_90  | (!flipped * FLIPPED_X));
          case TOP:    return static_cast<modification_flags_t>(ROTATED_180 | (!flipped * FLIPPED_X));
          case BOTTOM: return static_cast<modification_flags_t>(NONE        | (flipped  * FLIPPED_X));
          default: throw;
        }
      }
      default: throw;
    }
  };

  for (auto&& id : corners) {
    for (int dir = 0; dir < DIR_COUNT; dir++) {
      if (non_matches[id][dir]) {
        remove_id_dir(id, dir);
      }
    }
  }

  std::list<int64_t> chain;
  int chain_dir;
  std::size_t chain_count = 0;
  int64_t corner_id;
  while (!corners.empty()) {
    int dir;
    switch (chain_count) {
      case 0: {
        while (!tile_extents_by_id[*corners.begin()][false][BOTTOM].has_value() ||
               !tile_extents_by_id[*corners.begin()][false][RIGHT].has_value()) {
          reorient_id(*corners.begin(), ROTATED_90, tile_extents_by_id, tile_extents_to_id, images_strs);
        }
        corner_id = *corners.begin();
        dir = RIGHT;
        break;
      }
      case 1: {
        dir = BOTTOM;
        break;
      }
      case 2: {
        for (auto const& corner_id2 : corners) {
          std::size_t dir_count = 0;
          for (int dir2 = 0; dir2 < DIR_COUNT; dir2++) {
            if (tile_extents_by_id[corner_id2][false][dir2].has_value()) {
              dir_count++;
              dir = dir2;
            }
          }
          if ((dir_count == 1) &&
              (dir == RIGHT)) {
            corner_id = corner_id2;
            break;
          }
        }
        break;
      }
      case 3: {
        for (auto const& corner2 : corners) {
          if (tile_extents_by_id[corner2][false][BOTTOM].has_value()) {
            corner_id = corner2;
            break;
          }
        }
        dir = BOTTOM;
        break;
      }
    }

    chain.push_back(corner_id);
    chain_dir = OPPOSITE_DIR[dir];

    while (chain.size() < (length - 1)) {
      auto&& chain_last_id = chain.back();
      int64_t other_id;
      int other_direction, flipped_int;
      auto& extent = tile_extents_by_id[chain_last_id][false][OPPOSITE_DIR[chain_dir]];
      std::size_t result;

      if (extent.has_value() &&
          (result = try_find_id_for_extent(chain_last_id, *extent, &other_id, &other_direction, &flipped_int))) {
        assert(result < 2);
        auto& im1 = images_strs[chain_last_id];
        auto& im2 = images_strs[other_id];
        reorient_id(other_id, to_translation(chain_dir, other_direction, (flipped_int != 0)), tile_extents_by_id, tile_extents_to_id, images_strs);
        chain.push_back(other_id);
      }
    }

    {
      auto&& chain_last_id = chain.back();
      auto& extent = tile_extents_by_id[chain_last_id][false][OPPOSITE_DIR[chain_dir]];
      int64_t other_id;
      int other_direction, flipped_int;
      std::size_t result;
      if (extent.has_value() &&
          (result = try_find_id_for_extent(chain_last_id, *extent, &other_id, &other_direction, &flipped_int)) &&
          (corners.find(other_id) != corners.end())) {
        assert(result < 2);
        reorient_id(other_id, to_translation(chain_dir, other_direction, (flipped_int != 0)), tile_extents_by_id, tile_extents_to_id, images_strs);
        chain.push_back(other_id);
      }
    }

    std::size_t position_in_chain = 0;
    for (auto&& chain_id : chain) {
      auto& pos = [](std::size_t position_in_chain, std::size_t chain_count, decltype(position) position)
        -> decltype(position[0][0]) {
        switch (chain_count) {
          case 0: return position[0][position_in_chain];
          case 1: return position[position_in_chain][0];
          case 2: return position[position.size() - 1][position_in_chain];
          case 3: return position[position_in_chain][position.size() - 1];
          default: throw;
        }
      }(position_in_chain, chain_count, position);
      pos = chain_id;
      position_in_chain++;
      remove_id_dir(chain_id, chain_dir);
      remove_id_dir(chain_id, OPPOSITE_DIR[chain_dir]);
      if (tile_extents_by_id.find(chain_id) == tile_extents_by_id.cend()) {
        corners.erase(chain_id);
      }
    }
    chain.clear();
    chain_count++;
  }

  for (std::size_t y = 1; y < position.size() - 1; y++) {
    for (std::size_t x = 1; x < position[y].size() - 1; x++) {
      auto const& left_id = position[y][x - 1];
      auto const& top_id  = position[y - 1][x];
      auto& left_extent = tile_extents_by_id[left_id][false][RIGHT];
      auto& top_extent  = tile_extents_by_id[top_id] [false][BOTTOM];
      int64_t left_other_id, top_other_id;
      int left_other_direction, top_other_direction, left_flipped_int, top_flipped_int;
      std::size_t left_result, top_result;
      if (left_extent.has_value() &&
          top_extent.has_value() &&
          (left_result = try_find_id_for_extent(left_id, *left_extent, &left_other_id, &left_other_direction, &left_flipped_int)) &&
          (top_result  = try_find_id_for_extent(top_id,  *top_extent,  &top_other_id,  &top_other_direction,  &top_flipped_int))) {
        assert((left_result < 2)  || (top_result < 2));
        assert((left_result != 1) || (top_result != 1) || (left_result == top_result));
        if (left_result == 1) {
          position[y][x] = left_other_id;
          reorient_id(left_other_id, to_translation(LEFT, left_other_direction, (left_flipped_int != 0)), tile_extents_by_id, tile_extents_to_id, images_strs);
          remove_id_dir(left_other_id, LEFT);
          remove_id_dir(left_other_id, TOP);
        }
        else if (top_result == 1) {
          position[y][x] = top_other_id;
          reorient_id(top_other_id, to_translation(LEFT, top_other_direction, (top_flipped_int != 0)), tile_extents_by_id, tile_extents_to_id, images_strs);
          remove_id_dir(top_other_id, LEFT);
          remove_id_dir(top_other_id, TOP);
        }
      }
    }
  }
}

int main(int, char**)
{
  int64_t result = 0;
  {
    tile_extents_by_id_t tile_extents_by_id;
    tile_extents_to_id_t tile_extents_to_id;
    std::unordered_map<int64_t, std::vector<std::string>> images_strs;
    position_vector_t position;
    {
      static const std::regex TILE_ID_REGEX("Tile (\\d+):");

      std::ifstream infile;
      std::string line;
      std::smatch tile_id_match;

      infile.open("2020/dec20.txt");
      while (infile && std::getline(infile, line)) {
        decltype(tile_extents_by_id)::iterator tiles_insert_iter;
        decltype(images_strs)::iterator        strs_insert_iter;
        if (std::regex_match(line, tile_id_match, TILE_ID_REGEX)) {
          tiles_insert_iter = tile_extents_by_id.insert({ std::stoll(tile_id_match[1]), {} }).first;
          strs_insert_iter  = images_strs.insert({ tiles_insert_iter->first, {} }).first;
        }
        while (infile && std::getline(infile, line) && !line.empty()) {
          strs_insert_iter->second.push_back(line);
        }
        for (auto&& [dir, iter]: { std::pair{ TOP,    strs_insert_iter->second.begin() },
                                   std::pair{ BOTTOM, std::prev(strs_insert_iter->second.end()) } }) {
          auto& val = tiles_insert_iter->second[false][dir].emplace(0);
          for (std::size_t i = 0; i < iter->size(); i++) {
            val = (val | ((((*iter)[i]) == '#') << i));
          }
          tiles_insert_iter->second[true][dir] = flip(val, iter->size());
          for (auto [extent, flipped] : { std::pair(val,                     false),
                                          std::pair(flip(val, iter->size()), true) }) {
            auto extent_to_id_it = tile_extents_to_id.find(extent);
            if (extent_to_id_it == tile_extents_to_id.end()) {
              extent_to_id_it = tile_extents_to_id.insert({ extent, {} }).first;
            }
            tile_extent_to_id_val_t val_un;
            val_un.set.id      = tiles_insert_iter->first;
            val_un.set.dir     = dir;
            val_un.set.flipped = flipped;
            extent_to_id_it->second.insert(val_un.val);
          }
        }
        using pair_t = std::pair<direction_t, char const&(std::string::*)()const>;
        for (auto&& [dir, mem_fn]: { pair_t{ LEFT,  &std::string::front },
                                     pair_t{ RIGHT, &std::string::back } }) {
          auto& val = tiles_insert_iter->second[false][dir].emplace(0);
          for (std::size_t i = 0; i < strs_insert_iter->second.size(); i++) {
            val = (val | (((strs_insert_iter->second[i].*mem_fn)() == '#') << i));
          }
          tiles_insert_iter->second[true][dir] = flip(val, strs_insert_iter->second.size());
          for (auto [extent, flipped] : { std::pair(val,                                        false),
                                          std::pair(flip(val, strs_insert_iter->second.size()), true) }) {
            auto extent_to_id_it = tile_extents_to_id.find(extent);
            if (extent_to_id_it == tile_extents_to_id.end()) {
              extent_to_id_it = tile_extents_to_id.insert({ extent, {} }).first;
            }
            tile_extent_to_id_val_t val_un;
            val_un.set.id      = tiles_insert_iter->first;
            val_un.set.dir     = dir;
            val_un.set.flipped = flipped;
            extent_to_id_it->second.insert(val_un.val);
          }
        }
      }
    }

// tile_extents_by_id[0] = {};
// images_strs[0] = {
//   "0123456780",
//   "1   xx   1",
//   "2        2",
//   "3        3",
//   "4        4",
//   "5        5",
//   "6        6",
//   "7        7",
//   "8        8",
//   "9123456789",
// };
// reorient_id(0, ROTATED_270, tile_extents_by_id, images_strs);
// for (auto& str : images_strs[0]) {
//   std::cout << str << std::endl;
// }

    align_image(tile_extents_by_id, tile_extents_to_id, position, images_strs);

    // {
    //   const auto len = images_strs[position[0][0]].size();
    //   for (auto const& row : position) {
    //     for (std::size_t count = 0; count < len; count++) {
    //       for (auto row_iter = row.begin(); row_iter != row.end(); row_iter++) {
    //         if (row_iter != row.begin()) {
    //            std::cout << " ";
    //         }
    //         std::cout << images_strs[*row_iter][count];
    //       }
    //       std::cout << std::endl;
    //     }
    //     std::cout << std::endl;
    //   }
    // }

    std::vector<std::string> adjusted_strs;
    auto size = images_strs[*position.begin()->begin()].size();
    for (auto const& row : position) {
      for (std::size_t count = 1; count < size - 1; count++) {
        std::string str;
        for (auto const& id : row) {
          str += std::string(std::next(images_strs[id][count].begin()), std::prev(images_strs[id][count].end()));
        }
        adjusted_strs.push_back(std::move(str));
      }
    }

    auto found_count = [&adjusted_strs]() {
      static const std::regex MIDDLE_REGEX("(#)[\\.#][\\.#][\\.#][\\.#](##)[\\.#][\\.#][\\.#][\\.#](##)[\\.#][\\.#][\\.#][\\.#](###)");
      static const std::regex LAST_REGEX  ("[\\.#]#[\\.#][\\.#]#[\\.#][\\.#]#[\\.#][\\.#]#[\\.#][\\.#]#[\\.#][\\.#]#[\\.#][\\.#][\\.#]");

      std::vector<std::string> working;
      std::size_t max_found_count = 0;
      int max_translation;
      for (int rotation = NONE; rotation <= ROTATED_270; rotation++) {
        for (auto flip : { NONE, FLIPPED_X, FLIPPED_Y, FLIPPED }) {
          std::size_t found_count = 0;
          int         translation = (rotation | flip);
          working = adjusted_strs;
          reorient_text(working, translation);
          for (std::size_t i = 1; i < working.size() - 1; i++) {
            auto line = working[i];
            std::smatch middle_match, bottom_match;
            std::size_t middle_match_prefix_len = 0;
            while (std::regex_search(line, middle_match, MIDDLE_REGEX)) {
              middle_match_prefix_len += middle_match.prefix().str().size();
              auto next_line_to_match = std::string(working[i + 1].begin() + middle_match_prefix_len,
                                                    working[i + 1].end()   - middle_match.suffix().str().size());
              assert(next_line_to_match.size() == middle_match.str().size());
              if (std::regex_match(next_line_to_match,
                                   bottom_match,
                                   LAST_REGEX) &&
                  (working[i - 1][middle_match_prefix_len + middle_match.str().size() - 2] == '#')) {
                found_count++;
              }
              line = middle_match.suffix();
              middle_match_prefix_len += middle_match.str().size();
            }
          }
          if (found_count > max_found_count) {
            max_found_count = found_count;
            max_translation = translation;
          }
        }
      }
      if (max_found_count > 0) {
        working = adjusted_strs;
        reorient_text(working, max_translation);
        std::swap(adjusted_strs, working);
      }
      return max_found_count;
    }();

    // for (auto& str : adjusted_strs) {
    //   std::cout << str << std::endl;
    // }

    result = 0;
    for (auto& str : adjusted_strs) {
      result += std::count(str.begin(),
                           str.end(),
                           '#');
    }
    result -= (found_count * 15);
  }

  std::cout << "corner id product: " << result << std::endl;

  return 0;
}
