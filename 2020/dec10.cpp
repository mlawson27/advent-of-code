#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

template<typename iter>
int64_t get_count(iter begin, iter end)
{
  int64_t result = (begin == std::prev(end));

  for (auto iter = std::next(begin); iter != end; iter = std::next(iter)) {
    auto diff = (*iter - *begin);
    if (diff > 3) {
      break;
    }
    else {
      result += (get_count(iter, end));
    }
  }
  return result;
}


int main(int, char**)
{
  std::vector<int> nums;
  nums.push_back(0);

  int64_t result;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec10.txt");

    while (infile) {
      int num;
      infile >> num;

      nums.push_back(num);
    }
    nums.pop_back();
  }
  std::sort(nums.begin(), nums.end());
  nums.push_back(nums.back() + 3);

  int count1diff = 0, count3diff = 0;

  for (auto iter = nums.cbegin(); iter != std::prev(nums.cend()); iter = std::next(iter)) {
    count1diff += ((*std::next(iter) - *iter) == 1);
    count3diff += ((*std::next(iter) - *iter) == 3);
  }
  result = (count1diff * count3diff);

  result = 1;
  for (auto outer_iter = nums.cbegin(); outer_iter != std::prev(nums.cend()); outer_iter = std::next(outer_iter)) {
    auto inner_iter = outer_iter;
    while ((std::next(inner_iter) < nums.end()) &&
           (*std::next(inner_iter) - *inner_iter < 3)) {
      inner_iter = std::next(inner_iter);
    }
    result *= get_count(outer_iter, std::next(inner_iter));
    outer_iter = inner_iter;
  }

  // result = get_count(nums.cbegin(), nums.cend());

  std::cout << "3 jolt x 1 jolt: " << result << std::endl;
  // std::cout << "num bags needed: " << count_map(DESIRED_CONTAINER, containers) << std::endl;

  return 0;
}
