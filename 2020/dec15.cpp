#include <fstream>
#include <iostream>
#include <numeric>
#include <optional>
#include <regex>
#include <string>
#include <unordered_map>
#include <vector>


int main(int, char**)
{
  std::unordered_map<int64_t, int64_t> numbers;

  int64_t result, turn, last_spoken;
  {
    static const std::regex NUM_REGEX("\\d+");

    std::ifstream infile;
    infile.open("2020/dec15.txt");

    std::string line;
    std::smatch match;

    turn = 0;
    while (infile && std::getline(infile, line)) {
      while (std::regex_search(line, match, NUM_REGEX)) {
        if (turn)  {
          numbers[last_spoken] = turn;
        }
        turn++;
        last_spoken = std::stoll(match.str());
        line = match.suffix();
      }
    }
  }

  for (auto next_turn = turn; next_turn < 30000000; next_turn++) {
    decltype(last_spoken) next_spoken;
    if (numbers.find(last_spoken) == numbers.cend()) {
      next_spoken = 0;
    }
    else {
      next_spoken = (next_turn - numbers[last_spoken]);
    }
    numbers[last_spoken] = next_turn;
    last_spoken = next_spoken;
  }
  result = last_spoken;

  std::cout << "2020th word spoken: " << result << std::endl;

  return 0;
}
