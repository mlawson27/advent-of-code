#include <execution>
#include <fstream>
#include <iostream>
#include <optional>
#include <regex>
#include <string>
#include <variant>
#include <vector>
#include <unordered_map>
#include <unordered_set>

struct node {
  int64_t num;
  std::variant<char, std::vector<std::vector<node const*>>> data;
};

std::unordered_set<std::size_t> _passes_rule(node const& r, std::string::const_iterator str, std::string::const_iterator end)
{
  if (str == end) {
    return {};
  }
  else if (std::holds_alternative<char>(r.data)) {
    if (*str == std::get<char>(r.data)) {
      return { 1 };
    }
    else {
      return { };
    }
  }
  else {
    std::unordered_set<std::size_t> possible_matches;
    for (auto const& rule_outer_list : std::get<1>(r.data)) {
      std::unordered_set<std::size_t> node_results;
      for (auto node_it = rule_outer_list.begin(); node_it != rule_outer_list.end(); node_it++) {
        std::unordered_map<std::size_t, decltype(node_results)> local_node_results;
        if (node_it == rule_outer_list.begin()) {
          auto immediate_results = _passes_rule(*(*node_it), str, end);
          auto it = local_node_results.insert(decltype(local_node_results)::value_type { 0, {} }).first;
          for (auto&& result : immediate_results) {
            it->second.insert(result);
          }
        }
        else {
          for (auto&& node_result : node_results) {
            auto immediate_results = _passes_rule(*(*node_it), str + node_result, end);
            auto it = local_node_results.insert({ node_result, {} }).first;
            for (auto&& result : immediate_results) {
              it->second.insert(result);
            }
          }
        }

        decltype(node_results) node_results_working;
        for (auto&& [prev_result, new_result_set] : local_node_results) {
          for (auto&& new_result : new_result_set) {
            node_results_working.insert(prev_result + new_result);
          }
        }
        std::swap(node_results_working, node_results);
        if (node_results.empty()) {
          break;
        }
      }
      for (auto&& result : node_results) {
        possible_matches.insert(result);
      }
    }
    return possible_matches;
  }
}

bool passes_rule(node const& r, std::string const& str)
{
  auto str_iter = str.cbegin();
  auto options  = _passes_rule(r, str_iter, str.end());

  return (options.find(str.size()) != options.end());
}

int main(int, char**)
{
  int64_t result = 0;
  std::unordered_map<int64_t, node> rule_nodes;
  std::vector<std::string>          messages;
  {
    std::unordered_map<int64_t, std::string> orig_rule_lines;
    {
      static const std::regex GENERIC_RULE_REGEX("(\\d+)\\s*:\\s*");

      std::ifstream infile;
      std::string line;
      infile.open("2020/dec19.txt");
      std::smatch header_match;
      while (infile && std::getline(infile, line) && std::regex_search(line, header_match, GENERIC_RULE_REGEX)) {
        orig_rule_lines.insert(std::pair{ std::stoll(header_match[1]), header_match.suffix() });
      }
      while (infile && std::getline(infile, line) && !line.empty()) {
        messages.push_back(line);
      }
      orig_rule_lines[ 8] = "42 | 42 8";
      orig_rule_lines[11] = "42 31 | 42 11 31";
    }

    {
      static const std::regex TEXT_RULE_REGEX("\"(\\w+)\"");
      static const std::regex RULE_NUM_REGEX("((?:\\d+)|\\|)");

      std::vector<int64_t> keys_to_remove;
      std::smatch rule_match;
      for (auto&& [rule_num, rule_text]: orig_rule_lines) {
        if (std::regex_match(rule_text, rule_match, TEXT_RULE_REGEX)) {
          rule_nodes.insert(std::pair{ rule_num, node{ rule_num, rule_match[1].str()[0] } });
          keys_to_remove.push_back(rule_num);
        }
      }
      for (auto&& rule_num : keys_to_remove) {
        orig_rule_lines.erase(rule_num);
      }

      while (!orig_rule_lines.empty()) {
        keys_to_remove.clear();
        for (auto&& [rule_num, rule_text]: orig_rule_lines) {
          bool        success      = true;
          std::string working_line = rule_text;

          auto it = rule_nodes.insert(std::pair{ rule_num, node{ rule_num, {} } }).first;
          it->second.data.emplace<1>();
          auto* rule_vec = &std::get<1>(it->second.data).emplace_back();
          while (std::regex_search(working_line, rule_match, RULE_NUM_REGEX)) {
            if (rule_match[1].str()[0] == '|') {
              rule_vec = &std::get<1>(it->second.data).emplace_back();
            }
            else {
              auto other_rule_num = std::stoll(rule_match[1]);
              if (auto other_node_iter = rule_nodes.find(other_rule_num);
                  other_node_iter != rule_nodes.end()) {
                rule_vec->push_back(&other_node_iter->second);
              }
              else {
                rule_nodes.erase(rule_num);
                success = false;
                break;
              }
            }
            working_line = rule_match.suffix();
          }

          if (success) {
            keys_to_remove.push_back(rule_num);
          }
        }
        for (auto&& rule_num : keys_to_remove) {
          orig_rule_lines.erase(rule_num);
        }
      }
    }
  }

  result = std::count_if(std::execution::par,
                         messages.begin(),
                         messages.end(),
                         [&rule_nodes](auto const& message) { return passes_rule(rule_nodes[0], message); });

  std::cout << "# matching messages: " << result << std::endl;

  return 0;
}
