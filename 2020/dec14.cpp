#include <fstream>
#include <iostream>
#include <numeric>
#include <optional>
#include <regex>
#include <string>
#include <unordered_map>
#include <vector>

static void populate_memory_with_floating(uint64_t addr_working,
                                          uint64_t value,
                                          decltype(std::declval<std::vector<std::size_t>>().cbegin()) floating_indices_begin,
                                          decltype(std::declval<std::vector<std::size_t>>().cend())   floating_indices_end,
                                          std::unordered_map<uint64_t, uint64_t>& memory) {
  if (floating_indices_begin == floating_indices_end) {
    memory[addr_working] = value;
  }
  else {
    auto index = *floating_indices_begin;
    populate_memory_with_floating(addr_working & ~(UINT64_C(1) << index), value, std::next(floating_indices_begin), floating_indices_end, memory);
    populate_memory_with_floating(addr_working |  (UINT64_C(1) << index), value, std::next(floating_indices_begin), floating_indices_end, memory);
  }
};


int main(int, char**)
{
  // uint64_t and_bitmask = (INT64_C(1) << 36) - 1;
  // uint64_t or_bitmask  = 0;
  // std::unordered_map<uint64_t, uint64_t> memory;

  // int64_t result;
  // {
  //   static const std::regex MASK_REGEX("mask\\s+=\\s+([01X]{36})");
  //   static const std::regex MEM_REGEX("mem\\[(\\d+)\\]\\s+=\\s+(\\d+)");

  //   std::ifstream infile;
  //   infile.open("2020/dec14.txt");

  //   std::string line;
  //   std::smatch match;

  //   while (infile && std::getline(infile, line)) {
  //     if (std::regex_match(line, match, MASK_REGEX)) {
  //       and_bitmask = (INT64_C(1) << 36) - 1;
  //       or_bitmask  = 0;
  //       auto match_str = match[1].str();
  //       for (std::size_t i = 0; i < match_str.size(); i++) {
  //         auto index = (match_str.size() - i - 1);
  //         switch (match_str[i]) {
  //           case '0': {
  //             and_bitmask &= ~(UINT64_C(1) << index);
  //             break;
  //           }
  //           case '1': {
  //             or_bitmask |= (UINT64_C(1) << index);
  //             break;
  //           }
  //           default: break;
  //         }
  //       }
  //     }
  //     else if (std::regex_match(line, match, MEM_REGEX)) {
  //       memory[std::stoull(match[1])] = ((std::stoull(match[2]) | or_bitmask) & and_bitmask);
  //     }
  //   }
  // }

  // result = 0;
  // for (auto const& [addr, val] : memory) {
  //   result += val;
  // }

  uint64_t or_bitmask;
  std::vector<std::size_t> floating_indices = {};
  std::unordered_map<uint64_t, uint64_t> memory;

  int64_t result;
  {
    static const std::regex MASK_REGEX("mask\\s+=\\s+([01X]{36})");
    static const std::regex MEM_REGEX("mem\\[(\\d+)\\]\\s+=\\s+(\\d+)");

    std::ifstream infile;
    infile.open("2020/dec14.txt");

    std::string line;
    std::smatch match;

    while (infile && std::getline(infile, line)) {
      if (std::regex_match(line, match, MASK_REGEX)) {
        or_bitmask = 0;
        floating_indices.clear();
        auto match_str = match[1].str();
        for (std::size_t i = 0; i < match_str.size(); i++) {
          auto index = (match_str.size() - i - 1);
          switch (match_str[i]) {
            case '1': {
              or_bitmask |= (UINT64_C(1) << index);
              break;
            }
            case 'X': {
              floating_indices.push_back(index);
              break;
            }
            default: break;
          }
        }
      }
      else if (std::regex_match(line, match, MEM_REGEX)) {
        uint64_t addr_post_mask = (std::stoull(match[1]) | or_bitmask);
        populate_memory_with_floating(addr_post_mask, std::stoull(match[2]),  floating_indices.cbegin(), floating_indices.cend(), memory);
      }
    }
  }

  result = 0;
  for (auto const& [addr, val] : memory) {
    result += val;
  }

  std::cout << "memory sum: " << result << std::endl;

  return 0;
}
