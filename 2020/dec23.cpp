#include <array>
#include <execution>
#include <iostream>
#include <list>
#include <unordered_set>

template <uint64_t V>
static constexpr auto log10int()
{
  auto working = V;
  std::size_t result = 0;
  while (working > 0) {
    result++;
    working = (working / 10);
  }
  return result;
}

template <uint64_t V>
static constexpr auto to_array()
{
  std::array<int64_t, log10int<V>()> ret = {};
  auto working = V;
  for (std::size_t i = 0; working > 0; i++, working = working / 10) {
    ret[ret.size() - i - 1] = (working % 10);
  }
  return ret;
}

int main(int, char**)
{
  static constexpr auto INPUT = to_array<368195742>();

  int64_t result = 0;
  {
    std::list<decltype(INPUT)::value_type> working(INPUT.begin(), INPUT.end());
    std::vector<decltype(working)::iterator> positions;
    for (decltype(INPUT)::value_type val = static_cast<int64_t>(INPUT.size()) + 1; val <= 1000000U; val++) {
      working.push_back(val);
    }
    positions.resize(working.size() + 1);
    for (auto it = working.begin(); it != working.end(); it++) {
      positions[*it] = it;
    }

    auto current_it = working.begin();
    for (std::size_t move = 0; move < 10000000; move++) {
      auto next_val = *current_it;
      std::vector<decltype(working)::value_type> to_remove;
      std::size_t removed_count = 0;
      while ((std::next(current_it) != working.end()) && (removed_count < 3)) {
        to_remove.push_back(*std::next(current_it));
        working.erase(std::next(current_it));
        removed_count++;
      }
      while (removed_count < 3) {
        to_remove.push_back(*working.begin());
        working.erase(working.begin());
        removed_count++;
      }
      do {
        next_val = ((next_val == 1) ? (working.size() + to_remove.size()) : next_val - 1);
      } while (std::find(to_remove.begin(),
                         to_remove.end(),
                         next_val) != to_remove.end());
      auto to_insert = std::next(positions[next_val]);
      while (!to_remove.empty()) {
        to_insert = working.insert(to_insert, *to_remove.begin());
        positions[*to_insert] = to_insert;
        to_insert = std::next(to_insert);
        to_remove.erase(to_remove.begin());
      }
      current_it = (std::next(current_it) == working.end() ? working.begin() : std::next(current_it));
    }
    {
      current_it = std::next(positions[1]);
      result = 1;
      for (int i = 0; i < 2; i++) {
        result *= *current_it;
        current_it = (std::next(current_it) == working.end() ? working.begin() : std::next(current_it));
      }
    }
  }

  std::cout << "order starting after 1: " << result << std::endl;

  return 0;
}
