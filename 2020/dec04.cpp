#include <array>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <string_view>
#include <tuple>
#include <unordered_map>

int main(int, char**)
{
  using namespace std::literals;

  using field = std::tuple<std::string_view, bool, bool(*)(std::string const& str)>;

  static constexpr auto REQUIRED_FIELDS = std::array {
    field{ "byr"sv, true,  [](std::string const& str) { static const std::regex REGEX("(\\d+)"); std::smatch x; if (!std::regex_match(str, x, REGEX)) { return false; } auto y = std::stoi(str); return (y >= 1920) && (y <= 2002); } },
    field{ "iyr"sv, true,  [](std::string const& str) { static const std::regex REGEX("(\\d+)"); std::smatch x; if (!std::regex_match(str, x, REGEX)) { return false; } auto y = std::stoi(str); return (y >= 2010) && (y <= 2020); } },
    field{ "eyr"sv, true,  [](std::string const& str) { static const std::regex REGEX("(\\d+)"); std::smatch x; if (!std::regex_match(str, x, REGEX)) { return false; } auto y = std::stoi(str); return (y >= 2020) && (y <= 2030); } },
    field{ "hgt"sv, true,  [](std::string const& str) { static const std::regex REGEX("(\\d+)((cm)|(in))"); std::smatch x; if (!std::regex_match(str, x, REGEX)) { return false; } auto y = std::stoi(x[1]); return ((x[2] == "in") && (y >= 59) && (y <= 76)) || ((x[2] == "cm") && (y >= 150) && (y <= 193)); } },
    field{ "hcl"sv, true,  [](std::string const& str) { static const std::regex REGEX("#[a-f0-9]{6}"); std::smatch x; return std::regex_match(str, x, REGEX); } },
    field{ "ecl"sv, true,  [](std::string const& str) { static const std::regex REGEX("(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)"); std::smatch x; return std::regex_match(str, x, REGEX); } },
    field{ "pid"sv, true,  [](std::string const& str) { static const std::regex REGEX("[\\d]{9}"); std::smatch x; return std::regex_match(str, x, REGEX); } },
    field{ "cid"sv, false, [](std::string const&) { return true; }},
  };

  static const std::regex key_pair_regex("([^\\s:]+):(\\S+)");

  int result = 0;
  {
    std::ifstream infile;
    std::string line;
    std::unordered_map<std::string, std::string> pairs;
    std::smatch	match_results;
    infile.open("2020/dec04.txt");
    while (infile && std::getline(infile, line)) {
      if (line.empty()) {
        result += (std::all_of(REQUIRED_FIELDS.begin(),
                               REQUIRED_FIELDS.end(),
                               [&pairs](auto const& field) {
                                 auto const& [name, required, validator] = field;
                                 return (!required ||
                                         ((pairs.find(std::string(name)) != pairs.end()) &&
                                          validator(pairs[std::string(name)])));
                               }));
        pairs.clear();
      }
      else {
        while(std::regex_search(line, match_results, key_pair_regex))
        {
          pairs.insert(std::pair{ match_results[1].str(), match_results[2].str() });
          line = match_results.suffix();
        }
      }
    }
  }

  std::cout << "# valid passports: " << result << std::endl;

  return 0;
}
