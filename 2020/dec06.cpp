#include <bitset>
#include <fstream>
#include <iostream>
#include <string>

int main(int, char**)
{
  std::size_t result = 0;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec06.txt");
    std::bitset<26> results;
    bool first = true;
    while (infile && std::getline(infile, line)) {
      if (line.empty()) {
        result += results.count();
        results.reset();
        first = true;
      }
      else {
        decltype(results) local_results;
        for (char answer : line) {
          local_results.set(answer - 'a', true);
        }
        if (first) {
          results = local_results;
          first = false;
        }
        else {
          for (std::size_t i = 0; i < results.size(); i++) {
            results.set(i, results.test(i) && local_results.test(i));
          }
        }
      }
    }
  }

  std::cout << "answer count sum: " << result << std::endl;

  return 0;
}
