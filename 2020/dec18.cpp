#include <cassert>
#include <fstream>
#include <iostream>
#include <list>
#include <optional>
#include <string>

enum {
  PAREN_OPEN,
  PAREN_CLOSE,
  PLUS,
  MINUS,
  MULT,
  NUMBER
};

struct token {
  uint_fast8_t token;
  std::optional<int64_t> number;
};

token evaluate_expression_list(std::list<token>::const_iterator& current,
                               std::list<token>::const_iterator end)
{
  std::list<token> tokens;
  {
    bool exit_now = false;
    for (; !exit_now && (current != end); current = std::next(current))
    {
      switch (current->token) {
        case PAREN_OPEN:
          current = std::next(current);
          tokens.push_back(evaluate_expression_list(current, end));
          break;
        case PAREN_CLOSE:
          exit_now = true;
          break;
        default:
          tokens.push_back(*current);
          break;
      }
    }
  }
  current = std::prev(current);

  using operation = int64_t(*)(int64_t, int64_t);
  for (auto&& [arith_token, op] : { std::tuple { PLUS,  static_cast<operation>([](int64_t a, int64_t b) { return a + b; }) },
                                    std::tuple { MINUS, static_cast<operation>([](int64_t a, int64_t b) { return a - b; }) },
                                    std::tuple { MULT,  static_cast<operation>([](int64_t a, int64_t b) { return a * b; }) }})
  {
    std::list<token> new_tokens;
    token last_num = {};
    for (auto current = tokens.begin(); current != tokens.end(); current = std::next(current))
    {
      if (current->token == arith_token) {
        last_num = token{ NUMBER, op(*last_num.number, *std::next(current)->number) };
        current = std::next(current);
      }
      else if (current->token == NUMBER) {
        last_num = *current;
      }
      else {
        new_tokens.push_back(last_num);
        new_tokens.push_back(*current);
      }
    }
    new_tokens.push_back(last_num);
    std::swap(tokens, new_tokens);
  }
  assert(tokens.size() == 1);
  return *tokens.begin();
}

int64_t calculate_expression(std::string_view sv)
{
  std::list<token> tokens;

  std::optional<int64_t> working_new;
  std::optional<uint_fast8_t> token_type;
  for (auto c : sv)
  {
    switch (c)
    {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9': {
        token_type = NUMBER;
        working_new = (working_new.value_or(0) * 10) + (c - '0');
        break;
      }
      case '-': {
        token_type = MINUS;
        break;
      }
      case '+': {
        token_type = PLUS;
        break;
      }
      case '*': {
        token_type = MULT;
        break;
      }
      default: {
        if (token_type.has_value()) {
          tokens.push_back(token { *token_type, working_new });
          working_new.reset();
        }
        if (c == '(') {
          tokens.push_back(token { PAREN_OPEN, std::nullopt });
        }
        else if (c == ')') {
          tokens.push_back(token { PAREN_CLOSE, std::nullopt });
        }
        token_type.reset();
        break;
      }
    }
  }
  if (token_type.has_value()) {
    tokens.push_back(token { *token_type, working_new });
  }

  auto begin = tokens.begin();
  return *evaluate_expression_list(begin, tokens.end()).number;
}

int main(int, char**)
{
  int64_t result = 0;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec18.txt");
    while (infile && std::getline(infile, line)) {
      result += calculate_expression(line);
    }
  }

  std::cout << "result: " << result << std::endl;

  return 0;
}
