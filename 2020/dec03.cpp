#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <tuple>

int main(int, char**)
{
  using r_d_rp_dp_c = std::tuple<int, int, int, int, int>;

  auto right_down_count = std::array {
    r_d_rp_dp_c { 1, 1, 0, 0, 0},
    r_d_rp_dp_c { 3, 1, 0, 0, 0},
    r_d_rp_dp_c { 5, 1, 0, 0, 0},
    r_d_rp_dp_c { 7, 1, 0, 0, 0},
    r_d_rp_dp_c { 1, 2, 0, 0, 0},
  };

  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec03.txt");
    while (infile && std::getline(infile, line)) {
      for (auto& rdc : right_down_count) {
        auto& [right_inc, down_inc, right_pos, down_pos, count] = rdc;
        if (down_pos == 0) {
          count += (line[right_pos] == '#');
          right_pos = ((right_pos + right_inc) % line.size());
        }
        down_pos  = ((down_pos + 1) % down_inc);
      }
    }
  }

  int64_t result = 1;
  for (auto& rdc : right_down_count) {
    result *= std::get<std::tuple_size<r_d_rp_dp_c>::value - 1>(rdc);
  }

  std::cout << "# trees: " << result << std::endl;

  return 0;
}
