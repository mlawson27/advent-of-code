#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <string>
#include <vector>

int main(int, char**)
{
  // static constexpr std::size_t PREAMBLE_SIZE = 25;
  static constexpr int INVALID_NUM = 20874512;

  std::list<int> nums;

  int result;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec09.txt");

    while (infile) {
      int num;
      infile >> num;

      nums.push_back(num);
    //   if (nums.size() < PREAMBLE_SIZE) {
    //     nums.push_back(num);
    //   }
    //   else {
    //     bool found = false;
    //     for (auto outer_iter = nums.cbegin(); !found && outer_iter != nums.cend(); outer_iter = std::next(outer_iter)) {
    //       for (auto inner_iter = std::next(outer_iter); !found && inner_iter != nums.cend(); inner_iter = std::next(inner_iter)) {
    //         found = (num == (*outer_iter + *inner_iter));
    //       }
    //     }
    //     if (!found) {
    //       result = num;
    //       break;
    //     }
    //     nums.pop_front();
    //     nums.push_back(num);
    //   }
    }
  }

  bool found = false;
  decltype(nums.cbegin()) outer_iter, inner_iter;
  for (outer_iter = nums.cbegin(); !found && outer_iter != nums.cend(); outer_iter = std::next(outer_iter)) {
    int sum = 0;
    for (inner_iter = outer_iter; !found && inner_iter != nums.cend(); inner_iter = std::next(inner_iter)) {
      sum += *inner_iter;
      found = (sum == INVALID_NUM);
    }
  }
  outer_iter = std::prev(outer_iter);

  int min = std::numeric_limits<int>::max(), max = std::numeric_limits<int>::min();
  for (auto iter = outer_iter; iter != inner_iter; iter = std::next(iter)) {
    min = std::min(min, *iter);
    max = std::max(max, *iter);
  }
  result = (min + max);

  std::cout << "first invalid num: " << result << std::endl;
  // std::cout << "num bags needed: " << count_map(DESIRED_CONTAINER, containers) << std::endl;

  return 0;
}
