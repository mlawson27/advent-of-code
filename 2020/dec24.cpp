#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>

using key_t = std::array<int32_t, 2>;

struct key_hash {
  union mapper {
    int32_t vals[std::size(key_t())];
    uint64_t val;
  };

  size_t operator()(key_t const& key) const {
    mapper u;
    std::copy(key.begin(),
              key.end(),
              u.vals);

    return std::hash<decltype(u.val)>{}(u.val);
  }
};

int main(int, char**)
{
  std::unordered_set<key_t, key_hash> black_tile_pos;
  int64_t result = 0;
  {
    std::ifstream infile;
    std::string line;
    infile.open("2020/dec24.txt");
    while (infile && std::getline(infile, line) && !line.empty()) {
      key_t key = { 0, 0 };
      auto& [key_ns_x2, key_ew_x2] = key;
      key_t::value_type ns = 0;
      for (auto c : line) {
        switch (c) {
          case 'n': ns = -2; break;
          case 's': ns =  2; break;
          case 'e': {
            key_ew_x2 += (2 - (ns != 0));
            key_ns_x2 += ns;
            ns = 0;
            break;
          }
          case 'w': {
            key_ew_x2 -= (2 - (ns != 0));
            key_ns_x2 += ns;
            ns = 0;
            break;
          }
          default: break;
        }
      }
      if (black_tile_pos.find(key) == black_tile_pos.end()) {
        black_tile_pos.insert(key);
      }
      else {
        black_tile_pos.erase(key);
      }
    }
  }
  {
    static constexpr auto NEIGHBOR_ADJUSTS = { key_t{  -2, -1 },
                                               key_t{  -2,  1 },
                                               key_t{   2, -1 },
                                               key_t{   2,  1 },
                                               key_t{   0, -2 },
                                               key_t{   0,  2 } };

    decltype(black_tile_pos) working_black_pos;
    for (int i = 0; i < 100; i++) {
      decltype(black_tile_pos) tested_pos;
      for (auto const& tile_pos : black_tile_pos) {
        for (auto&& neighbor_adjusts : NEIGHBOR_ADJUSTS) {
          key_t neighbor_tile_pos = { std::get<0>(tile_pos) + std::get<0>(neighbor_adjusts),
                                      std::get<1>(tile_pos) + std::get<1>(neighbor_adjusts) };
          if (tested_pos.find(neighbor_tile_pos) == tested_pos.end()) {
            auto num_black_neighbors = 0;
            for (auto&& neighbor_neighbor_adjusts : NEIGHBOR_ADJUSTS) {
              key_t neighbor_neighbor_tile_pos = { std::get<0>(neighbor_tile_pos) + std::get<0>(neighbor_neighbor_adjusts),
                                                   std::get<1>(neighbor_tile_pos) + std::get<1>(neighbor_neighbor_adjusts) };
              num_black_neighbors += (black_tile_pos.find(neighbor_neighbor_tile_pos) != black_tile_pos.end());
            }
            if ((num_black_neighbors == 2) ||
                ((black_tile_pos.find(neighbor_tile_pos) != black_tile_pos.end()) &&
                 (num_black_neighbors == 1))) {
              working_black_pos.insert(neighbor_tile_pos);
            }
            tested_pos.insert(neighbor_tile_pos);
          }
        }
      }
      std::swap(working_black_pos, black_tile_pos);
      working_black_pos.clear();
    }
    result = static_cast<decltype(result)>(black_tile_pos.size());
  }

  std::cout << "# black tiles: " << result << std::endl;

  return 0;
}
